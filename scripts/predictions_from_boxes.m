

addpath(genpath('/afs/inf.ed.ac.uk/group/project/mscoco/Tools/coco-master/'));
% Path to whole image predictions
dataFile = '/afs/inf.ed.ac.uk/group/project/mscoco/Predictions/FastRCNNDetections/COCO_train2014_1/coco_caffenet_fast_rcnn_iter_40000/boxes.mat';
saveEmbeddingsDir = '/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/data/predictions'

load(dataFile);

dataSplit = 'val';
dataDir = '/afs/inf.ed.ac.uk/group/project/mscoco';

%% ----------------------------- %%
dataType = strcat(dataSplit, '2014');

annFile=sprintf('%s/Annotations/instances_%s.json',dataDir,dataType);
if(~exist('coco','var')), coco=CocoApi(annFile); end
% get all image ids
imgIds = coco.getImgIds();

%for i=1:size(filenames,1)
%    imageNames{i} = filenames(i,:);
%end

%fileIdx = (1:64) + 100;

%bestScores = zeros(length(fileIdx), 1);
%for idx = 1:length(fileIdx)
%    i = fileIdx(idx);
%    scores = zeros(length(boxes(i,:)),1);
%    for bI = 1:length(boxes(i,:))
%        currBoxes = boxes{i,bI};
%        if ~isempty(currBoxes)
%            scores(bI) = boxes{i,bI}(1,5);
%        end
%    end
    
%    [bestScores(idx), mI] = max(scores);
%end

boxes_t = transpose(boxes);
for i = 1:length(filenames)
    image = filenames(i,:);
    %disp(image);
    
    imgId = sscanf(image, 'train2014/COCO_train2014_%d');
    disp(imgId);
    
    box = boxes_t(i,:)
    disp(box)
    
    %saveFile=sprintf('%s/%s.csv',saveEmbeddingsDir,int2str(imgId));
    %fprintf(1, 'Save to file: %s\n', saveFile)
    %csvwrite(saveFile,probPlatt(i,:))
    
end;
