

addpath(genpath('/afs/inf.ed.ac.uk/group/project/mscoco/Tools/coco-master/'));
% Path to whole image predictions
dataFile = '/afs/inf.ed.ac.uk/group/project/mscoco/Predictions/cocoClassification/cnnClassifierNewLambdaAllF10.mat';
saveEmbeddingsDir = '/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/data/predictions'

load(dataFile)

for i = 1:length(testIms)
    image = testIms(i);
    disp(image{1});
    imgId = sscanf(image{1}, 'val2014/COCO_val2014_%d');
    disp(n);
    
    saveFile=sprintf('%s/%s.csv',saveEmbeddingsDir,int2str(imgId));
    fprintf(1, 'Save to file: %s\n', saveFile)
    csvwrite(saveFile,probPlatt(i,:))
    
end;


