#!/usr/bin/env bash
# Batch generate Triptychs iteratively.

common_command="triptych filter \
--word_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/word_embeddings/glove.840B.300d.txt \
 --image_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/image_embeddings \
--prediction_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions "

# Set number of Triptychs to generate and retry options.
iteration_options=" --number_to_generate 20 --max_iterations 500 --max_time_in_minutes 30 --iterations_without_improvement 5 --improvement_threshold 0.2 "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java
triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/data/David/triptychs/sample_all_features/

# Reellives home.
if [ -z "$REELLIVES_HOME" ]; then
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"
fi

# Set the reelout home if not already set.
if [ -z "$REELOUT_HOME" ]; then
   export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"
fi

# Needed for the generating the joined Triptych jpegs.
if [ -z "$LD_LIBRARY_PATH" ]; then
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib
fi

# Sources with different numbers of sentences.
source_params=(
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_1_has_all_features.xml|s1" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_2_has_all_features.xml|s2" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_3_has_all_features.xml|s3" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_4_has_all_features.xml|s4" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_5_has_all_features.xml|s5" \
)

query_params=(\
"  |all" \
)

# The model threshold parameters.
ilp_params=(
#"0.0|1.0|0.0|0.0|0.0|0.0|0.0" \
#"0.0|0.0|1.0|0.0|0.0|0.0|0.0" \
#"0.0|0.0|0.0|1.0|0.0|0.0|0.0" \
#"0.0|1.0|1.0|0.0|0.0|0.0|0.0" \
#"0.0|1.0|0.0|1.0|0.0|0.0|0.0" \
#"0.0|0.0|1.0|1.0|0.0|0.0|0.0" \
#"0.0|-1.0|1.0|0.0|0.0|0.0|0.0" \
#"0.0|-1.0|0.0|1.0|0.0|0.0|0.0" \
#"0.0|0.0|-1.0|1.0|0.0|0.0|0.0" \
#"0.0|1.0|-1.0|0.0|0.0|0.0|0.0" \
#"0.0|1.0|0.0|-1.0|0.0|0.0|0.0" \
#"0.0|0.0|1.0|-1.0|0.0|0.0|0.0" \
#"0.0|1.0|1.0|1.0|0.0|0.0|0.0" \
#"0.0|1.0|1.0|-1.0|0.0|0.0|0.0" \
#"0.0|1.0|-1.0|1.0|0.0|0.0|0.0" \
#"0.0|-1.0|1.0|1.0|0.0|0.0|0.0" \
#"0.0|1.0|-1.0|-1.0|0.0|0.0|0.0" \
#"0.0|-1.0|1.0|-1.0|0.0|0.0|0.0" \
#"0.0|-1.0|-1.0|1.0|0.0|0.0|0.0" \
#"0.0|0.5|1.0|1.0|0.0|0.0|0.0" \
#"0.0|0.5|1.0|-1.0|0.0|0.0|0.0" \
#"0.0|0.5|-1.0|1.0|0.0|0.0|0.0" \
#"0.0|0.5|1.0|1.0|0.0|0.0|0.0" \
#"0.0|0.5|-1.0|-1.0|0.0|0.0|0.0" \
#"0.0|0.5|1.0|-1.0|0.0|0.0|0.0" \
#"0.0|-0.5|-1.0|1.0|0.0|0.0|0.0" \
#"0.0|1.0|0.5|1.0|0.0|0.0|0.0" \
#"0.0|1.0|0.5|-1.0|0.0|0.0|0.0" \
#"0.0|1.0|0.5|1.0|0.0|0.0|0.0" \
#"0.0|-1.0|0.5|1.0|0.0|0.0|0.0" \
#"0.0|1.0|0.5|-1.0|0.0|0.0|0.0" \
#"0.0|-1.0|0.5|-1.0|0.0|0.0|0.0" \
#"0.0|-1.0|-0.5|1.0|0.0|0.0|0.0" \
#"0.0|1.0|1.0|0.5|0.0|0.0|0.0" \
#"0.0|1.0|1.0|0.5|0.0|0.0|0.0" \
#"0.0|1.0|-1.0|0.5|0.0|0.0|0.0" \
#"0.0|-1.0|1.0|0.5|0.0|0.0|0.0" \
#"0.0|1.0|-1.0|0.5|0.0|0.0|0.0" \
#"0.0|-1.0|1.0|0.5|0.0|0.0|0.0" \
#"0.0|-1.0|-1.0|0.5|0.0|0.0|0.0" \
#"0.0|0.5|0.5|1.0|0.0|0.0|0.0" \
#"0.0|0.5|0.5|-1.0|0.0|0.0|0.0" \
#"0.0|0.5|-0.5|1.0|0.0|0.0|0.0" \
#"0.0|-0.5|0.5|1.0|0.0|0.0|0.0" \
#"0.0|0.5|-0.5|-1.0|0.0|0.0|0.0" \
#"0.0|-0.5|0.5|-1.0|0.0|0.0|0.0" \
#"0.0|-0.5|-0.5|1.0|0.0|0.0|0.0" \
#"0.0|1.0|0.5|0.5|0.0|0.0|0.0" \
#"0.0|1.0|0.5|-0.5|0.0|0.0|0.0" \
#"0.0|1.0|-0.5|0.5|0.0|0.0|0.0" \
#"0.0|-1.0|0.5|0.5|0.0|0.0|0.0" \
#"0.0|1.0|-0.5|-0.5|0.0|0.0|0.0" \
#"0.0|-1.0|0.5|-0.5|0.0|0.0|0.0" \
#"0.0|-1.0|-0.5|0.5|0.0|0.0|0.0" \
#"0.0|0.5|1.0|0.5|0.0|0.0|0.0" \
#"0.0|0.5|1.0|-0.5|0.0|0.0|0.0" \
#"0.0|0.5|-1.0|0.5|0.0|0.0|0.0" \
#"0.0|-0.5|1.0|0.5|0.0|0.0|0.0" \
#"0.0|0.5|-1.0|-0.5|0.0|0.0|0.0" \
#"0.0|-0.5|1.0|-0.5|0.0|0.0|0.0" \
#"0.0|-0.5|-1.0|0.5|0.0|0.0|0.0" \
#"0.0|-1.0|-1.0|-1.0|0.0|0.0|0.0" \
#"0.0|0.0|0.0|-1.0|0.0|0.0|0.0" \
#"0.0|0.0|-1.0|0|0.0|0.0|0.0" \
#"0.0|-1.0|0.0|0.0|0.0|0.0|0.0" \
#"0.0|1.0|-0.25|-0.25|0.0|0.0|0.0" \
#"0.0|-0.25|1.0|-0.25|0.0|0.0|0.0" \
#"0.0|-0.25|-0.25|1.0|0.0|0.0|0.0" \
#"0.0|1.0|-0.35|-0.35|0.0|0.0|0.0" \
#"0.0|-0.35|1.0|-0.35|0.0|0.0|0.0" \
#"0.0|-0.35|-0.35|1.0|0.0|0.0|0.0" \
#"0.0|1.0|-0.25|-0.25|0.9|0.0|0.0" \
#"0.0|-0.25|1.0|-0.25|0.0|0.9|0.0" \
#"0.0|-0.25|-0.25|1.0|0.0|0.0|0.9" \
#"0.0|1.0|-0.35|-0.35|0.9|0.0|0.0" \
#"0.0|-0.35|1.0|-0.35|0.0|0.9|0.0" \
#"0.0|-0.35|-0.35|1.0|0.0|0.0|0.9" \
#"0.0|1.0|-0.25|-0.25|0.8|0.0|0.0" \
#"0.0|-0.25|1.0|-0.25|0.0|0.8|0.0" \
#"0.0|-0.25|-0.25|1.0|0.0|0.0|0.8" \
#"0.0|1.0|-0.35|-0.35|0.8|0.0|0.0" \
#"0.0|-0.35|1.0|-0.35|0.0|0.8|0.0" \
"0.0|1.0|-0.125|-0.125|0.0|0.0|0.0" \
"0.0|-0.125|1.0|-0.125|0.0|0.0|0.0" \
"0.0|-0.125|-0.125|1.0|0.0|0.0|0.0" \
"0.0|1.0|-0.075|-0.075|0.0|0.0|0.0" \
"0.0|-0.075|1.0|-0.075|0.0|0.0|0.0" \
"0.0|-0.075|-0.075|1.0|0.0|0.0|0.0" \
"0.0|-0.25|1.0|1.0|0.0|0.0|0.0" \
"0.0|1.0|-0.25|1.0|0.0|0.0|0.0" \
"0.0|1.0|1.0|0.25|0.0|0.0|0.0" \
"0.0|-0.35|1.0|1.0|0.0|0.0|0.0" \
"0.0|1.0|-0.35|1.0|0.0|0.0|0.0" \
"0.0|1.0|1.0|0.35|0.0|0.0|0.0" \
)

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"

	  for ilp_p in "${ilp_params[@]}"
	  do

		   # Unpack the model parameters.
		   unset ilp_array
	  	   IFS='|' read -r -a ilp_array <<< "$ilp_p"

     destination_filename="new_${source_array[1]}_${query_array[1]}_ws${ilp_array[0]}_wt${ilp_array[1]}_wi${ilp_array[2]}_wp${ilp_array[3]}_mt${ilp_array[4]}_mi${ilp_array[5]}_mp${ilp_array[6]}"
     destination="/afs/inf.ed.ac.uk/group/project/reellives/data/David/triptychs/sample_all_features/${destination_filename}"

		  nice --adjustment=1 \
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} \
		  ${iteration_options} ${query_array[0]} \
		  --weight_sentence ${ilp_array[0]} \
		  --weight_text ${ilp_array[1]} \
		  --weight_image ${ilp_array[2]} \
		  --weight_prediction ${ilp_array[3]} \
		  --max_similarity_text ${ilp_array[4]} \
		  --max_similarity_image ${ilp_array[5]} \
		  --max_similarity_prediction ${ilp_array[6]} \
		--destination ${destination}.html

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
  counter=1 # If there are multiple Triptychs then they should be written to separate jpegs.
  while read ids; do
    echo ${ids}
    cd ${java_examples}
    java -cp 'jar/*' GenerateStimulus ${source_array[0]} ${triptych_export}/${destination_filename}_${counter}.jpg ${ids}
    let "counter+=1"
  done < ${destination}.html.ssv

	  done
  done

done
