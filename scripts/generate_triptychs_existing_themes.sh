#!/usr/bin/env bash
# Batch generate Triptychs iteratively using the existing Reellives system.

common_command="reelout filter "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java
triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/html/images/David

# Reellives home.
#if [ -z "$REELLIVES_HOME" ]; then
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"
#fi

# Set the reelout home if not already set.
if [ -z "$REELOUT_HOME" ]; then
   export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"
fi

# Needed for the generating the joined Triptych jpegs.
#if [ -z "$LD_LIBRARY_PATH" ]; then
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib
#fi

# Sources with different numbers of sentences.
source_params=("/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_1_features.xml|s1" \
#/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_2_features.xml|s2" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_3_features.xml|s3" \
#/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_4_features.xml|s4" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_5_features.xml|s5" \
)

query_params=(\
" --themes 'earrings' |earrings" \
" --themes 'palmtops and pdas' |palmtops_and_pdas" \
" --themes 'consumer electronics' |consumer_electronics" \
" --themes 'photography' |photography" \
" --themes 'mattresses' |mattresses" \
" --themes 'forestry' |forestry" \
" --themes 'gloves' |gloves" \
" --themes 'grocery stores' |grocery_stores" \
" --themes 'lingerie' |lingerie" \
" --themes 'field hockey' |field_hockey" \
" --themes 'dating' |dating" \
" --themes 'walking' |walking" \
" --themes 'playstation' |playstation" \
" --themes 'fly fishing' |fly_fishing" \
" --themes 'wedding dresses' |wedding_dresses" \
" --themes 'soccer' |soccer" \
" --themes 'fire department' |fire_department" \
" --themes 'table tennis and ping-pong' |table_tennis_and_ping-pong" \
" --themes 'canoeing and kayaking' |canoeing_and_kayaking" \
" --themes 'nintendo' |nintendo" \
" --themes 'fishing' |fishing" \
" --themes 'hotels' |hotels" \
" --themes 'gardening' |gardening" \
" --themes 'farms and ranches' |farms_and_ranches" \
" --themes 'camping' |camping" \
" --themes 'candy and sweets' |candy_and_sweets" \
" --themes 'train travel' |train_travel" \
" --themes 'jewelry' |jewelry" \
" --themes 'backpacks' |backpacks" \
" --themes 'healthy eating' |healthy_eating" \
" --themes 'vpn and remote access' |vpn_and_remote_access" \
" --themes 'cricket' |cricket" \
" --themes 'softball' |softball" \
" --themes 'headaches and migraines' |headaches_and_migraines" \
" --themes 'computer peripherals' |computer_peripherals" \
" --themes 'police' |police" \
" --themes 'grains and pasta' |grains_and_pasta" \
" --themes 'surgery' |surgery" \
" --themes 'motorcycles' |motorcycles" \
" --themes 'drums' |drums" \
" --themes 'vegetarian' |vegetarian" \
" --themes 'windows' |windows" \
" --themes 'wagon' |wagon" \
" --themes 'pajamas' |pajamas" \
" --themes 'action figures' |action_figures" \
" --themes 'waterskiing' |waterskiing" \
" --themes 'american cuisine' |american_cuisine" \
" --themes 'education' |education" \
" --themes 'tornado' |tornado" \
" --themes 'c and c++' |c and c++" \
" --themes 'health and fitness' |health_and_fitness" \
" --themes 'orthopedics' |orthopedics" \
" --themes 'political parties' |political_parties" \
" --themes 'operating systems' |operating_systems" \
" --themes 'vegan' |vegan" \
" --themes 'aids and hiv' |aids_and_hiv" \
" --themes 'binoculars' |binoculars" \
" --themes 'sound cards' |sound_cards" \
" --themes 'swimwear' |swimwear" \
" --themes 'printers' |printers" \
" --themes 'country music' |country_music" \
" --themes 'jeep' |jeep" \
" --themes 'therapy' |therapy" \
" --themes 'tourist destinations' |tourist_destinations" \
" --themes 'english as a second language' |english_as_a_second_language" \
" --themes 'chess' |chess" \
" --themes 'nursing' |nursing" \
" --themes 'rugby' |rugby" \
" --themes 'vacation rentals' |vacation_rentals" \
" --themes 'arthritis' |arthritis" \
" --themes 'industrial and product design' |industrial_and_product_design" \
" --themes 'secret service' |secret_service" \
" --themes 'arson' |arson" \
" --themes 'blenders' |blenders" \
" --themes 'alumni and reunions' |alumni_and_reunions" \
" --themes 'minivan' |minivan" \
" --themes 'optics' |optics" \
" --themes 'courts and judiciary' |courts_and_judiciary" \
" --themes 'civil rights' |civil_rights" \
" --themes 'pest control' |pest_control" \
" --themes 'surveillance' |surveillance" \
" --themes 'tech news' |tech_news" \
" --themes 'scion' |scion" \
" --themes 'automotive and vehicles' |automotive_and_vehicles" \
" --themes 'belly dance' |belly_dance" \
" --themes 'sexuality' |sexuality" \
" --themes 'punk' |punk" \
" --themes 'finance' |finance" \
" --themes 'stereo systems and components' |stereo_systems_and_components" \
" --themes 'investing' |investing" \
" --themes 'astrology' |astrology" \
" --themes 'karate' |karate" \
" --themes 'resume writing and advice' |resume_writing_and_advice" \
" --themes 'product reviews' |product_reviews" \
" --themes 'retirement and pension' |retirement_and_pension" \
" --themes 'special education' |special_education" \
" --themes 'business process' |business_process" \
" --themes 'electronic music' |electronic_music" \
" --themes 'incest and abuse support' |incest_and_abuse_support" \
" --themes 'traveling with kids' |traveling_with_kids" \
" --themes 'referendums' |referendums" \
" --themes 'smuggling' |smuggling" \
" --themes 'bollywood' |bollywood" \
" --themes 'volcanic eruptions' |volcanic_eruptions" \
" --themes 'welfare' |welfare" \
" --themes 'waste disposal' |waste_disposal" \
" --themes 'contracts' |contracts" \
" --themes 'pedagogy' |pedagogy" \
" --themes 'graphic design' |graphic_design" \
" --themes 'soundtracks' |soundtracks" \
" --themes 'auctions' |auctions" \
" --themes 'card games' |card_games" \
" --themes 'porsche' |porsche" \
" --themes 'ballroom dance' |ballroom_dance" \
" --themes 'music composition and theory' |music_composition_and_theory" \
" --themes 'united kingdom' |united_kingdom" \
" --themes 'aquaculture' |aquaculture" \
" --themes 'international law' |international_law" \
" --themes 'philosophy' |philosophy" \
" --themes 'blues' |blues" )


for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"


     destination_filename="existing_${source_array[1]}_${query_array[1]}"
     destination="/afs/inf.ed.ac.uk/group/project/reellives/data/David/triptychs/themes/${destination_filename}"

		  # Creates an XML file containing only the filtered subset of RlUnits that can be fed to the generator.
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} ${query_array[0]} \
		--destination ${destination}.xml

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
    cd ${java_examples}
    java -cp 'jar/*' GenerateTriptychs ${destination}.xml ${destination}

	  done

done
