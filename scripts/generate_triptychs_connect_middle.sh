#!/usr/bin/env bash
# Batch generate Triptychs iteratively.

common_command="triptych filter \
--word_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/word_embeddings/glove.840B.300d.txt \
 --image_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/image_embeddings \
--prediction_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions \
--paragraph_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/paragraph_embeddings/paragraphs_s1_300d_dbow_with_words.txt --sentence_use_word false "

# Set number of Triptychs to generate and retry options.
iteration_options=" --number_to_generate 5 --max_iterations 1000 --max_time_in_minutes 30 --iterations_without_improvement 10 --improvement_threshold 0.0 "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java
triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/data/David/triptychs/connections/

# Reellives home.
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"

# Set the reelout home if not already set.
export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"


# Needed for the generating the joined Triptych jpegs.
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib

# Sources with different numbers of sentences.
source_params=(
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_5_features.xml|s5" \
)

# High frequency initial trial run.
query_params=(\
"--1_subject 'airliner' --3_subject  'red,stop' |1" \
"--1_subject  'small,elephant' --3_subject  vegetables |2" \
"--1_subject  'Two,young,children' --3_subject 'orange,motorcycle'  |3" \
"--1_subject  'white,teddy,bear' --3_subject displaying  |4" \
"--1_subject  'plant' --3_subject items  |5" \
"--1_subject  'bat' --3_subject kitchen |6" \
"--1_subject 'street,scene' --3_subject microwave |7" \
"--1_subject 'large,passenger,jet' --3_subject animal  |8" \
"--1_subject 'house' --3_subject 'city,bus' |9 " \
"--1_subject 'Two,dogs' --3_subject 'small,airplane' |10 " \
)

# The model threshold parameters.
ilp_params=(
"0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0" \
#"0.0|1.0|-0.125|-0.125|0.0|0.0|0.0|0.0|0.0" \
#"0.0|-0.125|-0.125|1.0|0.0|0.0|0.0|0.0|0.0" \
#"0.0|1.0|-0.125|-0.125|0.0|0.0|0.0|0.0|0.0" \
#"0.0|-0.125|-0.125|1.0|0.0|0.0|0.0|0.0|0.0" \
)

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"

	  for ilp_p in "${ilp_params[@]}"
	  do

		   # Unpack the model parameters.
		   unset ilp_array
	  	   IFS='|' read -r -a ilp_array <<< "$ilp_p"

      destination_filename="new_${source_array[1]}_${query_array[1]}_ws${ilp_array[0]}_wt${ilp_array[1]}_wi${ilp_array[2]}_wp${ilp_array[3]}_wd${ilp_array[7]}_mt${ilp_array[4]}_mi${ilp_array[5]}_mp${ilp_array[6]}_md${ilp_array[8]}"
      destination="/afs/inf.ed.ac.uk/group/project/reellives/data/David/triptychs/connections/${destination_filename}"

		  nice --adjustment=1 \
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} \
		  ${iteration_options} ${query_array[0]} \
		  --weight_sentence ${ilp_array[0]} \
		  --weight_text ${ilp_array[1]} \
		  --weight_image ${ilp_array[2]} \
		  --weight_prediction ${ilp_array[3]} \
		  --max_similarity_text ${ilp_array[4]} \
		  --max_similarity_image ${ilp_array[5]} \
		  --max_similarity_prediction ${ilp_array[6]} \
      --weight_paragraph ${ilp_array[7]} \
      --max_similarity_paragraph ${ilp_array[8]} \
		  --destination ${destination}.html

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
  counter=1 # If there are multiple Triptychs then they should be written to separate jpegs.
  while read ids; do
    echo ${ids}
    cd ${java_examples}
    java -cp 'jar/*' GenerateStimulus ${source_array[0]} ${triptych_export}/${destination_filename}_${counter}.jpg ${ids}
    let "counter+=1"
  done < ${destination}.html.ssv

	  done
  done

done
