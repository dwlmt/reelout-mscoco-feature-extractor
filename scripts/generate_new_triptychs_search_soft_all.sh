#!/usr/bin/env bash
# Batch generate Triptychs iteratively.

common_command="triptych filter \
--word_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/word_embeddings/glove.840B.300d.txt \
--image_feature_directory '/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg,/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg2' \
--prediction_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions  \
--paragraph_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/paragraph_embeddings/paragraphs_s5_300d_dbow_with_words_1.txt --sentence_use_word false "

# Set number of Triptychs to generate and retry options.
iteration_options=" --number_to_generate 1 --max_iterations 500 --max_time_in_minutes 60 --iterations_without_improvement 10 --improvement_threshold 0.01 "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java


# Reellives home.
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"

# Set the reelout home if not already set.
export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"


# Needed for the generating the joined Triptych jpegs.
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib

# Sources with different numbers of sentences.
source_params=(
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_1_features.xml|s1" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_2_features.xml|s2" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_3_features.xml|s3" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_4_features.xml|s4" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_5_features.xml|s5" \
)

query_params=(\
"--sentence 'standing is in grass'  |H1" \
"--sentence 'white plate topped with sandwich'  |H2" \
"--sentence 'zebras are grazing'  |H3" \
"--sentence 'snow covered hill'  |H4" \
"--sentence 'woman taking picture'  |H5" \
"--sentence 'woman holding tennis racket'  |H6" \
"--sentence 'bird perched on tree branch'  |H7" \
"--sentence 'man hit tennis ball'  |H8" \
"--sentence 'dog laying on bed'  |H9" \
"--sentence 'plate sitting on table'  |H10" \
"--sentence 'man riding skateboard'  |H11" \
"--sentence 'woman holding umbrella'  |H12" \
"--sentence 'playing game of frisbee'  |H13" \
"--sentence 'woman taking swing'  |H14" \
"--sentence 'cat sitting on table'  |H15" \
"--sentence 'person holding plate'  |M1" \
"--sentence 'truck parked on city street'  |M2" \
"--sentence 'man posing for camera'  |M3" \
"--sentence 'person riding brown horse'  |M4" \
"--sentence 'path is in forest'  |M5" \
"--sentence 'jet is parked'  |M6" \
"--sentence 'cat sits on table'  |M7" \
"--sentence 'toilet is with lid'  |M8" \
"--sentence 'white refrigerator freezer sitting inside kitchen'  |M9" \
"--sentence 'woman walking on sidewalk'  |M10" \
"--sentence 'man is with glove'  |M11" \
"--sentence 'umbrellas is in rain'  |M12" \
"--sentence 'man is in red jacket'  |M13" \
"--sentence 'Several boats are docked'  |M14" \
"--sentence 'person riding motorcycle on city street'  |M15" \
"--sentence 'man looking at cell phone'  |L1" \
"--sentence 'table is with electronics'  |L2" \
"--sentence 'couple walking across green field'  |L3" \
"--sentence 'male is in brown shirt'  |L4" \
"--sentence 'boy wearing red shirt'  |L5" \
"--sentence 'cake is with candle'  |L6" \
"--sentence 'pile sitting on counter'  |L7" \
"--sentence 'motorcycle are parked'  |L8" \
"--sentence 'couple standing next to tree'  |L9" \
"--sentence 'giraffe walking through field'  |L10" \
"--sentence 'this is cat laying'  |L11" \
"--sentence 'room filled with types'  |L12" \
"--sentence 'individual is taken'  |L13" \
"--sentence 'kitchen is with window'  |L14" \
"--sentence 'man doing stunt'  |L15" \
"--sentence 'man is taking picture'  |H16" \
"--sentence 'man holding tennis racquet on tennis court'  |H17" \
"--sentence 'Living room filled with furniture'  |H18" \
"--sentence 'pizza sitting on plate'  |H19" \
"--sentence 'dog holding frisbee'  |H20" \
"--sentence 'girl flying kite'  |H21" \
"--sentence 'person is riding horse'  |H22" \
"--sentence 'man swinging baseball bat'  |H23" \
"--sentence 'man flying kite'  |H24" \
"--sentence 'baseball player gets ready'  |H25" \
"--sentence 'group posing for picture'  |H26" \
"--sentence 'man is in kitchen'  |H27" \
"--sentence 'pizza is topped'  |H28" \
"--sentence 'bowl sitting on table'  |H29" \
"--sentence 'giraffe standing next to tree'  |H30" \
"--sentence 'group sitting at table'  |M16" \
"--sentence 'lot is in it'  |M17" \
"--sentence 'street scene is with cars'  |M18" \
"--sentence 'pizza is sitting on table'  |M19" \
"--sentence 'woman has hand'  |M20" \
"--sentence 'clean bathroom is with sink'  |M21" \
"--sentence 'man eating pizza'  |M22" \
"--sentence 'dog holding frisbee'  |M23" \
"--sentence 'man stands on skis'  |M24" \
"--sentence 'person is standing on skateboard'  |M25" \
"--sentence 'pizza topped with cheese'  |M26" \
"--sentence 'man preparing food in kitchen'  |M27" \
"--sentence 'two people sitting at table'  |M28" \
"--sentence 'fire hydrant is located'  |M29" \
"--sentence 'standing is in field flying'  |M30" \
"--sentence 'tall clock tower towering at night'  |L16" \
"--sentence 'man flying on snowboard'  |L17" \
"--sentence 'boy throws baseball'  |L18" \
"--sentence 'holding box of doughnuts'  |L19" \
"--sentence 'dog sitting on ground'  |L20" \
"--sentence 'city buses are parked'  |L21" \
"--sentence 'plate sit on table'  |L22" \
"--sentence 'court is with rackets'  |L23" \
"--sentence 'group sitting at dinner table'  |L24" \
"--sentence 'bus parked in bus stop'  |L25" \
"--sentence 'stove top with tea kettle'  |L26" \
"--sentence 'large brown bear walking through forest'  |L27" \
"--sentence 'man walking next building'  |L28" \
"--sentence 'street sign hanging from side of pole'  |L29" \
"--sentence 'hot dog sits on plate'  |L30" \
"--sentence 'playing game of baseball'  |H31" \
"--sentence 'woman talking on phone'  |H32" \
"--sentence 'kitchen filled with appliances'  |H33" \
"--sentence 'man riding surfboard on wave'  |H34" \
"--sentence 'jetliner sitting on airport tarmac'  |H35" \
"--sentence 'mountain is in background'  |H36" \
"--sentence 'tennis player swinging racket'  |H37" \
"--sentence 'toilet sitting in bathroom'  |H38" \
"--sentence 'bike is parked'  |H39" \
"--sentence 'white toilet sitting next sink'  |H40" \
"--sentence 'bus parked in parking lot'  |M31" \
"--sentence 'kitchen area is with table'  |M32" \
"--sentence 'man are flying kite'  |M33" \
"--sentence 'grazing is in open field'  |M34" \
"--sentence 'court is with racket'  |M35" \
"--sentence 'man carrying bag'  |M36" \
"--sentence 'group are skiing'  |M37" \
"--sentence 'food is ready'  |M38" \
"--sentence 'boats is in water'  |M39" \
"--sentence 'man is in baseball uniform'  |M40" \
"--sentence 'bathroom is with white bath tub'  |L31" \
"--sentence 'man is in yellow'  |L32" \
"--sentence 'cow laying on beach'  |L33" \
"--sentence 'dog covered in ketchup'  |L34" \
"--sentence 'dog is seen'  |L35" \
"--sentence 'dog is on plate'  |L36" \
"--sentence 'computer desk is with two monitors'  |L37" \
"--sentence 'woman is eating piece of pizza'  |L38" \
"--sentence 'field is with rocks'  |L39" \
"--sentence 'young man wearing tie'  |L40" \
)

# The model threshold parameters.
ilp_params=(
#"1.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|SoftSearch" \
#"3.0|0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|SoftSearchImage" \
#"3.0|0.0|1.0|0.0|0.0|0.675|0.0|0.0|0.0|SoftSearchImageCap" \
#"3.0|0.0|1.0|0.0|0.9|0.675|0.0|0.0|0.0|SoftSearchCaps" \
#"3.0|1.0|-0.4|0.0|0.0|0.0|0.0|0.0|0.0|SoftWordCentric" \
#"3.0|-0.80|1.0|0.0|0.0|0.0|0.0|0.0|0.0|SoftImageCentric" \
#3.0|0.0|0.5|0.0|0.0|0.0|0.0|0.0|0.0|SoftSearchImageLow" \
#"3.0|1.0|-0.6|0.0|0.0|0.0|0.0|0.0|0.0|SoftWordCentricHigh" \
#"3.0|1.0|-0.2|0.0|0.0|0.0|0.0|0.0|0.0|SoftWordCentricLow" \
#"3.0|-0.5|0.5|0.0|0.0|0.0|0.0|0.0|0.0|SoftImageCentricHigh" \
#"3.0|-0.25|0.5|0.0|0.0|0.0|0.0|0.0|0.0|SoftImageCentricLow" \

#"3.0|0.0|1.0|0.0|0.0|0.675|0.0|0.0|0.95|SoftSearchCapsPara" \
#"3.0|0.0|-0.4|0.0|0.0|0.0|0.0|1.0|0.0|SoftParaCentric" \
#"3.0|0.0|1.0|0.0|0.0|0.0|0.0|0.0|-0.8|SoftImageCentricPara" \
#"3.0|0.0|-0.6|0.0|0.0|0.0|0.0|0.0|1.0|SoftParaCentricHigh" \
"3.0|0.0|-0.2|0.0|0.0|0.0|0.0|0.0|1.0|SoftParaCentricLow" \
"3.0|0.0|0.5|0.0|0.0|0.0|0.0|0.0|-0.5|SoftImageCentricHighPara" \
"3.0|0.0|0.5|0.0|0.0|0.0|0.0|0.0|-0.25|SoftImageCentricLowPara" \
)

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"

	  for ilp_p in "${ilp_params[@]}"
	  do

      triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/data/David/evaluation_triptychs/search/${source_array[1]}/

		   # Unpack the model parameters.
		   unset ilp_array
	  	   IFS='|' read -r -a ilp_array <<< "$ilp_p"

         destination_filename="${query_array[1]}_search_${ilp_array[9]}"
         destination=${triptych_export}/${destination_filename}

		  nice --adjustment=1 \
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} \
		  ${iteration_options} ${query_array[0]} \
		  --weight_sentence ${ilp_array[0]} \
		  --weight_text ${ilp_array[1]} \
		  --weight_image ${ilp_array[2]} \
		  --weight_prediction ${ilp_array[3]} \
		  --max_similarity_text ${ilp_array[4]} \
		  --max_similarity_image ${ilp_array[5]} \
		  --max_similarity_prediction ${ilp_array[6]} \
      --weight_paragraph ${ilp_array[7]} \
      --max_similarity_paragraph ${ilp_array[8]} \
		  --destination ${destination}.html

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
  counter=1 # If there are multiple Triptychs then they should be written to separate jpegs.
  while read ids; do
    echo ${ids}
    cd ${java_examples}
    java -cp 'jar/*' GenerateStimulus ${source_array[0]} ${triptych_export}/${destination_filename}_${counter}.jpg ${ids}
    let "counter+=1"
  done < ${destination}.html.ssv

	  done
  done

done
