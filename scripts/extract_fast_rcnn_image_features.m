
%% -------- Adapted from Carina Silberers script -------- %%
dataSplit = 'train'; % 'val'

% Path to msCOCO Matlab API
addpath(genpath('/afs/inf.ed.ac.uk/group/project/mscoco/Tools/coco-master/'));
% Path to msCOCO annotation files
dataDir = '/afs/inf.ed.ac.uk/group/project/mscoco';

%% ----------------------------- %%
dataType = strcat(dataSplit, '2014');
featurePath = fullfile('/afs/inf.ed.ac.uk/group/project/mscoco/Features/', dataType);

%% ------ %%
% Embeddings path
saveEmbeddingsDir = '/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/data/image_embeddings'

%% ----------------------------- %%
annFile=sprintf('%s/Annotations/instances_%s.json',dataDir,dataType);
if(~exist('coco','var')), coco=CocoApi(annFile); end
% get all image ids
imgIds = coco.getImgIds();
% some example image ids: 
% imgIds = [89, 288943, 579312, 282473];

for i = 1:length(imgIds)
	%if i>200
	%	break;
	%end
	imgId = imgIds(i);
	featFilename = sprintf('%s/COCO_train2014_%012d.mat', featurePath, imgId);
	if ~exist(featFilename, 'file')
		fprintf(1, '%s\n', featFilename)
		continue
	end
	% load feature vector file
	load(sprintf('%s/COCO_train2014_%012d.mat', featurePath, imgId))
	
	% to inspect annotations provided by msCOCO
	annIds = coco.getAnnIds('imgIds', imgId);		
	anns = coco.loadAnns(annIds);
	anns % all info
	%anns(1).bbox % retrieve box for first object

	% get feature vector for whole image 
	img = coco.loadImgs(imgId);
	row = find(boxes(:,1)==1 & boxes(:,2)==1 & boxes(:,3)>=img.width-1 & boxes(:,4)>=img.height-1);
	if isempty(row)
		fprintf(1, 'No bbox found for %s\n', featFilename);
		%keyboard;
	end
	boxes(row,:)
	featvec = feat(row,:);
    saveFile=sprintf('%s/%s.csv',saveEmbeddingsDir,int2str(imgId));
    fprintf(1, 'Save to file: %s\n', saveFile)
    csvwrite(saveFile,featvec)
end



