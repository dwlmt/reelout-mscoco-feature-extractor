#!/usr/bin/env bash
# Use the randomly generated sample files to

common_command="reelout random "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java
triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/html/images/David

# Reellives home.
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"

# Set the reelout home if not already set.
if [ -z "$REELOUT_HOME" ]; then
   export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"
fi

# Needed for the generating the joined Triptych jpegs.

export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib


# Sources with different numbers of sentences.
source_params=( \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s1" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s2" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s3" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s4" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s5" \
)

query_params=(\
#"1000|1000"
"5000|5000" )

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"

    for i in `seq 1 40`;
       do
         destination_filename="p${i}_OLD_${source_array[1]}_${query_array[1]}"
         destination="/afs/inf.ed.ac.uk/group/project/reellives/data/David/evaluation_triptychs/random_samples/${destination_filename}"

       cd ${java_examples}
       java -cp 'jar/*' GenerateTriptychs ${source_array[0]}random_${source_array[1]}_${query_array[1]}_${i}.xml ${destination}
       done



	  done

done
