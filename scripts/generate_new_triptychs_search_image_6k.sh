#!/usr/bin/env bash
# Batch generate Triptychs iteratively.

common_command="triptych filter \
--word_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/word_embeddings/glove.840B.300d.txt \
 --image_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/image_embeddings \
--prediction_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions \
--paragraph_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/paragraph_embeddings/paragraphs_s1_300d_dbow_with_words.txt --sentence_use_word false "

# Set number of Triptychs to generate and retry options.
iteration_options=" --number_to_generate 1 --max_iterations 500 --max_time_in_minutes 30 --iterations_without_improvement 5 --improvement_threshold 0.0 "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java


# Reellives home.
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"

# Set the reelout home if not already set.
export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"


# Needed for the generating the joined Triptych jpegs.
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib

# Sources with different numbers of sentences.
source_params=(
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_1_has_image_features.xml|s1" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_2_has_all_features.xml|s2" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_3_has_all_features.xml|s3" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_4_has_all_features.xml|s4" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_5_has_image_features.xml|s5" \
)

query_params=(\
"--sentence ' standing   is in   grass ' "\
"--sentence ' white plate   topped with   sandwich ' "\
"--sentence 'zebras are grazing' "\
"--sentence ' snow   covered   hill ' "\
"--sentence ' woman   taking   picture ' "\
"--sentence ' woman   holding   tennis racket ' "\
"--sentence ' bird   perched on   tree branch ' "\
"--sentence ' man   hit   tennis ball ' "\
"--sentence ' dog   laying on   bed ' "\
"--sentence ' plate   sitting on   table ' "\
"--sentence ' man   riding   skateboard ' "\
"--sentence ' woman  holding umbrella' "\
"--sentence ' playing   game of   frisbee ' "\
"--sentence ' woman   taking   swing ' "\
"--sentence ' cat   sitting on   table ' "\
"--sentence ' man   is taking   picture ' "\
"--sentence ' man   holding tennis racquet on   tennis court ' "\
"--sentence 'Living room filled with furniture' "\
"--sentence ' pizza   sitting on   plate ' "\
"--sentence ' dog   holding   frisbee ' "\
"--sentence ' girl   flying   kite ' "\
"--sentence ' person   is riding   horse ' "\
"--sentence ' man   swinging   baseball bat ' "\
"--sentence ' man   flying   kite ' "\
"--sentence ' baseball player   gets   ready ' "\
"--sentence ' group   posing for   picture ' "\
"--sentence ' man   is in   kitchen ' "\
"--sentence ' pizza   is   topped ' "\
"--sentence ' bowl   sitting on   table ' "\
"--sentence 'giraffe standing next to tree' "\
"--sentence ' playing   game of   baseball ' "\
"--sentence ' woman   talking on   phone ' "\
"--sentence ' kitchen   filled with   appliances ' "\
"--sentence ' man   riding surfboard on   wave ' "\
"--sentence ' jetliner   sitting on   airport tarmac ' "\
"--sentence ' mountain   is in   background ' "\
"--sentence ' tennis player   swinging   racket ' "\
"--sentence ' toilet   sitting in   bathroom ' "\
"--sentence ' bike   is   parked ' "\
"--sentence ' white toilet   sitting next   sink' "\
"--subject ' standing ' --relation ' Is, in ' --object ' grass ' "\
"--subject ' white plate ' --relation ' Topped, with ' --object ' sandwich ' "\
"--subject 'zebras' --relation 'are' --object 'grazing' "\
"--subject ' snow ' --relation ' covered ' --object ' hill ' "\
"--subject ' woman ' --relation ' taking ' --object ' picture ' "\
"--subject ' woman ' --relation ' holding ' --object ' Tennis, racket ' "\
"--subject ' bird ' --relation ' Perched,on ' --object ' Tree, branch ' "\
"--subject ' man ' --relation ' hit ' --object ' Tennis, ball ' "\
"--subject ' dog ' --relation ' Laying,on ' --object ' bed ' "\
"--subject ' plate ' --relation ' Sitting,on ' --object ' table ' "\
"--subject ' man ' --relation ' riding ' --object ' skateboard ' "\
"--subject ' woman ' --relation 'holding' --object 'umbrella' "\
"--subject ' playing ' --relation ' Game, of ' --object ' frisbee ' "\
"--subject ' woman ' --relation ' taking ' --object ' swing ' "\
"--subject ' cat ' --relation ' sitting, on ' --object ' table ' "\
"--subject ' man ' --relation ' Is,taking ' --object ' picture ' "\
"--subject ' man ' --relation ' Holding,tennis,racquet,on ' --object ' Tennis,court ' "\
"--subject 'Living room' --relation 'Filled,with' --object 'furniture' "\
"--subject ' pizza ' --relation ' Sitting,on ' --object ' plate ' "\
"--subject ' dog ' --relation ' holding ' --object ' frisbee ' "\
"--subject ' girl ' --relation ' flying ' --object ' kite ' "\
"--subject ' person ' --relation ' Is,riding ' --object ' horse ' "\
"--subject ' man ' --relation ' swinging ' --object ' Baseball,bat ' "\
"--subject ' man ' --relation ' flying ' --object ' kite ' "\
"--subject ' baseball player ' --relation ' gets ' --object ' ready ' "\
"--subject ' group ' --relation ' Posing,for ' --object ' picture ' "\
"--subject ' man ' --relation ' Is,in ' --object ' kitchen ' "\
"--subject ' pizza ' --relation ' is ' --object ' topped ' "\
"--subject ' bowl ' --relation ' Sitting,on ' --object ' table ' "\
"--subject 'giraffe' --relation 'Standing,next,to' --object 'tree' "\
"--subject ' playing ' --relation ' Game,of ' --object ' baseball ' "\
"--subject ' woman ' --relation ' Talking,on ' --object ' phone ' "\
"--subject ' kitchen ' --relation ' Filled,with ' --object ' appliances ' "\
"--subject ' man ' --relation ' Riding,surfboard,on ' --object ' wave ' "\
"--subject ' jetliner ' --relation ' Sitting,on ' --object ' Airport,tarmac ' "\
"--subject ' mountain ' --relation ' Is,in ' --object ' background ' "\
"--subject ' tennis player ' --relation ' swinging ' --object ' racket ' "\
"--subject ' toilet ' --relation ' Sitting,in ' --object ' bathroom ' "\
"--subject ' bike ' --relation ' is ' --object ' parked ' "\
"--subject ' white toilet ' --relation ' Sitting,next ' --object ' sink' "\
)

# The model threshold parameters.
ilp_params=(
"1.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|SearchSame" \
"1.0|-0.25|0.0|0.0|0.0|0.0|0.0|0.0|0.0|SearchNegWord" \
"1.0|0.0|-0.25|0.0|0.0|0.0|0.0|0.0|0.0|SearchNegImage" \
"1.0|-0.125|-0.125|0.0|0.0|0.0|0.0|0.0|0.0|SearchNegWordImage" \
)

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"

    querycounter=1

	  for ilp_p in "${ilp_params[@]}"
	  do

      triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/data/David/evaluation_triptychs/6k_search/${source_array[1]}/

		   # Unpack the model parameters.
		   unset ilp_array
	  	   IFS='|' read -r -a ilp_array <<< "$ilp_p"

         destination_filename="p${querycounter}_theme_${ilp_array[9]}"
         destination=${triptych_export}/${destination_filename}

		  nice --adjustment=1 \
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} \
		  ${iteration_options} ${query_array[0]} \
		  --weight_sentence ${ilp_array[0]} \
		  --weight_text ${ilp_array[1]} \
		  --weight_image ${ilp_array[2]} \
		  --weight_prediction ${ilp_array[3]} \
		  --max_similarity_text ${ilp_array[4]} \
		  --max_similarity_image ${ilp_array[5]} \
		  --max_similarity_prediction ${ilp_array[6]} \
      --weight_paragraph ${ilp_array[7]} \
      --max_similarity_paragraph ${ilp_array[8]} \
		  --destination ${destination}.html

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
  counter=1 # If there are multiple Triptychs then they should be written to separate jpegs.
  while read ids; do
    echo ${ids}
    cd ${java_examples}
    java -cp 'jar/*' GenerateStimulus ${source_array[0]} ${triptych_export}/${destination_filename}_${counter}.jpg ${ids}
    let "counter+=1"
  done < ${destination}.html.ssv

	  done

    let "querycounter+=1"
  done

done
