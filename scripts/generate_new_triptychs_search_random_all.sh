#!/usr/bin/env bash
# Batch generate Triptychs iteratively.

common_command="triptych filter \
--word_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/word_embeddings/glove.840B.300d.txt \
--image_feature_directory '/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg,/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg2' \
--prediction_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions \
--paragraph_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/paragraph_embeddings/paragraphs_s1_300d_dbow_with_words.txt --sentence_use_word false "

# Set number of Triptychs to generate and retry options.
iteration_options=" --number_to_generate 1 --max_iterations 500 --max_time_in_minutes 45 --iterations_without_improvement 5 --improvement_threshold 0.0 "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java


# Reellives home.
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"

# Set the reelout home if not already set.
export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"


# Needed for the generating the joined Triptych jpegs.
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib

# Sources with different numbers of sentences.
source_params=(
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_1_features.xml|s1" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_2_features.xml|s2" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_3_features.xml|s3" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_4_features.xml|s4" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_5_features.xml|s5" \
)

query_params=(\
"--subject 'standing' --relation 'is,in' --object 'grass' --type random --whole_triple true  |H1" \
"--subject 'white,plate' --relation 'topped,with' --object 'sandwich' --type random --whole_triple true  |H2" \
"--subject 'zebras' --relation 'are' --object 'grazing' --type random --whole_triple true  |H3" \
"--subject 'snow' --relation 'covered' --object 'hill' --type random --whole_triple true  |H4" \
"--subject 'woman' --relation 'taking' --object 'picture' --type random --whole_triple true  |H5" \
"--subject 'woman' --relation 'holding' --object 'tennis,racket' --type random --whole_triple true  |H6" \
"--subject 'bird' --relation 'perched,on' --object 'tree,branch' --type random --whole_triple true  |H7" \
"--subject 'man' --relation 'hit' --object 'tennis,ball' --type random --whole_triple true  |H8" \
"--subject 'dog' --relation 'laying,on' --object 'bed' --type random --whole_triple true  |H9" \
"--subject 'plate' --relation 'sitting,on' --object 'table' --type random --whole_triple true  |H10" \
"--subject 'man' --relation 'riding' --object 'skateboard' --type random --whole_triple true  |H11" \
"--subject 'woman' --relation 'holding' --object 'umbrella' --type random --whole_triple true  |H12" \
"--subject 'playing' --relation 'game,of' --object 'frisbee' --type random --whole_triple true  |H13" \
"--subject 'woman' --relation 'taking' --object 'swing' --type random --whole_triple true  |H14" \
"--subject 'cat' --relation 'sitting,on' --object 'table' --type random --whole_triple true  |H15" \
"--subject 'man' --relation 'is,taking' --object 'picture' --type random --whole_triple true  |H16" \
"--subject 'man' --relation 'holding,tennis,racquet,on' --object 'tennis,court' --type random --whole_triple true  |H17" \
"--subject 'Living,room' --relation 'filled,with' --object 'furniture' --type random --whole_triple true  |H18" \
"--subject 'pizza' --relation 'sitting,on' --object 'plate' --type random --whole_triple true  |H19" \
"--subject 'dog' --relation 'holding' --object 'frisbee' --type random --whole_triple true  |H20" \
"--subject 'girl' --relation 'flying' --object 'kite' --type random --whole_triple true  |H21" \
"--subject 'person' --relation 'is,riding' --object 'horse' --type random --whole_triple true  |H22" \
"--subject 'man' --relation 'swinging' --object 'baseball,bat' --type random --whole_triple true  |H23" \
"--subject 'man' --relation 'flying' --object 'kite' --type random --whole_triple true  |H24" \
"--subject 'baseball,player' --relation 'gets' --object 'ready' --type random --whole_triple true  |H25" \
"--subject 'group' --relation 'posing,for' --object 'picture' --type random --whole_triple true  |H26" \
"--subject 'man' --relation 'is,in' --object 'kitchen' --type random --whole_triple true  |H27" \
"--subject 'pizza' --relation 'is' --object 'topped' --type random --whole_triple true  |H28" \
"--subject 'bowl' --relation 'sitting,on' --object 'table' --type random --whole_triple true  |H29" \
"--subject 'giraffe' --relation 'standing,next,to' --object 'tree' --type random --whole_triple true  |H30" \
"--subject 'playing' --relation 'game,of' --object 'baseball' --type random --whole_triple true  |H31" \
"--subject 'woman' --relation 'talking,on' --object 'phone' --type random --whole_triple true  |H32" \
"--subject 'kitchen' --relation 'filled,with' --object 'appliances' --type random --whole_triple true  |H33" \
"--subject 'man' --relation 'riding,surfboard,on' --object 'wave' --type random --whole_triple true  |H34" \
"--subject 'jetliner' --relation 'sitting,on' --object 'airport,tarmac' --type random --whole_triple true  |H35" \
"--subject 'mountain' --relation 'is,in' --object 'background' --type random --whole_triple true  |H36" \
"--subject 'tennis,player' --relation 'swinging' --object 'racket' --type random --whole_triple true  |H37" \
"--subject 'toilet' --relation 'sitting,in' --object 'bathroom' --type random --whole_triple true  |H38" \
"--subject 'bike' --relation 'is' --object 'parked' --type random --whole_triple true  |H39" \
"--subject 'white,toilet' --relation 'sitting,next' --object 'sink' --type random --whole_triple true  |H40" \
"--subject '' --relation '' --object '' --type random --whole_triple true  |" \
"--subject 'person' --relation 'holding' --object 'plate' --type random --whole_triple true  |M1" \
"--subject 'truck' --relation 'parked,on' --object 'city,street' --type random --whole_triple true  |M2" \
"--subject 'man' --relation 'posing,for' --object 'camera' --type random --whole_triple true  |M3" \
"--subject 'person' --relation 'riding' --object 'brown,horse' --type random --whole_triple true  |M4" \
"--subject 'path' --relation 'is,in' --object 'forest' --type random --whole_triple true  |M5" \
"--subject 'jet' --relation 'is' --object 'parked' --type random --whole_triple true  |M6" \
"--subject 'cat' --relation 'sits,on' --object 'table' --type random --whole_triple true  |M7" \
"--subject 'toilet' --relation 'is,with' --object 'lid' --type random --whole_triple true  |M8" \
"--subject 'white,refrigerator,freezer' --relation 'sitting,inside' --object 'kitchen' --type random --whole_triple true  |M9" \
"--subject 'woman' --relation 'walking,on' --object 'sidewalk' --type random --whole_triple true  |M10" \
"--subject 'man' --relation 'is,with' --object 'glove' --type random --whole_triple true  |M11" \
"--subject 'umbrellas' --relation 'is,in' --object 'rain' --type random --whole_triple true  |M12" \
"--subject 'man' --relation 'is,in' --object 'red,jacket' --type random --whole_triple true  |M13" \
"--subject 'Several,boats' --relation 'are' --object 'docked' --type random --whole_triple true  |M14" \
"--subject 'person' --relation 'riding,motorcycle,on' --object 'city,street' --type random --whole_triple true  |M15" \
"--subject 'group' --relation 'sitting,at' --object 'table' --type random --whole_triple true  |M16" \
"--subject 'lot' --relation 'is,in' --object 'it' --type random --whole_triple true  |M17" \
"--subject 'street,scene' --relation 'is,with' --object 'cars' --type random --whole_triple true  |M18" \
"--subject 'pizza' --relation 'is,sitting,on' --object 'table' --type random --whole_triple true  |M19" \
"--subject 'woman' --relation 'has' --object 'hand' --type random --whole_triple true  |M20" \
"--subject 'clean,bathroom' --relation 'is,with' --object 'sink' --type random --whole_triple true  |M21" \
"--subject 'man' --relation 'eating' --object 'pizza' --type random --whole_triple true  |M22" \
"--subject 'dog' --relation 'holding' --object 'frisbee' --type random --whole_triple true  |M23" \
"--subject 'man' --relation 'stands,on' --object 'skis' --type random --whole_triple true  |M24" \
"--subject 'person' --relation 'is,standing,on' --object 'skateboard' --type random --whole_triple true  |M25" \
"--subject 'pizza' --relation 'topped,with' --object 'cheese' --type random --whole_triple true  |M26" \
"--subject 'man' --relation 'preparing,food,in' --object 'kitchen' --type random --whole_triple true  |M27" \
"--subject 'two,people' --relation 'sitting,at' --object 'table' --type random --whole_triple true  |M28" \
"--subject 'fire,hydrant' --relation 'is' --object 'located' --type random --whole_triple true  |M29" \
"--subject 'standing' --relation 'is,in' --object 'field,flying' --type random --whole_triple true  |M30" \
"--subject 'bus' --relation 'parked,in' --object 'parking,lot' --type random --whole_triple true  |M31" \
"--subject 'kitchen,area' --relation 'is,with' --object 'table' --type random --whole_triple true  |M32" \
"--subject 'man' --relation 'are,flying' --object 'kite' --type random --whole_triple true  |M33" \
"--subject 'grazing' --relation 'is,in' --object 'open,field' --type random --whole_triple true  |M34" \
"--subject 'court' --relation 'is,with' --object 'racket' --type random --whole_triple true  |M35" \
"--subject 'man' --relation 'carrying' --object 'bag' --type random --whole_triple true  |M36" \
"--subject 'group' --relation 'are' --object 'skiing' --type random --whole_triple true  |M37" \
"--subject 'food' --relation 'is' --object 'ready' --type random --whole_triple true  |M38" \
"--subject 'boats' --relation 'is,in' --object 'water' --type random --whole_triple true  |M39" \
"--subject 'man' --relation 'is,in' --object 'baseball,uniform' --type random --whole_triple true  |M40" \
"--subject '' --relation '' --object '' --type random --whole_triple true  |" \
"--subject 'man' --relation 'looking,at' --object 'cell,phone' --type random --whole_triple true  |L1" \
"--subject 'table' --relation 'is,with' --object 'electronics' --type random --whole_triple true  |L2" \
"--subject 'couple' --relation 'walking,across' --object 'green,field' --type random --whole_triple true  |L3" \
"--subject 'male' --relation 'is,in' --object 'brown,shirt' --type random --whole_triple true  |L4" \
"--subject 'boy' --relation 'wearing' --object 'red,shirt' --type random --whole_triple true  |L5" \
"--subject 'cake' --relation 'is,with' --object 'candle' --type random --whole_triple true  |L6" \
"--subject 'pile' --relation 'sitting,on' --object 'counter' --type random --whole_triple true  |L7" \
"--subject 'motorcycle' --relation 'are' --object 'parked' --type random --whole_triple true  |L8" \
"--subject 'couple' --relation 'standing,next,to' --object 'tree' --type random --whole_triple true  |L9" \
"--subject 'giraffe' --relation 'walking,through' --object 'field' --type random --whole_triple true  |L10" \
"--subject 'this' --relation 'is' --object 'cat,laying' --type random --whole_triple true  |L11" \
"--subject 'room' --relation 'filled,with' --object 'types' --type random --whole_triple true  |L12" \
"--subject 'individual' --relation 'is' --object 'taken' --type random --whole_triple true  |L13" \
"--subject 'kitchen' --relation 'is,with' --object 'window' --type random --whole_triple true  |L14" \
"--subject 'man' --relation 'doing' --object 'stunt' --type random --whole_triple true  |L15" \
"--subject 'tall,clock,tower' --relation 'towering,at' --object 'night' --type random --whole_triple true  |L16" \
"--subject 'man' --relation 'flying,on' --object 'snowboard' --type random --whole_triple true  |L17" \
"--subject 'boy' --relation 'throws' --object 'baseball' --type random --whole_triple true  |L18" \
"--subject 'holding' --relation 'box,of' --object 'doughnuts' --type random --whole_triple true  |L19" \
"--subject 'dog' --relation 'sitting,on' --object 'ground' --type random --whole_triple true  |L20" \
"--subject 'city,buses' --relation 'are' --object 'parked' --type random --whole_triple true  |L21" \
"--subject 'plate' --relation 'sit,on' --object 'table' --type random --whole_triple true  |L22" \
"--subject 'court' --relation 'is,with' --object 'rackets' --type random --whole_triple true  |L23" \
"--subject 'group' --relation 'sitting,at' --object 'dinner,table' --type random --whole_triple true  |L24" \
"--subject 'bus' --relation 'parked,in' --object 'bus,stop' --type random --whole_triple true  |L25" \
"--subject 'stove' --relation 'top,with' --object 'tea,kettle' --type random --whole_triple true  |L26" \
"--subject 'large,brown,bear' --relation 'walking,through' --object 'forest' --type random --whole_triple true  |L27" \
"--subject 'man' --relation 'walking,next' --object 'building' --type random --whole_triple true  |L28" \
"--subject 'street,sign' --relation 'hanging,from' --object 'side,of,pole' --type random --whole_triple true  |L29" \
"--subject 'hot,dog' --relation 'sits,on' --object 'plate' --type random --whole_triple true  |L30" \
"--subject 'bathroom' --relation 'is,with' --object 'white,bath,tub' --type random --whole_triple true  |L31" \
"--subject 'man' --relation 'is,in' --object 'yellow' --type random --whole_triple true  |L32" \
"--subject 'cow' --relation 'laying,on' --object 'beach' --type random --whole_triple true  |L33" \
"--subject 'dog' --relation 'covered,in' --object 'ketchup' --type random --whole_triple true  |L34" \
"--subject 'dog' --relation 'is' --object 'seen' --type random --whole_triple true  |L35" \
"--subject 'dog' --relation 'is,on' --object 'plate' --type random --whole_triple true  |L36" \
"--subject 'computer,desk' --relation 'is,with' --object 'two,monitors' --type random --whole_triple true  |L37" \
"--subject 'woman' --relation 'is,eating' --object 'piece,of,pizza' --type random --whole_triple true  |L38" \
"--subject 'field' --relation 'is,with' --object 'rocks' --type random --whole_triple true  |L39" \
"--subject 'young,man' --relation 'wearing' --object 'tie' --type random --whole_triple true  |L40" \
)

# The model threshold parameters.
ilp_params=(
"0.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|Random" \
)

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"

	  for ilp_p in "${ilp_params[@]}"
	  do

      triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/data/David/evaluation_triptychs/search/${source_array[1]}/

		   # Unpack the model parameters.
		   unset ilp_array
	  	   IFS='|' read -r -a ilp_array <<< "$ilp_p"

         destination_filename="${query_array[1]}_search_${ilp_array[9]}"
         destination=${triptych_export}/${destination_filename}

         echo ${destination}

		  nice --adjustment=1 \
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} \
		  ${iteration_options} ${query_array[0]} \
		  --weight_sentence ${ilp_array[0]} \
		  --weight_text ${ilp_array[1]} \
		  --weight_image ${ilp_array[2]} \
		  --weight_prediction ${ilp_array[3]} \
		  --max_similarity_text ${ilp_array[4]} \
		  --max_similarity_image ${ilp_array[5]} \
		  --max_similarity_prediction ${ilp_array[6]} \
      --weight_paragraph ${ilp_array[7]} \
      --max_similarity_paragraph ${ilp_array[8]} \
		  --destination ${destination}.html

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
  counter=1 # If there are multiple Triptychs then they should be written to separate jpegs.
  while read ids; do
    echo ${ids}
    cd ${java_examples}
    java -cp 'jar/*' GenerateStimulus ${source_array[0]} ${triptych_export}/${destination_filename}_${counter}.jpg ${ids}
    let "counter+=1"
  done < ${destination}.html.ssv

	  done
  done

done
