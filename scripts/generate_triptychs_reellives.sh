#!/usr/bin/env bash
# Batch generate Triptychs iteratively using the existing Reellives system.

common_command="reelout filter "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java
triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/html/images/David

# Reellives home.
#if [ -z "$REELLIVES_HOME" ]; then
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"
#fi

# Set the reelout home if not already set.
if [ -z "$REELOUT_HOME" ]; then
   export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"
fi

# Needed for the generating the joined Triptych jpegs.
#if [ -z "$LD_LIBRARY_PATH" ]; then
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib
#fi

# Sources with different numbers of sentences.
source_params=("/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_1_features.xml|a1" \
#/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_2_features.xml|a2" \
#/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_3_features.xml|a3" \
#/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_4_features.xml|a4" \
#/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_5_features.xml|a5" \
)

query_params=(\
" --themes 'helicopters' |helicopters" \
" --themes 'surfing' |surfing" \
" --themes 'guitars' |guitars" \
)


for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"


     destination_filename="existing_${source_array[1]}_${query_array[1]}"
     destination="/afs/inf.ed.ac.uk/group/project/reellives/data/David/triptychs/${destination_filename}"

		  # Creates an XML file containing only the filtered subset of RlUnits that can be fed to the generator.
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} ${query_array[0]} \
		--destination ${destination}.xml

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
    cd ${java_examples}
    java -cp 'jar/*' GenerateTriptychs ${destination}.xml ${destination}

	  done

done
