#!/usr/bin/env bash
# Batch generate Triptychs iteratively.

common_command="triptych filter \
--word_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/word_embeddings/glove.840B.300d.txt \
--image_feature_directory '/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg,/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg2' \
--prediction_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions \
--paragraph_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/paragraph_embeddings/paragraphs_s5_300d_dbow_10.txt --sentence_use_word false "

# Set number of Triptychs to generate and retry options.
iteration_options=" --number_to_generate 1 --max_iterations 500 --max_time_in_minutes 45 --iterations_without_improvement 10 --improvement_threshold 0.01 "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java


# Reellives home.
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"

# Set the reelout home if not already set.
export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"


# Needed for the generating the joined Triptych jpegs.
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib

# Sources with different numbers of sentences.
source_params=(
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_1_features.xml|s1" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_2_features.xml|s2" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_3_features.xml|s3" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_4_features.xml|s4" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_5_features.xml|s5" \
)

query_params=(\
"--1_subject 'passangers' --3_subject 'row' |H1" \
"--1_subject 'swimming' --3_subject 'Boats' |H2" \
"--1_subject 'blond' --3_subject 'jeans' |H3" \
"--1_subject 'level' --3_subject 'bears' |H4" \
"--1_subject 'metal' --3_subject 'fries' |H5" \
"--1_subject 'cross' --3_subject 'jockey' |H6" \
"--1_subject 'bag' --3_subject 'pug' |H7" \
"--1_subject 'grizzly' --3_subject 'photos' |H8" \
"--1_subject 'pillows' --3_subject 'Laptop' |H9" \
"--1_subject 'decorated' --3_subject 'skiers' |H10" \
"--1_subject 'fan' --3_subject 'living' |H11" \
"--1_subject 'Clock' --3_subject 'cage' |H12" \
"--1_subject 'Air' --3_subject 'cabinets' |H13" \
"--1_subject 'apartment' --3_subject 'wood' |H14" \
"--1_subject 'motorcycles' --3_subject 'thru' |H15" \
"--1_subject 'Surfer' --3_subject 'dirt' |H16" \
"--1_subject 'purse' --3_subject 'painting' |H17" \
"--1_subject 'cut' --3_subject 'glove' |H18" \
"--1_subject 'persons' --3_subject 'vases' |H19" \
"--1_subject 'ornate' --3_subject 'flag' |H20" \
"--1_subject 'Light' --3_subject 'telephone' |H21" \
"--1_subject 'modern' --3_subject 'Double' |H22" \
"--1_subject 'Girl' --3_subject 'advertisement' |H23" \
"--1_subject 'doll' --3_subject 'bulls' |H24" \
"--1_subject 'Purple' --3_subject 'surfing' |H25" \
"--1_subject 'playing' --3_subject 'meal' |H26" \
"--1_subject 'horse-drawn' --3_subject 'bed' |H27" \
"--1_subject 'wire' --3_subject 'elephant' |H28" \
"--1_subject 'using' --3_subject 'books' |H29" \
"--1_subject 'cooked' --3_subject 'coat' |H30" \
"--1_subject 'Strange' --3_subject 'Someones' |M1" \
"--1_subject 'ribbons' --3_subject 'yaks' |M2" \
"--1_subject 'lamps' --3_subject 'Delta' |M3" \
"--1_subject 'framed' --3_subject 'teacher' |M4" \
"--1_subject 'parents' --3_subject 'pass' |M5" \
"--1_subject 'competition' --3_subject 'wake' |M6" \
"--1_subject 'cowboys' --3_subject 'fellow' |M7" \
"--1_subject 'bins' --3_subject 'towers' |M8" \
"--1_subject 'Banana' --3_subject 'inflated' |M9" \
"--1_subject 'strangely' --3_subject 'heads' |M10" \
"--1_subject 'Vegetable' --3_subject 'terminal' |M11" \
"--1_subject 'fly' --3_subject 'console' |M12" \
"--1_subject 'archway' --3_subject 'string' |M13" \
"--1_subject 'tourists' --3_subject 'curve' |M14" \
"--1_subject 'company' --3_subject 'Pastry' |M15" \
"--1_subject 'GROUP' --3_subject 'circus' |M16" \
"--1_subject 'tuxedo' --3_subject 'filthy' |M17" \
"--1_subject 'Animal' --3_subject 'lighthouse' |M18" \
"--1_subject 'winged' --3_subject 'ten' |M19" \
"--1_subject 'Refrigerator' --3_subject 'limes' |M20" \
"--1_subject 'damaged' --3_subject 'retail' |M21" \
"--1_subject 'Travelers' --3_subject 'medicine' |M22" \
"--1_subject 'vegetation' --3_subject 'nap' |M23" \
"--1_subject 'bundle' --3_subject 'dough' |M24" \
"--1_subject 'aerial' --3_subject 'citizen' |M25" \
"--1_subject 'polo' --3_subject 'stump' |M26" \
"--1_subject 'lined' --3_subject 'bone' |M27" \
"--1_subject 'uncovered' --3_subject 'rounded' |M28" \
"--1_subject 'PLATE' --3_subject 'patrol' |M29" \
"--1_subject 'galley' --3_subject 'businessmen' |M30" \
"--1_subject 'fogy' --3_subject 'Stardust' |L1" \
"--1_subject 'salade' --3_subject 'hedges' |L2" \
"--1_subject 'multi-tiered' --3_subject 'Clocks' |L3" \
"--1_subject 'Nicely' --3_subject 'prarie' |L4" \
"--1_subject 'Limes' --3_subject 'grain' |L5" \
"--1_subject 'Mickey' --3_subject 'heights' |L6" \
"--1_subject 'expo' --3_subject 'assemble' |L7" \
"--1_subject 'wielding' --3_subject 'square-shaped' |L8" \
"--1_subject 'shoeing' --3_subject 'pageant' |L9" \
"--1_subject 'hook' --3_subject 'helping' |L10" \
"--1_subject 'caged' --3_subject 'floppy' |L11" \
"--1_subject 'blouse' --3_subject 'Devil' |L12" \
"--1_subject 'chandler' --3_subject 'bloody' |L13" \
"--1_subject 'Ceramic' --3_subject 'buoy' |L14" \
"--1_subject 'Polson' --3_subject 'Sauce' |L15" \
"--1_subject 'child-sized' --3_subject 'Team' |L16" \
"--1_subject 'Mix' --3_subject 'semi-browned' |L17" \
"--1_subject 'bitty' --3_subject 'boar' |L18" \
"--1_subject 'murdered' --3_subject 'solider' |L19" \
"--1_subject 'dial' --3_subject 'swells' |L20" \
"--1_subject 'Mixed' --3_subject 'campfire' |L21" \
"--1_subject 'sinking' --3_subject 'shopped' |L22" \
"--1_subject 'punnets' --3_subject 'shorn' |L23" \
"--1_subject 'Cups' --3_subject 'examination' |L24" \
"--1_subject 'Omelet' --3_subject 'Formal' |L25" \
"--1_subject 'Class' --3_subject 'choppy' |L26" \
"--1_subject 'Slice' --3_subject 'leashes' |L27" \
"--1_subject 'unloads' --3_subject 'CARRYING' |L28" \
"--1_subject 'Candied' --3_subject 'paramedic' |L29" \
"--1_subject 'Neil' --3_subject 'Addicott' |L30" \
)

# The model threshold parameters.
ilp_params=(
"0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|WordConnection" \
"0.0|0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|ImageConnection" \
"0.0|0.0|0.0|0.0|0.0|0.0|0.0|1.0|0.0|ParaConnection" \
"0.0|0.0|1.0|0.0|0.0|0.0|0.0|1.0|0.0|ParaImageConnection" \
"0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|WordImageConnection" \
)

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"

	  for ilp_p in "${ilp_params[@]}"
	  do

      triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/data/David/evaluation_triptychs/connections/${source_array[1]}/

		   # Unpack the model parameters.
		   unset ilp_array
	  	   IFS='|' read -r -a ilp_array <<< "$ilp_p"

         destination_filename="${query_array[1]}_connections_${ilp_array[9]}"
         destination=${triptych_export}/${destination_filename}

		  nice --adjustment=1 \
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} \
		  ${iteration_options} ${query_array[0]} \
		  --weight_sentence ${ilp_array[0]} \
		  --weight_text ${ilp_array[1]} \
		  --weight_image ${ilp_array[2]} \
		  --weight_prediction ${ilp_array[3]} \
		  --max_similarity_text ${ilp_array[4]} \
		  --max_similarity_image ${ilp_array[5]} \
		  --max_similarity_prediction ${ilp_array[6]} \
      --weight_paragraph ${ilp_array[7]} \
      --max_similarity_paragraph ${ilp_array[8]} \
		  --destination ${destination}.html

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
  counter=1 # If there are multiple Triptychs then they should be written to separate jpegs.
  while read ids; do
    echo ${ids}
    cd ${java_examples}
    java -cp 'jar/*' GenerateStimulus ${source_array[0]} ${triptych_export}/${destination_filename}_${counter}.jpg ${ids}
    let "counter+=1"
  done < ${destination}.html.ssv

	  done
  done

done
