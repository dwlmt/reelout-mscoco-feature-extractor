#!/usr/bin/env bash
# Use the randomly generated sample files to

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

sample_size=5000

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java
triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/data/David/evaluation_triptychs/random_samples/new/${sample_size}

common_command="triptych filter \
--word_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/word_embeddings/glove.840B.300d.txt \
 --image_feature_directory '/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg,/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg2' \
--prediction_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions \
--paragraph_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/paragraph_embeddings/paragraphs_s1_300d_dbow_with_words.txt "

# Set number of Triptychs to generate and retry options.
iteration_options=" --number_to_generate 1 --max_iterations 500 --max_time_in_minutes 45 --iterations_without_improvement 10 --improvement_threshold 0.01 "


# Reellives home.
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"

# Set the reelout home if not already set.
export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"

# Needed for the generating the joined Triptych jpegs.

export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib


# Sources with different numbers of sentences.
source_params=(\
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s5" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s1" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s2" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s3" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/random_samples/|s4" \
)

query_params=(\
" |neutral" \
"--1_sentiments negative --2_sentiments neutral --3_sentiments positive|sentiment"
)

# The model threshold parameters.
ilp_params=(\
#"0.0|0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|Image"
#"0.0|0.0|1.0|0.0|0.0|0.675|0.0|0.0|0.0|ImageCap" \
#"0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|Word"
#"0.0|1.0|0.0|0.0|0.90|0.0|0.0|0.0|0.0|WordCap"
#"0.0|0.0|0.0|0.0|0.0|0.0|0.0|1.0|0.0|Para"
#"0.0|0.0|0.0|0.0|0.0|0.0|0.0|1.0|0.925|ParaCap"
#"0.0|1.0|-0.20|0.0|0.0|0.0|0.0|0.0|0.0|WordCentric" \
#"0.0|-0.60|1.0|0.0|0.0|0.0|0.0|0.0|0.0|ImageCentric" \
"0.0|0.0|-0.12|0.0|0.0|0.0|0.0|1.0|0.0|ParaCentric" \
"0.0|0.0|1.0|0.0|0.0|0.0|0.0|-0.70|0.0|ImageCentricPara" \
)

# Below are those tested but not used for the final experiments.
#"0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|Word" \
#"0.0|1.0|0.0|0.0|0.85|0.0|0.0|0.0|0.0|WordCap" \
#"0.0|1.0|0.0|0.0|0.70|0.0|0.0|0.0|0.0|WordCapLow" \
#"0.0|0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|Image" \
#"0.0|0.0|1.0|0.0|0.0|0.85|0.0|0.0|0.0|ImageCap" \
#"0.0|0.0|1.0|0.0|0.0|0.70|0.0|0.0|0.0|ImageCapLow" \
#"0.0|1.0|-0.70|0.0|0.0|0.0|0.0|0.0|0.0|WordNegImageXHigh" \
#"0.0|1.0|-0.50|0.0|0.0|0.0|0.0|0.0|0.0|WordNegImageVeryHigh" \
#"0.0|1.0|-0.35|0.0|0.0|0.0|0.0|0.0|0.0|WordNegImageHigh" \
#"0.0|1.0|-0.25|0.0|0.0|0.0|0.0|0.0|0.0|WordNegImageMid" \
#"0.0|1.0|-0.20|0.0|0.0|0.0|0.0|0.0|0.0|WordNegImageLowMid" \
#"0.0|1.0|-0.15|0.0|0.0|0.0|0.0|0.0|0.0|WordNegImageLow" \
#"0.0|-1.5|1.0|0.0|0.0|0.0|0.0|0.0|0.0|NegWordImageXXXXHigh" \
#"0.0|-1.1|1.0|0.0|0.0|0.0|0.0|0.0|0.0|NegWordImageXXXHigh" \
#"0.0|-0.90|1.0|0.0|0.0|0.0|0.0|0.0|0.0|NegWordImageXXHigh" \
#"0.0|-0.70|1.0|0.0|0.0|0.0|0.0|0.0|0.0|NegWordImageXHigh" \
#"0.0|-0.50|1.0|0.0|0.0|0.0|0.0|0.0|0.0|NegWordImageVeryHigh" \
#"0.0|-0.35|1.0|0.0|0.0|0.0|0.0|0.0|0.0|NegWordImageHigh" \
#"0.0|-0.25|1.0|0.0|0.0|0.0|0.0|0.0|0.0|NegWordImageMid" \
#"0.0|-0.15|1.0|0.0|0.0|0.0|0.0|0.0|0.0|NegWordImageLow" \
#"0.0|1.0|-0.25|0.0|0.85|0.0|0.0|0.0|0.0|WordCapNegImageMid" \
#"0.0|-0.25|1.0|0.0|0.0|0.65|0.0|0.0|0.0|NegWordImageMidCap" \
#"0.0|0.0|-0.70|0.0|0.0|0.0|0.0|1.0|0.0|ParaNegImageXHigh" \
#"0.0|0.0|-0.50|0.0|0.0|0.0|0.0|1.0|0.0|ParaNegImageVeryHigh" \
#"0.0|0.0|-0.35|0.0|0.0|0.0|0.0|1.0|0.0|ParaNegImageHigh" \
#"0.0|0.0|-0.25|0.0|0.0|0.0|0.0|1.0|0.0|ParaNegImageMid" \
#"0.0|0.0|-0.15|0.0|0.0|0.0|0.0|1.0|0.0|ParaNegImageLow" \
#"0.0|0.0|1.0|0.0|0.0|0.0|0.0|-0.7|0.0|NegParaImageXHigh" \
#"0.0|0.0|1.0|0.0|0.0|0.0|0.0|-0.5|0.0|NegParaImageVeryHigh" \
#"0.0|0.0|1.0|0.0|0.0|0.0|0.0|-0.35|0.0|NegParaImageHigh" \
#"0.0|0.0|1.0|0.0|0.0|0.0|0.0|-0.25|0.0|NegParaImageMid" \
#"0.0|0.0|1.0|0.0|0.0|0.0|0.0|-0.15|0.0|NegParaImageLow" \
#"0.0|0.0|-0.25|0.0|0.85|0.0|0.0|1.0|0.90|ParaCapNegImageMid" \
#"0.0|0.0|1.0|0.0|0.0|0.65|0.0|-0.25|0.0|NegParaImageMidCap" \

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

    for i in `seq 1 40`;
       do

         for query_p in "${query_params[@]}"
       	do
       	  unset query_array
       	  IFS='|' read -r -a query_array <<< "$query_p"

         for ilp_p in "${ilp_params[@]}"
     	   do

     		   # Unpack the model parameters.
     		   unset ilp_array
     	  	   IFS='|' read -r -a ilp_array <<< "$ilp_p"

             destination_filename="p${i}${source_array[1]}_${ilp_array[9]}_${query_array[1]}"
             destination="/afs/inf.ed.ac.uk/group/project/reellives/data/David/evaluation_triptychs/random_samples/new/${sample_size}/${destination_filename}"

             echo ${destination}

             source_file=${source_array[0]}random_${source_array[1]}_${sample_size}_${i}.xml

        		  nice --adjustment=1 \
              $REELOUT_HOME/reelout-mscoco-feature-extractor \
              ${common_command} \
        		   --source ${source_file} \
        		  ${iteration_options} ${query_array[0]} \
        		  --weight_sentence ${ilp_array[0]} \
        		  --weight_text ${ilp_array[1]} \
        		  --weight_image ${ilp_array[2]} \
        		  --weight_prediction ${ilp_array[3]} \
        		  --max_similarity_text ${ilp_array[4]} \
        		  --max_similarity_image ${ilp_array[5]} \
        		  --max_similarity_prediction ${ilp_array[6]} \
              --weight_paragraph ${ilp_array[7]} \
              --max_similarity_paragraph ${ilp_array[8]} \
        		--destination ${destination}.html

          # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
          counter=1 # If there are multiple Triptychs then they should be written to separate jpegs.
          while read ids; do
            echo ${ids}
            cd ${java_examples}
            java -cp 'jar/*' GenerateStimulus ${source_file} ${triptych_export}/${destination_filename}_${counter}.jpg ${ids}
            let "counter+=1"
          done < ${destination}.html.ssv
     done
       done



	  done

done
