#!/usr/bin/env bash
# Batch generate Triptychs iteratively.

common_command="triptych filter \
--word_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/word_embeddings/glove.840B.300d.txt \
--image_feature_directory '/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg,/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg2' \
--prediction_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions \
--paragraph_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/paragraph_embeddings/paragraphs_s1_300d_dbow_with_words.txt --sentence_use_word false "

# Set number of Triptychs to generate and retry options.
iteration_options=" --number_to_generate 1 --max_iterations 500 --max_time_in_minutes 45 --iterations_without_improvement 10 --improvement_threshold 0.01 "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java


# Reellives home.
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"

# Set the reelout home if not already set.
export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"


# Needed for the generating the joined Triptych jpegs.
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib

# Sources with different numbers of sentences.
source_params=(
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_1_features.xml|s1" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_2_features.xml|s2" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_3_features.xml|s3" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_4_features.xml|s4" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/basic_conversion/reelout_5_features.xml|s5" \
)

query_params=(\
"--subject 'standing' --relation 'is,in' --object 'grass' --whole_triple TRUE  |H1" \
"--subject 'white,plate' --relation 'topped,with' --object 'sandwich' --whole_triple TRUE  |H2" \
"--subject 'zebras' --relation 'are' --object 'grazing' --whole_triple TRUE  |H3" \
"--subject 'snow' --relation 'covered' --object 'hill' --whole_triple TRUE  |H4" \
"--subject 'woman' --relation 'taking' --object 'picture' --whole_triple TRUE  |H5" \
"--subject 'woman' --relation 'holding' --object 'tennis,racket' --whole_triple TRUE  |H6" \
"--subject 'bird' --relation 'perched,on' --object 'tree,branch' --whole_triple TRUE  |H7" \
"--subject 'man' --relation 'hit' --object 'tennis,ball' --whole_triple TRUE  |H8" \
"--subject 'dog' --relation 'laying,on' --object 'bed' --whole_triple TRUE  |H9" \
"--subject 'plate' --relation 'sitting,on' --object 'table' --whole_triple TRUE  |H10" \
"--subject 'man' --relation 'riding' --object 'skateboard' --whole_triple TRUE  |H11" \
"--subject 'woman' --relation 'holding' --object 'umbrella' --whole_triple TRUE  |H12" \
"--subject 'playing' --relation 'game,of' --object 'frisbee' --whole_triple TRUE  |H13" \
"--subject 'woman' --relation 'taking' --object 'swing' --whole_triple TRUE  |H14" \
"--subject 'cat' --relation 'sitting,on' --object 'table' --whole_triple TRUE  |H15" \
"--subject 'man' --relation 'is,taking' --object 'picture' --whole_triple TRUE  |H16" \
"--subject 'man' --relation 'holding,tennis,racquet,on' --object 'tennis,court' --whole_triple TRUE  |H17" \
"--subject 'Living,room' --relation 'filled,with' --object 'furniture' --whole_triple TRUE  |H18" \
"--subject 'pizza' --relation 'sitting,on' --object 'plate' --whole_triple TRUE  |H19" \
"--subject 'dog' --relation 'holding' --object 'frisbee' --whole_triple TRUE  |H20" \
"--subject 'girl' --relation 'flying' --object 'kite' --whole_triple TRUE  |H21" \
"--subject 'person' --relation 'is,riding' --object 'horse' --whole_triple TRUE  |H22" \
"--subject 'man' --relation 'swinging' --object 'baseball,bat' --whole_triple TRUE  |H23" \
"--subject 'man' --relation 'flying' --object 'kite' --whole_triple TRUE  |H24" \
"--subject 'baseball,player' --relation 'gets' --object 'ready' --whole_triple TRUE  |H25" \
"--subject 'group' --relation 'posing,for' --object 'picture' --whole_triple TRUE  |H26" \
"--subject 'man' --relation 'is,in' --object 'kitchen' --whole_triple TRUE  |H27" \
"--subject 'pizza' --relation 'is' --object 'topped' --whole_triple TRUE  |H28" \
"--subject 'bowl' --relation 'sitting,on' --object 'table' --whole_triple TRUE  |H29" \
"--subject 'giraffe' --relation 'standing,next,to' --object 'tree' --whole_triple TRUE  |H30" \
"--subject 'playing' --relation 'game,of' --object 'baseball' --whole_triple TRUE  |H31" \
"--subject 'woman' --relation 'talking,on' --object 'phone' --whole_triple TRUE  |H32" \
"--subject 'kitchen' --relation 'filled,with' --object 'appliances' --whole_triple TRUE  |H33" \
"--subject 'man' --relation 'riding,surfboard,on' --object 'wave' --whole_triple TRUE  |H34" \
"--subject 'jetliner' --relation 'sitting,on' --object 'airport,tarmac' --whole_triple TRUE  |H35" \
"--subject 'mountain' --relation 'is,in' --object 'background' --whole_triple TRUE  |H36" \
"--subject 'tennis,player' --relation 'swinging' --object 'racket' --whole_triple TRUE  |H37" \
"--subject 'toilet' --relation 'sitting,in' --object 'bathroom' --whole_triple TRUE  |H38" \
"--subject 'bike' --relation 'is' --object 'parked' --whole_triple TRUE  |H39" \
"--subject 'white,toilet' --relation 'sitting,next' --object 'sink' --whole_triple TRUE  |H40" \
"--subject 'person' --relation 'holding' --object 'plate' --whole_triple TRUE  |M1" \
"--subject 'truck' --relation 'parked,on' --object 'city,street' --whole_triple TRUE  |M2" \
"--subject 'man' --relation 'posing,for' --object 'camera' --whole_triple TRUE  |M3" \
"--subject 'person' --relation 'riding' --object 'brown,horse' --whole_triple TRUE  |M4" \
"--subject 'path' --relation 'is,in' --object 'forest' --whole_triple TRUE  |M5" \
"--subject 'jet' --relation 'is' --object 'parked' --whole_triple TRUE  |M6" \
"--subject 'cat' --relation 'sits,on' --object 'table' --whole_triple TRUE  |M7" \
"--subject 'toilet' --relation 'is,with' --object 'lid' --whole_triple TRUE  |M8" \
"--subject 'white,refrigerator,freezer' --relation 'sitting,inside' --object 'kitchen' --whole_triple TRUE  |M9" \
"--subject 'woman' --relation 'walking,on' --object 'sidewalk' --whole_triple TRUE  |M10" \
"--subject 'man' --relation 'is,with' --object 'glove' --whole_triple TRUE  |M11" \
"--subject 'umbrellas' --relation 'is,in' --object 'rain' --whole_triple TRUE  |M12" \
"--subject 'man' --relation 'is,in' --object 'red,jacket' --whole_triple TRUE  |M13" \
"--subject 'Several,boats' --relation 'are' --object 'docked' --whole_triple TRUE  |M14" \
"--subject 'person' --relation 'riding,motorcycle,on' --object 'city,street' --whole_triple TRUE  |M15" \
"--subject 'group' --relation 'sitting,at' --object 'table' --whole_triple TRUE  |M16" \
"--subject 'lot' --relation 'is,in' --object 'it' --whole_triple TRUE  |M17" \
"--subject 'street,scene' --relation 'is,with' --object 'cars' --whole_triple TRUE  |M18" \
"--subject 'pizza' --relation 'is,sitting,on' --object 'table' --whole_triple TRUE  |M19" \
"--subject 'woman' --relation 'has' --object 'hand' --whole_triple TRUE  |M20" \
"--subject 'clean,bathroom' --relation 'is,with' --object 'sink' --whole_triple TRUE  |M21" \
"--subject 'man' --relation 'eating' --object 'pizza' --whole_triple TRUE  |M22" \
"--subject 'dog' --relation 'holding' --object 'frisbee' --whole_triple TRUE  |M23" \
"--subject 'man' --relation 'stands,on' --object 'skis' --whole_triple TRUE  |M24" \
"--subject 'person' --relation 'is,standing,on' --object 'skateboard' --whole_triple TRUE  |M25" \
"--subject 'pizza' --relation 'topped,with' --object 'cheese' --whole_triple TRUE  |M26" \
"--subject 'man' --relation 'preparing,food,in' --object 'kitchen' --whole_triple TRUE  |M27" \
"--subject 'two,people' --relation 'sitting,at' --object 'table' --whole_triple TRUE  |M28" \
"--subject 'fire,hydrant' --relation 'is' --object 'located' --whole_triple TRUE  |M29" \
"--subject 'standing' --relation 'is,in' --object 'field,flying' --whole_triple TRUE  |M30" \
"--subject 'bus' --relation 'parked,in' --object 'parking,lot' --whole_triple TRUE  |M31" \
"--subject 'kitchen,area' --relation 'is,with' --object 'table' --whole_triple TRUE  |M32" \
"--subject 'man' --relation 'are,flying' --object 'kite' --whole_triple TRUE  |M33" \
"--subject 'grazing' --relation 'is,in' --object 'open,field' --whole_triple TRUE  |M34" \
"--subject 'court' --relation 'is,with' --object 'racket' --whole_triple TRUE  |M35" \
"--subject 'man' --relation 'carrying' --object 'bag' --whole_triple TRUE  |M36" \
"--subject 'group' --relation 'are' --object 'skiing' --whole_triple TRUE  |M37" \
"--subject 'food' --relation 'is' --object 'ready' --whole_triple TRUE  |M38" \
"--subject 'boats' --relation 'is,in' --object 'water' --whole_triple TRUE  |M39" \
"--subject 'man' --relation 'is,in' --object 'baseball,uniform' --whole_triple TRUE  |M40" \
"--subject 'man' --relation 'looking,at' --object 'cell,phone' --whole_triple TRUE  |L1" \
"--subject 'table' --relation 'is,with' --object 'electronics' --whole_triple TRUE  |L2" \
"--subject 'couple' --relation 'walking,across' --object 'green,field' --whole_triple TRUE  |L3" \
"--subject 'male' --relation 'is,in' --object 'brown,shirt' --whole_triple TRUE  |L4" \
"--subject 'boy' --relation 'wearing' --object 'red,shirt' --whole_triple TRUE  |L5" \
"--subject 'cake' --relation 'is,with' --object 'candle' --whole_triple TRUE  |L6" \
"--subject 'pile' --relation 'sitting,on' --object 'counter' --whole_triple TRUE  |L7" \
"--subject 'motorcycle' --relation 'are' --object 'parked' --whole_triple TRUE  |L8" \
"--subject 'couple' --relation 'standing,next,to' --object 'tree' --whole_triple TRUE  |L9" \
"--subject 'giraffe' --relation 'walking,through' --object 'field' --whole_triple TRUE  |L10" \
"--subject 'this' --relation 'is' --object 'cat,laying' --whole_triple TRUE  |L11" \
"--subject 'room' --relation 'filled,with' --object 'types' --whole_triple TRUE  |L12" \
"--subject 'individual' --relation 'is' --object 'taken' --whole_triple TRUE  |L13" \
"--subject 'kitchen' --relation 'is,with' --object 'window' --whole_triple TRUE  |L14" \
"--subject 'man' --relation 'doing' --object 'stunt' --whole_triple TRUE  |L15" \
"--subject 'tall,clock,tower' --relation 'towering,at' --object 'night' --whole_triple TRUE  |L16" \
"--subject 'man' --relation 'flying,on' --object 'snowboard' --whole_triple TRUE  |L17" \
"--subject 'boy' --relation 'throws' --object 'baseball' --whole_triple TRUE  |L18" \
"--subject 'holding' --relation 'box,of' --object 'doughnuts' --whole_triple TRUE  |L19" \
"--subject 'dog' --relation 'sitting,on' --object 'ground' --whole_triple TRUE  |L20" \
"--subject 'city,buses' --relation 'are' --object 'parked' --whole_triple TRUE  |L21" \
"--subject 'plate' --relation 'sit,on' --object 'table' --whole_triple TRUE  |L22" \
"--subject 'court' --relation 'is,with' --object 'rackets' --whole_triple TRUE  |L23" \
"--subject 'group' --relation 'sitting,at' --object 'dinner,table' --whole_triple TRUE  |L24" \
"--subject 'bus' --relation 'parked,in' --object 'bus,stop' --whole_triple TRUE  |L25" \
"--subject 'stove' --relation 'top,with' --object 'tea,kettle' --whole_triple TRUE  |L26" \
"--subject 'large,brown,bear' --relation 'walking,through' --object 'forest' --whole_triple TRUE  |L27" \
"--subject 'man' --relation 'walking,next' --object 'building' --whole_triple TRUE  |L28" \
"--subject 'street,sign' --relation 'hanging,from' --object 'side,of,pole' --whole_triple TRUE  |L29" \
"--subject 'hot,dog' --relation 'sits,on' --object 'plate' --whole_triple TRUE  |L30" \
"--subject 'bathroom' --relation 'is,with' --object 'white,bath,tub' --whole_triple TRUE  |L31" \
"--subject 'man' --relation 'is,in' --object 'yellow' --whole_triple TRUE  |L32" \
"--subject 'cow' --relation 'laying,on' --object 'beach' --whole_triple TRUE  |L33" \
"--subject 'dog' --relation 'covered,in' --object 'ketchup' --whole_triple TRUE  |L34" \
"--subject 'dog' --relation 'is' --object 'seen' --whole_triple TRUE  |L35" \
"--subject 'dog' --relation 'is,on' --object 'plate' --whole_triple TRUE  |L36" \
"--subject 'computer,desk' --relation 'is,with' --object 'two,monitors' --whole_triple TRUE  |L37" \
"--subject 'woman' --relation 'is,eating' --object 'piece,of,pizza' --whole_triple TRUE  |L38" \
"--subject 'field' --relation 'is,with' --object 'rocks' --whole_triple TRUE  |L39" \
"--subject 'young,man' --relation 'wearing' --object 'tie' --whole_triple TRUE  |L40" \
)

# The model threshold parameters.
ilp_params=(
"0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|HardWordCentric" \
"0.0|0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|HardImage" \
"0.0|0.0|1.0|0.0|0.0|0.675|0.0|0.0|0.0|HardImageCap" \
#"0.0|1.0|-0.20|0.0|0.0|0.0|0.0|0.0|0.0|HardWordCentric" \
#"0.0|-0.6|1.0|0.0|0.0|0.0|0.0|0.0|0.0|HardImageCentric" \
#"0.0|0.0|-0.20|0.0|0.0|0.0|0.0|1.0|0.0|HardParaCentric" \
#"0.0|-0.5|1.0|0.0|0.0|0.0|0.0|0.0|0.0|HardImageCentricPara" \
)

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"

	  for ilp_p in "${ilp_params[@]}"
	  do

      triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/data/David/evaluation_triptychs/search/${source_array[1]}/

		   # Unpack the model parameters.
		   unset ilp_array
	  	   IFS='|' read -r -a ilp_array <<< "$ilp_p"

         destination_filename="${query_array[1]}_search_${ilp_array[9]}"
         destination=${triptych_export}/${destination_filename}

		  nice --adjustment=1 \
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} \
		  ${iteration_options} ${query_array[0]} \
		  --weight_sentence ${ilp_array[0]} \
		  --weight_text ${ilp_array[1]} \
		  --weight_image ${ilp_array[2]} \
		  --weight_prediction ${ilp_array[3]} \
		  --max_similarity_text ${ilp_array[4]} \
		  --max_similarity_image ${ilp_array[5]} \
		  --max_similarity_prediction ${ilp_array[6]} \
      --weight_paragraph ${ilp_array[7]} \
      --max_similarity_paragraph ${ilp_array[8]} \
		  --destination ${destination}.html

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
  counter=1 # If there are multiple Triptychs then they should be written to separate jpegs.
  while read ids; do
    echo ${ids}
    cd ${java_examples}
    java -cp 'jar/*' GenerateStimulus ${source_array[0]} ${triptych_export}/${destination_filename}_${counter}.jpg ${ids}
    let "counter+=1"
  done < ${destination}.html.ssv

	  done
  done

done
