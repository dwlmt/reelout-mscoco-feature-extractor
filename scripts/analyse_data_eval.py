#by subjects data grid

import glob, os, sys

lines = open('eval/randomisation.dat').readlines()

cat = []

for l in lines:
    c = l.split('_')[2][:-4]
    cat.append(c)


dirs = glob.glob('eval/*[0-9]')

dirs.sort()

genstats = {}
genn = {}

ques = ['top', 'ord','sto','eval','act']

tcatstr = ['imagecap', 'imagecentric', 'image', 'random', 'sentiment', 'wordcentric', 'wordsentiment']

print 'subjid gender age',
for q in ques:
    for s in tcatstr:
        print s + '_' + q,
print

for d in dirs:
    subj = d.split('/')[1][:3]
    #subj data
    lines = open(d + '/00_01.res').readlines()
    age = lines[0].split('=')[1].strip()
    if age[:3] == 'und':
        age = 'under20'
    gender = lines[1].split('=')[1].strip()
    stats = {}
    n = {}
    for i in range(1,70):
        filename = d + '/' + ('00' + str(i))[-2:] + '_01.res'
        if os.path.isfile(filename):
            lines = open(filename).readlines()
            tcat = cat[i - 1]
            #if tcat[:1] == 'o' or tcat[:1] == 'b':
                #print tcat
                #tcat = tcat[:-2]
            #    tcat = 'hum'
            topic = int(lines[0].split('=')[1].strip())
            order = int(lines[1].split('=')[1].strip())
            story = int(lines[2].split('=')[1].strip())
            eval = int(lines[3].split('=')[1].strip())
            act = int(lines[4].split('=')[1].strip())

            if not stats.has_key(tcat):
                stats[tcat] = [0.0, 0.0, 0.0, 0.0, 0.0]
                n[tcat] = 0.0
            if not genstats.has_key(tcat):
                genstats[tcat] = [0.0, 0.0, 0.0, 0.0, 0.0]
                genn[tcat] = 0.0
            stats[tcat][0] += topic
            stats[tcat][1] += order
            stats[tcat][2] += story
            stats[tcat][3] += eval
            stats[tcat][4] += act
            genstats[tcat][0] += topic
            genstats[tcat][1] += order
            genstats[tcat][2] += story
            genstats[tcat][3] += eval
            genstats[tcat][4] += act
            n[tcat] += 1
            genn[tcat] += 1

            #print stats
    keys = stats.keys()
    keys.sort()
    if len(keys) < len(tcatstr):
        #print "Not all values are present, skipping"
        continue
    #print stats
    print subj, gender, age,
    for j in range(5):
        for k in keys:
            print stats[k][j]/n[k],
    print
#print keys
keys = genstats.keys()
keys.sort()

#for k in keys:
#    for j in range(5):
#        print "%.2f" % (genstats[k][j]/genn[k]),
#print
