#!/usr/bin/env bash
# Batch generate Triptychs iteratively.

common_command="triptych filter \
--word_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/word_embeddings/glove.840B.300d.txt \
 --image_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/image_embeddings \
--prediction_feature_directory /afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions \
--paragraph_embeddings_file /afs/inf.ed.ac.uk/group/project/reellives/data/David/paragraph_embeddings/paragraphs_s1_300d_dbow_with_words.txt --sentence_use_word false "

# Set number of Triptychs to generate and retry options.
iteration_options=" --number_to_generate 1 --max_iterations 500 --max_time_in_minutes 30 --iterations_without_improvement 5 --improvement_threshold 0.0 "

# Set Java options to point at the GLPK library.
export JAVA_OPTS="-Djava.library.path=/afs/inf.ed.ac.uk/user/s15/s1569885/LIBGLPK/lib/jni/"

java_examples=/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build/examples/java


# Reellives home.
export REELLIVES_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reellivessystem/trunk/reelout/build"

# Set the reelout home if not already set.
export REELOUT_HOME="/afs/inf.ed.ac.uk/user/s15/s1569885/project/reelout-mscoco-feature-extractor/build/install/reelout-mscoco-feature-extractor/bin"


# Needed for the generating the joined Triptych jpegs.
export LD_LIBRARY_PATH=${REELLIVES_HOME}/src/base:${REELLIVES_HOME}/src/javalib:${REELLIVES_HOME}/src/pythonlib:${REELLIVES_HOME}/tools/lib

# Sources with different numbers of sentences.
source_params=(
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_1_has_image_features.xml|s1" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_2_has_all_features.xml|s2" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_3_has_all_features.xml|s3" \
#"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_4_has_all_features.xml|s4" \
"/afs/inf.ed.ac.uk/group/project/reellives/data/David/mscoco/reelout_5_has_image_features.xml|s5" \
)

query_params=(\
"--1_subject 'sailboat' --3_subject 'riding'"\
"--1_subject 'building' --3_subject 'street,signs'"\
"--1_subject 'books' --3_subject 'laptops'"\
"--1_subject 'fire,hydrant' --3_subject 'giraffe,standing'"\
"--1_subject 'individual' --3_subject 'hotdog'"\
"--1_subject 'White' --3_subject 'couch'"\
"--1_subject 'tennis,player' --3_subject 'large,crowd'"\
"--1_subject 'cheese,pizza' --3_subject 'large,bear'"\
"--1_subject 'Two,men' --3_subject 'Two,giraffe'"\
"--1_subject 'PERSON' --3_subject 'red'"\
"--1_subject 'plant' --3_subject 'pretty,young,lady'"\
"--1_subject 'pigeon' --3_subject 'passenger,train'"\
"--1_subject 'blue,motorcycle' --3_subject 'Asian,man'"\
"--1_subject 'ball' --3_subject 'Two,birds'"\
"--1_subject 'person' --3_subject 'tray'"\
"--1_subject 'Spectators' --3_subject 'large,bus'"\
"--1_subject 'two,cows' --3_subject 'Two,cows'"\
"--1_subject 'white,bowl' --3_subject 'Someone'"\
"--1_subject 'seagull' --3_subject 'sailboat'"\
"--1_subject 'large,building' --3_subject 'cakes'"\
"--1_subject 'remote,control' --3_subject 'black,cat'"\
"--1_subject 'Two,birds' --3_subject 'bird'"\
"--1_subject 'house' --3_subject 'Two,buses'"\
"--1_subject 'freight,train' --3_subject 'luggage'"\
"--1_subject 'red,fire,hydrant' --3_subject 'red,car'"\
"--1_subject 'cook' --3_subject 'Surfer'"\
"--1_subject 'cattle' --3_subject 'making'"\
"--1_subject 'slope' --3_subject 'Two,trains'"\
"--1_subject 'young,men' --3_subject 'displaying'"\
"--1_subject 'MAN' --3_subject 'large,airplane'"\
"--1_subject 'red,train' --3_subject 'driver'"\
"--1_subject 'skier' --3_subject 'motorcycles'"\
"--1_subject 'zebra,standing' --3_subject 'blue,train'"\
"--1_subject 'marina' --3_subject 'Black,photograph'"\
"--1_subject 'elderly,man' --3_subject 'grassy,field'"\
"--1_subject 'church' --3_subject 'colorful,bird'"\
"--1_subject 'white,plate' --3_subject 'Two,pizzas'"\
"--1_subject 'closeup' --3_subject 'rider'"\
"--1_subject 'Two,cows' --3_subject 'bicyclist'"\
"--1_subject 'Several,children' --3_subject 'blanket'"\
)

# The model threshold parameters.
ilp_params=(
"0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|0.0|Text" \
"0.0|0.0|1.0|0.0|0.0|0.0|0.0|0.0|0.0|Image" \
"0.0|1.0|-0.25|0.0|0.0|0.0|0.0|0.0|0.0|TextNegImage" \
"0.0|-0.25|1.0|0.0|0.0|0.0|0.0|0.0|0.0|ImageNegText" \
)

for source_p in "${source_params[@]}"
do
  unset source_array
  IFS='|' read -r -a source_array <<< "$source_p"

  querycounter=1

	for query_p in "${query_params[@]}"
	do
	  unset query_array
	  IFS='|' read -r -a query_array <<< "$query_p"

	  for ilp_p in "${ilp_params[@]}"
	  do

      triptych_export=/afs/inf.ed.ac.uk/group/project/reellives/data/David/evaluation_triptychs/6k_connections/${source_array[1]}/

		   # Unpack the model parameters.
		   unset ilp_array
	  	   IFS='|' read -r -a ilp_array <<< "$ilp_p"

         destination_filename="p${querycounter}_theme_${ilp_array[9]}"
         destination=${triptych_export}/${destination_filename}

		  nice --adjustment=1 \
      $REELOUT_HOME/reelout-mscoco-feature-extractor ${common_command} \
		   --source ${source_array[0]} \
		  ${iteration_options} ${query_array[0]} \
		  --weight_sentence ${ilp_array[0]} \
		  --weight_text ${ilp_array[1]} \
		  --weight_image ${ilp_array[2]} \
		  --weight_prediction ${ilp_array[3]} \
		  --max_similarity_text ${ilp_array[4]} \
		  --max_similarity_image ${ilp_array[5]} \
		  --max_similarity_prediction ${ilp_array[6]} \
      --weight_paragraph ${ilp_array[7]} \
      --max_similarity_paragraph ${ilp_array[8]} \
		  --destination ${destination}.html

  # Read line by line the Triptychs from the ids file and generate the Triptych jpegs
  counter=1 # If there are multiple Triptychs then they should be written to separate jpegs.
  while read ids; do
    echo ${ids}
    cd ${java_examples}
    java -cp 'jar/*' GenerateStimulus ${source_array[0]} ${triptych_export}/${destination_filename}_${counter}.jpg ${ids}
    let "counter+=1"
  done < ${destination}.html.ssv

	  done

    let "querycounter+=1"
  done

done
