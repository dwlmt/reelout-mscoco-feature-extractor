%load('/afs/inf.ed.ac.uk/user/s15/s1569885/coco/vgg_feats.mat');
%feats_t = transpose(feats);

saveEmbeddingsDir = '/afs/inf.ed.ac.uk/group/project/reellives/data/David/vgg2'

text = importdata('/afs/inf.ed.ac.uk/user/s15/s1569885/coco/cocoids.txt',',')

for i = 64435:length(feats_t)
    
    try
        feature = feats_t(i,:);
    
        saveFile=sprintf('%s/%s.csv',saveEmbeddingsDir,int2str(text(i)));
        fprintf(1, 'Save to file: %s\n', saveFile)
        csvwrite(saveFile,feature)
    catch ME
        
    end
end; 
    