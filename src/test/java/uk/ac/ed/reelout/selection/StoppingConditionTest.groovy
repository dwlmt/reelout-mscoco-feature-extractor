package uk.ac.ed.reelout.selection

import spock.lang.Specification

/**
 * Tests the retry functionality
 */
class StoppingConditionTest extends Specification {

    def "Specific numbers of iterations are specified"() {
        when: "Iterations are specified"
        def retry = new StoppingCondition(5, 3, 5, 0.0)
        then: "Retry lasts the correct number of times."
        assert retry.shouldContinue();
        assert retry.shouldContinue();
        assert retry.shouldContinue();
        assert !retry.shouldContinue();
    }

    def "Improvement stops"() {
        when: "There is improvement"
        def retry = new StoppingCondition(3, 100, 100, 0.2)
        retry.score(2.0);
        retry.score(4.0);
        retry.score(6.0);
        then: "Retries continue"
        assert retry.shouldContinue();
        assert retry.shouldContinue();

        when: "Improvement stop and starts"
        retry.score(5.0);
        retry.score(5.0);
        retry.score(7.0);

        then: "Retries continue"
        assert retry.shouldContinue();

        when: "Improvement stops"
        retry.score(5.0);
        retry.score(5.1);
        retry.score(5.1);

        then: "Retries stop"
        assert !retry.shouldContinue();
    }

    def "Timeout expires"() {
        when: "0 minutes "
        def retry = new StoppingCondition(3, 100, 0, 0.0)
        then: "Time is up"
        sleep(1000)
        assert !retry.shouldContinue();

    }

}