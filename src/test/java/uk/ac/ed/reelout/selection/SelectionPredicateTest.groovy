package uk.ac.ed.reelout.selection

import spock.lang.Specification
import uk.ac.ed.reelout.domain.*

import java.time.LocalDateTime
import java.util.stream.Collectors

/**
 * Tests the predicates used for filtering RLUnits.
 */
class SelectionPredicateTest extends Specification {


    def "Id Predicate Filter"() {
        when: "A list of RlUnits"
        List<RlUnit> units = new ArrayList<RlUnit>();
        units.add(new RlUnit("1"))
        units.add(new RlUnit("2"))
        units.add(new RlUnit("3"))

        def filtered = units.stream().filter(SelectionPredicates.id("2")).collect(Collectors.toList());

        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("2"));
    }

    def "Message Text Predicate Filter"() {
        when: "A list of RlUnits"
        List<RlUnit> units = new ArrayList<RlUnit>();

        def one = new RlUnit("1")
        def two = new RlUnit("2")
        def three = new RlUnit("3");

        one.setMessageText("cat cat cat")
        two.setMessageText("Dog")
        three.setMessageText("fish fish DOG  ")

        units.add(one)
        units.add(two)
        units.add(three)

        def filtered = units.stream().filter(SelectionPredicates.text("dog")).collect(Collectors.toList());

        then: "Only those matching the predicate are returned."
        assert filtered.size() == 2
        filtered.contains(new RlUnit("2"));
        filtered.contains(new RlUnit("3"));
    }

    def "Sentiment Predicate Filter"() {
        when: "A list of RlUnits"
        List<RlUnit> units = new ArrayList<RlUnit>();

        def one = new RlUnit("1")
        def two = new RlUnit("2")
        def three = new RlUnit("3");

        one.setSentiment(new SentimentHolder(Sentiment.negative))
        two.setSentiment(new SentimentHolder(Sentiment.positive))
        // three has no sentiment. Should be excluded.

        units.add(one)
        units.add(two)
        units.add(three)

        def filtered = units.stream().filter(SelectionPredicates.sentiment(Sentiment.positive)).collect(Collectors.toList());

        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("2"));
    }

    def "Theme Predicate Filter"() {
        when: "A list of RlUnits"
        List<RlUnit> units = new ArrayList<RlUnit>();

        def one = new RlUnit("1")
        def two = new RlUnit("2")
        def three = new RlUnit("3");

        def t1 = [new Theme(0.3, ["Home and Gardening", "Flowers"])];
        one.setThemes(t1);

        def t2 = [new Theme(0.8, ["Home and Gardening", "Flowers"])];
        two.setThemes(t2);

        def t3 = [new Theme(0.8, ["Flowers and Decorations", "Roses"]), new Theme(0.9, ["Sport", "Tennis"])];
        three.setThemes(t3);

        units.add(one)
        units.add(two)
        units.add(three)

        def filtered = units.stream().filter(SelectionPredicates.theme("FLOWER", 0.7)).collect(Collectors.toList());

        then: "Only those matching the predicate are returned."
        assert filtered.size() == 2
        filtered.contains(new RlUnit("2"));
        filtered.contains(new RlUnit("3"));
    }

    def "Location Predicate Filter"() {
        when: "A list of RlUnits"
        List<RlUnit> units = new ArrayList<RlUnit>();

        def one = new RlUnit("1")
        def two = new RlUnit("2")
        def three = new RlUnit("3");

        one.setLocations([new Location(["City", "London"]), new Location(["Country", "United Kingdom"])])
        two.setLocations([new Location(["City", "Edinburgh"]), new Location(["Country", "United Kingdom"])])
        // Three has no locations.

        units.add(one)
        units.add(two)
        units.add(three)

        def filtered = units.stream().filter(SelectionPredicates.location("edinburgh")).collect(Collectors.toList());

        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("2"));
    }

    def "Entity Predicate Filter"() {
        when: "A list of RlUnits"
        List<RlUnit> units = new ArrayList<RlUnit>();

        def one = new RlUnit("1")
        def two = new RlUnit("2")
        def three = new RlUnit("3");

        one.setEntities([new Entity(["Sport", "Football"])])
        two.setEntities([new Entity(["Person", "John"])])
        // Three has no entities.

        units.add(one)
        units.add(two)
        units.add(three)

        def filtered = units.stream().filter(SelectionPredicates.entity("football")).collect(Collectors.toList());

        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("1"));
    }

    def "Open IE Subject Predicate Filter"() {
        given: "A list of RlUnits"
        List<RlUnit> units = new ArrayList<RlUnit>();

        def one = new RlUnit("1")
        def two = new RlUnit("2")
        def three = new RlUnit("3")

        one.setRelationTriples([new RlRelationTriple(1.0, ["bright", "Kite"], ["Fly", "up"], ["Hill", "Air"]), new RlRelationTriple(1.0, ["Boy"], ["flying", "a"], ["in", "the", "park"])] as SortedSet)
        two.setRelationTriples([new RlRelationTriple(1.0, ["Elephant"], ["drinking"], ["water"]), new RlRelationTriple(1.0, ["Elephant"], ["playing", "in"], ["river", "water"])] as SortedSet)
        // Three has no triples.

        units.add(one)
        units.add(two)
        units.add(three)

        when: "Subject is searched for"
        def filtered = units.stream().filter(SelectionPredicates.triple((String [])["elephant"], null, null, false)).collect(Collectors.toList());
        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("2"));

        when: "Multiple subjects are searched for"
        filtered = units.stream().filter(SelectionPredicates.triple((String[]) ["kite", "bright"], null, null, false)).collect(Collectors.toList());
        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("1"));


        when: "Relation is searched for"
        filtered = units.stream().filter(SelectionPredicates.triple(null, (String []) ["fly"], null, false)).collect(Collectors.toList());
        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("1"));


        when: "Multiple relations are searched for"
        filtered = units.stream().filter(SelectionPredicates.triple(null, (String[]) ["flying", "a"], null, false)).collect(Collectors.toList());
        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("1"));


        when: "Object is searched for"
        filtered = units.stream().filter(SelectionPredicates.triple(null, null, (String []) [" RIVER "], false)).collect(Collectors.toList());
        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("2"));

        when: "Multiple objects are searched for"
        filtered = units.stream().filter(SelectionPredicates.triple(null, null, (String[]) ["water", "river"], false)).collect(Collectors.toList());
        then: "Only those matching the predicate are returned."
        assert filtered.size() == 1
        filtered.contains(new RlUnit("2"));


        when: "All is selected but does not match"
        filtered = units.stream().filter(SelectionPredicates.triple((String []) (String [])["Boy"],  (String [])["drinking"],(String []) [" RIVER "], true)).collect(Collectors.toList());
        then: "No results are returned"
        assert filtered.size() == 0

        when: "All is selected and does match"
        filtered = units.stream().filter(SelectionPredicates.triple((String []) ["Boy"], (String []) ["flying"], (String []) ["park"], true))
                .collect(Collectors.toList());
        then: "A triple is returned."
        assert filtered.size() == 1

    }

    def "Time Predicates Filter"() {
        given: "A list of RlUnits"
        List<RlUnit> units = new ArrayList<RlUnit>();

        def one = new RlUnit("1")
        def two = new RlUnit("2")
        def three = new RlUnit("3");

        one.setTime(LocalDateTime.now().plusMinutes(30))
        two.setTime(LocalDateTime.now().minusMinutes(70))
        three.setTime(LocalDateTime.now().plusMinutes(120))

        units.add(one)
        units.add(two)
        units.add(three)

        when:
        def filtered = units.stream().filter(SelectionPredicates.before(LocalDateTime.now().plusMinutes(100))).collect(Collectors.toList());

        then: "Only those matching the predicate are returned."
        assert filtered.size() == 2
        filtered.contains(new RlUnit("1"));
        filtered.contains(new RlUnit("2"));

        when:
        filtered = units.stream().filter(SelectionPredicates.after(LocalDateTime.now())).collect(Collectors.toList());

        then: "Only those matching the predicate are returned."
        assert filtered.size() == 2
        filtered.contains(new RlUnit("1"));
        filtered.contains(new RlUnit("3"));
    }


}