package uk.ac.ed.reelout.selection

import spock.lang.Specification
import uk.ac.ed.reelout.domain.RlUnit

/**
 * Tests the predicates used for filtering RLUnits.
 */
class SelectionFiltersTest extends Specification {


    def "Message Text Predicate Filter"() {
        given: "A list of RlUnits"
        NavigableSet<RlUnit> units = new TreeSet<RlUnit>();

        def one = new RlUnit("1")
        def two = new RlUnit("2")
        def three = new RlUnit("3");

        one.setMessageText("cat dog fish")
        two.setMessageText("dog giraffe tiger")
        three.setMessageText("fish fish eel")

        units.add(one)
        units.add(two)
        units.add(three)

        when: "Default and filtering is used"

        def filtered = new SelectionFilters().filterOnMessageText((String[]) ["dog","giraffe"], units);

        then: "Only those matching the filter are returned."
        assert filtered.size() == 2
        assert filtered.contains(new RlUnit("1"));
        assert filtered.contains(new RlUnit("2"));

        when: "An AND filter is used"

        filtered = new SelectionFilters().filterOnMessageText((String[]) ["dog","and","fish"], units);

        then: "Only those matching the filter are returned."
        assert filtered.size() == 1
        assert filtered.contains(new RlUnit("1"));

        when: "A NOT filter is used"

        filtered = new SelectionFilters().filterOnMessageText((String[]) ["not", "eel"], units);

        then: "Only those matching the filter are returned."
        assert filtered.size() == 2
        assert filtered.contains(new RlUnit("1"));
        assert filtered.contains(new RlUnit("2"));

        when: "The filter is reset."

        filtered = new SelectionFilters().filterOnMessageText((String[]) ["dog","and","fish","eel"], units);

        then: "Only those matching the filter are returned."
        assert filtered.size() == 2
        assert filtered.contains(new RlUnit("1"));
        assert filtered.contains(new RlUnit("3"));

    }

}