package uk.ac.ed.reelout.repository

import com.google.common.collect.HashBiMap
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.factory.Nd4j
import spock.lang.Specification
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit
import uk.ac.ed.reelout.services.ReeloutConversionService

class WordEmbeddingFeatureRepositoryTest extends Specification {


    def "Fill Array with Image Features"() {

        given: "An image feature service"
        final WordEmbeddingFeatureRepository word = new WordEmbeddingFeatureRepository()
        word.loadFeatures("./data/embeddings/glove.6B.50d.txt")

        INDArray array = Nd4j.zeros(5, 5); // Create a zero Array.

        Map<String, Integer> idToNumMap = HashBiMap.create();
        idToNumMap.put("2024", 1)
        idToNumMap.put("118851", 2)
        idToNumMap.put("235953", 3)
        idToNumMap.put("445893", 4)
        idToNumMap.put("999999", 5)

        Set<RlUnit> first = new TreeSet<RlUnit>();

        def rl1 = new RlUnit("2024")
        def rl2 = new RlUnit("118851")
        def rl3 = new RlUnit("235953")
        def rl4 = new RlUnit("445893")
        def rl5 = new RlUnit("999999")

        rl1.setTokens(["tennis"]);
        rl2.setTokens(["kite", "beach"]);
        rl3.setTokens(["beach", "water"]);
        rl4.setTokens(["beach", "dog"])
        rl5.setTokens(["badminton"])


        first.add(rl1);
        first.add(rl2);

        Set<RlUnit> second = new TreeSet<RlUnit>();
        second.add(rl2);
        second.add(rl3);
        second.add(rl4);
        second.add(rl5);

        when: "the array is filled with Cosine similarities between image features."
        def filledArray = word.fillSimilarityArray(first, second, idToNumMap, array)

        then: "Array is filled correctly"
        assert filledArray != null;
        assert filledArray.getDouble(0, 1).round(2) == 0.46;
        assert filledArray.getDouble(0, 2).round(2) == 0.38;
        assert filledArray.getDouble(0, 3).round(2) == 0.42;
        assert filledArray.getDouble(0, 4).round(2) == 0.82;
        assert filledArray.getDouble(1, 2).round(2) == 0.74;
        assert filledArray.getDouble(1, 3).round(2) == 0.85;
        assert filledArray.getDouble(1, 4).round(2) == 0.34;

        assert filledArray.getDouble(2, 0) == 0.0;
        assert filledArray.getDouble(3, 0) == 0.0;
        assert filledArray.getDouble(4, 0) == 0.0;

    }

    def "Word embedding from query"() {

        given: "An image feature service"
        final WordEmbeddingFeatureRepository word = new WordEmbeddingFeatureRepository()
        word.loadFeatures("./data/embeddings/glove.6B.50d.txt")
        INDArray zeroes = Nd4j.zeros(50);

        when: "A sentence is provided"
        def embedding = word.createWordEmbeddingForQuerySentence("A boy is flying a kite on a beach.")

        then: "The embedding is set"
        assert embedding != zeroes

    }

    def "Similarity of a query with RlUnits"() {
        given: "A word embedding feature service"
        def export = new ReeloutConversionService()
        def WordEmbeddingFeatureRepository word = new WordEmbeddingFeatureRepository()
        word.loadFeatures("./data/embeddings/glove.6B.50d.txt")

        final String expExportFile = "./src/test/resources/reelout-test-export-split-annotations.xml"


        when: "RlUnits in the are compared against the query array"
        final RlDatabase repFromXml = export.convertToDomainFromFile(expExportFile)
        def idMap = new HashMap<String, Integer>();
        for (int i = 1; i <= repFromXml.getRlUnits().size(); i++) {
            idMap.put(repFromXml.getRlUnits().getAt(i - 1).getId(), i)
        }
        def similarities = word.similaritiesToQuery(word.createWordEmbeddingForQuerySentence("Cups of tea and a pot on the table."), repFromXml.getRlUnits(), idMap)

        then: "Similarities are populated"
        assert similarities != null
        for (int i = 0; i < idMap.size(); i++) {
            assert similarities.getDouble(0, i) > 0.0;  // Check a cosine similarity has been calculated
        }
        // Check the max and the min.
        assert Nd4j.max(similarities).getDouble(0).round(2) == 0.75
        assert Nd4j.min(similarities).getDouble(0).round(2) == 0.34


    }

}