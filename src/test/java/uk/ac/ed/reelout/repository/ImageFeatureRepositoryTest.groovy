package uk.ac.ed.reelout.repository

import com.google.common.collect.HashBiMap
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.factory.Nd4j
import spock.lang.Shared
import spock.lang.Specification
import uk.ac.ed.reelout.domain.RlUnit

class ImageFeatureRepositoryTest extends Specification {

    @Shared
    ImageFeatureRepository image

    def setupSpec() {
        image = new ImageFeatureRepository()
        image.loadFeatures("./src/test/resources/image_embeddings/")
    }

    def "Cosine Similarity"() {

        when: "features are loaded"

        then: "The ids and vectors are correct"
        assert image.features.size() == 8
        assert image.features.containsKey("287003")
        assert image.features.containsKey("537372")
        assert image.features.containsKey("581409")

        double similarity = image.cosineSimilarity(new RlUnit("287003"), new RlUnit("537372"))

        then: "The ids and vectors are correct"
        assert similarity.round(3) == 0.097;
    }

    def "Cosine Similarity features not available "() {

        when: "the cosine similarity between a missing feature is performed"
        double similarity = image.cosineSimilarity(new RlUnit("1111111"), new RlUnit("537372"))

        then: "The similarity is 0.0"
        assert similarity == 0.0;

        when: "the cosine similarity between a missing feature for second id is performed"
        double similarity2 = image.cosineSimilarity(new RlUnit("287003"), new RlUnit("9999999"))

        then: "The similarity is 0.0"
        assert similarity2 == 0.0;
    }

    def "Fill Array with Image Features"() {

        given: "An image feature service"
        image.loadFeatures("./data/image_embeddings/")

        INDArray array = Nd4j.zeros(5, 5); // Create a zero Array.

        Map<String, Integer> idToNumMap = HashBiMap.create();
        idToNumMap.put("2024", 1)
        idToNumMap.put("118851", 2)
        idToNumMap.put("235953", 3)
        idToNumMap.put("445893", 4)
        idToNumMap.put("9999999999", 5) // Doesn't exist.

        Set<RlUnit> first = new TreeSet<RlUnit>();
        first.add(new RlUnit("2024"));
        first.add(new RlUnit("118851"));

        Set<RlUnit> second = new TreeSet<RlUnit>();
        second.add(new RlUnit("118851"));
        first.add(new RlUnit("235953"));
        second.add(new RlUnit("445893"));
        second.add(new RlUnit("9999999999"));

        when: "the array is filled with Cosine similarities between image features."
        def filledArray = image.fillSimilarityArray(first, second, idToNumMap, array)

        then: "Array is filled correctly"
        assert filledArray != null;
        assert filledArray.getDouble(0, 1).round(2) == 0.09;
        assert filledArray.getDouble(0, 3).round(2) == 0.12;
        assert filledArray.getDouble(1, 3).round(2) == 0.11;
        assert filledArray.getDouble(2, 1).round(2) == 0.06;
        assert filledArray.getDouble(2, 3).round(2) == 0.11;

    }

}