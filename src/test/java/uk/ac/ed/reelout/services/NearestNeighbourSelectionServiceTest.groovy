package uk.ac.ed.reelout.services

import spock.lang.Specification
import uk.ac.ed.reelout.domain.ImageCriteria

class NearestNeighbourSelectionServiceTest extends Specification {

    def "Select images according to cosine similarity of nearest neighbours"(
            final String sourceXml, final ImageCriteria imageCriteria) {

        given: "Image selection based on nearest neighbour."
        final ReeloutConversionService conversion = new ReeloutConversionService()
        final NearestNeighbourImageSelectionService service = new NearestNeighbourImageSelectionService();

        def rlDb = conversion.convertToDomainFromFile(sourceXml)

        def triptych1 = service.selectTriptychImages(rlDb, imageCriteria);
        def triptych2 = service.selectTriptychImages(rlDb, imageCriteria);

        expect: "Both Triptychs contain the correct amount of images and are not the same."
        assert triptych1.size() == 3
        assert triptych2.size() == 3
        assert triptych1 != triptych2


        where:
        sourceXml                                         | imageCriteria
        "./src/test/resources/selection/reelout_1000.xml" | new ImageCriteria()
    }

}