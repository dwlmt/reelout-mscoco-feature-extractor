package uk.ac.ed.reelout.services

import spock.lang.Specification
import uk.ac.ed.reelout.domain.ImageCriteria

class RandomImageSelectionServiceTest extends Specification {

    def "Select random images "(final String sourceXml, final ImageCriteria imageCriteria) {

        given: "An image selection service randomly selects images"
        final ReeloutConversionService conversion = new ReeloutConversionService()
        final RandomImageSelectionService service = new RandomImageSelectionService();

        def rlDb = conversion.convertToDomainFromFile(sourceXml)

        def triptych1 = service.selectTriptychImages(rlDb, imageCriteria);
        def triptych2 = service.selectTriptychImages(rlDb, imageCriteria);

        expect: "Both Triptychs contain the correct amount of images and are not the same."
        assert triptych1.size() == 3
        assert triptych2.size() == 3
        assert triptych1 != triptych2


        where:
        sourceXml                                         | imageCriteria
        "./src/test/resources/selection/reelout_1000.xml" | new ImageCriteria()
    }

}