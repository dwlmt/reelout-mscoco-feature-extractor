package uk.ac.ed.reelout.services

import spock.lang.Specification
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit

import java.time.LocalDateTime

/**
 * Tests for the MS-COCO conversion service.
 */
class MsCocoConversionServiceTest extends Specification {

    def "convertToReeloutDomain join annotations"() {
        given: "An MS-COCO service."
        final MsCocoConversionService serv = new MsCocoConversionService()

        final String fileName = "./src/test/resources/mscoco-test.json"
        final RlDatabase expRep = new RlDatabase(new Float(0.2))
        expRep.addRlUnit(new RlUnit("122688", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 52, 26), "http://farm4.staticflickr.com/3249/2911640772_49daa557a0_z.jpg"
                , "The huge clock on the wall is near a wooden table. The clock has a large face with numbers. a clock with big numbers at the end of a table A table and chairs with a large clock on the wall. a large clock on the wall above a radiator "))
        expRep.addRlUnit(new RlUnit("222016", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 37, 59), "http://farm2.staticflickr.com/1431/1118526611_09172475e5_z.jpg",
                "a big red telephone booth that a man is standing in a person standing inside of a phone booth  this is an image of a man in a phone booth. A man is standing in a red phone booth. A man using a phone in a phone booth."))
        expRep.addRlUnit(new RlUnit("57870", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 28, 13), "http://farm4.staticflickr.com/3153/2970773875_164f0c0b83_z.jpg",
                "A restaurant has modern wooden tables and chairs. A long restaurant table with rattan rounded back chairs. a long table with a plant on top of it surrounded with wooden chairs  A long table with a flower arrangement in the middle for meetings A table is adorned with wooden chairs with blue accents."))
        expRep.addRlUnit(new RlUnit("69675", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 46, 33), "http://farm8.staticflickr.com/7156/6415223357_5ef0955050_z.jpg",
                "A child and woman are cooking in the kitchen. A woman glances at a young girl's cooking on the stovetop A young girl and a woman preparing food in a kitchen. a young person and an older person in a kitchen Two women cooking on stove in a kitchen together."))


        when: "Converted to the ReelOut Domain"
        final String fileContents = new File(fileName).text
        final RlDatabase rep = serv.convertToReeloutDomain(fileContents, true, 10)
        int counter = 0


        then: "The mapped domain objects mapped those in the source JSON"
        assert expRep == rep
        // Check each field for each object as the Equals check will only look at Id.
        for (final RlUnit rl : rep.getRlUnits()) {
            RlUnit expRl = expRep.getRlUnits().toList()[counter]
            assert rl.getImage() == expRl.getImage()
            assert rl.getSrc() == expRl.getSrc()
            assert rl.getTime() == expRl.getTime()
            assert rl.getMessageText() == expRl.getMessageText()

            counter++
        }

    }

    def "convertToReeloutDomain join annotations not all sentences"() {
        given: "An MS-COCO service."
        final MsCocoConversionService serv = new MsCocoConversionService()

        final String fileName = "./src/test/resources/mscoco-test.json"
        final RlDatabase expRep = new RlDatabase(new Float(0.2))
        expRep.addRlUnit(new RlUnit("122688", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 52, 26), "http://farm4.staticflickr.com/3249/2911640772_49daa557a0_z.jpg"
                , "The huge clock on the wall is near a wooden table. The clock has a large face with numbers."))
        expRep.addRlUnit(new RlUnit("222016", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 37, 59), "http://farm2.staticflickr.com/1431/1118526611_09172475e5_z.jpg",
                "a big red telephone booth that a man is standing in a person standing inside of a phone booth "))
        expRep.addRlUnit(new RlUnit("57870", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 28, 13), "http://farm4.staticflickr.com/3153/2970773875_164f0c0b83_z.jpg",
                "A restaurant has modern wooden tables and chairs. A long restaurant table with rattan rounded back chairs."))
        expRep.addRlUnit(new RlUnit("69675", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 46, 33), "http://farm8.staticflickr.com/7156/6415223357_5ef0955050_z.jpg",
                "A child and woman are cooking in the kitchen. A woman glances at a young girl's cooking on the stovetop"))


        when: "Converted to the ReelOut Domain"
        final String fileContents = new File(fileName).text
        final RlDatabase rep = serv.convertToReeloutDomain(fileContents, true, 2) // Only select the first two sentences.
        int counter = 0


        then: "The mapped domain objects mapped those in the source JSON"
        assert expRep == rep
        // Check each field for each object as the Equals check will only look at Id.
        for (final RlUnit rl : rep.getRlUnits()) {
            RlUnit expRl = expRep.getRlUnits().toList()[counter]
            assert rl.getMessageText() == expRl.getMessageText()

            counter++
        }

    }

    def "convertToReeloutDomain split annotations"() {
        given: "An MS-COCO service."
        final MsCocoConversionService serv = new MsCocoConversionService()

        final String fileName = "./src/test/resources/mscoco-test-single-image.json"
        final RlDatabase expRep = new RlDatabase(new Float(0.2))
        expRep.addRlUnit(new RlUnit("787980", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 28, 13), "http://farm4.staticflickr.com/3153/2970773875_164f0c0b83_z.jpg",
                "A restaurant has modern wooden tables and chairs."))
        expRep.addRlUnit(new RlUnit("789366", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 28, 13), "http://farm4.staticflickr.com/3153/2970773875_164f0c0b83_z.jpg",
                "A long restaurant table with rattan rounded back chairs."))
        expRep.addRlUnit(new RlUnit("789888", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 28, 13), "http://farm4.staticflickr.com/3153/2970773875_164f0c0b83_z.jpg",
                "a long table with a plant on top of it surrounded with wooden chairs "))
        expRep.addRlUnit(new RlUnit("791316", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 28, 13), "http://farm4.staticflickr.com/3153/2970773875_164f0c0b83_z.jpg",
                "A long table with a flower arrangement in the middle for meetings"))
        expRep.addRlUnit(new RlUnit("794853", "mscoco", LocalDateTime.of(2013, 11, 14, 16, 28, 13), "http://farm4.staticflickr.com/3153/2970773875_164f0c0b83_z.jpg",
                "A table is adorned with wooden chairs with blue accents."))


        when: "Converted to the ReelOut Domain"
        final String fileContents = new File(fileName).text
        final RlDatabase rep = serv.convertToReeloutDomain(fileContents, false, 10)
        int counter = 0


        then: "The mapped domain objects mapped those in the source JSON"
        assert expRep == rep
        // Check each field for each object as the Equals check will only look at Id.
        for (final RlUnit rl : rep.getRlUnits()) {
            RlUnit expRl = expRep.getRlUnits().toList()[counter]
            assert rl.getImage() == expRl.getImage()
            assert rl.getSrc() == expRl.getSrc()
            assert rl.getTime() == expRl.getTime()
            assert rl.getMessageText() == expRl.getMessageText()

            counter++
        }

    }
}
