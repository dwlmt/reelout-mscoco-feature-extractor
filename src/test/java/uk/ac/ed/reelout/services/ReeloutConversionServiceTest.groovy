package uk.ac.ed.reelout.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit
import uk.ac.ed.reelout.features.FeatureChainFactory
import uk.ac.ed.reelout.features.FeatureType

/**
 * Tests for the Reelout exportImport service.
 */
@ContextConfiguration(locations = "classpath:META-INF/spring/spring-shell-plugin.xml")
class ReeloutConversionServiceTest extends Specification {

    @Autowired
    FeatureChainFactory factory;

    @Autowired
    MsCocoConversionService mscoco;

    @Autowired
    ReeloutConversionService export;

    def "convertToXml Basic POS tags."() {
        given: "A Reelout Export Service"

        final String fileName = "./src/test/resources/mscoco-test.json"
        final String expExportFile = "./src/test/resources/reelout-test-export.xml"


        when: "The file is loaded, POS tagged, and converted to XML. "
        final String fileContents = new File(fileName).text
        final String expExport = new File(expExportFile).text
        RlDatabase rep = mscoco.convertToReeloutDomain(fileContents, true, 10)
        rep = factory.createFeatureChain([FeatureType.POS]).createFeatures(rep, null)
        final String xml = export.convertToXML(rep)

        then: "the correct XML is created."
        assert new XmlSlurper().parseText(xml) == new XmlSlurper().parseText(expExport)

    }

    def "convertToXml Basic POS tags with split annotations"() {
        given: "A Reelout Export Service"

        final String fileName = "./src/test/resources/mscoco-test.json"
        final String expExportFile = "./src/test/resources/reelout-test-export-split-annotations.xml"


        when: "The file is loaded, POS tagged, and converted to XML. "
        final String fileContents = new File(fileName).text
        final String expExport = new File(expExportFile).text
        RlDatabase rep = mscoco.convertToReeloutDomain(fileContents, false, 10)
        rep = factory.createFeatureChain([FeatureType.POS]).createFeatures(rep, null)
        final String xml = export.convertToXML(rep)


        then: "the correct XML is created."
        assert xml == expExport

    }

    def "Read XML and convert to Domain objects"() {
        given: "A Reelout Conversion Service"

        final String fileName = "./src/test/resources/mscoco-test.json"
        final String expExportFile = "./src/test/resources/reelout-test-export.xml"


        when: "The file is loaded, POS tagged, and converted to XML. "
        final String fileContents = new File(fileName).text
        final String fileToImport = new File(expExportFile).text
        RlDatabase repFromJson = mscoco.convertToReeloutDomain(fileContents, true, 10)
        repFromJson = factory.createFeatureChain([FeatureType.POS]).createFeatures(repFromJson, null)
        final RlDatabase repFromXml = export.convertToDomain(fileToImport)
        int counter = 0

        then: "The domain objects are equal if converted from JSON and deserialized from XML"
        assert repFromJson == repFromXml

        for (final RlUnit rl : repFromJson.getRlUnits()) {
            RlUnit expRl = repFromXml.getRlUnits().toList()[counter]
            assert rl.getImage() == expRl.getImage()
            assert rl.getSrc() == expRl.getSrc()
            assert rl.getTime() == expRl.getTime()
            assert rl.getMessageText() == expRl.getMessageText()
            assert rl.getTags() == expRl.getTags()
            assert rl.getTokens() == expRl.getTokens()
            assert rl.getEmbeddings() == rl.getEmbeddings()

            counter++
        }
    }

    def "Read XML and convert to Domain objects use direct file reader"() {
        given: "A Reelout Conversion Service"

        final String fileName = "./src/test/resources/mscoco-test.json"
        final String expExportFile = "./src/test/resources/reelout-test-export.xml"


        when: "The file is loaded, POS tagged, and converted to XML. "
        final String fileContents = new File(fileName).text
        RlDatabase repFromJson = mscoco.convertToReeloutDomain(fileContents, true, 10)
        repFromJson = factory.createFeatureChain([FeatureType.POS]).createFeatures(repFromJson, null)
        final RlDatabase repFromXml = export.convertToDomainFromFile(expExportFile)
        int counter = 0

        then: "The domain objects are equal if converted from JSON and deserialized from XML"
        assert repFromJson == repFromXml

        for (final RlUnit rl : repFromJson.getRlUnits()) {
            RlUnit expRl = repFromXml.getRlUnits().toList()[counter]
            assert rl.getImage() == expRl.getImage()
            assert rl.getSrc() == expRl.getSrc()
            assert rl.getTime() == expRl.getTime()
            assert rl.getMessageText() == expRl.getMessageText()
            assert rl.getTags() == expRl.getTags()
            assert rl.getTokens() == expRl.getTokens()
            assert rl.getEmbeddings() == rl.getEmbeddings()

            counter++
        }

    }

    def "Read XML and convert to Domain objects split annotations"() {
        given: "A Reelout Conversion Service"

        final String fileName = "./src/test/resources/mscoco-test.json"
        final String expExportFile = "./src/test/resources/reelout-test-export-split-annotations.xml"


        when: "The file is loaded, POS tagged, and converted to XML. "
        final String fileContents = new File(fileName).text
        final String fileToImport = new File(expExportFile).text
        RlDatabase repFromJson = mscoco.convertToReeloutDomain(fileContents, false, 10)
        repFromJson = factory.createFeatureChain([FeatureType.POS]).createFeatures(repFromJson, null)
        final RlDatabase repFromXml = export.convertToDomain(fileToImport)
        int counter = 0

        then: "The domain objects are equal if converted from JSON and deserialized from XML"
        assert repFromJson == repFromXml

        for (final RlUnit rl : repFromJson.getRlUnits()) {
            RlUnit expRl = repFromXml.getRlUnits().toList()[counter]
            assert rl.getImage() == expRl.getImage()
            assert rl.getSrc() == expRl.getSrc()
            assert rl.getTime() == expRl.getTime()
            assert rl.getMessageText() == expRl.getMessageText()
            assert rl.getTags() == expRl.getTags()
            assert rl.getTokens() == expRl.getTokens()

            counter++
        }

    }

    def "Join two RL Databases."() {

        when: "Two RlDatabases are joined."
        final String expExportFile = "./src/test/resources/reelout-test-export-split-annotations.xml"
        final String fileToImport = new File(expExportFile).text
        final RlDatabase firstDatabase = this.export.convertToDomain(fileToImport)
        final RlDatabase secondDatabase = new RlDatabase();
        secondDatabase.getRlUnits().add(new RlUnit("AKey"))
        secondDatabase.getRlUnits().add(new RlUnit("BKey"))
        final RlDatabase combined = this.export.join(firstDatabase, secondDatabase)

        then: "The combined database contains RlUnits from both the first and second database."
        assert combined.getRlUnits().containsAll(firstDatabase.getRlUnits())
        assert combined.getRlUnits().containsAll(secondDatabase.getRlUnits())
    }

    def "Cut two RL Databases."() {

        when: "Two RlDatabases are joined, and then cut back to the orginal keys."
        final String expExportFile = "./src/test/resources/reelout-test-export-split-annotations.xml"
        final String fileToImport = new File(expExportFile).text
        final RlDatabase firstDatabase = this.export.convertToDomain(fileToImport)
        final RlDatabase secondDatabase = new RlDatabase();
        secondDatabase.getRlUnits().add(new RlUnit("AKey"))
        secondDatabase.getRlUnits().add(new RlUnit("BKey"))
        final RlDatabase combined = this.export.join(firstDatabase, secondDatabase)
        final RlDatabase cut1 = this.export.cut(combined, null, "AKey");
        final RlDatabase cut2 = this.export.cut(combined, "AKey", null);

        then: "The cut databases only contain the original keys."
        assert cut1.getRlUnits().containsAll(firstDatabase.getRlUnits())
        assert cut2.getRlUnits().containsAll(secondDatabase.getRlUnits())
        assert !cut2.getRlUnits().containsAll(firstDatabase.getRlUnits())
        assert !cut1.getRlUnits().containsAll(secondDatabase.getRlUnits())
    }
}
