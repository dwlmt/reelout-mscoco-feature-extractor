package uk.ac.ed.reelout.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.features.FeatureChainFactory

import static uk.ac.ed.reelout.features.FeatureType.*

/**
 * Tests exportImport with all features applied.
 */
@ContextConfiguration(locations = "classpath:META-INF/spring/spring-shell-plugin.xml")
class XmlFeatureExportTest extends Specification {

    @Autowired
    FeatureChainFactory featureChainFactory;

    @Autowired
    MsCocoConversionService mscoco;

    @Autowired
    ReeloutConversionService exportImport;

    def "Convert with all existing features."() {
        given: "Files"

        final String fileName = "./src/test/resources/mscoco-test.json"
        final String expExportFile = "./src/test/resources/all-features-export.xml"

        when: "The file is loaded, features processed, and converted to XML. "
        final String fileContents = new File(fileName).text
        final String expExport = new File(expExportFile).text
        RlDatabase rep = mscoco.convertToReeloutDomain(fileContents, true, 10)
        rep = featureChainFactory.createFeatureChain([POS, ENTITY, SENTIMENT, THEME]).createFeatures(rep, null)
        final String xml = exportImport.convertToXML(rep)
        final RlDatabase loadedDomain = exportImport.convertToDomain(expExport)

        then: "Serialised XML is the same, and deserialised domain objects are the same."
        assert new XmlSlurper().parseText(xml) == new XmlSlurper().parseText(expExport)
        assert loadedDomain == rep

    }

    def "Relation - Triple feature"() {
        given: "Files"

        final String fileName = "./src/test/resources/mscoco-test.json"
        final String expExportFile = "./src/test/resources/relation-triple-export.xml"

        when: "The file is loaded, features processed, and converted to XML. "
        final String fileContents = new File(fileName).text
        final String expExport = new File(expExportFile).text
        RlDatabase rep = mscoco.convertToReeloutDomain(fileContents, true, 10)
        rep = featureChainFactory.createFeatureChain([RELATION_TRIPLE]).createFeatures(rep, null)
        final String xml = exportImport.convertToXML(rep)
        final RlDatabase loadedDomain = exportImport.convertToDomain(expExport)

        then: "Serialised XML is the same, and deserialised domain objects are the same."
        assert new XmlSlurper().parseText(xml) == new XmlSlurper().parseText(expExport)
        assert loadedDomain == rep

    }

    /**
     * Needed as the basic test file doesn't have named entities. This one does so it tests can be exported and
     * imported correctly.
     */
    def "Convert for named entities"() {
        given: "Export file that has Named Entities."

        final String fileName = "./src/test/resources/mscoco-named-entity-test.json"
        final String expExportFile = "./src/test/resources/all-features-named-entity-export.xml"

        when: "The file is loaded, features processed, and converted to XML. "
        final String fileContents = new File(fileName).text
        final String expExport = new File(expExportFile).text
        RlDatabase rep = mscoco.convertToReeloutDomain(fileContents, true, 10)
        rep = featureChainFactory.createFeatureChain([POS, ENTITY, SENTIMENT, THEME]).createFeatures(rep, null)
        final String xml = exportImport.convertToXML(rep)
        final RlDatabase loadedDomain = exportImport.convertToDomain(expExport)


        then: "Serialised XML is the same, and deserialised domain objects are the same."
        assert xml == expExport
        assert loadedDomain == rep

    }

    def "Whole Text Word Addition Embedding feature"() {
        given: "Files"

        final String fileName = "./src/test/resources/mscoco-test.json"
        final String expExportFile = "./src/test/resources/word-embedding-export.xml"

        when: "The file is loaded, features processed, and converted to XML. "
        final String fileContents = new File(fileName).text
        final String expExport = new File(expExportFile).text
        RlDatabase rep = mscoco.convertToReeloutDomain(fileContents, true, 10)
        final Map<String, Object> options = new HashMap<String, Object>()
        options["embeddingsFile"] = "./data/embeddings/glove.6B.50d.txt";
        options["embeddingsType"] = "glove";

        rep = featureChainFactory.createFeatureChain([POS, ADD_WORD_EMBEDDING]).createFeatures(rep, options)

        final RlDatabase loadedDomain = exportImport.convertToDomain(expExport)

        final double TOLERANCE = 0.0001;

        then: "Serialised XML is the same, and deserialised domain objects are the same."
        // Need to loop as the exact values of the vector could be different because of the variation in
        // floating point operations in different environments.
        rep.getRlUnits().eachWithIndex { final item, final index ->
            final def rlCompare = loadedDomain.getRlUnits().toList()[index]
            item.getEmbeddings().eachWithIndex { final embed, final embedInd ->
                final def embedCompare = rlCompare.getEmbeddings()[embedInd]
                assert embed.getLabel() == embedCompare.getLabel()
                assert embed.getType() == embedCompare.getType()
                assert embed.getDimensionality() == embedCompare.getDimensionality()

                embed.getVector().eachWithIndex { final elem, final elemInd ->
                    final def vecCompare = embedCompare.getVector()
                    assert Math.abs(elem - vecCompare[elemInd]) < TOLERANCE;

                }
            }
        }


    }


}
