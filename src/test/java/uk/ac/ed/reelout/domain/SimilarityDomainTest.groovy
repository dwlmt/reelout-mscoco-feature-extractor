package uk.ac.ed.reelout.domain

import spock.lang.Specification

/**
 * Tests the equality and construction of the
 */
class SimilarityDomainTest extends Specification {


    def "SimilarityMeasure objects equality and sort comparison"() {

        when: "A list of similarities is supplied and sorted."
        def similarities = []
        similarities.add(Similarity.createSimilarity("1", 0.5))
        similarities.add(Similarity.createSimilarity("3", 0.6))
        similarities.add(Similarity.createSimilarity("3", 0.7))
        similarities.add(Similarity.createSimilarity("2", 0.8))

        def measures = [SimilarityMeasure.createSimilarityMeasure("b", similarities, 0.0, 1.0),
                        SimilarityMeasure.createSimilarityMeasure("a", similarities, 2.0, 3.0)]
        measures = measures.sort()


        def expSimilarities = []
        expSimilarities.add(Similarity.createSimilarity("2", 0.8))
        expSimilarities.add(Similarity.createSimilarity("3", 0.7))
        expSimilarities.add(Similarity.createSimilarity("3", 0.6))
        expSimilarities.add(Similarity.createSimilarity("1", 0.5))

        def expMeasures = [SimilarityMeasure.createSimilarityMeasure("a", similarities, 2.0, 3.0),
                           SimilarityMeasure.createSimilarityMeasure("b", similarities, 0.0, 1.0),
        ]

        then: "The sorted list is as expected."
        assert measures == expMeasures
        assert measures.toString() == expMeasures.toString()
    }


}

