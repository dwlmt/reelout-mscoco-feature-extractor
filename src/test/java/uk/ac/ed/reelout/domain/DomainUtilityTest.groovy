package uk.ac.ed.reelout.domain

import spock.lang.Specification

import java.time.LocalDateTime

/**
 * Tests the basic utility classes of the domain objects such as equality, hashcode, etc.
 */
class DomainUtilityTest extends Specification {


    def "For a RlUnit objects are Equal"(
            final String id,
            final String src,
            final LocalDateTime time, final List<String> tags, final List<String> tokens, final String image,
            final String id2,
            final String src2,
            final LocalDateTime time2, final List<String> tags2, final List<String> tokens2, final String image2) {

        expect: "Objects are equal"
        final RlUnit rl = new RlUnit(id)
        rl.setSrc(src)
        rl.setTags(tags)
        rl.setTokens(tokens)
        rl.setImage(image)

        final RlUnit rl2 = new RlUnit(id2)
        rl2.setSrc(src2)
        rl2.setTags(tags2)
        rl2.setTokens(tokens2)
        rl2.setImage(image2)

        rl == rl2
        rl.hashCode() == rl2.hashCode()
        rl.compareTo(rl2) == 0

        where:
        id  | src | time                | tags            | tokens          | image          | id2 | src2 | time2               | tags2              | tokens2            | image2
        "1" | "A" | LocalDateTime.now() | ["A", "B", "C"] | ["D", "E", "f"] | "http://url/1" | "1" | "A"  | LocalDateTime.now() | ["A", "B", "C"]    | ["D", "E", "f"]    | "http://url/1"
        "1" | "A" | LocalDateTime.now() | ["A", "B", "C"] | ["D", "E", "f"] | "http://url/1" | "1" | "A2" | LocalDateTime.now() | ["A2", "B2", "C2"] | ["D2", "E2", "f2"] | "http://url/2"
    }

    def "For a RlUnit objects are Not Equal"(
            final String id,
            final String src,
            final LocalDateTime time, final List<String> tags, final List<String> tokens, final String image,
            final String id2,
            final String src2,
            final LocalDateTime time2, final List<String> tags2, final List<String> tokens2, final String image2) {

        expect: "Objects are equal"
        final RlUnit rl = new RlUnit(id)
        rl.setSrc(src)
        rl.setTags(tags)
        rl.setTokens(tokens)
        rl.setImage(image)

        final RlUnit rl2 = new RlUnit(id2)
        rl2.setSrc(src2)
        rl2.setTags(tags2)
        rl2.setTokens(tokens2)
        rl2.setImage(image2)

        rl != rl2
        rl.hashCode() != rl2.hashCode()
        rl.compareTo(rl2) != 0
        rl.toString() != rl2.toString()

        where:
        id  | src | time                | tags            | tokens          | image          | id2 | src2 | time2               | tags2              | tokens2            | image2
        "1" | "A" | LocalDateTime.now() | ["A", "B", "C"] | ["D", "E", "f"] | "http://url/1" | "2" | "A"  | LocalDateTime.now() | ["A", "B", "C"]    | ["D", "E", "f"]    | "http://url/1"
        "1" | "A" | LocalDateTime.now() | ["A", "B", "C"] | ["D", "E", "f"] | "http://url/2" | "2" | "A2" | LocalDateTime.now() | ["A2", "B2", "C2"] | ["D2", "E2", "f2"] | "http://url/2"
    }

    def "RlUnit compareTo"() {

        when: "Different unordered RlUnits."
        final RlDatabase rlDb = new RlDatabase()
        rlDb.setVersion(0.2)
        final RlUnit r1 = new RlUnit("1")
        final RlUnit r2 = new RlUnit("2")
        final RlUnit r3 = new RlUnit("3")
        final RlUnit r2b = new RlUnit("2")

        // Add to list in incorrect order.
        final SortedSet<RlUnit> l = new TreeSet<RlUnit>()
        l.add(r3)
        l.add(r2)
        l.add(r1)
        l.add(r2b)
        rlDb.setRlUnits(l)

        then: "The list is sorted"
        [r1, r2, r3] == rlDb.getRlUnits().toList()
    }

    def "Theme equality"(
            final double confidence1,
            final List<String> categories1, final double confidence2, final List<String> categories2) {

        expect: "Objects are equal"
        final def theme1 = new Theme(confidence1, categories1)
        final def theme2 = new Theme(confidence2, categories2)
        assert theme1.equals(theme2)
        assert theme1.hashCode() == theme2.hashCode()
        assert theme1.toString() == theme2.toString()

        where:
        confidence1 | categories1                                | confidence2 | categories2
        0.8         | ["a category"]                             | 0.8         | ["a category"]
        0.6         | ["a category", "b category", "c category"] | 0.6         | ["a category", "b category", "c category"]
    }

    def "Theme Inequality"(
            final double confidence1,
            final List<String> categories1, final double confidence2, final List<String> categories2) {

        expect: "Objects are equal"
        final def theme1 = new Theme(confidence1, categories1)
        final def theme2 = new Theme(confidence2, categories2)
        assert !theme1.equals(theme2)
        assert theme1.hashCode() != theme2.hashCode()
        assert theme1.toString() != theme2.toString()

        where:
        confidence1 | categories1                                | confidence2 | categories2
        0.9         | ["a category"]                             | 0.3564      | ["a category"]
        0.8         | ["a category"]                             | 0.8         | []
        0.6         | ["a category", "b category", "c category"] | 0.6         | ["a category", "b category", "d category"]
    }

    def "Location equality"(final List<String> locations1, final List<String> locations2) {

        expect: "Objects are equal"
        final def loc1 = new Location(locations1)
        final def loc2 = new Location(locations2)
        assert loc1.equals(loc2)
        assert loc1.hashCode() == loc2.hashCode()
        assert loc1.toString() == loc2.toString()

        where:
        locations1      | locations2
        ["a"]           | ["a"]
        ["a", "b", "c"] | ["a", "b", "c"]
    }

    def "Location Inequality"(final List<String> locations1, final List<String> locations2) {

        expect: "Objects are not equal"
        final def loc1 = new Location(locations1)
        final def loc2 = new Location(locations2)
        assert !loc1.equals(loc2)
        assert loc1.hashCode() != loc2.hashCode()
        assert loc1.toString() != loc2.toString()

        where:
        locations1      | locations2
        ["a"]           | ["b"]
        ["a", "b", "c"] | ["a", "b", "c", "d"]
    }

    def "Entity equality"(final List<String> entities1, final List<String> entities2) {

        expect: "Objects are equal"
        final def ent1 = new Entity(entities1)
        final def ent2 = new Entity(entities2)
        assert ent1.equals(ent2)
        assert ent1.hashCode() == ent2.hashCode()
        assert ent1.toString() == ent2.toString()

        where:
        entities1       | entities2
        ["a"]           | ["a"]
        ["a", "b", "c"] | ["a", "b", "c"]
    }

    def "Entity Inequality"(final List<String> entities1, final List<String> entities2) {

        expect: "Objects are not equal"
        final def ent1 = new Entity(entities1)
        final def ent2 = new Entity(entities2)
        assert !ent1.equals(ent2)
        assert ent1.hashCode() != ent2.hashCode()
        assert ent1.toString() != ent2.toString()

        where:
        entities1       | entities2
        ["a"]           | ["b"]
        ["a", "b", "c"] | ["a", "b", "c", "d"]
    }

    def "RelationTriple equality"(final Map triples1, final Map triples2) {

        expect: "Objects are equal"
        assert triples1.equals(triples2)
        assert triples1.hashCode() == triples2.hashCode()

        where:
        triples1                                                                        | triples2
        // Confidence shouldn't effect equality.
        [(new RlRelationTriple(0.8, ["light", "bedroom"], ["is", "with"], ["bed"])): 3] | [(new RlRelationTriple(1.0, ["light", "bedroom"], ["is", "with"], ["bed"])): 3]
    }

    def "RelationTriple Inequality"(final Map triples1, final Map triples2) {

        expect: "Objects are equal"
        assert !triples1.equals(triples2)
        assert triples1.hashCode() != triples2.hashCode()

        where:
        triples1                                                                        | triples2
        // Confidence shouldn't effect equality.
        [(new RlRelationTriple(0.8, ["light", "bedroom"], ["is", "with"], ["bed"])): 3] | [(new RlRelationTriple(1.0, ["light", "bedroom"], ["is", "with"], ["table"])): 3]
        [(new RlRelationTriple(0.8, ["light", "bedroom"], ["is", "with"], ["bed"])): 3] | [(new RlRelationTriple(1.0, ["light", "bedroom"], ["is"], ["bed"])): 3]
        [(new RlRelationTriple(0.8, ["light", "bedroom"], ["is", "with"], ["bed"])): 3] | [(new RlRelationTriple(1.0, ["bright", "light", "bedroom"], ["is", "with"], ["bed"])): 3]

    }


}