package uk.ac.ed.reelout.command

import org.springframework.shell.Bootstrap
import org.springframework.shell.core.CommandResult
import org.springframework.shell.core.JLineShellComponent
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Tests the MSCOCO convert command run from the Spring shell command.
 */
@Unroll
class ReeloutMsCocoCommandTest extends Specification {

    def "MSCoco Convert"(final String source, final String destination, final String join, final Integer maxSentences,
                         final String expXml) {

        given: "An MS-COCO command is run"
        final
        def command = String.format("mscoco convert --source '%s' --destination '%s' --join '%s' --max_sentences '%s' ",
                source, destination, join, maxSentences)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("MS-COCO transformed and written to XML format")
        assert new XmlSlurper().parseText(new File(destination).text) == new XmlSlurper().parseText(new File(expXml).text)

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                  | destination                                             | join    | maxSentences | expXml
        "./src/test/resources/mscoco-test.json" | "./src/test/resources/mscoco-test-command-out.xml"      | "false" | 10           | "./src/test/resources/mscoco-test-command-out-exp.xml"
        "./src/test/resources/mscoco-test.json" | "./src/test/resources/mscoco-test-command-out-2.xml"    | "true"  | 2            | "./src/test/resources/mscoco-test-command-out-2-exp.xml"
        "./src/test/resources/mscoco-test.json" | "./src/test/resources/mscoco-test-command-out-3.xml"    | "true"  | 3            | "./src/test/resources/mscoco-test-command-out-3-exp.xml"
        "./src/test/resources/mscoco-test.json" | "./src/test/resources/mscoco-test-command-join-out.xml" | "true"  | 10           | "./src/test/resources/mscoco-test-command-join-out-exp.xml"
    }

    def "MS-COCO convert invalid command options"(final String source, final String destination, final Boolean join) {

        given: "An MS-COCO command is run"
        final def command = String.format("mscoco convert --source '%s' --destination '%s' --join '%s'",
                source, destination, join)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == false

        where:
        source                                                 | destination                                            | join
        ""                                                     | "./src/test/resources/mscoco-test-command-out-add.xml" | "True"
        "./src/test/resources/mscoco-test-command-out-exp.xml" | ""                                                     | "False"
        "./src/test/resources/mscoco-test-command-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-add.xml" | ""
    }

    def "mscoco convert missing options"(final String command, final Boolean exp) {

        given: "mscoco convert command is run"

        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == exp

        where:
        command                                                                                     | exp
        "reelout add --destination 'bfile' --join 'true' "                                          | false
        "reelout add --source './src/test/resources/mscoco-test-command-out-exp.xml' --join 'true'" | false
    }

}