package uk.ac.ed.reelout.command

import org.springframework.shell.Bootstrap
import org.springframework.shell.core.CommandResult
import org.springframework.shell.core.JLineShellComponent
import spock.lang.Specification
import spock.lang.Unroll
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.services.ReeloutConversionService

/**
 * Tests the basic utility classes of the domain objects such as equality, hashcode, etc.
 */
@Unroll
class ReeloutAddFeaturesCommandTest extends Specification {

    def "Reelout add"(final String source, final String destination, final String features, final String expXml) {

        given: "A Reelout Add command is run"
        final def command = String.format("reelout add --source '%s' --destination '%s' --features '%s'",
                source, destination, features)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Added Reelout features")
        assert new XmlSlurper().parseText(new File(destination).text) == new XmlSlurper().parseText(new File(expXml).text)

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                                          | destination                                            | features        | expXml
        "./src/test/resources/mscoco-test-command-out-exp.xml"          | "./src/test/resources/mscoco-test-command-out-add.xml" | "pos,sentiment" | "./src/test/resources/mscoco-test-command-features-out-exp.xml"
        "./src/test/resources/mscoco-test-command-features-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-add.xml" | "pos,sentiment" | "./src/test/resources/mscoco-test-command-features-out-exp.xml" // File doesn't change if features are present.
    }

    def "Reelout add whole text embedding"(
            final String source,
            final String destination, final String features, final boolean removeStopWords, final String expXml) {

        given: "A Reelout Add command is run with a word embedding file specified."
        final def command = String.format("reelout add --source '%s' --destination '%s' --features '%s' " +
                "--word_embeddings_file './data/embeddings/glove.6B.50d.txt' --remove_stop_words '%s' --embedding_closest_n '20'",
                source, destination, features, removeStopWords)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        final RlDatabase loadedDomain = new ReeloutConversionService().convertToDomain(new File(expXml).text)
        final RlDatabase destDomain = new ReeloutConversionService().convertToDomain(new File(destination).text)

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Added Reelout features")

        final double TOLERANCE = 0.0001;

        // Need to loop as the exact values of the vector could be different because of the variation in
        // floating point operations in different environments.
        destDomain.getRlUnits().eachWithIndex { final item, final index ->
            final def rlCompare = loadedDomain.getRlUnits().toList()[index]
            item.getEmbeddings().eachWithIndex { final embed, final embedInd ->
                final def embedCompare = rlCompare.getEmbeddings()[embedInd]
                assert embed.getLabel() == embedCompare.getLabel()
                assert embed.getType() == embedCompare.getType()
                assert embed.getDimensionality() == embedCompare.getDimensionality()

                embed.getVector().eachWithIndex { final elem, final elemInd ->
                    final def vecCompare = embedCompare.getVector()
                    assert Math.abs(elem - vecCompare[elemInd]) < TOLERANCE;
                }

            }

            item.getSimilarityMeasures().eachWithIndex { final simil, final similInd ->
                final def similCompare = rlCompare.getSimilarityMeasures()[similInd]
                assert simil.getType() == similCompare.getType()
                assert Math.abs(simil.getMean() - similCompare.getMean()) < TOLERANCE
                assert Math.abs(simil.getVariance() - similCompare.getVariance()) < TOLERANCE

                simil.getSimilarities().eachWithIndex { final sim, final simInd ->
                    def simComp = similCompare.getSimilarities()[simInd]
                    assert sim.getId() == simComp.getId()
                    assert Math.abs(sim.getValue() - simComp.getValue()) < TOLERANCE
                }
            }
        }

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                                 | destination                                                            | features                                             | removeStopWords | expXml
        "./src/test/resources/mscoco-test-command-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-embedding.xml"           | "pos,add_word_embedding,cosine_similarity_embedding" | true            | "./src/test/resources/mscoco-test-command-out-embedding-exp.xml"
        "./src/test/resources/mscoco-test-command-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-embedding-keep-stop.xml" | "pos,add_word_embedding,cosine_similarity_embedding" | false           | "./src/test/resources/mscoco-test-command-out-embedding-keep-stop-exp.xml"

    }

    def "Reelout image feature cosine similarity"(
            final String source,
            final String destination, final String features, final String expXml) {

        given: "A Reelout Add command is run with a word embedding file specified."
        final def command = String.format("reelout add --source '%s' --destination '%s' --features '%s' --image_feature_directory './data/image_embeddings' --embedding_closest_n '20'",
                source, destination, features)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        final RlDatabase loadedDomain = new ReeloutConversionService().convertToDomain(new File(expXml).text)
        final RlDatabase destDomain = new ReeloutConversionService().convertToDomain(new File(destination).text)

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Added Reelout features")

        final double TOLERANCE = 0.0001;

        destDomain.getRlUnits().eachWithIndex { final item, final index ->
            final def rlCompare = loadedDomain.getRlUnits().toList()[index]

            item.getSimilarityMeasures().eachWithIndex { final simil, final similInd ->
                final def similCompare = rlCompare.getSimilarityMeasures()[similInd]
                assert simil.getType() == similCompare.getType()
                assert Math.abs(simil.getMean() - similCompare.getMean()) < TOLERANCE
                assert Math.abs(simil.getVariance() - similCompare.getVariance()) < TOLERANCE

                simil.getSimilarities().eachWithIndex { final sim, final simInd ->
                    def simComp = similCompare.getSimilarities()[simInd]
                    assert sim.getId() == simComp.getId()
                    assert Math.abs(sim.getValue() - simComp.getValue()) < TOLERANCE
                }
            }
        }

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                                 | destination                                                            | features                                             | expXml
        "./src/test/resources/reelout_image_features_test.xml" | "./src/test/resources/reelout_image_features_text_out.xml"           | "cosine_similarity_image_embedding"        | "./src/test/resources/reelout_image_features_text_out.xml"
    }

    def "Reelout add with filtering"(final String source, final String destination, final String features,
                                     final String fromId, final String toId, final String expXml) {

        given: "A Reelout Add command is run"
        final
        def command = String.format("reelout add --source '%s' --destination '%s' --features '%s' --from_id '%s' --to_id '%s'",
                source, destination, features, fromId, toId)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Added Reelout features")
        assert new XmlSlurper().parseText(new File(destination).text) == new XmlSlurper().parseText(new File(expXml).text)

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                              | destination                                            | features        | fromId | toId | expXml
        "./src/test/resources/mscoco-test-command-base.xml" | "./src/test/resources/mscoco-test-command-out-add.xml" | "pos,sentiment" | "78"   | "84" | "./src/test/resources/mscoco-test-command-out-filter-exp.xml"
    }

    def "Reelout add invalid command options"(final String source, final String destination, final String features) {

        given: "An Reelout add command is run"
        final def command = String.format("reelout add --source '%s' --destination '%s' --features '%s'",
                source, destination, features)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == false

        where:
        source                                                 | destination                                            | features
        "./src/test/resources/mscoco-test-command-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-add.xml" | ""
        "./src/test/resources/mscoco-test-command-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-add.xml" | null
        ""                                                     | "./src/test/resources/mscoco-test-command-out-add.xml" | "pos,sentiment"
        "./src/test/resources/mscoco-test-command-out-exp.xml" | ""                                                     | "pos,sentiment"
        "./src/test/resources/mscoco-test-command-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-add.xml" | "notavalidfeature"
    }

    def "reelout add missing options"(final String command, final Boolean exp) {

        given: "An MS-COCO command is run"

        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == exp

        where:
        command                                                                                                                                                                   | exp
        "reelout add --source './src/test/resources/mscoco-test-command-out-exp.xml' --destination './src/test/resources/mscoco-test-command-out-add.xml' "                       | false
        "reelout add --destination './src/test/resources/mscoco-test-command-out-add.xml' --features 'pos'"                                                                       | false
        "reelout add --source './src/test/resources/mscoco-test-command-out-exp.xml' --features 'pos'"                                                                            | false
        "reelout add --source './src/test/resources/mscoco-test-command-out-exp.xml' --features 'pos,add_word_embedding'"                                                         | false
        "reelout add --source './src/test/resources/mscoco-test-command-out-exp.xml' --features 'add_word_embedding' --word_embeddings_file './data/embeddings/glove.6B.50d.txt'" | false
    }
}