package uk.ac.ed.reelout.command

import org.springframework.shell.Bootstrap
import org.springframework.shell.core.CommandResult
import org.springframework.shell.core.JLineShellComponent
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Tests the basic utility classes of the domain objects such as equality, hashcode, etc.
 */
@Unroll
class ReeloutStatisticsCommandTest extends Specification {

    @Ignore
    def "reelout stats"(final String source, final String destination, final String features, final String expStats) {

        given: "A Reelout stats command is run"
        final def command = String.format("reelout stats --source '%s' --destination '%s' --features '%s'",
                source, destination, features)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Feature stats written")
        //assert new File(destination).text == new File(expStats).text

        cleanup: "Delete exported stats."
        new File(destination).delete()

        where:
        source                                                               | destination                                          | features                     | expStats
        "./src/test/resources/mscoco-test-command-join-features-out-exp.xml" | "./src/test/resources/mscoco-test-command-stats.txt" | "pos,sentiment,entity,theme" | "./src/test/resources/mscoco-test-command-stats-exp.txt"
    }

    def "reelout embedding and cosine similarity stats"(
            final String source, final String destination, final String features, final String expStats) {

        given: "A Reelout stats command is run on embedding statistics"
        // Gets the closest 3 for each image according to Cosine Similarity.
        final
        def command = String.format("reelout stats --source '%s' --destination '%s' --features '%s' --embedding_closest_n '3' ",
                source, destination, features)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Feature stats written")
        //assert new File(destination).text == new File(expStats).text

        cleanup: "Delete exported stats."
        new File(destination).delete()

        where:
        source                                                           | destination                                                    | features                                          | expStats
        "./src/test/resources/mscoco-test-command-out-embedding-exp.xml" | "./src/test/resources/mscoco-test-command-embedding-stats.txt" | "ADD_WORD_EMBEDDING, COSINE_SIMILARITY_EMBEDDING" | "./src/test/resources/mscoco-test-command-embedding-stats-exp.txt"
    }

    def "reelout stats missing options"(final String command, final Boolean exp) {

        given: "An MS-COCO command is run"

        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == exp

        where:
        command                                                                                                             | exp
        "reelout stats --destination 'dfile' --features 'pos'"                                                              | false
        "reelout stats --source './src/test/resources/mscoco-test-command-join-features-out-exp.xml' --features 'pos'"      | false
        "reelout stats --source './src/test/resources/mscoco-test-command-join-features-out-exp.xml' --destination 'dfile'" | false

    }

}