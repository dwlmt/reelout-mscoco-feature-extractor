package uk.ac.ed.reelout.command

import org.springframework.shell.Bootstrap
import org.springframework.shell.core.CommandResult
import org.springframework.shell.core.JLineShellComponent
import spock.lang.Specification
import spock.lang.Unroll

/**
 * Tests the basic utility classes of the domain objects such as equality, hashcode, etc.
 */
@Unroll
class ReeloutRemoveFeaturesCommandTest extends Specification {

    def "Reelout remove"(final String source, final String destination, final String features, final String expXml) {

        given: "A Reelout remove command is run"
        final def command = String.format("reelout remove --source '%s' --destination '%s' --features '%s'",
                source, destination, features)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Removed Reelout features")
        assert new XmlSlurper().parseText(new File(destination).text) == new XmlSlurper().parseText(new File(expXml).text)

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                                          | destination                                               | features        | expXml
        "./src/test/resources/mscoco-test-command-features-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-remove.xml" | "pos,sentiment" | "./src/test/resources/mscoco-test-command-base.xml"
        // removing fetures already empty will not change the file.
        "./src/test/resources/mscoco-test-command-out-exp.xml"          | "./src/test/resources/mscoco-test-command-out-remove.xml" | "pos,sentiment" | "./src/test/resources/mscoco-test-command-base.xml"
    }

    def "Reelout remove filtered by id "(final String source, final String destination, final String features,
                                         final String fromId, final String toId, final String expXml) {

        given: "A Reelout remove command is run"
        final
        def command = String.format("reelout remove --source '%s' --destination '%s' --features '%s' --from_id '%s' --to_id '%s'",
                source, destination, features, fromId, toId)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Removed Reelout features")
        assert new XmlSlurper().parseText(new File(destination).text) == new XmlSlurper().parseText(new File(expXml).text)

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                                        | destination                                               | features        | fromId | toId | expXml
        "./src/test/resources/mscoco-test-command-out-filter-exp.xml" | "./src/test/resources/mscoco-test-command-out-remove.xml" | "pos,sentiment" | "78"   | "84" | "./src/test/resources/mscoco-test-command-base.xml"
    }

    def "Reelout remove invalid command options"(final String source, final String destination, final String features) {

        given: "An Reelout remove command is run"
        final def command = String.format("reelout remove --source '%s' --destination '%s' --features '%s'",
                source, destination, features)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == false

        where:
        source                                                 | destination                                               | features
        "./src/test/resources/mscoco-test-command-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-remove.xml" | ""
        "./src/test/resources/mscoco-test-command-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-remove.xml" | null
        ""                                                     | "./src/test/resources/mscoco-test-command-out-remove.xml" | "pos,sentiment"
        "./src/test/resources/mscoco-test-command-out-exp.xml" | ""                                                        | "pos,sentiment"
        "./src/test/resources/mscoco-test-command-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-remove.xml" | "notavalidfeature"
    }

    def "mscoco convert missing options"(final String command, final Boolean exp) {

        given: "An MS-COCO command is run"

        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == exp

        where:
        command                                                                                                                                                   | exp
        "reelout remove --source './src/test/resources/mscoco-test-command-out-exp.xml' --destination './src/test/resources/mscoco-test-command-out-remove.xml' " | false
        "reelout remove --destination './src/test/resources/mscoco-test-command-out-remove.xml' --features 'pos'"                                                 | false
        "reelout remove --source './src/test/resources/mscoco-test-command-out-exp.xml' --features 'pos'"                                                         | false
    }
}