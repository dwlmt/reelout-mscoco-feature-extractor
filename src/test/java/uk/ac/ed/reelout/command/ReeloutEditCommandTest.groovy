package uk.ac.ed.reelout.command

import org.springframework.shell.Bootstrap
import org.springframework.shell.core.CommandResult
import org.springframework.shell.core.JLineShellComponent
import spock.lang.Specification
import spock.lang.Unroll
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.services.ReeloutConversionService

/**
 * Tests the edit commands.
 */
@Unroll
class ReeloutEditCommandTest extends Specification {

    def "reelout Join"(final String sources, final String destination, final String expXml) {

        given: "A reelout join command is run"
        final def command = String.format("reelout join --sources '%s' --destination '%s'",
                sources, destination)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Databases have been joined successfully")
        assert new XmlSlurper().parseText(new File(destination).text) == new XmlSlurper().parseText(new File(expXml).text)

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        sources                                                                                                               | destination                                             | expXml
        "./src/test/resources/mscoco-test-command-features-out-exp.xml,./src/test/resources/all-features-a-different-key.xml" | "./src/test/resources/mscoco-test-command-out-join.xml" | "./src/test/resources/mscoco-test-command-out-join-exp.xml"
    }

    def "reelout join/split missing parameters"(final String command, final Boolean exp) {

        given: "An Join/Split command is being run."

        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == exp

        where:
        command                                                        | exp
        "reelout join --source './src/test/resources/afile.xml' "      | false
        "reelout join --destination './src/test/resources/bfile.xml' " | false

    }

    def "reelout Cut"(
            final String source, final String destination, final String from, final String to, final String expXml) {

        given: "A reelout cut command is run"
        final def command = String.format("reelout cut --source '%s' --destination '%s' --from_id '%s' --to_id '%s'",
                source, destination, from, to)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Database range has been cut")
        assert new XmlSlurper().parseText(new File(destination).text) == new XmlSlurper().parseText(new File(expXml).text)

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                                          | destination                                            | from     | to       | expXml
        "./src/test/resources/mscoco-test-command-features-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-cut.xml" | "813979" | "816950" | "./src/test/resources/mscoco-test-command-out-cut-exp.xml"
    }

    def "reelout cut missing options"(final String command, final Boolean exp) {

        given: "A reelout cut command is run"

        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == exp

        where:
        command                                                                                                | exp
        "reelout cut --source './src/test/resources/afile.xml' --from_id 'a' --to_id 'a' "                     | false
        "reelout cut --destination './src/test/resources/bfile.xml' --from_id 'a' --to_id 'a'"                 | false
        "reelout cut --source './src/test/resources/afile.xml' --destination './src/test/resources/bfile.xml'" | false
    }

    def "reelout Random"(
            final String source, final String destination, final int selectN) {

        given: "A reelout cut command is run"
        final def command = String.format("reelout random --source '%s' --destination '%s' --select_n '%s'",
                source, destination, selectN)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        final RlDatabase loadedDomain = new ReeloutConversionService().convertToDomain(new File(destination).text)

        expect: "The command is a success. The number of selected units matches selectN"
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Randomly selected RlUnits")
        assert loadedDomain.getRlUnits().size() == selectN

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                                          | destination                                               | selectN
        "./src/test/resources/mscoco-test-command-features-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-random.xml" | 4
        "./src/test/resources/mscoco-test-command-features-out-exp.xml" | "./src/test/resources/mscoco-test-command-out-random.xml" | 1
    }

    def "reelout random invalid parameters"(final String command, final Boolean exp) {

        given: "An Join/Split command is being run."

        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command fails."
        assert cr.isSuccess() == exp

        where:
        command                                                                                                                                                                                | exp
        "reelout random --source './src/test/resources/mscoco-test-command-features-out-exp.xml' --destination './src/test/resources/mscoco-test-command-out-random.xml' --select_n '0' "      | false
        "reelout random --source './src/test/resources/mscoco-test-command-features-out-exp.xml' --destination './src/test/resources/mscoco-test-command-out-random.xml' --select_n '-1' "     | false
        "reelout random --source './src/test/resources/mscoco-test-command-features-out-exp.xml' --destination './src/test/resources/mscoco-test-command-out-random.xml' --select_n '500000' " | false
    }

    def "reelout swap text"(
            final String sourceText,
            final String sourceFeatures,
            final String destination, final String expXml) {

        given: "A reelout swap text command is run"
        final
        def command = String.format("reelout swap text --source_text '%s' --source_features '%s' --destination '%s'",
                sourceText, sourceFeatures, destination)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Reelout message text has been swapped")
        assert new XmlSlurper().parseText(new File(destination).text) == new XmlSlurper().parseText(new File(expXml).text)

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        sourceText                                               | sourceFeatures                                           | destination                                              | expXml
        "./src/test/resources/mscoco-test-command-out-3-exp.xml" | "./src/test/resources/mscoco-test-command-out-2-exp.xml" | "./src/test/resources/mscoco-test-command-swap-text.xml" | "./src/test/resources/mscoco-test-command-swap-text-exp.xml"
    }

    def "reelout filter"(final String command, final String source, final String destination, final Integer expCount) {

        given: "An Spring Boot filter command is run."

        ReeloutConversionService conv = new ReeloutConversionService()

        final String commandToExec = String.format(command, source, destination);

        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(commandToExec);

        expect: "The command runs"
        assert cr.isSuccess() == true
        assert conv.convertToDomainFromFile(destination).getRlUnits().size() == expCount


        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        command                                                                                                                     | source                                            | destination                                 | expCount
        "reelout filter --source '%s' --destination '%s' --ids '374107, 401071' "                                                   | './src/test/resources/selection/reelout_1000.xml' | './src/test/resources/selection/filter.xml' | 2
        "reelout filter --source '%s' --destination '%s' --message_text 'Elephants, Giraffe' "                                      | './src/test/resources/selection/reelout_1000.xml' | './src/test/resources/selection/filter.xml' | 36
        "reelout filter --source '%s' --destination '%s' --themes 'Tennis' --theme_threshold '0.6' "                                | './src/test/resources/selection/reelout_1000.xml' | './src/test/resources/selection/filter.xml' | 32
        "reelout filter --source '%s' --destination '%s' --locations 'London' "                                                     | './src/test/resources/selection/reelout_1000.xml' | './src/test/resources/selection/filter.xml' | 0
        "reelout filter --source '%s' --destination '%s' --entities 'Football' "                                                    | './src/test/resources/selection/reelout_1000.xml' | './src/test/resources/selection/filter.xml' | 2
        "reelout filter --source '%s' --destination '%s' --sentiments 'positive' "                                                  | './src/test/resources/selection/reelout_1000.xml' | './src/test/resources/selection/filter.xml' | 55
        "reelout filter --source '%s' --destination '%s' --subject 'clock' "                                                        | './src/test/resources/relation-triple-export.xml' | './src/test/resources/selection/filter.xml' | 1
        "reelout filter --source '%s' --destination '%s' --verb 'standing' "                                                        | './src/test/resources/relation-triple-export.xml' | './src/test/resources/selection/filter.xml' | 1
        "reelout filter --source '%s' --destination '%s' --object 'chairs,xyzk' "                                                        | './src/test/resources/relation-triple-export.xml' | './src/test/resources/selection/filter.xml' | 1
        "reelout filter --source '%s' --destination '%s' --verb 'standing' --subject 'man' --object 'booth' --whole_triple 'true' " | './src/test/resources/relation-triple-export.xml' | './src/test/resources/selection/filter.xml' | 1

    }

}