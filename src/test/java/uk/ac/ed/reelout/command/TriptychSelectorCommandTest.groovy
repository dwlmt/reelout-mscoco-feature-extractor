package uk.ac.ed.reelout.command

import org.springframework.shell.Bootstrap
import org.springframework.shell.core.CommandResult
import org.springframework.shell.core.JLineShellComponent
import spock.lang.Ignore
import spock.lang.Specification

/**
 * Tests the basic utility classes of the domain objects such as equality, hashcode, etc.
 */
class TriptychSelectorCommandTest extends Specification {

    def "triptych random"(final String source, final String destination, final numToGenerate) {

        given: "Tryptych selection command is run"
        final def command = String.format("triptych random --source '%s' --destination '%s' --number_to_generate '%s' ",
                source, destination, numToGenerate)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Triptychs generated successfully")
        assert new File(destination).text.size() > 0 // Just check a files is written, not the contents.

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                            | destination                                               | numToGenerate
        "./src/test/resources/selection/reelout_1000.xml" | "./src/test/resources/selection/reelout_random_1000.html" | 50
    }

    def "triptych nearest"(final String source, final String destination, final numToGenerate) {

        given: "Tryptych selection command is run"
        final
        def command = String.format("triptych nearest --source '%s' --destination '%s' --number_to_generate '%s' ",
                source, destination, numToGenerate)
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();
        final CommandResult cr = shell.executeCommand(command);

        expect: "The command is a success."
        assert cr.isSuccess() == true
        assert cr.getResult().toString().startsWith("Triptychs generated successfully")
        assert new File(destination).text.size() > 0 // Just check a files is written, not the contents.

        cleanup: "Delete exported files."
        new File(destination).delete()

        where:
        source                                            | destination                                                 | numToGenerate
        "./src/test/resources/selection/reelout_1000.xml" | "./src/test/resources/selection/reelout__nearest_1000.html" | 50
    }

    @Ignore  // Ignore test as GLPK isn't setup to run on Gitlab Ci.
    def "triptych with filtering" (String command, final String source, final String destination, final numToGenerate) {

        given: "Tryptych selection command is run"
        final Bootstrap bootstrap = new Bootstrap();
        final JLineShellComponent shell = bootstrap.getJLineShellComponent();


        def options = ["--message_text 'dog' --weight_text '1.0' --weight_image '1.0' ",
                       "--message_text 'dog' --weight_text '-1.0' --weight_image '-1.0' ",
                       "--message_text 'dog' --weight_text '1.0' --weight_image '1.0' --max_similarity_image '0.5' ",
                       "--message_text 'dog' --weight_text '1.0' --weight_image '1.0' --max_similarity_text '0.5' ",
                       "--message_text 'dog' --1_sentiments 'negative' --2_sentiments 'neutral' --3_sentiments 'positive' --weight_text '1.0' --weight_image '-1.0' ",
                       "--message_text 'dog' --type 'random' ",
                       "--sentence 'A dog chasing a frisbee into the water. ' --weight_text '1.0' --weight_image '-1.0' "
        ]
        int counter = 1;
        for(def option: options) {
            def destinationToWriteTo = String.format(destination,counter)
            def commandToExecute = String.format(command,
                    source, destinationToWriteTo, numToGenerate, option)
            final CommandResult cr = shell.executeCommand(commandToExecute);
            counter++;
        }

        expect: "All of the different parameters produce different sets of images."
        for(int i = 1; i < counter; i++) {
            for(int j = 1; j < counter; j++) {
                if (i != j) {
                    assert new File(String.format(destination, i)).text != new File(String.format(destination, j)).text;
                }
            }
        }

        cleanup: "Delete exported files."
        for (int i = 1; i < counter; i++) {
            new File(String.format(destination)).delete()
        }

        where:
        command                                                                                                                                      | source                                           | destination                                    | numToGenerate
        "triptych filter --source '%s' --word_embeddings_file './data/embeddings/glove.6B.50d.txt' --prediction_feature_directory //afs/inf.ed.ac.uk/group/project/reellives/data/David/predictions/ " +
                "--image_feature_directory //afs/inf.ed.ac.uk/group/project/reellives/data/David/image_embeddings/ --destination '%s' " +
                "--number_to_generate '%s' %s "                                                                                                      | "./data/reelout/reelout_all_features_sample.xml" | "./src/test/resources/selection/output%s.html" | 10
    }
}