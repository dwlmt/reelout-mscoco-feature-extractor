package uk.ac.ed.reelout.features

import com.google.common.util.concurrent.AtomicDouble
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import uk.ac.ed.reelout.domain.*

import java.util.concurrent.ConcurrentSkipListMap

import static java.lang.String.valueOf

/**
 * Tests the theming from the Alchemy API.
 */
@ContextConfiguration(locations = "classpath:META-INF/spring/spring-shell-plugin.xml")
class NamedEntityFeatureProcessorTest extends Specification {

    @Autowired
    NamedEntityFeatureProcessor proc


    def "NamedEntity Test"(final String messageText, final List<Location> locations, final List<Entity> entities) {


        given: "The named entities match those expected."
        RlUnit rl = new RlUnit()
        rl.appendMessageText(messageText);
        rl = proc.createFeature(rl, null)

        expect: "Named entities and locations are correct."
        assert rl.getLocations() == locations
        assert rl.getEntities() == entities



        where:
        messageText                                                                    || locations                                                                                                                                                       || entities
        "Driving from Edinburgh in Scotland to London to visit the Royal Opera House." || [new Location(["City", "Edinburgh"]), new Location(["Country", "Scotland"]), new Location(["City", "London"]), new Location(["Facility", "Royal Opera House"])] || []
        "Barack Obama visited Google today"                                            || []                                                                                                                                                              || [new Entity(["Person", "Barack Obama"]), new Entity(["Company", "Google"])]

    }

    def "Named Entity statistics"() {


        when: "POS tagging is performed."
        final def messages = ["President Obama is visiting London.", "A train travels from London to the coast."]
        final def expCounts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        expCounts[new ReeloutStat(FeatureType.ENTITY.getName(), "entity", "JobTitle/President")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ENTITY.getName(), "entity", "Person/Obama")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ENTITY.getName(), "location", "City/London")] = new AtomicDouble(2)

        RlDatabase db = new RlDatabase(0.2)
        def counter = 1
        for (final def m in messages) {
            def r = new RlUnit(valueOf(counter))
            r.appendMessageText(m)
            db.addRlUnit(r)
            counter++
        }
        db = proc.createFeature(db, null);
        final NavigableMap<ReeloutStat, AtomicDouble> counts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        proc.countFeatures(db, counts, null, null, [:])


        then: "The counts are correct"
        // AtomicDouble causes problems with equality check.
        assert counts.toString() == expCounts.toString()

    }
}