package uk.ac.ed.reelout.features

import spock.lang.Specification
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit

/**
 * Tests the Generic Feature parent can will apply the processor to each RlUnit.
 */
class GenericFeatureProcessorTest extends Specification {


    def "Apply Sentiment to multiple RLUnits."() {

        given: "A sentiment feature processor."
        final SentimentFeatureProcessor proc = new SentimentFeatureProcessor();

        when: "Multiple RLUnits are provided."
        final RlDatabase db = new RlDatabase(0.2)

        final def r1 = new RlUnit("1")
        r1.setMessageText("happy")
        db.addRlUnit(r1)

        final def r2 = new RlUnit("2")
        r2.setMessageText("sad")
        db.addRlUnit(r2)

        final def r3 = new RlUnit("3")
        r3.setMessageText("okay")
        db.addRlUnit(r3)

        final def resDb = proc.createFeature(db, null);

        then: "Each RLUnit is processed, Sentiment is set."
        for (final RlUnit rl : resDb.getRlUnits()) {
            assert rl.getSentiment() != null
        }
    }

    def "No filtering "() {

        given: "A set of RlUnits with ids 0 - 9"
        // Setup ids
        final RlDatabase db = new RlDatabase(0.2)
        (0..9).each {
            final def rl = new RlUnit("${it}")
            rl.setMessageText("${it}")
            db.addRlUnit(rl);
        }

        when: "There is no filter"
        final PosFeatureProcessor proc = new PosFeatureProcessor();
        final def all = proc.createFeature(db, null);

        then: "POS is applied to all RlUnits."
        all.getRlUnits().each {
            assert it.getTags().size() == 1
        }

    }

    def "Filtering the range"() {

        given: "A set of RlUnits with ids 0 - 9"
        // Setup ids
        final RlDatabase db = new RlDatabase(0.2)
        (0..9).each {
            final def rl = new RlUnit("${it}")
            rl.setMessageText("${it}")
            db.addRlUnit(rl);
        }

        when: "There is a range filter."
        final PosFeatureProcessor proc = new PosFeatureProcessor();
        final def all = proc.createFeature(db, "3", "6", null);

        then: "POS is only applied to the matching range."
        all.getRlUnits().each {
            if (it.getId() in ["3", "4", "5"]) {
                assert it.getTags().size() == 1
            } else {
                assert it.getTags().size() == 0
            }
        }
    }

    def "Filtering the tail - only fromId set"() {

        given: "A set of RlUnits with ids 0 - 9"
        // Setup ids
        final RlDatabase db = new RlDatabase(0.2)
        (0..9).each {
            final def rl = new RlUnit("${it}")
            rl.setMessageText("${it}")
            db.addRlUnit(rl);
        }

        when: "There is from filter."
        final PosFeatureProcessor proc = new PosFeatureProcessor();
        final def all = proc.createFeature(db, "8", null, null);

        then: "POS is only applied to the matching range."
        all.getRlUnits().each {
            if (it.getId() in ["8", "9"]) {
                assert it.getTags().size() == 1
            } else {
                assert it.getTags().size() == 0
            }
        }
    }

    def "Filtering the head - only toId set "() {

        given: "A set of RlUnits with ids 0 - 9"
        // Setup ids
        final RlDatabase db = new RlDatabase(0.2)
        (0..9).each {
            final def rl = new RlUnit("${it}")
            rl.setMessageText("${it}")
            db.addRlUnit(rl);
        }

        when: "There is a range filter."
        final PosFeatureProcessor proc = new PosFeatureProcessor();
        final def all = proc.createFeature(db, null, "4", null);

        then: "POS is only applied to the matching range."
        all.getRlUnits().each {
            if (it.getId() in ["0", "1", "2", "3"]) {
                assert it.getTags().size() == 1
            } else {
                assert it.getTags().size() == 0
            }
        }
    }
}