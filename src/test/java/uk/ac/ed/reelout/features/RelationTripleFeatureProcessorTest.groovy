package uk.ac.ed.reelout.features

import com.google.common.util.concurrent.AtomicDouble
import spock.lang.Specification
import uk.ac.ed.reelout.domain.ReeloutStat
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlRelationTriple
import uk.ac.ed.reelout.domain.RlUnit

import java.util.concurrent.ConcurrentSkipListMap

import static java.lang.String.valueOf
import static uk.ac.ed.reelout.features.FeatureType.RELATION_TRIPLE

/**
 * Tests the Stanford CoreNLP based feature processor.
 */
class RelationTripleFeatureProcessorTest extends Specification {


    def "Open IE Triples Test"(final String messageText, final Set expTriples) {

        given: "A Open IS triple feature processor."
        final RelationTripleFeatureProcessor proc = new RelationTripleFeatureProcessor();

        expect: "The triples match that expected from the message text."
        final RlUnit rl = new RlUnit()
        rl.appendMessageText(messageText);
        assert proc.createFeature(rl, null).getRelationTriples() == expTriples

        where:
        messageText                                                                                                                                                || expTriples
        "A person that is surfing in the ocean by a wave. a person riding a surf board on a wave"                                                                  || [new RlRelationTriple(1.0, ["person"], ["riding"], ["surf", "board"]), new RlRelationTriple(1.0, ["person"], ["riding", "surf", "board", "on"], ["wave"])]
        "A dimly light bedroom with a bed and four pillows. A dimly light bedroom with a bed and four pillows. A dimly light bedroom with a bed and four pillows." || [new RlRelationTriple(1.0, ["light", "bedroom"], ["is", "with"], ["bed"])]
    }

    def "Counts Open IE Triple statistics"() {

        given: "A Open IS triple feature processor."
        final RelationTripleFeatureProcessor proc = new RelationTripleFeatureProcessor();

        when: "Open IE extraction is performed."
        final def messages = ["A white and pink kite flying through a blue cloud filled sky."]
        final def expCounts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "object", "blue")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "object", "cloud")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "object-concat", "blue cloud")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "object-concat", "cloud")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "relation", "flying")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "relation", "through")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "relation-concat", "flying through")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "subject", "kite")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "subject", "white")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "subject-concat", "kite")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "subject-concat", "white kite")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(RELATION_TRIPLE.getName(), "triple-concat", "kite : flying through : blue cloud")] = new AtomicDouble(1)


        RlDatabase db = new RlDatabase(0.2)
        def counter = 1
        for (final def m in messages) {
            def r = new RlUnit(valueOf(counter))
            r.appendMessageText(m)
            db.addRlUnit(r)
            counter++
        }
        db = proc.createFeature(db, null);
        final NavigableMap<ReeloutStat, AtomicDouble> counts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        proc.countFeatures(db, counts, null, null, [:])


        then: "The counts are correct"
        for (stat in expCounts) {
            assert stat.value.doubleValue() == counts[stat.key].doubleValue()
        }

    }
}