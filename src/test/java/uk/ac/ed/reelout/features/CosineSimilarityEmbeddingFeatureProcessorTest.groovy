package uk.ac.ed.reelout.features

import com.google.common.util.concurrent.AtomicDouble
import spock.lang.Specification
import uk.ac.ed.reelout.domain.ReeloutStat
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit

import java.util.concurrent.ConcurrentSkipListMap

import static java.lang.String.valueOf

/**
 * Tests cosine similarity statistics are calculated correctly.
 */
class CosineSimilarityEmbeddingFeatureProcessorTest extends Specification {

    def "Cosine Similarity embedding processor"() {

        when: "Embedding are calculated"
        final def messages = ["A row of boats next to each other in the water.",
                              "three identical boats next to each other one has a blue curtain",
                              "A set of three boats floating on water next to each other.",
                              "Three boats that are just alike sitting in the water beside each other.",
                              "Three white boats lined up next to each other in water."]

        // Options
        final Map<String, Object> options = new HashMap<String, Object>()
        options["embeddingClosestN"] = 2
        options["embeddingsFile"] = "./data/embeddings/glove.6B.50d.txt" // Use the smallest embeddings for the test.
        options["embeddingsType"] = "glove";

        // Processors.
        final WholeTextAddEmbeddingFeatureProcessor wholeText = new WholeTextAddEmbeddingFeatureProcessor();
        final CosineSimilarityEmbeddingFeatureProcessor proc = new CosineSimilarityEmbeddingFeatureProcessor();
        final PosFeatureProcessor pos = new PosFeatureProcessor()

        // Setup the RLUnits.
        RlDatabase db = new RlDatabase(0.2)
        def counter = 1
        for (final def m in messages) {
            def r = new RlUnit(valueOf(counter))
            r.appendMessageText(m)
            db.addRlUnit(r)
            counter++
        }

        // Create features.
        wholeText.setup(options) // Load embeddings
        db = pos.createFeature(db, options);
        db = wholeText.createFeature(db, options);

        // Count statistics for embedding Cosine similarity.
        db = proc.createFeature(db, null, null, options)


        final def TOLERANCE = 0.001 // How close to similarities have to be to match.


        def firstSimilarity = db.getRlUnits().first().getSimilarityMeasures().first();

        def lastSimilarity = db.getRlUnits().last().getSimilarityMeasures().first();

        then: "Similarity is as expected."
        // Just check the first RlUnit/
        firstSimilarity.getType() == FeatureType.COSINE_SIMILARITY_EMBEDDING.getName()
        assert Math.abs(firstSimilarity.getMean() - 0.9368) < TOLERANCE
        assert Math.abs(firstSimilarity.getVariance() - 0.00135) < TOLERANCE

        assert firstSimilarity.getSimilarities().get(0).getId() == "3"
        assert firstSimilarity.getSimilarities().get(1).getId() == "5"

        assert Math.abs(firstSimilarity.getSimilarities().get(0).getValue() - 0.9790) < TOLERANCE;
        assert Math.abs(firstSimilarity.getSimilarities().get(1).getValue() - 0.9535) < TOLERANCE;

        lastSimilarity.getType() == FeatureType.COSINE_SIMILARITY_EMBEDDING.getName()
        assert Math.abs(lastSimilarity.getMean() - 0.9508) < TOLERANCE;
        assert Math.abs(lastSimilarity.getVariance() - 5.642072210321203E-5) < TOLERANCE
        assert lastSimilarity.getSimilarities().get(0).getId() == "3"
        assert lastSimilarity.getSimilarities().get(1).getId() == "1"

        assert Math.abs(lastSimilarity.getSimilarities().get(0).getValue() - 0.9607) < TOLERANCE;
        assert Math.abs(lastSimilarity.getSimilarities().get(1).getValue() - 0.9535) < TOLERANCE;

    }


    def "Count embedding with nearest statistics"() {

        when: "Embedding are calculated"
        final def messages = ["A row of boats next to each other in the water.",
                              "three identical boats next to each other one has a blue curtain",
                              "A set of three boats floating on water next to each other.",
                              "Three boats that are just alike sitting in the water beside each other.",
                              "Three white boats lined up next to each other in water."]
        final def expCounts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()

        expCounts[new ReeloutStat(FeatureType.COSINE_SIMILARITY_EMBEDDING.getName(), FeatureType.COSINE_SIMILARITY_EMBEDDING.getName(),
                "mean")] = new AtomicDouble(0.9790180325508118)
        expCounts[new ReeloutStat(FeatureType.COSINE_SIMILARITY_EMBEDDING.getName(), FeatureType.COSINE_SIMILARITY_EMBEDDING.getName(),
                "variance")] = new AtomicDouble(4.16685885284096E-4)

        // Options
        final Map<String, Object> options = new HashMap<String, Object>()
        options["embeddingClosestN"] = 2
        options["embeddingsFile"] = "./data/embeddings/glove.6B.50d.txt" // Use the smallest embeddings for the test.
        options["embeddingsType"] = "glove";

        // Processors.
        final WholeTextAddEmbeddingFeatureProcessor wholeText = new WholeTextAddEmbeddingFeatureProcessor();
        final CosineSimilarityEmbeddingFeatureProcessor proc = new CosineSimilarityEmbeddingFeatureProcessor();
        final PosFeatureProcessor pos = new PosFeatureProcessor()

        // Setup the RLUnits.
        RlDatabase db = new RlDatabase(0.2)
        def counter = 1
        for (final def m in messages) {
            def r = new RlUnit(valueOf(counter))
            r.appendMessageText(m)
            db.addRlUnit(r)
            counter++
        }

        // Create features.
        wholeText.setup(options) // Load embeddings
        db = pos.createFeature(db, options);
        db = wholeText.createFeature(db, options);

        // Count statistics for embedding Cosine similarity.
        final NavigableMap<ReeloutStat, AtomicDouble> counts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        proc.createFeature(db, null, null, options)
        proc.countFeatures(db, counts, null, null, options)


        final def TOLERANCE = 0.00001 // How close to similarities have to be to match.


        then: "The counts are correct"
        // AtomicDouble causes problems with equality check.
        assert counts.keySet() == expCounts.keySet()
        for (final ReeloutStat key : counts.keySet()) {

            assert Math.abs(counts[key] - expCounts[key]) < TOLERANCE;
        }

    }
}