package uk.ac.ed.reelout.features

import com.google.common.util.concurrent.AtomicDouble
import spock.lang.Specification
import uk.ac.ed.reelout.domain.Embedding
import uk.ac.ed.reelout.domain.ReeloutStat
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit

import java.util.concurrent.ConcurrentSkipListMap

import static java.lang.String.valueOf

/**
 * Tests that whole text word embeddings can be added to RLUnits.
 */
class WholeTextAddEmbeddingFeatureProcessorTest extends Specification {


    def "Wholetext embedding processor"(
            final String messageText, final Boolean removeStopWords, final List<Embedding> expEmbeddings) {

        given: "An Embedding feature processor."
        final WholeTextAddEmbeddingFeatureProcessor proc = new WholeTextAddEmbeddingFeatureProcessor();
        final PosFeatureProcessor pos = new PosFeatureProcessor()
        RlUnit rl = new RlUnit()
        rl.appendMessageText(messageText);
        rl = pos.createFeature(rl, null);
        final Map<String, Object> options = new HashMap<String, Object>()
        options["embeddingsFile"] = "./data/embeddings/glove.6B.50d.txt" // Use the smallest embeddings for the test.
        options["embeddingsType"] = "glove";
        options["removeStopWords"] = removeStopWords
        proc.setup(options)
        rl = proc.createFeature(rl, options);

        expect: "The embeddings match expectations"
        assert rl.getEmbeddings() == expEmbeddings
        // Don't check all the values. Just that it isn't zero.
        assert rl.getEmbeddings().first().getVector() != expEmbeddings.first().getVector()

        where:
        messageText                                                       | removeStopWords || expEmbeddings
        "One zebra stands guard while the other eats on the savannah. Two zebras standing in tall grass near brush. " +
                "two zebras are eating and standing in some tall grass Two zebras are grazing in tall grass on a savannah. " +
                "One zebra is grazing while another watches from behind." | true             |
                [Embedding.createEmbedding("another behind brush eating eats grass grazing guard near one savannah " +
                        "standing stands tall two watches zebra zebras",
                        "glove", "addition", "text", new ArrayList<Double>(), 50)]
        "One zebra stands guard while the other eats on the savannah. Two zebras standing in tall grass near brush. " +
                "two zebras are eating and standing in some tall grass Two zebras are grazing in tall grass on a savannah. " +
                "One zebra is grazing while another watches from behind." | false            |
                [Embedding.createEmbedding(". a and another are behind brush eating eats from grass grazing guard in " +
                        "is near on one other savannah some standing stands tall the two watches while zebra zebras",
                        "glove", "addition", "text", new ArrayList<Double>(), 50)]

    }

    def "Count embedding statistics"() {

        when: "Embedding are calculated"
        final def messages = ["A row of boats next to each other in the water.",
                              "three identical boats next to each other one has a blue curtain",
                              "A set of three boats floating on water next to each other.",
                              "Three boats that are just alike sitting in the water beside each other.",
                              "Three white boats lined up next to each other in water."]
        final def expCounts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()

        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "length", "4")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "length", "6")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "length", "7")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "alike")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "beside")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "blue")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "boats")] = new AtomicDouble(5)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "curtain")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "floating")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "identical")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "just")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "lined")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "next")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "one")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "row")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "set")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "sitting")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "three")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "water")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "white")] = new AtomicDouble(1)


        final WholeTextAddEmbeddingFeatureProcessor proc = new WholeTextAddEmbeddingFeatureProcessor();
        final PosFeatureProcessor pos = new PosFeatureProcessor()
        RlDatabase db = new RlDatabase(0.2)
        def counter = 1
        for (final def m in messages) {
            def r = new RlUnit(valueOf(counter))
            r.appendMessageText(m)
            db.addRlUnit(r)
            counter++
        }
        final Map<String, Object> options = new HashMap<String, Object>()
        options["embeddingsFile"] = "./data/embeddings/glove.6B.50d.txt" // Use the smallest embeddings for the test.
        options["embeddingsType"] = "glove";

        db = pos.createFeature(db, options);
        db = proc.createFeature(db, options);

        final NavigableMap<ReeloutStat, AtomicDouble> counts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        proc.countFeatures(db, counts, null, null, [:])


        then: "The counts are correct"
        // AtomicDouble causes problems with equality check.
        assert counts.toString() == expCounts.toString()

    }

    def "Count embedding with nearest statistics"() {

        when: "Embedding are calculated"
        final def messages = ["A row of boats next to each other in the water.",
                              "three identical boats next to each other one has a blue curtain",
                              "A set of three boats floating on water next to each other.",
                              "Three boats that are just alike sitting in the water beside each other.",
                              "Three white boats lined up next to each other in water."]
        final def expCounts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()

        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "length", "4")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "length", "6")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "length", "7")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "alike")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "beside")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "blue")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "boats")] = new AtomicDouble(5)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "curtain")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "floating")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "identical")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "just")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "lined")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "next")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "one")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "row")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "set")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "sitting")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "three")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "water")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word", "white")] = new AtomicDouble(1)


        final WholeTextAddEmbeddingFeatureProcessor proc = new WholeTextAddEmbeddingFeatureProcessor();
        final PosFeatureProcessor pos = new PosFeatureProcessor()
        RlDatabase db = new RlDatabase(0.2)
        def counter = 1
        for (final def m in messages) {
            def r = new RlUnit(valueOf(counter))
            r.appendMessageText(m)
            db.addRlUnit(r)
            counter++
        }
        final Map<String, Object> options = new HashMap<String, Object>()
        options["embeddingsFile"] = "./data/embeddings/glove.6B.50d.txt" // Use the smallest embeddings for the test.
        options["embeddingsType"] = "glove";
        options["findClosestEmbedding"] = true
        options["embeddingClosestN"] = 2

        db = pos.createFeature(db, options);
        db = proc.createFeature(db, options);

        final NavigableMap<ReeloutStat, AtomicDouble> counts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        proc.countFeatures(db, counts, null, null, options)


        then: "The counts are correct"
        // AtomicDouble causes problems with equality check.
        assert counts.toString() == expCounts.toString()

    }
}