package uk.ac.ed.reelout.features

import com.google.common.util.concurrent.AtomicDouble
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import uk.ac.ed.reelout.domain.ReeloutStat
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit

import java.util.concurrent.ConcurrentSkipListMap

import static java.lang.String.valueOf
import static uk.ac.ed.reelout.features.FeatureType.POS

/**
 * Tests the theming from the Alchemy API.
 */
@ContextConfiguration(locations = "classpath:META-INF/spring/spring-shell-plugin.xml")
class PosFeatureProcessorTest extends Specification {

    @Autowired
    PosFeatureProcessor proc


    def "Part of Speech Test"(final String messageText, final List<String> tags, final List<String> tokens) {

        given: "POS tagging is performed."
        RlUnit rl = new RlUnit()
        rl.appendMessageText(messageText)
        rl = proc.createFeature(rl, null)
        final def actTags = rl.getTags()
        final def actTokens = rl.getTokens()

        expect: "Word tokens and pos tags match expectations."
        assert actTags == tags
        assert actTokens == tokens

        where:
        messageText                                         || tags                                                           || tokens
        "Two Elephants are playing in water near a forest." || ["CD", "NNS", "VBP", "VBG", "IN", "NN", "IN", "DT", "NN", "."] || ["Two", "Elephants", "are", "playing", "in", "water", "near", "a", "forest", "."]
        "The Cat. The Dog."                                 || ["DT", "NN", ".", "DT", "NN", "."]                             || ["The", "Cat", ".", "The", "Dog", "."]
    }


    def "Counts POS tags"() {

        when: "POS tagging is performed."
        final
        def messages = ["One", "One", "One", "Two Two", "Three Three Three", "Four Four Four Four", "Four Four Four Four", "A B C C"]
        final def expCounts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        expCounts[new ReeloutStat(POS.getName(), "tag", "CD")] = new AtomicDouble(16)
        expCounts[new ReeloutStat(POS.getName(), "tag", "DT")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "tag", "NN")] = new AtomicDouble(3)
        expCounts[new ReeloutStat(POS.getName(), "textlength", valueOf(1))] = new AtomicDouble(3)
        expCounts[new ReeloutStat(POS.getName(), "textlength", valueOf(2))] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "textlength", valueOf(3))] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "textlength", valueOf(4))] = new AtomicDouble(3)
        expCounts[new ReeloutStat(POS.getName(), "unigram", "One")] = new AtomicDouble(3)
        expCounts[new ReeloutStat(POS.getName(), "unigram", "Two")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(POS.getName(), "unigram", "Three")] = new AtomicDouble(3)
        expCounts[new ReeloutStat(POS.getName(), "unigram", "Four")] = new AtomicDouble(8)
        expCounts[new ReeloutStat(POS.getName(), "unigram", "A")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "unigram", "B")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "unigram", "C")] = new AtomicDouble(2)
        // Just test a couple of the bigram and trigrams.
        expCounts[new ReeloutStat(POS.getName(), "bigram", "A B")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "bigram", "B C")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "trigram", "A B C")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "trigram", "B C C")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "coocurrence", "A B")] = new AtomicDouble(1)
        expCounts[new ReeloutStat(POS.getName(), "coocurrence", "A C")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(POS.getName(), "coocurrence", "B C")] = new AtomicDouble(2)

        RlDatabase db = new RlDatabase(0.2)
        def counter = 1
        for (final def m in messages) {
            def r = new RlUnit(valueOf(counter))
            r.appendMessageText(m)
            db.addRlUnit(r)
            counter++
        }
        db = proc.createFeature(db, null);
        final NavigableMap<ReeloutStat, AtomicDouble> counts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        proc.countFeatures(db, counts, null, null, [:])


        then: "The counts are correct"
        // AtomicDouble causes problems with equality check.
        for (stat in expCounts) {
            assert stat.value.doubleValue() == counts[stat.key].doubleValue()
        }

    }


}