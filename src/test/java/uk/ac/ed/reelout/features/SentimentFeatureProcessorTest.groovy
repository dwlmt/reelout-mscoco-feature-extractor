package uk.ac.ed.reelout.features

import com.google.common.util.concurrent.AtomicDouble
import spock.lang.Specification
import uk.ac.ed.reelout.domain.ReeloutStat
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit
import uk.ac.ed.reelout.domain.Sentiment

import java.util.concurrent.ConcurrentSkipListMap

import static java.lang.String.valueOf
import static uk.ac.ed.reelout.features.FeatureType.SENTIMENT

/**
 * Tests the Stanford CoreNLP based feature processor.
 */
class SentimentFeatureProcessorTest extends Specification {


    def "Sentiment Test"(final String messageText, final Sentiment sentiment) {

        given: "A sentiment feature processor."
        final SentimentFeatureProcessor proc = new SentimentFeatureProcessor();

        expect: "The sentiment matches that expected from the message text."
        final RlUnit rl = new RlUnit()
        rl.appendMessageText(messageText);
        proc.createFeature(rl, null).getSentiment().getSentiment() == sentiment


        where:
        messageText                                                                                                                                                                                                                                                                         || sentiment
        "I really love watching the Elephants play in the water, they look great."                                                                                                                                                                                                          || Sentiment.positive
        "This is a really badly taken photo."                                                                                                                                                                                                                                               || Sentiment.negative
        "A neutral sentence."                                                                                                                                                                                                                                                               || Sentiment.neutral
        "A great sentences. A good one. A quite good one."                                                                                                                                                                                                                                  || Sentiment.positive
        "An awful sentence. A bad one."                                                                                                                                                                                                                                                     || Sentiment.negative
        "neutral. neutral. neutral"                                                                                                                                                                                                                                                         || Sentiment.neutral
        "A flower in a vase and other plants in a closed in porch. Flower pots sitting on a table near two large windows. A blue vase with a single yellow flowers in it. A long stem yellow rose set in a tall blue vase one yellow flower with a long stem inside of a blue curved vase." || Sentiment.neutral
    }

    def "Counts Sentiment statistics"() {

        given: "A sentiment feature processor."
        final SentimentFeatureProcessor proc = new SentimentFeatureProcessor();

        when: "Sentiment analysis is performed"
        final def messages = ["Happy", "Happy", "Happy", "Happy", "Okay", "Okay", "Terrible", "Terrible", "Terrible"]
        final def expCounts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        expCounts[new ReeloutStat(SENTIMENT.getName(), "", "positive")] = new AtomicDouble(4)
        expCounts[new ReeloutStat(SENTIMENT.getName(), "", "neutral")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(SENTIMENT.getName(), "", "negative")] = new AtomicDouble(3)


        RlDatabase db = new RlDatabase(0.2)
        def counter = 1
        for (final def m in messages) {
            def r = new RlUnit(valueOf(counter))
            r.appendMessageText(m)
            db.addRlUnit(r)
            counter++
        }
        db = proc.createFeature(db, null);
        final NavigableMap<ReeloutStat, AtomicDouble> counts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        proc.countFeatures(db, counts, null, null, [:])


        then: "The counts are correct"
        // AtomicDouble causes problems with equality check.
        assert counts.toString() == expCounts.toString()

    }
}