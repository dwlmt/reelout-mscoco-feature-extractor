package uk.ac.ed.reelout.features

import com.google.common.util.concurrent.AtomicDouble
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import uk.ac.ed.reelout.domain.ReeloutStat
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit
import uk.ac.ed.reelout.domain.Theme

import java.util.concurrent.ConcurrentSkipListMap

import static java.lang.String.valueOf

/**
 * Tests the theming from the Alchemy API.
 */
@ContextConfiguration(locations = "classpath:META-INF/spring/spring-shell-plugin.xml")
class ThemeFeatureProcessorTest extends Specification {

    @Autowired
    ThemeFeatureProcessor proc


    def "Theming Test"(final String messageText, final List<Theme> themes) {


        expect: "The themes match those expected."
        final RlUnit rl = new RlUnit()
        rl.appendMessageText(messageText);
        assert proc.createFeature(rl, null).getThemes() == themes

        where:
        messageText                                                                || themes
        "I really love watching the Elephants play in the water, they look great." || [new Theme(0.99139, ["pets", "large animals"]), new Theme(0.0868709, ["hobbies and interests", "birdwatching"]), new Theme(0.0247926, ["pets", "zoo"])]
        "Two women cooking on stove in a kitchen together."                        || [new Theme(0.895109, ["home and garden", "appliances", "stoves"]), new Theme(0.240657, ["food and drink"]), new Theme(0.195943, ["home and garden", "appliances"])]
    }

    def "Counts Theme statistics"() {


        when: "Theming is performed"
        final
        def messages = ["A white and pink kite flying through a blue cloud filled sky.", "A white and pink kite flying through a blue cloud filled sky."]
        final def expCounts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        expCounts[new ReeloutStat(FeatureType.THEME.getName(), "", "art and entertainment/books and literature")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(FeatureType.THEME.getName(), "", "sports/surfing and bodyboarding")] = new AtomicDouble(2)
        expCounts[new ReeloutStat(FeatureType.THEME.getName(), "", "technology and computing")] = new AtomicDouble(2)

        RlDatabase db = new RlDatabase(0.2)
        def counter = 1
        for (final def m in messages) {
            def r = new RlUnit(valueOf(counter))
            r.appendMessageText(m)
            db.addRlUnit(r)
            counter++
        }
        db = proc.createFeature(db, null);
        final NavigableMap<ReeloutStat, AtomicDouble> counts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>()
        proc.countFeatures(db, counts, null, null, [:])


        then: "The counts are correct"
        // AtomicDouble causes problems with equality check.
        assert counts.toString() == expCounts.toString()

    }
}