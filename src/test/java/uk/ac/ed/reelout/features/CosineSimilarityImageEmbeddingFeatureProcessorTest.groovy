package uk.ac.ed.reelout.features

import spock.lang.Specification
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit
import uk.ac.ed.reelout.repository.ImageFeatureRepository

/**
 * Tests cosine similarity statistics are calculated correctly.
 */
class CosineSimilarityImageEmbeddingFeatureProcessorTest extends Specification {

    def "Cosine Similarity embedding processor"() {

        when: "Embedding are calculated"
        final def ids = ["203744","257149","287003","292030","537372","551931","569645","581409"]

        // Options
        final Map<String, Object> options = new HashMap<String, Object>()
        options["embeddingClosestN"] = 2
        options["imageFeatureDirectory"] = "./src/test/resources/image_embeddings/"

        final CosineSimilarityImageEmbeddingFeatureProcessor proc = new CosineSimilarityImageEmbeddingFeatureProcessor(new ImageFeatureRepository());

        // Setup the RLUnits.
        RlDatabase db = new RlDatabase(0.2)
        for (final def id in ids) {
            def r = new RlUnit(id)
            db.addRlUnit(r)
        }

        // Count statistics for embedding Cosine similarity.
        db = proc.createFeature(db, null, null, options)


        final def TOLERANCE = 0.001 // How close to similarities have to be to match.


        def firstSimilarity = db.getRlUnits().first().getSimilarityMeasures().first();
        def lastSimilarity = db.getRlUnits().last().getSimilarityMeasures().first();

        then: "Similarity is as expected."
        // TODO: Don't test real values yet as the matrices are mock and will be replaced with the proper image feature vectors.
        firstSimilarity.getType() == FeatureType.COSINE_SIMILARITY_IMAGE_EMBEDDING.getName()


        assert firstSimilarity.getSimilarities().get(0).getId() == "581409"
        assert firstSimilarity.getSimilarities().get(1).getId() == "569645"

        assert Math.abs(firstSimilarity.getMean() - 0.1948) < TOLERANCE;
        assert Math.abs(firstSimilarity.getVariance() - 0.01391) < TOLERANCE;
        assert Math.abs(firstSimilarity.getSimilarities().get(0).getValue() - 0.3339) < TOLERANCE;
        assert Math.abs(firstSimilarity.getSimilarities().get(1).getValue() - 0.3073) < TOLERANCE;

        lastSimilarity.getType() == FeatureType.COSINE_SIMILARITY_IMAGE_EMBEDDING.getName()

        assert lastSimilarity.getSimilarities().get(0).getId() == "569645"
        assert lastSimilarity.getSimilarities().get(1).getId() == "257149"

        assert Math.abs(lastSimilarity.getMean() - 0.26981) < TOLERANCE;
        assert Math.abs(lastSimilarity.getVariance() - 0.01636) < TOLERANCE;
        assert Math.abs(lastSimilarity.getSimilarities().get(0).getValue() - 0.4656) < TOLERANCE;
        assert Math.abs(lastSimilarity.getSimilarities().get(1).getValue() - 0.3741) < TOLERANCE;

    }

}