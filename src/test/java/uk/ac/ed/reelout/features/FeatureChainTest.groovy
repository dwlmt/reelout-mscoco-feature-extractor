package uk.ac.ed.reelout.features

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification
import uk.ac.ed.reelout.domain.ReeloutStat
import uk.ac.ed.reelout.domain.RlDatabase
import uk.ac.ed.reelout.domain.RlUnit

/**
 * Tests that feature chains can be constructed with the specified features.
 */
@ContextConfiguration(locations = "classpath:META-INF/spring/spring-shell-plugin.xml")
class FeatureChainTest extends Specification {

    @Autowired
    FeatureChainFactory factory

    def "Feature chain test with one feature"() {

        when: "A feature chain and set of RlUnits containing the message text."
        final def messageText = "Sarah is happy."
        final FeatureChain features = factory.createFeatureChain([FeatureType.POS]);
        final RlDatabase db = new RlDatabase(0.2);
        final def rlSet = new TreeSet<RlUnit>()
        final def rl = new RlUnit("1")
        rl.appendMessageText(messageText)
        rlSet.add(rl)
        db.setRlUnits(rlSet)

        final def resDb = features.createFeatures(db, null);

        then: "Features are processed as expected."
        assert resDb.getRlUnits().first().getTags() == ["NNP", "VBZ", "JJ", "."]
        assert resDb.getRlUnits().first().getTokens() == ["Sarah", "is", "happy", "."]

    }

    def "Feature chain test with multiple features"() {

        when: "A feature chain and set of RlUnits containing the message text."
        final def messageText = "Sarah is happy in Edinburgh."
        final FeatureChain features = factory.createFeatureChain([FeatureType.POS,
                                                                  FeatureType.ENTITY,
                                                                  FeatureType.SENTIMENT,
                                                                  FeatureType.THEME,
                                                                  FeatureType.RELATION_TRIPLE,
                                                                  FeatureType.ADD_WORD_EMBEDDING]);
        final RlDatabase db = new RlDatabase(0.2);
        final def rlSet = new TreeSet<RlUnit>()
        final def rl = new RlUnit("1")
        rl.appendMessageText(messageText)
        rlSet.add(rl)
        db.setRlUnits(rlSet)

        final Map<String, Object> options = new HashMap<String, Object>()
        options["embeddingsFile"] = "./data/embeddings/glove.6B.50d.txt";
        options["embeddingsType"] = "glove";


        final def resDb = features.createFeatures(db, options);

        then: "Features are being set. Don't worry about values."
        assert resDb.getRlUnits().first().getTags().size() > 0
        assert resDb.getRlUnits().first().getTokens().size() > 0
        assert resDb.getRlUnits().first().getEntities().size() > 0
        assert resDb.getRlUnits().first().getThemes().size() > 0
        assert resDb.getRlUnits().first().getSentiment() != null
        assert resDb.getRlUnits().first().getRelationTriples().size() > 0

    }

    def "Feature chain test with a reset"() {

        when: "A feature chain and set of RlUnits containing the message text."
        final def messageText = "Sarah is happy in Edinburgh."
        final FeatureChain features = factory.createFeatureChain([FeatureType.POS,
                                                                  FeatureType.ENTITY,
                                                                  FeatureType.SENTIMENT,
                                                                  FeatureType.THEME,
                                                                  FeatureType.RELATION_TRIPLE]);
        final RlDatabase db = new RlDatabase(0.2);
        final def rlSet = new TreeSet<RlUnit>()
        final def rl = new RlUnit("1")
        rl.appendMessageText(messageText)
        rlSet.add(rl)
        db.setRlUnits(rlSet)

        def resDb = features.createFeatures(db, null);
        resDb = features.resetFeatures(resDb);

        then: "Features have been reset so they should all be null or empty."
        assert resDb.getRlUnits().first().getTags().size() == 0
        assert resDb.getRlUnits().first().getTokens().size() == 0
        assert resDb.getRlUnits().first().getEntities().size() == 0
        assert resDb.getRlUnits().first().getThemes().size() == 0
        assert resDb.getRlUnits().first().getSentiment() == null
        assert resDb.getRlUnits().first().getRelationTriples().size() == 0

    }

    def "Feature chain applies from/to filters."() {

        when: "A feature chain and set of RlUnits containing the message text."
        final FeatureChain features = factory.createFeatureChain([FeatureType.POS,
                                                                  FeatureType.SENTIMENT])
                .withFromIdRange("3")
                .withToIdRange("6");

        final RlDatabase db = new RlDatabase(0.2);
        (0..9).each {
            final def rl = new RlUnit("${it}")
            rl.setMessageText("${it}")
            db.addRlUnit(rl);
        }

        final def resDb = features.createFeatures(db, null);

        then: "Features are only applied to those RlUnits in the range."
        resDb.getRlUnits().each {
            if (it.getId() in ["3", "4", "5"]) {
                assert it.getTags().size() == 1
            } else {
                assert it.getTags().size() == 0
            }
        }

    }

    def "Feature chain counting features"() {

        when: "A feature chain and set of RlUnits containing the message text."
        final def messageText = "A sail boat sailing across a lake beneath a tall mountain. water, water, water"
        final FeatureChain features = factory.createFeatureChain([FeatureType.POS,
        ]);
        final RlDatabase db = new RlDatabase(0.2);
        final def rlSet = new TreeSet<RlUnit>()
        final def rl = new RlUnit("1")
        rl.appendMessageText(messageText)
        rlSet.add(rl)
        db.setRlUnits(rlSet)

        final def resDb = features.createFeatures(db, null);
        final def counts = features.countFeatures(resDb, [:])

        then: "Features counts have been populated."
        assert counts[new ReeloutStat("POS", "tag", "NN")].get() == 8
        assert counts[new ReeloutStat("POS", "textlength", "17")].get() == 1
        assert counts[new ReeloutStat("POS", "tag", "NN")].get() == 8
        assert counts[new ReeloutStat("POS", "token", "water")].get() == 3

    }
}