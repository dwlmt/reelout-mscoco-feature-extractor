package uk.ac.ed.reelout.selection;

import edu.stanford.nlp.util.Sets;
import uk.ac.ed.reelout.domain.*;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Creates parametrised predicates  for filtering RlUnits.
 */
public class SelectionPredicates {

    /**
     * Does the RlUnit have the same Id.
     *
     * @param id to match.
     * @return Predicate
     */
    public static Predicate<RlUnit> id(String id) {
        return rl -> rl.getId().equals(id);
    }

    /**
     * Text insensitive matching of the messageText.
     *
     * @param text to match.
     * @return Predicate
     */
    public static Predicate<RlUnit> text(String text) {
        return rl -> rl.getMessageText().trim().toLowerCase().contains(text.trim().toLowerCase());
    }

    /**
     * Match the Sentiment.
     *
     * @param sentiment to match.
     * @return Predicate
     */

    public static Predicate<RlUnit> sentiment(Sentiment sentiment) {
        return rl -> {
            if (rl.getSentiment() == null) {
                return false;
            }
            return sentiment.equals(rl.getSentiment().getSentiment());
        };
    }

    /**
     * Match the Theme
     *
     * @param theme     text of the theme.
     * @param threshold for the theme.
     * @return Predicate
     */

    public static Predicate<RlUnit> theme(String theme, Double threshold) {
        return rl -> {
            if (rl.getThemes().size() == 0) {
                return false;
            }

            for (Theme t : rl.getThemes()) {
                if (threshold != null) {
                    // Don't consider themes with confidence below the threshold.
                    if (t.getConfidence() <= threshold) {
                        continue;
                    }
                }

                for (String category : t.getCategories()) {
                    if (category.toLowerCase().contains(theme.trim().toLowerCase())) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    /**
     * Match the entities.
     *
     * @param text for the entities.
     * @return Predicate
     */
    public static Predicate<RlUnit> location(String text) {
        return rl -> {
            if (rl.getLocations().size() == 0) {
                return false;
            }

            for (Location location : rl.getLocations()) {
                for (String place : location.getPlaces()) {
                    if (place.toLowerCase().contains(text.trim().toLowerCase())) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    /**
     * Match the entity.
     *
     * @param text match the named entity found via Alchemy.
     * @return Predicate
     */
    public static Predicate<RlUnit> entity(String text) {
        return rl -> {
            if (rl.getEntities().size() == 0) {
                return false;
            }
            for (Entity entities : rl.getEntities()) {
                for (String entity : entities.getEntities()) {
                    if (entity.toLowerCase().contains(text.trim().toLowerCase())) {
                        return true;
                    }
                }
            }
            return false;
        };
    }

    /**
     * Matches the Open IE Triples.
     *
     * @param subject  of a sentence.
     * @param relation (most commonly verb) of a sentence.
     * @param object   of a sentence.
     * @param all      do all or any have to be present or all components of the Triple.
     * @return Predicate
     */
    public static Predicate<RlUnit> triple(String[] subject, String[] relation, String[] object, Boolean all) {
        return rl -> {
            if (rl.getRelationTriples().size() == 0) {
                return false;
            }

            boolean subjectPresent = false;
            boolean objectPresent = false;
            boolean relationPresent = false;

            for (RlRelationTriple triple : rl.getRelationTriples()) {
                if (subject != null && subject.length > 0) {
                    if (Sets.intersection(new HashSet<String>(convertStrings(triple.getSubject())),
                            convertStrings(Arrays.asList(subject))).size() == subject.length) {
                        subjectPresent = true;
                    }
                }
                if (object != null && object.length > 0) {
                    if (Sets.intersection(new HashSet<String>(convertStrings(triple.getObject())),
                            convertStrings(Arrays.asList(object))).size() == object.length) {
                        objectPresent = true;
                    }
                }

                if (relation != null && relation.length > 0) {
                    if (Sets.intersection(new HashSet<String>(convertStrings(triple.getRelation())),
                            convertStrings(Arrays.asList(relation))).size() == relation.length) {
                        relationPresent = true;
                    }
                }
            }
            // If any are present.
            if (all == null || all == false) {
                return subjectPresent || objectPresent || relationPresent;
                // All need to be present.
            } else {
                return subjectPresent && objectPresent && relationPresent;
            }
        };
    }

    /**
     * Lowercase and trim the array and return as a set.
     *
     * @param parts
     * @return Set<String>
     */
    private static Set<String> convertStrings(Collection<String> parts) {
        return parts
                .stream()
                .map(String::toLowerCase)
                .map(String::trim)
                .collect(Collectors.toCollection(HashSet::new));
    }

    /**
     * Is it before?
     *
     * @param time
     * @return Predicate
     */
    public static Predicate<RlUnit> before(LocalDateTime time) {
        return rl -> {
            return rl.getTime().isBefore(time);
        };
    }

    /**
     * Is it after?
     *
     * @param time
     * @return Predicate
     */
    public static Predicate<RlUnit> after(LocalDateTime time) {
        return rl -> {
            return rl.getTime().isAfter(time);
        };
    }

}
