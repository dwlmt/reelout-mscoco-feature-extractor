package uk.ac.ed.reelout.selection;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

/**
 * Retry object to control whether to retry to continue to search for a better solution.
 */
public class StoppingCondition {

    private final Logger logger = LoggerFactory.getLogger(StoppingCondition.class);

    /**
     * Time the search will finish by.
     */
    private final LocalDateTime finishTime;
    /**
     * How many iterations to allow without improvements to the best score.
     */
    int iterationsWithoutImprovement;
    /**
     * Best score found so far.
     */
    double bestScore;
    /**
     * Maximum number of iterations to allow.
     */
    int maxIterations;
    /**
     * Maximum time to search for the best solution in minutes.
     */
    int maxTimeInMinutes;

    /**
     * How much of an improvement does there need to be to count as an improvement.
     */
    double improvementThreshold;

    /**
     * Countdown for number of iterations without improvement.
     */
    private int noImprovementCountdown;


    public StoppingCondition(int iterationsWithoutImprovement, int maxIterations, int maxTimeInMinutes,
                             double improvementThreshold) {
        this.iterationsWithoutImprovement = iterationsWithoutImprovement;
        noImprovementCountdown = iterationsWithoutImprovement;
        this.maxIterations = maxIterations;
        this.maxTimeInMinutes = maxTimeInMinutes;
        this.improvementThreshold = improvementThreshold;

        // Initialise the latest finish time.
        finishTime = LocalDateTime.now().plus(maxTimeInMinutes, ChronoUnit.MINUTES);
    }

    /**
     * Method to query whether the Solution search should continue.
     */
    public boolean shouldContinue() {
        if (this.finishTime.isBefore(LocalDateTime.now())) {
            this.logger.warn("Max time limit reached, stop searching for better selection.");
            return false;
        }

        if (this.maxIterations == 0) {
            this.logger.warn("Max number of iterations reached, stop searching for better selection.");
            return false;
        }

        if (noImprovementCountdown == 0) {
            this.logger.warn("No improvement after {} iterations, stop searching for better selection.",
                    getIterationsWithoutImprovement());
            return false;
        }

        // Decrement an iteration.
        maxIterations--;

        return true;
    }

    /**
     * Update the best score.
     *
     * @param score double
     */
    public void score(double score) {
        if (score - bestScore > improvementThreshold) {
            this.logger.info("Improvement to the best score: New score = {}, Old Score = {}", score, this.bestScore);
            bestScore = score;
            noImprovementCountdown = iterationsWithoutImprovement; // Reset the countdown.
        } else {
            noImprovementCountdown--;
            this.logger.info("No improvement for {} iterations ", iterationsWithoutImprovement - noImprovementCountdown);
        }

    }


    public int getIterationsWithoutImprovement() {
        return iterationsWithoutImprovement;
    }

    public double getBestScore() {
        return bestScore;
    }

    public int getMaxIterations() {
        return maxIterations;
    }

    public int getMaxTimeInMinutes() {
        return maxTimeInMinutes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        StoppingCondition that = (StoppingCondition) o;
        return this.iterationsWithoutImprovement == that.iterationsWithoutImprovement &&
                Double.compare(that.bestScore, this.bestScore) == 0 &&
                this.maxIterations == that.maxIterations &&
                this.maxTimeInMinutes == that.maxTimeInMinutes &&
                Double.compare(that.improvementThreshold, this.improvementThreshold) == 0 &&
                this.noImprovementCountdown == that.noImprovementCountdown &&
                Objects.equal(this.finishTime, that.finishTime);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.finishTime, this.iterationsWithoutImprovement, this.bestScore, this.maxIterations,
                this.maxTimeInMinutes, this.improvementThreshold, this.noImprovementCountdown);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("iterationsWithoutImprovement", iterationsWithoutImprovement)
                .add("maxIterations", maxIterations)
                .add("maxTimeInMinutes", maxTimeInMinutes)
                .add("improvementThreshold", improvementThreshold)
                .toString();
    }
}
