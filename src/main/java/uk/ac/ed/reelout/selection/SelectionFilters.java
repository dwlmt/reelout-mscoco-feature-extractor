package uk.ac.ed.reelout.selection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.domain.Sentiment;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Methods for applying filters to select subsets of RlUnits.
 */
@Component
public class SelectionFilters {

    private final Logger logger = LoggerFactory.getLogger(SelectionFilters.class);

    /**
     * Filter on Id.
     *
     * @param ids
     * @param units RlUnits before filtering.
     * @return NavigableSet<RlUnit>
     */
    public NavigableSet<RlUnit> filterOnIds(String[] ids, NavigableSet<RlUnit> units) {

        if (ids == null || ids.length == 0) {
            return units;
        }
        logger.info("Filter on Ids: before = {}", units.size());
        Predicate<RlUnit> filter = null;
        for (String s : ids) {
            filter = filter == null ? SelectionPredicates.id(s.trim()) : filter.or(SelectionPredicates.id(s.trim()));
        }
        ConcurrentSkipListSet<RlUnit> filteredUnits = units.parallelStream().filter(filter)
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        logger.info("Filter on Ids: after = {}", filteredUnits.size());
        return filteredUnits;
    }

    /**
     * Filter on the message text.
     *
     * @param messageText filter on the message text.
     * @param units       RlUnits before filtering.
     * @return NavigableSet<RlUnit>
     */
    public NavigableSet<RlUnit> filterOnMessageText(String[] messageText, NavigableSet<RlUnit> units) {

        if (messageText == null || messageText.length == 0) {
            return units;
        }
        logger.info("Filter on message text: before = {}", units.size());
        boolean and = false;
        boolean not = false;
        Predicate<RlUnit> filter = null;
        for (String s : messageText) {
            Predicate<RlUnit> pred = SelectionPredicates.text(s.trim());

            if (not) {
                pred = pred.negate();
            }

            if (s.trim().equalsIgnoreCase("and")) {
                and = true;
                // Set the and flag and continue;
            } else if (s.trim().equalsIgnoreCase("not")) {
                not = true;
            } else {
                if (and) {
                    filter = filter == null ? pred : filter.and(pred);
                } else {
                    filter = filter == null ? pred : filter.or(pred);
                }

                if (and) {
                    and = false;
                }
                if (not) {
                    not = false;
                }
            }
        }
        ConcurrentSkipListSet<RlUnit> filteredUnits = units.parallelStream().filter(filter)
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        logger.info("Filter on message text: after = {}", filteredUnits.size());
        return filteredUnits;
    }

    /**
     * Filter on locations.
     *
     * @param locations to filter on.
     * @param units     RlUnits before filtering.
     * @return NavigableSet<RlUnit>
     */
    public NavigableSet<RlUnit> filterOnLocations(String[] locations, NavigableSet<RlUnit> units) {

        if (locations == null || locations.length == 0) {
            return units;
        }
        logger.info("Filter on locations: before = {}", units.size());
        Predicate<RlUnit> filter = null;
        for (String s : locations) {
            filter = filter == null ? SelectionPredicates.location(s.trim()) : filter.or(SelectionPredicates.location(s.trim()));
        }
        ConcurrentSkipListSet<RlUnit> filteredUnits = units.parallelStream().filter(filter)
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        logger.info("Filter on locations: after = {}", filteredUnits.size());
        return filteredUnits;
    }

    /**
     * Filter on entities.
     *
     * @param entities to filter on.
     * @param units    RlUnits before filtering.
     * @return NavigableSet<RlUnit>
     */
    public NavigableSet<RlUnit> filterOnEntities(String[] entities, NavigableSet<RlUnit> units) {

        if (entities == null || entities.length == 0) {
            return units;
        }
        logger.info("Filter on entities: before = {}", units.size());
        Predicate<RlUnit> filter = null;
        for (String s : entities) {
            filter = filter == null ? SelectionPredicates.entity(s.trim()) : filter.or(SelectionPredicates.entity(s.trim()));
        }
        ConcurrentSkipListSet<RlUnit> filteredUnits = units.parallelStream().filter(filter)
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        logger.info("Filter on entities: after = {}", filteredUnits.size());
        return filteredUnits;
    }

    /**
     * Filter on themes.
     *
     * @param themes         to filter on.
     * @param themeThreshold confidence threshold for the theme.
     * @param units          RlUnits before filtering.
     * @return NavigableSet<RlUnit>
     */
    public NavigableSet<RlUnit> filterOnThemes(String[] themes, Double themeThreshold, NavigableSet<RlUnit> units) {

        if (themes == null || themes.length == 0) {
            return units;
        }
        logger.info("Filter on theme: before = {}", units.size());
        Predicate<RlUnit> filter = null;
        for (String s : themes) {
            filter = filter == null ? SelectionPredicates.theme(s.trim(), themeThreshold) :
                    filter.or(SelectionPredicates.theme(s.trim(), themeThreshold));
        }
        ConcurrentSkipListSet<RlUnit> filteredUnits = units.parallelStream().filter(filter)
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        logger.info("Filter on theme: after = {}", filteredUnits.size());
        return filteredUnits;
    }

    /**
     * Filter on entities.
     *
     * @param sentiments to filter on.
     * @param units      RlUnits before filtering.
     * @return NavigableSet<RlUnit>
     */
    public NavigableSet<RlUnit> filterOnSentiments(String[] sentiments, NavigableSet<RlUnit> units) {

        if (sentiments == null || sentiments.length == 0) {
            return units;
        }
        logger.info("Filter on sentiments: before = {}", units.size());
        Predicate<RlUnit> filter = null;
        for (String s : sentiments) {
            // Will throw and an exception to the console if it is not a valid sentiment.
            Sentiment sentiment = Sentiment.valueOf(s.trim().toLowerCase());
            filter = filter == null ? SelectionPredicates.sentiment(sentiment) : filter.or(SelectionPredicates.sentiment(sentiment));
        }
        ConcurrentSkipListSet<RlUnit> filteredUnits = units.parallelStream().filter(filter)
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        logger.info("Filter on sentiments: after = {}", filteredUnits.size());
        return filteredUnits;
    }

    /**
     * Filter on the Open IE Triples.
     *
     * @param subject
     * @param relation
     * @param object
     * @param allTogether
     * @param units
     * @return
     */
    public NavigableSet<RlUnit> filterOnTriple(String[] subject, String[] relation, String[] object,
                                               Boolean allTogether, NavigableSet<RlUnit> units) {

        if (subject == null && relation == null && object == null) {
            return units;
        }
        logger.info("Filter on Open IE Triples: before = {}", units.size());

        ConcurrentSkipListSet<RlUnit> filteredUnits = units.parallelStream().filter(
                SelectionPredicates.triple(subject, relation, object, allTogether))
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        logger.info("Filter on Open IE Triples: after = {}", filteredUnits.size());
        return filteredUnits;
    }

    /**
     * Image must be before the specified date and time.
     *
     * @param before the date the element should be before.
     * @param units  RlUnits before filtering.
     * @return NavigableSet<RlUnit>
     */
    public NavigableSet<RlUnit> filterBefore(Date before, NavigableSet<RlUnit> units) {

        if (before == null) {
            return units;
        }
        logger.info("Filter images before time and date: before = {}", units.size());
        ConcurrentSkipListSet<RlUnit> filteredUnits = units.parallelStream().filter(
                SelectionPredicates.before(LocalDateTime.ofInstant(before.toInstant(), ZoneId.systemDefault())))
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        logger.info("Filter images before time and date: after = {}", filteredUnits.size());
        return filteredUnits;
    }

    /**
     * Image must be after the specified date and time.
     *
     * @param after the date the element should be before.
     * @param units RlUnits before filtering.
     * @return NavigableSet<RlUnit>
     */
    public NavigableSet<RlUnit> filterAfter(Date after, NavigableSet<RlUnit> units) {

        if (after == null) {
            return units;
        }
        logger.info("Filter images after time and date: before = {}", units.size());
        ConcurrentSkipListSet<RlUnit> filteredUnits = units.parallelStream().filter(
                SelectionPredicates.after(LocalDateTime.ofInstant(after.toInstant(), ZoneId.systemDefault())))
                .collect(Collectors.toCollection(ConcurrentSkipListSet::new));
        logger.info("Filter images after time and date: after = {}", filteredUnits.size());
        return filteredUnits;
    }

    /**
     * Groups together the parameters that can be used to filter RLUnits.
     *
     * @param ids            of the RlUnits.
     * @param messageText    on the text of the message
     * @param themes         from Alchemy.
     * @param themeThreshold between 0.0-1.0
     * @param locations      from Alchemy named entity recognition
     * @param entities       from Alchemy named entity recognition
     * @param subject        of a sentence in the text
     * @param relation       relation/verb of a sentence in the text
     * @param object         of a sentence in the text
     * @param wholeTriple    should all elements of the triple have to be present to include an image in the results
     * @param sentiments     out of positive/neutral/negative
     * @param before         include those before this Date
     * @param after          include this after the date.
     * @param unitsToFilter  set of RlUnits to filter on.
     * @return NavigableSet<RlUnit>
     */
    public NavigableSet<RlUnit> filterRlUnits(String[] ids, String[] messageText, String[] themes,
                                              Double themeThreshold, String[] locations, String[] entities,
                                              String[] subject, String[] relation, String[] object,
                                              Boolean wholeTriple, String[] sentiments, Date before,
                                              Date after, NavigableSet<RlUnit> unitsToFilter) {
        unitsToFilter = this.filterOnIds(ids, unitsToFilter);
        unitsToFilter = this.filterOnMessageText(messageText, unitsToFilter);
        unitsToFilter = this.filterOnLocations(locations, unitsToFilter);
        unitsToFilter = this.filterOnEntities(entities, unitsToFilter);
        unitsToFilter = this.filterOnThemes(themes, themeThreshold, unitsToFilter);
        unitsToFilter = this.filterOnSentiments(sentiments, unitsToFilter);
        unitsToFilter = this.filterOnTriple(subject, relation, object, wholeTriple, unitsToFilter);
        unitsToFilter = this.filterBefore(before, unitsToFilter);
        unitsToFilter = this.filterAfter(after, unitsToFilter);
        return unitsToFilter;
    }
}
