package uk.ac.ed.reelout.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentSkipListSet;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_TIME;

/**
 * Provides mapping services for converting the MS-COCO image data to a ReelOut internal representation.
 */
@Service
public class MsCocoConversionService {

    private static final String IMAGES_JSON = "images";
    private static final Float RL_UNIT_SCHEMA_VERSION = new Float(0.2);
    private static final String MSCOCO_SRC = "mscoco";
    private static final String ANNOTATIONS_JSON = "annotations";
    /**
     * Formatter to parse dates in the format "2013-11-14 16:46:33"
     */
    private static final DateTimeFormatter MSCOCO_LOCAL_DATE_TIME;

    static {
        MSCOCO_LOCAL_DATE_TIME = new DateTimeFormatterBuilder()
                .parseCaseInsensitive()
                .append(ISO_LOCAL_DATE)
                .appendLiteral(' ')
                .append(ISO_LOCAL_TIME).toFormatter();
    }

    private final Logger logger = LoggerFactory.getLogger(MsCocoConversionService.class);
    /**
     * Object mapper for converting JSON to Java objects.
     */
    private final ObjectMapper mapper;

    /**
     * Default no arg constructor.
     */
    public MsCocoConversionService() {
        mapper = new ObjectMapper();
    }

    /**
     * Provide a custom mapper constructor.
     *
     * @param mapper
     */
    public MsCocoConversionService(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * Converts input MS-COCO annotation JSON string to the ReeloutDomain representation.
     *
     * @param json         String representation of the MS-COCO annotations file.
     * @param joinCaptions boolean join the captions into the same representation or keep them separate.
     * @param maxSentences
     * @throws IOException if the JSON cannot be parsed.
     */
    public RlDatabase convertToReeloutDomain(String json, boolean joinCaptions, Integer maxSentences) throws IOException {

        RlDatabase rlDb = new RlDatabase(MsCocoConversionService.RL_UNIT_SCHEMA_VERSION);

        JsonNode rootNode = mapper.readValue(json, JsonNode.class);

        // Iterate over the images within the JSON.
        JsonNode images = rootNode.get(MsCocoConversionService.IMAGES_JSON);
        images.elements().forEachRemaining(image -> {

            // Create a new RlUnit
            RlUnit rl = new RlUnit(image.get("id").asText());

            logger.debug("Image details = {}", image);

            rl.setSrc(MsCocoConversionService.MSCOCO_SRC);
            rl.setImage(image.get("url").asText());
            // Convert MS-COCO date format.
            rl.setTime(LocalDateTime.parse(image.get("date_captured").asText(), MsCocoConversionService.MSCOCO_LOCAL_DATE_TIME));

            rlDb.addRlUnit(rl);
        });

        // If captions are kept distinct then the Id needs to be based on the captions id and not the image Id.
        NavigableSet<RlUnit> imageIdRlUnits = rlDb.getRlUnits();
        if (!joinCaptions) {
            rlDb.setRlUnits(new ConcurrentSkipListSet<RlUnit>());
        }

        // Iterate over the annotations within the JSON.
        JsonNode annotations = rootNode.get(MsCocoConversionService.ANNOTATIONS_JSON);

        annotations.elements().forEachRemaining(annotation -> {

            logger.debug("Annotation details = {}", annotation);

            RlUnit rlUnit;
            RlUnit rSearch = new RlUnit(annotation.get("image_id").asText());
            if (joinCaptions) {
                // If joining the caption then get the existing RL Unit.
                rlUnit = rlDb.getRlUnits().tailSet(rSearch).first();
            } else {
                //
                rlUnit = new RlUnit(annotation.get("id").asText());
                RlUnit imageUnit = imageIdRlUnits.tailSet(rSearch).first();
                rlUnit.setImage(imageUnit.getImage());
                rlUnit.setSrc(imageUnit.getSrc());
                rlUnit.setTime(imageUnit.getTime());
                rlDb.addRlUnit(rlUnit);
            }

            if (rlUnit.getNumOfSentences() < maxSentences) {
                rlUnit.appendMessageText(annotation.get("caption").asText());
                rlUnit.incrementNumOfSentences();
            }

        });

        logger.info("convertToReeloutDomain: total images converted = {}", rlDb.getRlUnits().size());
        return rlDb;
    }

}
