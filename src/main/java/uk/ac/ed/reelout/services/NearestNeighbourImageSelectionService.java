package uk.ac.ed.reelout.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import uk.ac.ed.reelout.domain.*;

import javax.ws.rs.NotSupportedException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Selects Triptychs based on the nearest neighbour.
 */
@Service
public class NearestNeighbourImageSelectionService implements ImageSelectionService {

    private final Logger logger = LoggerFactory.getLogger(NearestNeighbourImageSelectionService.class);

    /**
     * Initially will randomly select the first images and then selects the second, third based on nearest neighbours.
     * <p>
     * TODO: Extend to include criteria.
     *
     * @param rlDb     the RlDatabase containing the images features.
     * @param criteria @return the selected images.
     * @rlDb the images database containing RLUnits.
     */
    @Override
    public List<RlUnit> selectTriptychImages(RlDatabase rlDb, SelectionCriteria criteria) {

        logger.info("Select random images: rlDb images count = {}, criteria = {}", rlDb.getRlUnits().size(), criteria);

        List<RlUnit> triptych = new ArrayList<RlUnit>(3);
        List<RlUnit> rlUnitList = new ArrayList<RlUnit>(rlDb.getRlUnits());

        // Randomly select the first images.
        Collections.shuffle(rlUnitList);

        RlUnit firstImage = rlUnitList.get(0);
        triptych.add(firstImage);

        // Get second images.
        RlUnit rlUnit2 = getClosest(rlDb, triptych);
        triptych.add(rlUnit2);

        // Get third images.
        RlUnit rlUnit3 = getClosest(rlDb, triptych);
        triptych.add(rlUnit3);


        logger.info("Triptych images selected are:");
        for (RlUnit rl : triptych) {
            logger.info("Image: id = {}, image url = {}, messageText = {}", rl.getId(), rl.getImage(), rl.getMessageText());
        }

        return triptych;

    }

    /**
     * @param start           set of RlUnits that can be in the start position of the Triptych.
     * @param middle          set of RlUnits that can be in the middle position of the Triptych.
     * @param end             set of RlUnits that can be in the end position of the Triptych.
     * @param criteria        / constraints to apply to Triptych selection.
     * @param numberOfStories how many n best Triptychs to select.
     * @return List <List<RlUnit>> multiple Triptychs represented within the inner list.
     * @throws IOException
     */
    @Override
    public List<List<RlUnit>> selectTriptychWithPositions(Set<RlUnit> start, Set<RlUnit> middle, Set<RlUnit> end, SelectionCriteria criteria, Integer numberOfStories) throws IOException {
        throw new NotSupportedException("Not yet implemented.");
    }

    /**
     * Gets the closest based on similarity measures.
     *
     * @param rlDb   the database.
     * @param images list of RlUnits selected so far.
     * @return the next image in the
     */
    private RlUnit getClosest(RlDatabase rlDb, List<RlUnit> images) {
        RlUnit lastImage = images.get(images.size() - 1);


        if (lastImage.getSimilarityMeasures().size() == 0) {
            throw new IllegalArgumentException("Nearest neighbour selection must have Similarity Measures in the XML feature file.");
        }

        // Supports only 1 similarity measure for now.
        SimilarityMeasure similarityMeasure = lastImage.getSimilarityMeasures().get(0);
        List<Similarity> similarities = similarityMeasure.getSimilarities();

        // Find the next closest image. Each one needs to be checked to be unique to avoid potentially
        // returning to the first image in the Triptych.
        boolean newImage = false;
        int index = 0;

        RlUnit nextImage = null;
        while (!newImage) {
            Similarity closest = similarities.get(index);
            logger.debug("For images: id = {}, closest images id = {}, similarity = {}",
                    lastImage.getId(), closest.getId(), closest.getValue());

            // Get the closest RLUnit that hae been found.
            nextImage = rlDb.getRlUnitFromId(closest.getId());

            if (!images.contains(nextImage)) {
                newImage = true;
            }

            index++;
        }
        return nextImage;
    }
}
