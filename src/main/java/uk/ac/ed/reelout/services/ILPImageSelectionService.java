package uk.ac.ed.reelout.services;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Sets;
import com.google.common.collect.Sets.SetView;
import com.google.common.io.Files;
import org.apache.commons.io.IOUtils;
import org.deeplearning4j.berkeley.Counter;
import org.gnu.glpk.*;
import org.nd4j.linalg.api.iter.NdIndexIterator;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import uk.ac.ed.reelout.domain.*;
import uk.ac.ed.reelout.exceptions.TriptychSelectionException;
import uk.ac.ed.reelout.features.FeatureType;
import uk.ac.ed.reelout.repository.ImageFeatureRepository;
import uk.ac.ed.reelout.repository.ParagraphEmbeddingFeatureRepository;
import uk.ac.ed.reelout.repository.PredictionFeatureRepository;
import uk.ac.ed.reelout.repository.WordEmbeddingFeatureRepository;
import uk.ac.ed.reelout.selection.StoppingCondition;

import javax.ws.rs.NotSupportedException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Implementation using an Integer Linear Programming Solution.
 */
@Service
@Configuration
@PropertySource("classpath:config.properties")
public class ILPImageSelectionService implements ImageSelectionService, GlpkCallbackListener, GlpkTerminalListener {

    public static final double DEFAULT_SIMILARITY = 0.5;
    private final Logger logger = LoggerFactory.getLogger(ILPImageSelectionService.class);
    /**
     * Counter for storing an ordered list of the best solutions found so far.
     */
    private final Counter<List<RlUnit>> solutionsList = new Counter<>();
    /**
     * Keep a list of RLUnits here.
     */
    private final RlDatabase rlDb = new RlDatabase();
    @Value("${selector.complexity.threshold}")
    public int complexityThreshold;
    @Value("${selector.sample.size}")
    public int sampleSize;

    @Value("${selector.keep.best.n}")
    public int keepBestN;

    @Autowired
    ImageFeatureRepository imageFeatureRepository;

    @Autowired
    PredictionFeatureRepository predictionFeatureRepository;

    @Autowired
    WordEmbeddingFeatureRepository wordEmbeddingFeatureRepository;

    @Autowired
    ParagraphEmbeddingFeatureRepository paragraphEmbeddingFeatureRepository;

    private glp_prob lp;
    /**
     * Map the RlUnit ids to the numerical indices used for variables in GLPK.
     */
    private BiMap<String, Integer> idNumMap = HashBiMap.create();
    private Integer numberOfTriptychs;
    private String modelFileName = "";

    /**
     * Default constructor.
     */
    public ILPImageSelectionService() {
    }

    /**
     * Uses GCLP Solver to find Triptychs.
     *
     * @param rlDb     the RlDatabase containing the images features.
     * @param criteria @return the selected images.
     * @rlDb the images database containing RLUnits.
     */
    @Override
    @Deprecated
    public List<RlUnit> selectTriptychImages(RlDatabase rlDb, SelectionCriteria criteria) throws IOException {
        throw new NotSupportedException("Deprecated method, to be removed.");
    }

    /**
     * Extract the selected Ids from the String names.
     *
     * @param lp GLPK problem.
     * @param n  number of column variables.
     * @return List<String[]>
     */
    private List<String[]> extractIdsFromVariables(glp_prob lp, int n) {
        List<String[]> idList = new ArrayList<String[]>();
        // Iterate over all the columns.
        for (int i = 1; i <= n; i++) {
            String name = GLPK.glp_get_col_name(lp, i);
            double val = GLPK.glp_mip_col_val(lp, i);
            // The Triptychs will be 1.0
            if (val == 1.0) {
                if (name.startsWith("y")) {
                    break; // Only need to go through one set of the similarities to identify the Triptych.
                }
                logger.debug("name = {}, val = {}", name, 1.0);
                name = name.replace("x[", "");
                name = name.replace("]", "");
                String[] ids = name.split(",");
                idList.add(ids);
                logger.debug("Ids extracted = {}", ids);
            }
        }
        return idList;
    }

    /**
     * @param start             set of RlUnits that can be in the start position of the Triptych.
     * @param middle            set of RlUnits that can be in the middle position of the Triptych.
     * @param end               set of RlUnits that can be in the end position of the Triptych.
     * @param criteria          / constraints to apply to Triptych selection.
     * @param numberOfTriptychs how many n best Triptychs to select.
     * @return List <List<RlUnit>> multiple Triptychs represented within the inner list.
     * @throws IOException
     */
    @Override
    public List<List<RlUnit>> selectTriptychWithPositions(Set<RlUnit> start, Set<RlUnit> middle, Set<RlUnit> end,
                                                          SelectionCriteria criteria, Integer numberOfTriptychs)
            throws IOException {

        if (start.isEmpty() || middle.isEmpty() || end.isEmpty()) {
            throw new TriptychSelectionException("Cannot return a valid Triptych as some slots are empty. Please change the criteria.");
        }

        // Make sure each model is unique so there isn't interference from multiple runs in parallel.
        modelFileName = "triptych_" + Long.toString(System.nanoTime()) + ".mod";

        List<List<RlUnit>> triptychs = new ArrayList<List<RlUnit>>();

        solutionsList.clear();
        this.numberOfTriptychs = numberOfTriptychs;

        INDArray sentenceQuery = null;
        if (criteria.getSentence() != null) {
            logger.info("Sentence query = {}", criteria.getSentence());
            if (criteria.getSentenceUseWordEmbeddings()) {
                logger.info("Word embedding based query");
                wordEmbeddingFeatureRepository.loadFeatures(criteria.getWordEmbeddings());
                sentenceQuery = wordEmbeddingFeatureRepository.createWordEmbeddingForQuerySentence(criteria.getSentence());
            } else {
                logger.info("Paragraph embedding based query");
                paragraphEmbeddingFeatureRepository.loadFeatures(criteria.getParagraphEmbeddings());
            }
        }

        // If below a given complexity then solve in one iteration then do so otherwise begin iterative search.
        if (Math.pow(this.complexityThreshold, 3) > (double) start.size() * middle.size() * end.size() &&
                Math.pow(this.complexityThreshold * 3, 2) > Math.pow(start.size() + middle.size() + end.size(), 2)) {
            logger.info("Problem size is small enough to solve directly.");
            this.selectTriptychIteration(start, middle, end, criteria, numberOfTriptychs, sentenceQuery, criteria.getSentence());
        } else {
            int keepN;
            if (numberOfTriptychs < this.keepBestN) {
                keepN = this.keepBestN;
            } else {
                keepN = numberOfTriptychs;
            }

            StoppingCondition retry = criteria.getStoppingCondition();

            List<RlUnit> startList = new ArrayList<RlUnit>(start);
            List<RlUnit> middleList = new ArrayList<RlUnit>(middle);
            List<RlUnit> finishList = new ArrayList<RlUnit>(end);

            int sampleToAllocate = this.sampleSize * 3;
            int totalUnits = startList.size() + middleList.size() + finishList.size();
            int startSampleSize = (int) ((double) startList.size() / (double) totalUnits * sampleToAllocate);
            int middleSampleSize = (int) ((double) middleList.size() / (double) totalUnits * sampleToAllocate);
            int finishSampleSize = (int) ((double) finishList.size() / (double) totalUnits * sampleToAllocate);

            // Stop the sample being too big. Add one on to stop one position being empty if the ratio is too big.
            startSampleSize = Math.min(startSampleSize + 1, startList.size());
            middleSampleSize = Math.min(middleSampleSize + 1, middleList.size());
            finishSampleSize = Math.min(finishSampleSize + 1, finishList.size());

            logger.info("Size of the sample for each position: start = {}, middle = {}, finish = {}", startSampleSize,
                    middleSampleSize, finishSampleSize);

            int iter = 1;

            Random random = new Random();

            while (retry.shouldContinue()) {

                logger.info("Starting iteration {}", iter);
                iter++;

                // Select random sample from
                Collections.shuffle(startList, random);
                Collections.shuffle(middleList, random);
                Collections.shuffle(finishList, random);

                Set<RlUnit> startSample = new TreeSet<RlUnit>(startList.subList(0, startSampleSize));
                Set<RlUnit> middleSample = new TreeSet<RlUnit>(middleList.subList(0, middleSampleSize));
                Set<RlUnit> finishSample = new TreeSet<RlUnit>(finishList.subList(0, finishSampleSize));

                // Put the RlUnits from the best n solutions back into the candidate list.
                for (List<RlUnit> triptych : this.solutionsList.keySet()) {
                    RlUnit s = triptych.get(0);
                    RlUnit m = triptych.get(1);
                    RlUnit f = triptych.get(2);
                    logger.info("Adding best solution into iteration = ([{}, {}], [{}, {}], [{}, {}])",
                            s.getId(), s.getImage(),
                            m.getId(), m.getImage(),
                            f.getId(), f.getImage());
                    startSample.add(s);
                    middleSample.add(m);
                    finishSample.add(f);
                }

                this.selectTriptychIteration(startSample, middleSample, finishSample, criteria, numberOfTriptychs,
                        sentenceQuery, criteria.getSentence());

                // Keep the N best solutions.
                this.solutionsList.keepTopNKeys(keepN-1);

                retry.score(this.solutionsList.max()); // Sets the score. Used to judge if to continue after no improvement.

            }
        }
        // Filter down to the correct number to return.
        this.solutionsList.keepTopNKeys(numberOfTriptychs-1);
        triptychs.addAll(this.solutionsList.keySet());


        try {
            new File(modelFileName).delete();
        } catch (Exception ex) {
            this.logger.error("Could not delete the model file: {}", ex);
        }

        return triptychs;
    }

    /**
     * Internal method to perform one iteration of the selection.
     *
     * @param start             set
     * @param middle            set
     * @param end               set
     * @param criteria          query criteria
     * @param numberOfTriptychs
     * @param sentenceQuery     if converted to a Word embedding.
     * @param sentence
     * @return List<List<RlUnit>>
     * @throws IOException
     */
    private List<List<RlUnit>> selectTriptychIteration(Set<RlUnit> start, Set<RlUnit> middle, Set<RlUnit> end,
                                                       SelectionCriteria criteria, Integer numberOfTriptychs,
                                                       INDArray sentenceQuery, final String sentence) throws IOException {

        List<List<RlUnit>> triptychs = new ArrayList<List<RlUnit>>();

        // Add all the separate RlUnits to a combined set.
        NavigableSet<RlUnit> rlUnits = new TreeSet<RlUnit>();
        rlUnits.addAll(start);
        rlUnits.addAll(middle);
        rlUnits.addAll(end);
        this.rlDb.setRlUnits(rlUnits);

        // Assign Ids
        // Create a map that can map the RLUnit
        idNumMap = this.mapRlUnitsToVariables(start, middle, end);

        StringBuilder sb = loadTemplateModel();

        if (sentence != null) {
            sb.append("set F;\n");
            sb.append("param textsim{i in F};\n");
        }

        sb.append(String.format("maximize triptychsimilarity: sum{i in START, j in MIDDLE, k in FINISH : i != j && j != k && i != k } " +
                        "(c[i,j] * x[i,j,k] * %s + c[j,k] * x[i,j,k] * %s + d[i,j] * " +
                        "x[i,j,k] * %s + d[j,k] * x[i,j,k] * %s + " +
                        " b[i,j] * x[i,j,k] * %s + b[j,k] * x[i,j,k] * %s + " +
                        " a[i,j] * x[i,j,k] * %s + a[j,k] * x[i,j,k] * %s ", criteria.getTextSimilarityWeight(),
                criteria.getTextSimilarityWeight(), criteria.getImageSimilarityWeight(), criteria.getImageSimilarityWeight(),
                criteria.getPredictionSimilarityWeight(), criteria.getPredictionSimilarityWeight(),
                criteria.getParagraphSimilarityWeight(), criteria.getParagraphSimilarityWeight()));

        if (sentence != null) {
            sb.append(String.format(" + x[i,j,k] * textsim[i] * %s + x[i,j,k] * textsim[j] * %s + x[i,j,k] * textsim[k] * %s ",
                    criteria.getSentenceSimilarityWeight(), criteria.getSentenceSimilarityWeight(), criteria.getSentenceSimilarityWeight()));
        }
        sb.append(");\n");

        sb.append(String.format("triptichlength: sum{i in START, j in MIDDLE, k in FINISH : i != j && j != k && i != k } x[i,j,k] = %s;\n"
                , numberOfTriptychs));

        // Append a constraint on max similarity passed through from the Criteria object.
        addThresholdCriteria(criteria, sb);

        sb.append("data; \n");

        sb.append(String.format("param n := %s;\n", rlUnits.size()));

        this.addPositionalSets(start, middle, end, this.idNumMap, sb);

        sb.append("param : E : c d b a:=  \n");

        // Create matrices with similarities between the text and images..
        Integer numberOfImages = rlUnits.size();


        INDArray textSimilarityArray = Nd4j.zeros(numberOfImages, numberOfImages).addi(ILPImageSelectionService.DEFAULT_SIMILARITY);
        // Image similarities.
        INDArray imageSimilarityArray = Nd4j.zeros(numberOfImages, numberOfImages).addi(ILPImageSelectionService.DEFAULT_SIMILARITY);
        INDArray predictionSimilarityArray = Nd4j.zeros(numberOfImages, numberOfImages).addi(ILPImageSelectionService.DEFAULT_SIMILARITY);
        INDArray paragraphSimilarityArray = Nd4j.zeros(numberOfImages, numberOfImages).addi(ILPImageSelectionService.DEFAULT_SIMILARITY);


        if (criteria.getCalculateSimilarities()) {

            // Use the paths provided to load the image features.
            if (criteria.getTextSimilarityWeight() != null && criteria.getTextSimilarityWeight() != 0.0) {
                wordEmbeddingFeatureRepository.loadFeatures(criteria.getWordEmbeddings());
                wordEmbeddingFeatureRepository.setExcludeStopWords(criteria.getExcludeStopWords());
                wordEmbeddingFeatureRepository.fillSimilarityArray(start, middle, this.idNumMap, textSimilarityArray);
                wordEmbeddingFeatureRepository.fillSimilarityArray(middle, end, this.idNumMap, textSimilarityArray);
            }


            if (criteria.getImageSimilarityWeight() != null && criteria.getImageSimilarityWeight() != 0.0) {
                imageFeatureRepository.loadFeatures(criteria.getImageFeatures());
                imageFeatureRepository.fillSimilarityArray(start, middle, this.idNumMap, imageSimilarityArray);
                imageFeatureRepository.fillSimilarityArray(middle, end, this.idNumMap, imageSimilarityArray);
            }

            if (criteria.getPredictionSimilarityWeight() != null && criteria.getPredictionSimilarityWeight() != 0.0) {
                predictionFeatureRepository.loadFeatures(criteria.getPredictionFeatures());
                predictionFeatureRepository.fillSimilarityArray(start, middle, this.idNumMap, predictionSimilarityArray);
                predictionFeatureRepository.fillSimilarityArray(middle, end, this.idNumMap, predictionSimilarityArray);
            }

            if (criteria.getParagraphSimilarityWeight() != null && criteria.getParagraphSimilarityWeight() != 0.0) {
                paragraphEmbeddingFeatureRepository.loadFeatures(criteria.getParagraphEmbeddings());
                paragraphEmbeddingFeatureRepository.fillSimilarityArray(start, middle, this.idNumMap, paragraphSimilarityArray);
                paragraphEmbeddingFeatureRepository.fillSimilarityArray(middle, end, this.idNumMap, paragraphSimilarityArray);
            }

        } else {
            // Depracated: Should remove the precalculation similarities.
            this.fillSimilarityArray(start, middle, this.idNumMap, textSimilarityArray, FeatureType.COSINE_SIMILARITY_EMBEDDING);
            this.fillSimilarityArray(middle, end, this.idNumMap, textSimilarityArray, FeatureType.COSINE_SIMILARITY_EMBEDDING);

            this.fillSimilarityArray(start, middle, this.idNumMap, imageSimilarityArray, FeatureType.COSINE_SIMILARITY_IMAGE_EMBEDDING);
            this.fillSimilarityArray(middle, end, this.idNumMap, imageSimilarityArray, FeatureType.COSINE_SIMILARITY_IMAGE_EMBEDDING);
        }

        this.writeMatrix(sb, numberOfImages, textSimilarityArray, imageSimilarityArray,
                predictionSimilarityArray, paragraphSimilarityArray);
        textSimilarityArray = null;
        imageSimilarityArray = null;
        predictionSimilarityArray = null;
        paragraphSimilarityArray = null;


        // Write the text similarity data to the criteria.
        if (sentence != null) {
            sb.append("param : F : textsim := ");
            INDArray sentenceQuerySimilarities = null;
            if (criteria.getSentenceUseWordEmbeddings()) {
                sentenceQuerySimilarities = wordEmbeddingFeatureRepository.similaritiesToQuery(
                        sentenceQuery, rlUnits, this.idNumMap);
            } else {
                sentenceQuerySimilarities = paragraphEmbeddingFeatureRepository.similaritiesToQuery(
                        criteria.getSentence(), rlUnits, this.idNumMap);
            }
            for (int i = 0; i < rlUnits.size(); i++) {
                sb.append(String.format("%s %s \n", i + 1, sentenceQuerySimilarities.getScalar(0, i)));
            }
            sb.append(";\n");
        }

        sb.append("end;\n"); // Last line of the model file.

        // Write the model to be executed.
        Files.write(sb, new File(modelFileName), Charset.defaultCharset());

        // reclaim memory. Make eligible for GC.
        sb = null;


        ILPImageSelectionService.GLPK_Setup GLPK_Setup = new ILPImageSelectionService.GLPK_Setup().invoke();
        this.lp = GLPK_Setup.getLp();
        glp_iocp iocp = GLPK_Setup.getIocp();
        glp_tran tran = GLPK_Setup.getTran();

        List<String[]> idList = new ArrayList<String[]>(3);

        idList.clear();
        int ret = GLPK.glp_intopt(this.lp, iocp);

        // Retrieve solution
        if (ret == 0) {

            int n = GLPK.glp_get_num_cols(this.lp);
            double objectiveValue = GLPK.glp_get_obj_val(this.lp);
            this.logger.info("Optimiser objective value = {}", objectiveValue);

        } else {
            throw new IllegalStateException("Linear solver could not identify a Triptych.");
        }

        triptychs.addAll(convertToTriptychs(this.rlDb, this.idNumMap, idList));


        cleanupGlpk(tran, this.lp);
        return triptychs;
    }

    /**
     * Add the optional threshold criteria if set.
     *
     * @param criteria from the query
     * @param sb       the Model
     */
    private void addThresholdCriteria(SelectionCriteria criteria, StringBuilder sb) {
        if (criteria.getMaxTextSimilarity() > 0.0) {

            double similarity = criteria.getMaxTextSimilarity();
            if (criteria.getTextSimilarityWeight() >= 0.0) {
                sb.append(String.format("s.t. maxtextsimilarityfortriptych {i in START, j in MIDDLE, k in FINISH : " +
                                "i != j && j != k && i != k }: c[i,j] * x[i,j,k] + c[j,k] * x[i,j,k] <= %s;\n",
                        similarity + similarity));
            } else {
                // If the weight is negative then need to use as a sum as the required weight will be away from 0.
                similarity = 1 - similarity; // Reverse the similarity if negative.
                sb.append(String.format("s.t. maxtextsimilarityfortriptych: sum{i in START, j in MIDDLE, k in FINISH : " +
                                "i != j && j != k && i != k } " +
                                "(c[i,j] * x[i,j,k] * %s + c[j,k] * x[i,j,k] * %s) <= %s;\n",
                        criteria.getTextSimilarityWeight(), criteria.getTextSimilarityWeight(),
                        (similarity + similarity) * criteria.getTextSimilarityWeight() * numberOfTriptychs));
            }


        }
        if (criteria.getMaxImageSimilarity() > 0.0) {
            double similarity = criteria.getMaxImageSimilarity();
            if (criteria.getImageSimilarityWeight() > 0.0) {
                sb.append(String.format("s.t. maximagesimilarityfortriptych {i in START, j in MIDDLE, k in FINISH : " +
                                "i != j && j != k && i != k }: d[i,j] * x[i,j,k] + d[j,k] * x[i,j,k] <= %s;\n",
                        similarity + similarity));
            } else {
                // If the weight is negative then need to use as a sum as the required weight will be away from 0.
                similarity = 1 - similarity; // Reverse the similarity if negative.
                sb.append(String.format("s.t. maximagesimilarityfortriptych: sum{i in START, j in MIDDLE, k in FINISH : " +
                                "i != j && j != k && i != k } " +
                                "(d[i,j] * x[i,j,k] * %s + d[j,k] * x[i,j,k] * %s) <= %s;\n",
                        criteria.getImageSimilarityWeight(), criteria.getImageSimilarityWeight(),
                        (similarity + similarity) * criteria.getImageSimilarityWeight() * numberOfTriptychs));
            }
        }

        if (criteria.getMaxPredictionSimilarity() > 0.0) {
            double similarity = criteria.getMaxPredictionSimilarity();
            if (criteria.getPredictionSimilarityWeight() > 0.0) {
                sb.append(String.format("s.t. maxpredictionsimilarityfortriptych {i in START, j in MIDDLE, k in FINISH : " +
                                "i != j && j != k && i != k }: b[i,j] * x[i,j,k] + b[j,k] * x[i,j,k] <= %s;\n",
                        similarity + similarity));
            } else {
                // If the weight is negative then need to use as a sum as the required weight will be away from 0.
                similarity = 1 - similarity; // Reverse the similarity if negative.
                sb.append(String.format("s.t. maxpredictionsimilarityfortriptych: sum{i in START, j in MIDDLE, k in FINISH : " +
                                "i != j && j != k && i != k } " +
                                "(b[i,j] * x[i,j,k] * %s + b[j,k] * x[i,j,k] * %s) <= %s;\n",
                        criteria.getPredictionSimilarityWeight(), criteria.getPredictionSimilarityWeight(),
                        (similarity + similarity) * criteria.getPredictionSimilarityWeight() * numberOfTriptychs));
            }
        }
    }

    /**
     * Write the positional datasets to the model file.
     *
     * @param start
     * @param middle
     * @param end
     * @param idNumMap
     * @param sb
     */
    private void addPositionalSets(Set<RlUnit> start, Set<RlUnit> middle, Set<RlUnit> end,
                                   BiMap<String, Integer> idNumMap, StringBuilder sb) {
        sb.append("set START := ");
        for (RlUnit s : start) {
            sb.append(String.format(" %s ", idNumMap.get(s.getId())));
        }
        sb.append(";\n");
        sb.append("set MIDDLE := ");
        for (RlUnit s : middle) {
            sb.append(String.format(" %s ", idNumMap.get(s.getId())));
        }
        sb.append(";\n");
        sb.append("set FINISH := ");
        for (RlUnit s : end) {
            sb.append(String.format(" %s ", idNumMap.get(s.getId())));
        }
        sb.append(";\n");
    }

    /**
     * Converts the ids returned by GLPK back into a list of RlUnits.
     *
     * @param rlDb     the database with RlUnits to lookup.
     * @param idNumMap mapping GLPK ids to RlUnit ids.
     * @param idList   with Ids returned by GLPK.
     * @return List<RlUnit>
     */
    private List<RlUnit> convertToTriptych(RlDatabase rlDb, BiMap<String, Integer> idNumMap, List<String[]> idList) {
        List<RlUnit> triptych = new ArrayList<RlUnit>();

        if (idList.get(0).length == 3) {
            triptych.add(rlDb.getRlUnitFromId(idNumMap.inverse().get(Integer.parseInt(idList.get(0)[0]))));
            triptych.add(rlDb.getRlUnitFromId(idNumMap.inverse().get(Integer.parseInt(idList.get(0)[1]))));
            triptych.add(rlDb.getRlUnitFromId(idNumMap.inverse().get(Integer.parseInt(idList.get(0)[2]))));
            return triptych;
        }
        for (String[] ids : idList) {

            RlUnit startRl = rlDb.getRlUnitFromId(idNumMap.inverse().get(Integer.parseInt(ids[0])));
            RlUnit endRl = rlDb.getRlUnitFromId(idNumMap.inverse().get(Integer.parseInt(ids[1])));

            // Get the closest RLUnit that hae been found.
            if (!triptych.contains(startRl)) triptych.add(startRl);
            if (!triptych.contains(endRl)) triptych.add(endRl);
        }
        return triptych;
    }

    /**
     * Converts the ids returned by GLPK back into a list of RlUnits.
     *
     * @param rlDb     the database with RlUnits to lookup.
     * @param idNumMap mapping GLPK ids to RlUnit ids.
     * @param idList   with Ids returned by GLPK.
     * @return List <List<RlUnit>>
     */
    private List<List<RlUnit>> convertToTriptychs(RlDatabase rlDb, BiMap<String, Integer> idNumMap, List<String[]> idList) {
        List<List<RlUnit>> triptychs = new ArrayList<List<RlUnit>>();

        for (String[] ids : idList) {
            List<RlUnit> triptych = new ArrayList<RlUnit>();
            triptych.add(rlDb.getRlUnitFromId(idNumMap.inverse().get(Integer.parseInt(ids[0]))));
            triptych.add(rlDb.getRlUnitFromId(idNumMap.inverse().get(Integer.parseInt(ids[1]))));
            triptych.add(rlDb.getRlUnitFromId(idNumMap.inverse().get(Integer.parseInt(ids[2]))));
            triptychs.add(triptych);
        }

        return triptychs;
    }


    /**
     * Write the similarity matrix into the model file.
     *
     * @param sb                        the output String.
     * @param numberOfImages            in the matrix.
     * @param textSimilarityArray
     * @param imageSimilarityArray      the matrix to write.
     * @param predictionSimilarityArray
     * @param paragraphSimilarityArray
     */
    private void writeMatrix(StringBuilder sb, Integer numberOfImages, INDArray textSimilarityArray,
                             INDArray imageSimilarityArray, INDArray predictionSimilarityArray, INDArray paragraphSimilarityArray) {
        // Iterate over the matrix writing the weights to the model.
        NdIndexIterator iter = new NdIndexIterator(numberOfImages, numberOfImages);
        while (iter.hasNext()) {
            int[] nextIndex = iter.next();
            double nextText = textSimilarityArray.getDouble(nextIndex);
            double nextImage = imageSimilarityArray.getDouble(nextIndex);
            double nextPrediction = predictionSimilarityArray.getDouble(nextIndex);
            double nextParagraph = paragraphSimilarityArray.getDouble(nextIndex);

            sb.append(String.format("%s %s %s %s %s %s \n", nextIndex[0] + 1, nextIndex[1] + 1, nextText, nextImage,
                    nextPrediction, nextParagraph));
        }
        sb.append(";\n");
    }

    /**
     * Fills and INDArray with similarities of a given type in the mapped attribute position.
     *
     * @param first           source of similarity.
     * @param second          destination of similarity.
     * @param idNumMap        mapping from id to position.
     * @param similarityArray to fill.
     * @param featureType     type of similarity to use.
     */
    private void fillSimilarityArray(Set<RlUnit> first, Set<RlUnit> second, BiMap<String, Integer> idNumMap, INDArray similarityArray,
                                     FeatureType featureType) {
        first.parallelStream().forEach(rl -> {
            // Only supports one similarity per image at the moment.
            for (SimilarityMeasure similarityMeasure : rl.getSimilarityMeasures()) {

                if (!featureType.getName().equals(similarityMeasure.getType())) {
                    continue; // Skip over if not the required feature type.
                }
                // Populate those similarities that have haves values.
                Set<RlUnit> inSimilarities = new TreeSet<RlUnit>();
                similarityMeasure.getSimilarities().stream().filter(s -> second.contains(new RlUnit(s.getId()))).forEach(s -> {
                    int[] indices = {idNumMap.get(rl.getId()).intValue() - 1, idNumMap.get(s.getId()).intValue() - 1};
                    inSimilarities.add(new RlUnit(s.getId()));
                    similarityArray.putScalar(indices, s.getValue());
                });

                // Use the mean where there is no value.
                SetView<RlUnit> onlyInSecond = Sets.difference(second, first);
                // Populate with the mean.
                for (RlUnit noValueRl : onlyInSecond) {
                    int[] indices = {idNumMap.get(rl.getId()).intValue() - 1, idNumMap.get(noValueRl.getId()).intValue() - 1};
                    if (similarityArray.getDouble(indices) <= 0.0) {
                        similarityArray.putScalar(indices, similarityMeasure.getMean());
                    }
                }

            }
        });
    }

    /**
     * map RlUnit ids to sequential variable numbers that can be used in the GLPK model.
     *
     * @param start  RlUnits
     * @param middle RlUnits
     * @param end    RlUnits
     */
    private BiMap<String, Integer> mapRlUnitsToVariables(Set<RlUnit> start, Set<RlUnit> middle, Set<RlUnit> end) {
        BiMap<String, Integer> idNumMap = HashBiMap.create();
        int counter = 1;
        for (RlUnit rl : start) {
            idNumMap.put(rl.getId(), counter);
            counter++;
        }
        for (RlUnit rl : middle) {
            if (idNumMap.containsKey(rl.getId())) {
                continue;
            }
            idNumMap.put(rl.getId(), counter);
            counter++;
        }
        for (RlUnit rl : end) {
            if (idNumMap.containsKey(rl.getId())) {
                continue;
            }
            idNumMap.put(rl.getId(), counter);
            counter++;
        }
        return idNumMap;
    }

    /**
     * Get units linked to a central image.
     *
     * @param rlDb        database.
     * @param middleImage the linked image.
     * @return NavigableSet<RlUnit>
     */
    private NavigableSet<RlUnit> getLinkedRlUnits(RlDatabase rlDb, RlUnit middleImage) {
        NavigableSet<RlUnit> prefilteredUnits = new TreeSet<RlUnit>();
        prefilteredUnits.add(middleImage);
        // Iterate through and put the similarity edges into the array.
        for (RlUnit rl : rlDb.getRlUnits()) {
            // Only supports one similarity per image at the moment.
            List<Similarity> similarities = rl.getSimilarityMeasures().get(0).getSimilarities();
            for (Similarity s : similarities) {

                // Add links from the middle image to others.
                if (middleImage.getId().equals(rl.getId())) {
                    prefilteredUnits.add(rlDb.getRlUnitFromId(s.getId()));
                }
                // Add links from other images to the middle image.
                else if (middleImage.getId().equals(s.getId())) {
                    prefilteredUnits.add(rl);
                }


            }
        }
        return prefilteredUnits;
    }

    /**
     * Add a constraint for the unjoined Triptych so that it won't be found next time.
     *
     * @param lp     GLPk problem.
     * @param idList the Ids identified.
     */
    private void addConstraintStoppingSelection(glp_prob lp, List<String[]> idList) {
        logger.info("Add stopping constraint for solution = {}", idList);
        int rows = GLPK.glp_get_num_rows(lp);

        int newRow = rows + 1;
        GLPK.glp_add_rows(lp, 1);


        GLPK.glp_set_row_name(lp, newRow, "st" + newRow);
        GLPK.glp_set_row_bnds(lp, newRow, GLPKConstants.GLP_UP, 0.0, 1.0);

        int pos = 1;

        SWIGTYPE_p_int ind = GLPK.new_intArray(3);
        SWIGTYPE_p_double newVal = GLPK.new_doubleArray(3);

        for (String[] ids : idList) {
            GLPK.intArray_setitem(ind, pos, GLPK.glp_find_col(lp, String.format("x[%s,%s]", ids[0], ids[1])));
            pos++;
        }

        GLPK.doubleArray_setitem(newVal, 1, 1);
        GLPK.doubleArray_setitem(newVal, 2, 1);

        GLPK.glp_set_mat_row(lp, newRow, 2, ind, newVal);

        // Free memory.
        GLPK.delete_intArray(ind);
        GLPK.delete_doubleArray(newVal);

    }

    /**
     * Loads the template model.
     *
     * @return StringBuilder
     * @throws IOException
     */
    private StringBuilder loadTemplateModel() throws IOException {
        // Load the MathProg template for the solver
        InputStream inputStream = ILPImageSelectionService.class.getClassLoader().getResourceAsStream("triptych.mod");
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, "UTF-8");
        return new StringBuilder(writer.toString());
    }

    /**
     * Cleanup GLPK, reclaim memory.
     *
     * @param tran the transaction.
     * @param lp   the problem.
     */
    private void cleanupGlpk(glp_tran tran, glp_prob lp) {
        GlpkCallback.removeListener(this);
        GlpkTerminal.removeListener(this);

        // free memory
        GLPK.glp_mpl_free_wksp(tran);
        GLPK.glp_delete_prob(lp);

    }

    @Override
    public void callback(glp_tree glp_tree) {
        int reason = GLPK.glp_ios_reason(glp_tree);
        if (reason == GLPKConstants.GLP_IBINGO) {
            int n = GLPK.glp_get_num_cols(this.lp);

            // Iterate over all the columns.
            List<String[]> idList = this.extractIdsFromVariables(this.lp, n);
            List<List<RlUnit>> triptychs = convertToTriptychs(this.rlDb, this.idNumMap, idList);
            double objectiveValue = GLPK.glp_get_obj_val(this.lp);
            for (List<RlUnit> triptych : triptychs) {
                this.logger.debug("Solution: Triptych [{}, {}, {}], objective value = {}", triptych.get(0).getId(),
                        triptych.get(1).getId(), triptych.get(2).getId(), objectiveValue);
                solutionsList.put(triptych, objectiveValue, true);
            }
            // Keep a cap on the total solutions so the list doesn't get too big in cases of a direct search.
            solutionsList.keepTopNKeys(1000);

        } else if (reason == GLPKConstants.GLP_ICUTGEN) {

        }
    }

    @Override
    public boolean output(String s) {
        logger.debug("GLPK output = {}", s);
        return true;
    }

    /**
     * Private class for setting up GLPK parameters.
     */
    private class GLPK_Setup {
        private glp_tran tran;
        private glp_prob lp;
        private glp_iocp iocp;

        public glp_tran getTran() {
            return this.tran;
        }

        public glp_prob getLp() {
            return this.lp;
        }

        public glp_iocp getIocp() {
            return this.iocp;
        }

        public ILPImageSelectionService.GLPK_Setup invoke() {
            this.tran = GLPK.glp_mpl_alloc_wksp();
            int o = GLPK.glp_mpl_read_model(this.tran, ILPImageSelectionService.this.modelFileName, 0);
            // Setup for the GLPK solver.
            GlpkCallback.addListener(ILPImageSelectionService.this);
            this.lp = GLPK.glp_create_prob();
            GLPK.glp_set_prob_name(this.lp, "Triptych" + Long.toString(System.nanoTime()));

            // generate model
            GLPK.glp_mpl_generate(this.tran, null);

            // set GCLP parameters.
            this.iocp = new glp_iocp();
            //this.iocp.setTm_lim(60 * 1000); // 1 minutes.
            GLPK.glp_init_iocp(this.iocp);
            this.iocp.setPresolve(GLPKConstants.GLP_OFF);
            GLPK.glp_mpl_build_prob(this.tran, this.lp);
            GLPK.glp_create_index(this.lp);

            // Call Simplex separately and turn off presolve as it stops rerunning presolve each time a constraint is added.
            glp_smcp smcp = new glp_smcp();
            GLPK.glp_init_smcp(smcp);
            GLPK.glp_simplex(this.lp, smcp);

            return this;
        }
    }
}
