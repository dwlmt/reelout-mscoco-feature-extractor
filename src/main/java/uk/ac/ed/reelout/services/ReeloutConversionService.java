package uk.ac.ed.reelout.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.features.RlUnitFilter;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Exports the internal representation to XML.
 */
@Service
public class ReeloutConversionService {

    private final Logger logger = LoggerFactory.getLogger(ReeloutConversionService.class);

    private final XmlMapper mapper;

    public ReeloutConversionService() {
        mapper = new XmlMapper();

        mapper.enable(SerializationFeature.INDENT_OUTPUT); // Pretty printing.
        mapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true); // UTF header.
        //mapper.enable(MapperFeature.USE_WRAPPER_NAME_AS_PROPERTY_NAME);
        mapper.registerModule(new JavaTimeModule()); // Allows JDK 8 date and time fields to be used.
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")); // Export date format.

    }


    /**
     * Convert the RlDatabase domain object to XML in the Reelout format.
     *
     * @param rlDb the ReelOut domain database object.
     * @return xml
     */
    public String convertToXML(RlDatabase rlDb) throws JsonProcessingException {
        String xml = mapper.writeValueAsString(rlDb);
        logger.debug("convertToXml: xml = {}", xml);
        logger.info("convertToXml: RLUnits exported = {}", rlDb.getRlUnits().size());
        return xml;
    }

    /**
     * Convert the RlDatabase domain object to XML in the Reelout format. Writes directly to the file.
     *
     * @param fileName the name of the file to write out to.
     * @param rlDb     the ReelOut domain database object.
     * @throws IOException if the file cannot be written.
     */
    public void convertAndWriteXMLToFile(String fileName, RlDatabase rlDb) throws IOException {
        mapper.writeValue(new File(fileName), rlDb);
        logger.info("convertToXml: RLUnits exported = {}, to destination = {}", rlDb.getRlUnits().size(), fileName);
    }

    /**
     * Convert the ReelOut XML to domain objects.
     *
     * @param xml String of RAW XML.
     * @return RlDatabase
     * @throws IOException if the XML cannot be deserialized correctly.
     */
    public RlDatabase convertToDomain(String xml) throws IOException {
        RlDatabase rlDb = mapper.readValue(xml, RlDatabase.class);
        logger.debug("convertToDomain: RlDatabase = {}", rlDb);
        logger.info("convertToDomain: RLUnits imported = {}", rlDb.getRlUnits().size());
        return rlDb;
    }

    /**
     * Convert the ReelOut XML to domain objects from a File.
     *
     * @param fileName The filename to read from.
     * @return RlDatabase
     * @throws IOException if the XML cannot be deserialized correctly.
     */
    public RlDatabase convertToDomainFromFile(String fileName) throws IOException {
        RlDatabase rlDb = mapper.readValue(new File(fileName), RlDatabase.class);
        logger.debug("convertToDomain: RlDatabase = {}", rlDb);
        logger.info("convertToDomain: RLUnits imported = {}", Integer.valueOf(rlDb.getRlUnits().size()));
        return rlDb;
    }

    /**
     * Join the Rl Units from two RlDatabases together.
     *
     * @param db1 first RlDatabase.
     * @param db2 second RlDatabase.
     * @return RlDatabase
     */
    public RlDatabase join(RlDatabase db1, RlDatabase db2) {
        RlDatabase result = new RlDatabase();
        result.setVersion(db1.getVersion()); // Assume both are the same version.
        result.getRlUnits().addAll(db1.getRlUnits());
        result.getRlUnits().addAll(db2.getRlUnits());
        logger.info("Combined databases with RL Units = {}, and Rl Units = {}, total combined = {} ",
                db1.getRlUnits().size(), db2.getRlUnits().size(), result.getRlUnits().size());
        return result;
    }

    /**
     * Cut a database according to a range specified. If one of the Ids is null then will return the tail or head
     * of the list.
     *
     * @param db           first RlDatabase.
     * @param fromIdFilter from Id.
     * @param toIdFilter   from Id..
     * @return RlDatabase recut.
     */
    public RlDatabase cut(RlDatabase db, String fromIdFilter, String toIdFilter) {
        RlDatabase res = new RlDatabase(db.getVersion());
        res.setRlUnits(new ConcurrentSkipListSet<RlUnit>(RlUnitFilter.getFilteredRlUnits(db, fromIdFilter, toIdFilter)));
        return res;
    }

}
