package uk.ac.ed.reelout.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.domain.SelectionCriteria;
import uk.ac.ed.reelout.exceptions.TriptychSelectionException;

import java.io.IOException;
import java.util.*;

/**
 * Randomly selects 3 images from the database.
 */
@Service
public class RandomImageSelectionService implements ImageSelectionService {

    private final Logger logger = LoggerFactory.getLogger(RandomImageSelectionService.class);

    /**
     * Select images according to the provided Criteria.
     *
     * @param rlDb     the RlDatabase containing the image features.
     * @param criteria @return the selected images.
     * @rlDb the image database containing RLUnits.
     */
    @Override
    public List<RlUnit> selectTriptychImages(RlDatabase rlDb, SelectionCriteria criteria) {

        this.logger.info("Select random images: rlDb image count = {}, criteria = {}", rlDb.getRlUnits().size(), criteria);

        ArrayList<RlUnit> rlUnitList = new ArrayList<RlUnit>(rlDb.getRlUnits());

        // Shuffle and select the first 3 images.
        Collections.shuffle(rlUnitList);
        List<RlUnit> subList = rlUnitList.subList(0, 3); // Select the first 3 images.

        this.logger.info("Triptych images selected are:");
        for (RlUnit rl : subList) {
            this.logger.info("Image: id = {}, image url = {}, messageText = {}", rl.getId(), rl.getImage(), rl.getMessageText());
        }

        return subList;

    }

    /**
     * Randomly selects images from those that
     *
     * @param start           set of RlUnits that can be in the start position of the Triptych.
     * @param middle          set of RlUnits that can be in the middle position of the Triptych.
     * @param end             set of RlUnits that can be in the end position of the Triptych.
     * @param criteria        / constraints to apply to Triptych selection.
     * @param numberOfStories how many n best Triptychs to select.
     * @return List <List<RlUnit>> multiple Triptychs represented within the inner list.
     * @throws IOException
     */
    @Override
    public List<List<RlUnit>> selectTriptychWithPositions(Set<RlUnit> start, Set<RlUnit> middle, Set<RlUnit> end, SelectionCriteria criteria, Integer numberOfStories) throws IOException {
        if (start.isEmpty() || middle.isEmpty() || end.isEmpty()) {
            throw new TriptychSelectionException("Cannot return a valid Triptych as some slots are empty. Please change the criteria.");
        }

        List<RlUnit> startList = new ArrayList<RlUnit>(start);
        List<RlUnit> middleList = new ArrayList<RlUnit>(middle);
        List<RlUnit> endList = new ArrayList<RlUnit>(end);

        List<List<RlUnit>> triptychs = new ArrayList<List<RlUnit>>();

        Random random = new Random();

        for (int i = 0; i < numberOfStories; i++) {
            List<RlUnit> triptych = new ArrayList<RlUnit>();
            triptych.add(startList.get(random.nextInt(startList.size())));
            triptych.add(middleList.get(random.nextInt(middleList.size())));
            triptych.add(endList.get(random.nextInt(endList.size())));

            triptychs.add(triptych);
        }


        return triptychs;
    }
}
