package uk.ac.ed.reelout.services;

import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.domain.SelectionCriteria;

import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * Interface for an image select service.
 */
public interface ImageSelectionService {

    /**
     * Select images according to the provided Criteria.
     *
     * @param criteria
     * @return the selected images.
     * @rlDb the image database containing RLUnits.
     */
    List<RlUnit> selectTriptychImages(RlDatabase rlDb, SelectionCriteria criteria) throws IOException;


    /**
     * @param start           set of RlUnits that can be in the start position of the Triptych.
     * @param middle          set of RlUnits that can be in the middle position of the Triptych.
     * @param end             set of RlUnits that can be in the end position of the Triptych.
     * @param criteria        / constraints to apply to Triptych selection.
     * @param numberOfStories how many n best Triptychs to select.
     * @return List <List<RlUnit>> multiple Triptychs represented within the inner list.
     * @throws IOException
     */
    List<List<RlUnit>> selectTriptychWithPositions(Set<RlUnit> start, Set<RlUnit> middle, Set<RlUnit> end,
                                                   SelectionCriteria criteria, Integer numberOfStories) throws IOException;
}
