package uk.ac.ed.reelout.domain;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;

import java.io.Serializable;

/**
 * Simple representation for keeping distinct features and values.
 */
public class ReeloutStat implements Comparable<ReeloutStat>, Serializable {

    private final String feature;
    private final String discriminator;
    private final String value;

    /**
     * Default constructor.
     *
     * @param feature
     * @param discriminator
     * @param value
     */
    public ReeloutStat(String feature, String discriminator, String value) {
        this.feature = feature;
        this.discriminator = discriminator;
        this.value = value;
    }

    /**
     * Used to discriminate if there are multiple counts for the same feature.
     */
    public String getDiscriminator() {
        return this.discriminator;
    }

    /**
     * Name of the feature.
     */
    public String getFeature() {
        return this.feature;
    }

    /**
     * Value of the feature.
     */
    public String getValue() {
        return this.value;
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("feature", this.feature)
                .add("discriminator", this.discriminator)
                .add("value", this.value)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ReeloutStat that = (ReeloutStat) o;
        return Objects.equal(this.feature, that.feature) &&
                Objects.equal(this.discriminator, that.discriminator) &&
                Objects.equal(this.value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.feature, this.value);
    }

    @Override
    public int compareTo(ReeloutStat o) {
        return ComparisonChain.start()
                .compare(this.feature, o.feature)
                .compare(this.discriminator, o.discriminator)
                .compare(this.value, o.value)
                .result();
    }

    /**
     * Simple formatting to pipe for output.
     *
     * @return String
     */
    public String toPipeDelimited() {
        return String.format("%s | %s | %s", this.feature, this.discriminator, this.value);
    }
}
