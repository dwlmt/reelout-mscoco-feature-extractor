package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * This is an awkward workaround holder object to make it simpler to match the Serialization format to XML:
 * <sentiment><value>positive</value></sentiment>
 */
public class SentimentHolder implements Serializable {

    @JacksonXmlProperty(localName = "value")
    private Sentiment sentiment;

    private SentimentHolder() {
    }

    public SentimentHolder(Sentiment sentiment) {
        this();
        this.sentiment = sentiment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SentimentHolder that = (SentimentHolder) o;
        return this.sentiment == that.sentiment;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.sentiment);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("sentiment", this.sentiment)
                .toString();
    }

    public Sentiment getSentiment() {
        return this.sentiment;
    }

    public void setSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
    }
}
