package uk.ac.ed.reelout.domain;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import uk.ac.ed.reelout.selection.StoppingCondition;

/**
 * Object for specifying criteria for selecting images.
 */
public class SelectionCriteria {

    /**
     * Change the max text similarity.
     */
    Double maxTextSimilarity;
    /**
     * Change the max image similarity across the Triptych.
     */
    Double maxImageSimilarity;

    /**
     * Weighting for the text similarity. Negative weights can be used for divergence rather than similarity.
     */
    Double textSimilarityWeight;

    /**
     * Weighting for the image similarity. Negative weights can be used for divergence rather than similarity.
     */
    Double imageSimilarityWeight;

    /**
     * Weighting for the Paragraph Vector similarity.
     */
    Double paragraphSimilarityWeight;

    /**
     * Max prediction similarity threshold.
     */
    Double maxParagraphSimilarity;

    /**
     * Holds the details of the retries allowed over the search space.
     */
    StoppingCondition stoppingCondition;

    /**
     * The word embeddings file either in Glove or Word2Vec format.
     */
    String wordEmbeddings;
    /**
     * The Doc2Vec trained paragraph embeddings.
     */
    String paragraphEmbeddings;
    /**
     * Path to the directory with image features.
     */
    String imageFeatures;
    /**
     * Path to prediction features.
     */
    String predictionFeatures;
    /**
     * Weight applied to the feature.
     */
    Double predictionSimilarityWeight;
    /**
     * Max prediction similarity threshold.
     */
    Double maxPredictionSimilarity;
    /**
     * Calculate Similarities on demand rather than from the feature file usisng the provided word embeddings
     * and image features.
     */
    Boolean calculateSimilarities;
    /**
     * Natural language query.
     */
    String sentence;
    /**
     * Use word embeddings on the natural sentence mapping or try to use the paragraph mapping.
     */
    Boolean sentenceUseWordEmbeddings;
    /**
     * Weights for similarity with the natural language query.
     */
    Double sentenceSimilarityWeight;
    Boolean excludeStopWords;

    public SelectionCriteria() {
        maxTextSimilarity = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SelectionCriteria that = (SelectionCriteria) o;
        return Objects.equal(maxTextSimilarity, that.maxTextSimilarity) &&
                Objects.equal(maxImageSimilarity, that.maxImageSimilarity) &&
                Objects.equal(textSimilarityWeight, that.textSimilarityWeight) &&
                Objects.equal(imageSimilarityWeight, that.imageSimilarityWeight) &&
                Objects.equal(paragraphSimilarityWeight, that.paragraphSimilarityWeight) &&
                Objects.equal(maxParagraphSimilarity, that.maxParagraphSimilarity) &&
                Objects.equal(stoppingCondition, that.stoppingCondition) &&
                Objects.equal(wordEmbeddings, that.wordEmbeddings) &&
                Objects.equal(paragraphEmbeddings, that.paragraphEmbeddings) &&
                Objects.equal(imageFeatures, that.imageFeatures) &&
                Objects.equal(predictionFeatures, that.predictionFeatures) &&
                Objects.equal(predictionSimilarityWeight, that.predictionSimilarityWeight) &&
                Objects.equal(maxPredictionSimilarity, that.maxPredictionSimilarity) &&
                Objects.equal(calculateSimilarities, that.calculateSimilarities) &&
                Objects.equal(sentence, that.sentence) &&
                Objects.equal(sentenceUseWordEmbeddings, that.sentenceUseWordEmbeddings) &&
                Objects.equal(sentenceSimilarityWeight, that.sentenceSimilarityWeight) &&
                Objects.equal(excludeStopWords, that.excludeStopWords);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(maxTextSimilarity, maxImageSimilarity, textSimilarityWeight,
                imageSimilarityWeight, paragraphSimilarityWeight, maxParagraphSimilarity,
                stoppingCondition, wordEmbeddings, paragraphEmbeddings, imageFeatures,
                predictionFeatures, predictionSimilarityWeight, maxPredictionSimilarity,
                calculateSimilarities, sentence, sentenceUseWordEmbeddings,
                sentenceSimilarityWeight, excludeStopWords);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("maxTextSimilarity", this.maxTextSimilarity)
                .add("maxImageSimilarity", this.maxImageSimilarity)
                .add("textSimilarityWeight", this.textSimilarityWeight)
                .add("imageSimilarityWeight", this.imageSimilarityWeight)
                .add("paragraphSimilarityWeight", this.paragraphSimilarityWeight)
                .add("maxParagraphSimilarity", this.maxParagraphSimilarity)
                .add("stoppingCondition", this.stoppingCondition)
                .add("wordEmbeddings", this.wordEmbeddings)
                .add("paragraphEmbeddings", this.paragraphEmbeddings)
                .add("imageFeatures", this.imageFeatures)
                .add("predictionFeatures", this.predictionFeatures)
                .add("predictionSimilarityWeight", this.predictionSimilarityWeight)
                .add("maxPredictionSimilarity", this.maxPredictionSimilarity)
                .add("calculateSimilarities", this.calculateSimilarities)
                .add("sentence", this.sentence)
                .add("sentenceUseWordEmbeddings", this.sentenceUseWordEmbeddings)
                .add("sentenceSimilarityWeight", this.sentenceSimilarityWeight)
                .add("excludeStopWords", this.excludeStopWords)
                .toString();
    }

    public Boolean getSentenceUseWordEmbeddings() {
        return sentenceUseWordEmbeddings;
    }

    public void setSentenceUseWordEmbeddings(Boolean sentenceUseWordEmbeddings) {
        this.sentenceUseWordEmbeddings = sentenceUseWordEmbeddings;
    }

    public Double getParagraphSimilarityWeight() {
        return paragraphSimilarityWeight;
    }

    public void setParagraphSimilarityWeight(Double paragraphSimilarityWeight) {
        this.paragraphSimilarityWeight = paragraphSimilarityWeight;
    }

    public Double getMaxParagraphSimilarity() {
        return maxParagraphSimilarity;
    }

    public void setMaxParagraphSimilarity(Double maxParagraphSimilarity) {
        this.maxParagraphSimilarity = maxParagraphSimilarity;
    }

    public String getParagraphEmbeddings() {
        return paragraphEmbeddings;
    }

    public void setParagraphEmbeddings(String paragraphEmbeddings) {
        this.paragraphEmbeddings = paragraphEmbeddings;
    }

    public Double getSentenceSimilarityWeight() {
        return sentenceSimilarityWeight;
    }

    public void setSentenceSimilarityWeight(Double sentenceSimilarityWeight) {
        this.sentenceSimilarityWeight = sentenceSimilarityWeight;
    }

    public Double getPredictionSimilarityWeight() {
        return predictionSimilarityWeight;
    }

    public void setPredictionSimilarityWeight(Double predictionSimilarityWeight) {
        this.predictionSimilarityWeight = predictionSimilarityWeight;
    }

    public Double getMaxPredictionSimilarity() {
        return maxPredictionSimilarity;
    }

    public void setMaxPredictionSimilarity(Double maxPredictionSimilarity) {
        this.maxPredictionSimilarity = maxPredictionSimilarity;
    }

    public String getPredictionFeatures() {
        return predictionFeatures;
    }

    public void setPredictionFeatures(String predictionFeatures) {
        this.predictionFeatures = predictionFeatures;
    }

    public String getSentence() {
        return this.sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getWordEmbeddings() {
        return wordEmbeddings;
    }

    public void setWordEmbeddings(String wordEmbeddings) {
        this.wordEmbeddings = wordEmbeddings;
    }

    public String getImageFeatures() {
        return imageFeatures;
    }

    public void setImageFeatures(String imageFeatures) {
        this.imageFeatures = imageFeatures;
    }

    public Boolean getCalculateSimilarities() {
        return calculateSimilarities;
    }

    public void setCalculateSimilarities(Boolean calculateSimilarities) {
        this.calculateSimilarities = calculateSimilarities;
    }

    public StoppingCondition getStoppingCondition() {
        return stoppingCondition;
    }

    public void setStoppingCondition(StoppingCondition stoppingCondition) {
        this.stoppingCondition = stoppingCondition;
    }

    public Double getMaxImageSimilarity() {
        return maxImageSimilarity;
    }

    public void setMaxImageSimilarity(Double maxImageSimilarity) {
        this.maxImageSimilarity = maxImageSimilarity;
    }

    public Double getMaxTextSimilarity() {
        return this.maxTextSimilarity;
    }

    public void setMaxTextSimilarity(Double maxTextSimilarity) {
        this.maxTextSimilarity = maxTextSimilarity;
    }

    public Double getTextSimilarityWeight() {
        return textSimilarityWeight;
    }

    public void setTextSimilarityWeight(Double textSimilarityWeight) {
        this.textSimilarityWeight = textSimilarityWeight;
    }

    public Double getImageSimilarityWeight() {
        return imageSimilarityWeight;
    }

    public void setImageSimilarityWeight(Double imageSimilarityWeight) {
        this.imageSimilarityWeight = imageSimilarityWeight;
    }

    public Boolean getExcludeStopWords() {
        return this.excludeStopWords;
    }

    public void setExcludeStopWords(Boolean excludeStopWords) {
        this.excludeStopWords = excludeStopWords;
    }
}
