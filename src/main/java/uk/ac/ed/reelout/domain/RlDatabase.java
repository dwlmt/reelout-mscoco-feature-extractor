package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Holder class for the RL Unit XML.
 */
@JacksonXmlRootElement(localName = "rldatabase")
public class RlDatabase implements Serializable {

    @JacksonXmlProperty(isAttribute = true)
    private Float version;

    @JacksonXmlProperty(localName = "rlunit")
    @JacksonXmlElementWrapper(useWrapping = false)
    private NavigableSet<RlUnit> rlUnits;

    /**
     * Default constructor.
     */
    public RlDatabase() {
        this.rlUnits = new ConcurrentSkipListSet<RlUnit>();
    }

    /**
     * Contruct a database with a version number.
     *
     * @param version of the schema.
     */
    public RlDatabase(Float version) {
        this();
        this.version = version;
    }


    /**
     * The list of images.
     * Disabling wrapping stops each RlUnit being within a RlUnits holding list.
     */
    public NavigableSet<RlUnit> getRlUnits() {
        return this.rlUnits;
    }

    public void setRlUnits(NavigableSet<RlUnit> rlUnits) {
        this.rlUnits = rlUnits;
    }

    /**
     * Version number for the database.
     */
    /**
     * Getter for the version. Annotation will serialize to XML as an attribute.
     *
     * @return Float
     */
    public Float getVersion() {
        return this.version;
    }

    public void setVersion(Float version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RlDatabase that = (RlDatabase) o;
        return Objects.equal(this.version, that.version) &&
                Objects.equal(this.rlUnits, that.rlUnits);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.version, this.rlUnits);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("version", this.version)
                .add("rlUnits", this.rlUnits)
                .toString();
    }

    /**
     * Add an RlUnit.
     *
     * @param rl RlUnit to add.
     */
    public void addRlUnit(RlUnit rl) {
        this.rlUnits.add(rl);
    }

    /**
     * Get a specific RlUnit based on the Id.
     *
     * @param rlId
     * @return RLUnit
     */
    public RlUnit getRlUnitFromId(String rlId) {
        RlUnit rSearch = new RlUnit(rlId);
        return getRlUnits().tailSet(rSearch).first();
    }
}
