package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import java.io.Serializable;

/**
 * Object to hold similarity measures with another image.
 */
public class Similarity implements Comparable<Similarity>, Serializable {

    /**
     * Id of the other photo.
     */
    @JacksonXmlProperty(isAttribute = true)
    String id;

    /**
     * The similarity between the holding photo and this one.
     */
    @JacksonXmlProperty(isAttribute = true)
    Double value;

    /**
     * @param id    of the other RlUnit.
     * @param value Similarity value.
     */
    private Similarity(String id, Double value) {
        this.id = id;
        this.value = value;
    }

    public Similarity() {
    }

    /**
     * Create similarity.
     *
     * @param id    of the other RlUnit.
     * @param value Similarity value.
     */
    public static Similarity createSimilarity(String id, Double value) {
        return new Similarity(id, value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Similarity that = (Similarity) o;
        return Objects.equal(id, that.id) &&
                Objects.equal(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, value);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", this.id)
                .add("value", this.value)
                .toString();
    }

    /**
     * Ordering is based on the descending value
     *
     * @param o Similarity
     * @return int
     */
    @Override
    public int compareTo(Similarity o) {
        return ComparisonChain.start()
                .compare(value, o.value, Ordering.natural().reversed())
                .compare(id, o.id)
                .result();
    }

    public String getId() {
        return id;
    }

    public Double getValue() {
        return value;
    }
}
