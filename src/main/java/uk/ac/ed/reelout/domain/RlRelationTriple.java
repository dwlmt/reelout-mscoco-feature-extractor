package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;
import edu.stanford.nlp.ie.util.RelationTriple;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Domain object for holding Open IE Object, Relation, and Verb triples.
 */
public class RlRelationTriple implements Comparable<RlRelationTriple>, Serializable {

    /**
     * Confidence in the Relation.
     */
    //@JacksonXmlProperty(isAttribute = true)
    @JsonIgnore
    private
    double confidence;
    @JacksonXmlElementWrapper(useWrapping = false)
    private
    List<String> subject;
    @JacksonXmlElementWrapper(useWrapping = false)
    private
    List<String> relation;
    @JacksonXmlElementWrapper(useWrapping = false)
    private
    List<String> object;

    /**
     * Default no arg constructor.
     */
    private RlRelationTriple() {
    }

    /**
     * Construct a triple from the Stanford Open IE implementation.
     */
    public RlRelationTriple(RelationTriple triple) {
        this();
        // The implementation does not make use of the Wordnet word senses just the raw word token.
        this.confidence = triple.confidence;
        this.subject = Arrays.asList(triple.subjectGloss().split(" "));
        this.relation = Arrays.asList(triple.relationGloss().split(" "));
        this.object = Arrays.asList(triple.objectGloss().split(" "));

    }

    /**
     * This constructor is expected to only be used for tests.
     *
     * @param confidence
     * @param subject
     * @param relation
     * @param object
     */
    public RlRelationTriple(double confidence, List<String> subject, List<String> relation, List<String> object) {
        this.confidence = confidence;
        this.setSubject(subject);
        this.setRelation(relation);
        this.setObject(object);
    }

    /**
     * Don't use confidence in equality so that only the tokens in parts of the triple are used in equality.
     *
     * @param o Relation
     * @return Equality.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RlRelationTriple that = (RlRelationTriple) o;
        return Objects.equal(this.getSubject(), that.getSubject()) &&
                Objects.equal(this.getRelation(), that.getRelation()) &&
                Objects.equal(this.getObject(), that.getObject());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("confidence", this.confidence)
                .add("subject", this.getSubject())
                .add("relation", this.getRelation())
                .add("object", this.getObject())
                .toString();
    }

    /**
     * Don't use confidence in equality so that only the tokens in parts of the triple are used in equality.
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(this.getSubject(), this.getRelation(), this.getObject());
    }

    public double getConfidence() {
        return this.confidence;
    }

    private void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    /**
     * Tokens representing the Subject of the sentence.
     */
    public List<String> getSubject() {
        return this.subject;
    }

    private void setSubject(List<String> subject) {
        this.subject = subject;
    }

    /**
     * The relation, normally the verb.
     */
    public List<String> getRelation() {
        return this.relation;
    }

    private void setRelation(List<String> relation) {
        this.relation = relation;
    }

    /**
     * The object of the sentence.
     */
    public List<String> getObject() {
        return this.object;
    }

    private void setObject(List<String> object) {
        this.object = object;
    }

    /**
     * Compares the subject relation and object using string lexical ordering.
     *
     * @param o to compare with.
     * @return int
     */
    @Override
    public int compareTo(RlRelationTriple o) {
        return ComparisonChain.start()
                .compare(this.getSubject(), o.getSubject(), Ordering.<String>natural().lexicographical())
                .compare(this.getRelation(), o.getRelation(), Ordering.<String>natural().lexicographical())
                .compare(this.getObject(), o.getObject(), Ordering.<String>natural().lexicographical())
                .result();
    }
}
