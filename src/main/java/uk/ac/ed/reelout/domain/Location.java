package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Holds locations identified as being named entities.
 */
public class Location implements Serializable {

    /**
     * List containing the location details e.g. [City, Edinburgh]
     */
    @JacksonXmlProperty(localName = "value") // Details should be exported to XML as value.
    @JacksonXmlElementWrapper(useWrapping = false)
    List<String> places;

    /**
     * No arg constructor.
     */
    public Location() {
        this.places = new LinkedList<String>();
    }

    /**
     * Constructor taking a list of entities.
     *
     * @param places List.
     */
    public Location(List<String> places) {
        this.places = places;
    }

    public List<String> getPlaces() {
        return this.places;
    }

    public void setPlaces(List<String> places) {
        this.places = places;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Location location = (Location) o;
        return Objects.equal(this.places, location.places);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.places);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("entities", this.places)
                .toString();
    }
}
