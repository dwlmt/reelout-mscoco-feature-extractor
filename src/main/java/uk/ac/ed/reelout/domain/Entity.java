package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Holds non location named entities such as Companies and People.
 */
@SuppressWarnings("ControlFlowStatementWithoutBraces")
public class Entity implements Serializable {

    @JacksonXmlProperty(localName = "value") // Details should be exported to XML as value.
    @JacksonXmlElementWrapper(useWrapping = false)
    private
    List<String> entities;

    /**
     * No arg constructor.
     */
    public Entity() {
        this.setEntities(new LinkedList<String>());
    }

    /**
     * Constructor taking a list of entities.
     *
     * @param places List.
     */
    public Entity(List<String> places) {
        this.setEntities(places);
    }

    /**
     * List containing the location details e.g. [City, Edinburgh]
     */
    public List<String> getEntities() {
        return this.entities;
    }

    public void setEntities(List<String> entities) {
        this.entities = entities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Entity location = (Entity) o;
        return Objects.equal(this.getEntities(), location.getEntities());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getEntities());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("entities", this.getEntities())
                .toString();
    }
}
