package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

/**
 * RlUnit which represents an individual image.
 * <p>
 * Equality and hashcodes are basically solely on the id.
 * <p>
 * This JSON property applies to XML as well.
 * The order with reflection is not stable so needs to specified explicitly for correct order in the output XML.
 */
@JsonPropertyOrder({"tag", "token", "image", "sentiment", "entity", "location", "theme",
        "relationtriple", "embedding", "similaritymeasure", "messagetext"})
@JsonInclude(Include.NON_EMPTY) // Don't include empty elements in the output.
public class RlUnit implements Comparable<RlUnit>, Serializable {

    @JacksonXmlProperty(isAttribute = true)
    private String id;
    /**
     * Source for the Unit.
     */
    @JacksonXmlProperty(isAttribute = true)
    private String src;
    /**
     * Date/time of the image.
     */
    @JacksonXmlProperty(isAttribute = true)
    private LocalDateTime time;
    /**
     * POS tags.
     */
    @JacksonXmlProperty(localName = "tag")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<String> tags;
    /**
     * The text tokens for the sentences.
     */
    @JacksonXmlProperty(localName = "token")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<String> tokens;
    /**
     * Themes from Alchemy API.
     */
    @JacksonXmlProperty(localName = "theme")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Theme> themes;
    /**
     * Image URL. Treat as a string as no need to createFeature this.
     */
    private String image;
    /**
     * The RAW text of the message.
     */
    @JacksonXmlProperty(localName = "messagetext")
    private String messageText;
    /**
     * Number of sentences that have been appended. Strictly for control to be able to limit the size of the message length.
     */
    @JsonIgnore
    private int numOfSentences;
    /**
     * The sentiment of the caption.
     */
    @JacksonXmlProperty(localName = "sentiment")
    private SentimentHolder sentiment;
    /**
     * Locations identified from Named Entity recognition.
     */
    @JacksonXmlProperty(localName = "location")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Location> locations;
    /**
     * Locations identified from Named Entity recognition.
     */
    @JacksonXmlProperty(localName = "entity")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Entity> entities;
    /**
     * Map of Open IE relation triples with a count of how often the occur in the text.
     */
    @JacksonXmlProperty(localName = "relationtriple")
    @JacksonXmlElementWrapper(useWrapping = false)
    private SortedSet<RlRelationTriple> relationTriples;
    @JacksonXmlProperty(localName = "embedding")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Embedding> embeddings;
    @JacksonXmlProperty(localName = "similaritymeasure")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<SimilarityMeasure> similarityMeasures;


    /**
     * No arg constructor.
     */
    private RlUnit() {
        tokens = new LinkedList<String>();
        tags = new LinkedList<String>();
        messageText = ""; // Make non null so can use append and not set.
        locations = new LinkedList<Location>();
        entities = new LinkedList<Entity>();
        themes = new LinkedList<Theme>();
        relationTriples = new TreeSet<RlRelationTriple>();
        embeddings = new LinkedList<Embedding>();
        similarityMeasures = new ArrayList<SimilarityMeasure>();
    }

    /**
     * Constructor for the basic version of RlUnit with only POS tagging.
     *
     * @param id          of the image.
     * @param src         source of the image.
     * @param time        Date/time.
     * @param image       url.
     * @param messageText raw caption text.
     */
    protected RlUnit(String id, String src, LocalDateTime time, String image, String messageText) {
        this(id);
        this.src = src;
        this.time = time;
        this.image = image;
        this.messageText = messageText;
    }

    /**
     * Must be constructed with an Id.
     *
     * @param id
     */
    public RlUnit(String id) {
        this();
        this.id = id;
    }

    public int getNumOfSentences() {
        return this.numOfSentences;
    }

    public void incrementNumOfSentences() {
        this.numOfSentences++;
    }

    public List<SimilarityMeasure> getSimilarityMeasures() {
        return similarityMeasures;
    }

    public void setSimilarityMeasures(List<SimilarityMeasure> similarityMeasures) {
        this.similarityMeasures = similarityMeasures;
    }

    public SortedSet<RlRelationTriple> getRelationTriples() {
        return relationTriples;
    }

    public void setRelationTriples(SortedSet<RlRelationTriple> relationTrips) {
        relationTriples = relationTrips;
    }

    public List<Embedding> getEmbeddings() {
        return embeddings;
    }

    public void setEmbeddings(List<Embedding> embeddings) {
        this.embeddings = embeddings;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("src", src)
                .add("time", time)
                .add("tags", tags)
                .add("tokens", tokens)
                .add("themes", themes)
                .add("image", image)
                .add("messageText", messageText)
                .add("numOfSentences", numOfSentences)
                .add("sentiment", sentiment)
                .add("locations", locations)
                .add("entities", entities)
                .add("relationtriples", relationTriples)
                .add("embeddings", embeddings)
                .add("similarityMeasures", similarityMeasures)
                .toString();
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }

    public List<Entity> getEntities() {
        return entities;
    }

    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }

    public List<Theme> getThemes() {
        return themes;
    }

    public void setThemes(List<Theme> themes) {
        this.themes = themes;
    }

    /**
     * Represents the sentiment of the caption.
     */
    public SentimentHolder getSentiment() {
        return sentiment;
    }

    public void setSentiment(SentimentHolder sentiment) {
        this.sentiment = sentiment;
    }

    /**
     * Identifier for the RL Unit.
     */
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getTokens() {
        return tokens;
    }

    public void setTokens(List<String> tokens) {
        this.tokens = tokens;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    /**
     * Compare to should only be based on the Id so objects with the sam id have equality.
     *
     * @param o RLUnit to compare to.
     * @return int
     */
    @Override
    public int compareTo(RlUnit o) {
        return ComparisonChain.start()
                .compare(id, o.id)
                .result();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        RlUnit rlUnit = (RlUnit) o;
        return Objects.equal(id, rlUnit.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    /**
     * Append the caption to the existing caption.
     * Space appended as some captions do not have full stops which will cause different captions to join incorrectly.
     *
     * @param caption to append.
     */
    public void appendMessageText(String caption) {
        if (messageText.length() > 1) {
            messageText += " ";
        }
        messageText += caption;
    }

    /**
     * Append tokenized words to the existing List.
     *
     * @param words as a list.
     */
    public void appendTokens(List<String> words) {
        tokens.addAll(words);
    }

    /**
     * Append POS tags to the existing tags.
     *
     * @param tags as a list.
     */
    public void appendTags(List<String> tags) {
        this.tags.addAll(tags);
    }
}
