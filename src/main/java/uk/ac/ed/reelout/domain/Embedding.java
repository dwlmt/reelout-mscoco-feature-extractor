package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.io.Serializable;
import java.util.List;

/**
 * Domain object for holding the embedding.
 * <p>
 * Equality and comparisons are based solely on the tokens.
 */
public class Embedding implements Comparable<Embedding>, Serializable {

    /**
     * Identifier for the embedding.
     */
    @JacksonXmlProperty(isAttribute = true)
    String label;
    /**
     * Type for the embedding
     */
    @JacksonXmlProperty(isAttribute = true)
    String type;
    /**
     * How was it created.
     */
    @JacksonXmlProperty(isAttribute = true)
    String composition;
    /**
     * text, image, both.
     */
    @JacksonXmlProperty(isAttribute = true)
    String mode;
    /**
     * The embedding.
     */
    @JacksonXmlProperty(localName = "element")
    @JacksonXmlElementWrapper(useWrapping = false)
    List<Double> vector;
    /**
     * The number of dimensions in the embedding.
     */
    @JacksonXmlProperty(isAttribute = true, localName = "dimensions")
    int dimensionality;

    /**
     * Method for constructing a new Embedding.
     *
     * @param label          unique identifier. The words used joined together.
     * @param type           for the type of embedding.
     * @param composition    how was the embedding created, e.g. addition.
     * @param mode           what does it apply to, e.g. all text, text plus images. Open IE, text plus images.
     * @param vector         the vector with embedding weights.
     * @param dimensionality number of dimension in the embedding.
     * @return Embedding.
     */
    private Embedding(String label, String type, String composition, String mode, List<Double> vector, int dimensionality) {
        this.label = label;
        this.type = type;
        this.composition = composition;
        this.mode = mode;
        this.vector = vector;
        this.dimensionality = dimensionality;
    }

    public Embedding() {
    }

    /**
     * Method for constructing a new Embedding.
     *
     * @param label          unique identifier. The words used joined together.
     * @param type           for the type of embedding.
     * @param vector         the vector with embedding weights.
     * @param dimensionality number of dimension in the embedding.
     * @return Embedding.
     */
    public static Embedding createEmbedding(String label, String type, String composition, String mode, List<Double> vector, int dimensionality) {
        return new Embedding(label, type, composition, mode, vector, dimensionality);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || this.getClass() != o.getClass()) return false;
        Embedding embedding = (Embedding) o;
        return this.dimensionality == embedding.dimensionality &&
                Objects.equal(this.label, embedding.label) &&
                Objects.equal(this.type, embedding.type) &&
                Objects.equal(this.composition, embedding.composition) &&
                Objects.equal(this.mode, embedding.mode);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.label, this.type, this.composition, this.mode, this.dimensionality);
    }

    public String getComposition() {
        return this.composition;
    }

    public String getMode() {
        return this.mode;
    }

    public List<Double> getVector() {
        return this.vector;
    }

    public void setVector(List<Double> vector) {
        this.vector = vector;
    }

    /**
     * Convert back to an IndArray.
     *
     * @return INDArray
     */
    @JsonIgnore
    public INDArray getVectorAsIndArray() {
        return Nd4j.create(vector.stream().mapToDouble(d -> d).toArray());
    }

    public int getDimensionality() {
        return this.dimensionality;
    }

    public void setDimensionality(int dimensionality) {
        this.dimensionality = dimensionality;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("label", this.label)
                .add("type", this.type)
                .add("vector", this.vector)
                .add("dimensionality", this.dimensionality)
                .add("mode", this.mode)
                .add("composition", this.composition)
                .toString();
    }

    @Override
    public int compareTo(Embedding o) {
        return ComparisonChain.start()
                .compare(label, o.label)
                .compare(type, o.type)
                .compare(dimensionality, o.dimensionality)
                .compare(mode, o.mode)
                .compare(composition, o.composition)
                .result();
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
