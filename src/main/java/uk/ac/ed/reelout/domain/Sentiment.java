package uk.ac.ed.reelout.domain;

/**
 * Enum representing the allowed sentiments. The original is on a 0-4 scale with 2 being neutral.
 * The enum is remapped from these values.
 */
public enum Sentiment {
    positive, neutral, negative
}

