package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents Themes returned by the Alchemy API.
 */
public class Theme implements Serializable {

    @JacksonXmlProperty(isAttribute = true)
    private double confidence;

    @JacksonXmlProperty(localName = "value")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<String> categories;

    public Theme() {
        setCategories(new LinkedList<String>());
    }

    public Theme(double confidence, List<String> categories) {
        setConfidence(confidence);
        setCategories(categories);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        Theme theme = (Theme) o;
        return Double.compare(theme.getConfidence(), getConfidence()) == 0 &&
                Objects.equal(getCategories(), theme.getCategories());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getConfidence(), getCategories());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("confidence", getConfidence())
                .add("categories", getCategories())
                .toString();
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }
}
