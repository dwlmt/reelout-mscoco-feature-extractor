package uk.ac.ed.reelout.domain;

/**
 * Object for specifying criteria for selecting images.
 */
public class ImageCriteria {

    /**
     * General search text to filter images over.
     */
    String text;

    /**
     * Filter according to Open IE subject.
     */
    String subject;

    /**
     * Filter according to Open IE relation.
     */
    String relation;

    /**
     * Filter according to the Open IE object.
     */
    String object;

    /**
     * Filter based on the Alchemy theme.
     */
    String theme;

    /**
     * Filter based on named entity or location.
     */
    String entity;

    /**
     * Filter based on Sentiment.
     */
    Sentiment sentiment;

}
