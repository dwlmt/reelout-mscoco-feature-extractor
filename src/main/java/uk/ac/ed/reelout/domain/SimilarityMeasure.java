package uk.ac.ed.reelout.domain;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * Holds a list of similarities of topN.
 * <p>
 * Equality based only on type and mean.
 */
public class SimilarityMeasure implements Comparable<SimilarityMeasure>, Serializable {

    /**
     * A type for the measure.
     */
    @JacksonXmlProperty(isAttribute = true)
    String type;

    /**
     * List of similarities. Will typically be the topN closest.
     */
    @JacksonXmlProperty(localName = "similarity")
    @JacksonXmlElementWrapper(useWrapping = false)
    List<Similarity> similarities;

    /**
     * Records the mean distance across the whole set.
     */
    @JacksonXmlProperty(isAttribute = true)
    Double mean;

    /**
     * Variance across the similarities.
     */
    @JacksonXmlProperty(isAttribute = true)
    Double variance;


    public SimilarityMeasure() {
    }

    /**
     * Constructor.
     *
     * @param type         of the similarity.
     * @param similarities list of similarities with other image.
     * @param mean         of similarity across the dataset.
     * @param variance     of the similarities.
     */
    private SimilarityMeasure(String type, List<Similarity> similarities, Double mean,
                              Double variance) {
        this.type = type;
        this.similarities = similarities;
        this.mean = mean;
        this.variance = variance;
    }

    /**
     * Builder method.
     *
     * @param type         of the similarity.
     * @param similarities list of similarities with other image.
     * @param mean         of similarity across the dataset.
     * @param variance     of the similarities.
     */
    public static SimilarityMeasure createSimilarityMeasure(String type, List<Similarity> similarities, Double mean,
                                                            Double variance) {
        Collections.sort(similarities);
        return new SimilarityMeasure(type, similarities, mean, variance);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimilarityMeasure that = (SimilarityMeasure) o;
        return Objects.equal(type, that.type);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(type);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("type", this.type)
                .add("similarities", this.similarities)
                .add("mean", this.mean)
                .add("variance", this.variance)
                .toString();
    }

    /**
     * Ordering is based on the type.
     *
     * @param o Similarity
     * @return int
     */
    @Override
    public int compareTo(SimilarityMeasure o) {
        return ComparisonChain.start()
                .compare(type, o.type)
                .result();
    }

    public String getType() {
        return this.type;
    }

    public List<Similarity> getSimilarities() {
        return this.similarities;
    }

    public Double getMean() {
        return this.mean;
    }

    public Double getVariance() {
        return this.variance;
    }
}
