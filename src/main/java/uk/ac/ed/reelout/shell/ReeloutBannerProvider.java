package uk.ac.ed.reelout.shell;


import org.springframework.core.annotation.Order;
import org.springframework.shell.plugin.BannerProvider;
import org.springframework.stereotype.Component;

/**
 * Overides the splash banner for the shell startup.
 */
@Component
@Order(Integer.MIN_VALUE)
public class ReeloutBannerProvider implements BannerProvider {

    private static final String VERSION = "1.0-ALPHA";
    private static final String BANNER = "########  ######## ######## ##        #######  ##     ## ########\n" +
            "##     ## ##       ##       ##       ##     ## ##     ##    ##\n" +
            "##     ## ##       ##       ##       ##     ## ##     ##    ##\n" +
            "########  ######   ######   ##       ##     ## ##     ##    ##\n" +
            "##   ##   ##       ##       ##       ##     ## ##     ##    ##\n" +
            "##    ##  ##       ##       ##       ##     ## ##     ##    ##\n" +
            "##     ## ######## ######## ########  #######   #######     ##\n";

    private static final String WELCOME = "Welcome to the Reelout MS-COCO feature extractor. For assistance type \"help\".";

    @Override
    public String getBanner() {
        return ReeloutBannerProvider.BANNER + ReeloutBannerProvider.VERSION + "\n";
    }

    @Override
    public String getVersion() {
        return ReeloutBannerProvider.VERSION;
    }

    @Override
    public String getWelcomeMessage() {
        return ReeloutBannerProvider.WELCOME;
    }

    @Override
    public String getProviderName() {
        return "reelout-mscoco-feature-extractor";
    }

}
