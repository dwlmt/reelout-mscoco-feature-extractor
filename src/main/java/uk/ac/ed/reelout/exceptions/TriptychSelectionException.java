package uk.ac.ed.reelout.exceptions;

/**
 * Exception for when Triptych selection cannot be performed.
 */
public class TriptychSelectionException extends IllegalArgumentException {

    public TriptychSelectionException() {
    }

    public TriptychSelectionException(String s) {
        super(s);
    }

    public TriptychSelectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public TriptychSelectionException(Throwable cause) {
        super(cause);
    }
}
