package uk.ac.ed.reelout.features;

import com.google.common.base.MoreObjects;

/**
 * Map an enum of possible feature to the Spring beans that provide it.
 */
public enum FeatureType {
    POS("POS", "posFeatureProcessor"),
    SENTIMENT("Sentiment", "sentimentFeatureProcessor"),
    ENTITY("Named Entity", "namedEntityFeatureProcessor"),
    THEME("Theme", "themeFeatureProcessor"),
    RELATION_TRIPLE("Open IE Relation Triple", "relationTripleFeatureProcessor"),
    ADD_WORD_EMBEDDING("Add Word Vector", "wholeTextAddEmbeddingFeatureProcessor"),
    COSINE_SIMILARITY_EMBEDDING("Cosine Similarity Embedding", "cosineSimilarityEmbeddingFeatureProcessor"),
    COSINE_SIMILARITY_IMAGE_EMBEDDING("Cosine Similarity Image Embedding", "cosineSimilarityImageEmbeddingFeatureProcessor");

    private final String beanName;
    private final String name;

    FeatureType(String name, String beanName) {
        this.name = name;
        this.beanName = beanName;
    }

    public String getName() {
        return name;
    }

    public String getBeanName() {
        return beanName;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("name", name)
                .toString();
    }
}
