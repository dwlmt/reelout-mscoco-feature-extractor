package uk.ac.ed.reelout.features;

import com.ibm.watson.developer_cloud.alchemy.v1.AlchemyLanguage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import javax.annotation.PostConstruct;

/**
 * Performs alchemy key property injection for the key.
 */
@Configuration
@PropertySource("classpath:config.properties")
abstract class AlchemyFeatureProcessor extends GenericFeatureProcessor {
    final AlchemyLanguage alchLang = new AlchemyLanguage();
    @Value("${alchemy.key}")
    String alchemyKey;

    // Allow properties to be injected.
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    /**
     * Set the API key to access Alchemy.
     *
     * @throws Exception
     */
    @PostConstruct
    public void setup() throws Exception {
        alchLang.setApiKey(alchemyKey);
    }
}
