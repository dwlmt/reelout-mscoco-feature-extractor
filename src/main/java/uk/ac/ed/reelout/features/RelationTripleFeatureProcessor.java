package uk.ac.ed.reelout.features;

import com.google.common.util.concurrent.AtomicDouble;
import edu.stanford.nlp.ie.util.RelationTriple;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.naturalli.NaturalLogicAnnotations;
import edu.stanford.nlp.naturalli.NaturalLogicAnnotations.RelationTriplesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlRelationTriple;
import uk.ac.ed.reelout.domain.RlUnit;

import java.util.*;

/**
 * Extracts Open IE Triples from the messageText using Stanford Core NLP.
 */
@Component
public class RelationTripleFeatureProcessor extends GenericFeatureProcessor {

    private final StanfordCoreNLP pipeline;

    public RelationTripleFeatureProcessor() {
        Properties props = PropertiesUtils.asProperties("annotators", "tokenize, ssplit, pos, lemma, depparse, natlog, openie",
                "openie.triple.strict", "true", "openie.triple.all_nominals", "false",
                "openie.resolve_coref", "true", "openie.max_entailments_per_clause", "1000");
        pipeline = new StanfordCoreNLP(props);
    }

    @Override
    public RlUnit createFeature(RlUnit rlUnit, Map<String, Object> options) {
        try {
            Annotation annotation = new Annotation(rlUnit.getMessageText());
            pipeline.annotate(annotation, annotated -> {

                List<CoreMap> sentences = annotation
                        .get(SentencesAnnotation.class);

                SortedSet<RlRelationTriple> triplesList = new TreeSet<RlRelationTriple>();
                for (CoreMap sentence : sentences) {
                    Collection<RelationTriple> triples = sentence.get(RelationTriplesAnnotation.class);

                    for (RelationTriple triple : triples) {
                        logger.debug("OpenIETriplesFeatureProcessor: Confidence = {}, Subject = {}, Relation = {}, Object = {}"
                                , triple.confidenceGloss(), triple.subjectGloss(), triple.relationGloss(), triple.objectGloss());
                        triplesList.add(new RlRelationTriple(triple));
                    }
                }
                // List will keep a count in case of the same triples occurring  multiple times across sentences.
                logger.debug("OpenIETriplesFeatureProcessor: Open IE triples = {}", triplesList);
                rlUnit.setRelationTriples(triplesList);


            });
        } catch (NoSuchElementException nsee) {
            /*
             * Stanford CoreNLP is seeming to use a subclass of this exception NoSuchParseException
             * when Open IE extraction cannot be performed.
             * Catch and continue to next RlUnit if it happens.
             * This hasn't been seen on non Open IE Stanford features but if it does this can be generalised
             * to a common superclass.
              */
            logger.error("RelationTripleFeatureProcessor: Open IE extraction failed on {}", rlUnit);
        }
        return rlUnit;
    }

    @Override
    public RlUnit resetFeature(RlUnit rlUnit) {
        rlUnit.setRelationTriples(new TreeSet<RlRelationTriple>());
        return rlUnit;
    }

    @Override
    public int getOrder() {
        return 0;
    }

    /**
     * Accumulates the counts of different Themes.
     *
     * @param rlUnits the image features.
     * @param counts  to be added to.
     * @param options
     */
    @Override
    public void countFeatures(SortedSet<RlUnit> rlUnits, NavigableMap<ReeloutStat, AtomicDouble> counts, Map<String, Object> options) {
        // Go Over all of the RlUnits in parallel.
        rlUnits.parallelStream().forEach(s -> {
            for (RlRelationTriple t : s.getRelationTriples()) {
                // Join the levels of the theme using a / separator.
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.RELATION_TRIPLE.getName(), "subject-concat",
                                StringUtils.join(StringUtils.join(t.getSubject(), " "))),
                        k -> new AtomicDouble()).addAndGet(1.0);
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.RELATION_TRIPLE.getName(), "relation-concat",
                                StringUtils.join(StringUtils.join(t.getRelation(), " "))),
                        k -> new AtomicDouble()).addAndGet(1.0);
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.RELATION_TRIPLE.getName(), "object-concat",
                                StringUtils.join(StringUtils.join(t.getObject(), " "))),
                        k -> new AtomicDouble()).addAndGet(1.0);

                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.RELATION_TRIPLE.getName(), "triple-concat",
                                StringUtils.join(StringUtils.join(t.getSubject(), " "), " : ",
                                        StringUtils.join(t.getRelation(), " "), " : ",
                                        StringUtils.join(t.getObject(), " "))),
                        k -> new AtomicDouble()).addAndGet(1.0);

                for (String o : t.getSubject()) {
                    counts.computeIfAbsent(
                            new ReeloutStat(FeatureType.RELATION_TRIPLE.getName(), "subject",
                                    o),
                            k -> new AtomicDouble()).addAndGet(1.0);
                }

                for (String o : t.getRelation()) {
                    counts.computeIfAbsent(
                            new ReeloutStat(FeatureType.RELATION_TRIPLE.getName(), "relation",
                                    o),
                            k -> new AtomicDouble()).addAndGet(1.0);
                }

                for (String o : t.getObject()) {
                    counts.computeIfAbsent(
                            new ReeloutStat(FeatureType.RELATION_TRIPLE.getName(), "object",
                                    o),
                            k -> new AtomicDouble()).addAndGet(1.0);
                }

            }

        });

    }
}
