package uk.ac.ed.reelout.features;

import com.google.common.util.concurrent.AtomicDouble;
import com.ibm.watson.developer_cloud.alchemy.v1.AlchemyLanguage;
import com.ibm.watson.developer_cloud.alchemy.v1.model.Entities;
import com.ibm.watson.developer_cloud.alchemy.v1.model.Entity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.Location;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlUnit;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Creates and adds named entities identified from Alechemy.
 * <p>
 * These themes are from IBMs Bluemix Alchemy API (http://www.alchemyapi.com/)
 */
@Component
public class NamedEntityFeatureProcessor extends AlchemyFeatureProcessor {

    @Value("${location.type}")
    String locationTypeStrings;


    private Set<String> locationTypes;

    private Set<String> entityTypes;

    public NamedEntityFeatureProcessor() {
    }

    @Override
    public RlUnit createFeature(RlUnit rlUnit, Map<String, Object> options) {
        Map<String, Object> params = new HashMap<String, Object>(1);
        params.put(AlchemyLanguage.TEXT, rlUnit.getMessageText());
        Entities namedEntities = alchLang.getEntities(params).execute();
        List<Entity> entities = namedEntities.getEntities();

        List<Location> locations = rlUnit.getLocations();
        List<uk.ac.ed.reelout.domain.Entity> reeloutEntities = rlUnit.getEntities();

        for (Entity e : entities) {

            // If Named Entity is a location then add it.
            if (locationTypes.contains(e.getType())) {
                Location l = new Location();
                l.getPlaces().add(e.getType());
                l.getPlaces().add(e.getText());
                locations.add(l);
            }

            // Entities are all types that are not location types.
            if (!locationTypes.contains(e.getType())) {
                uk.ac.ed.reelout.domain.Entity l = new uk.ac.ed.reelout.domain.Entity();
                l.getEntities().add(e.getType());
                l.getEntities().add(e.getText());
                reeloutEntities.add(l);
            }

        }
        logger.debug("NamedEntityFeatureProcessor: locations = {}, entities = []", locations, reeloutEntities);
        return rlUnit;
    }

    @Override
    public RlUnit resetFeature(RlUnit rlUnit) {
        rlUnit.setLocations(new LinkedList<Location>());
        rlUnit.setEntities(new LinkedList<uk.ac.ed.reelout.domain.Entity>());
        return rlUnit;
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    /**
     * Set the API key to access Alchemy.
     *
     * @throws Exception
     */
    @Override
    @PostConstruct
    public void setup() throws Exception {
        super.setup();
        locationTypes = new TreeSet<String>(Arrays.asList(locationTypeStrings.trim().split(",")));
    }

    /**
     * Accumulates the counts of different Entity and location types..
     *
     * @param rlUnits the image features.
     * @param counts  to be added to.
     * @param options
     */
    @Override
    public void countFeatures(SortedSet<RlUnit> rlUnits, NavigableMap<ReeloutStat, AtomicDouble> counts, Map<String, Object> options) {
        // Go Over all of the RlUnits in parallel.
        rlUnits.parallelStream().forEach(s -> {
            for (Location t : s.getLocations()) {
                // Join the levels of the theme using a / separator.
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.ENTITY.getName(), "location",
                                StringUtils.join(t.getPlaces(), "/")),
                        k -> new AtomicDouble()).addAndGet(1.0);
            }

            for (uk.ac.ed.reelout.domain.Entity t : s.getEntities()) {
                // Join the levels of the theme using a / separator.
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.ENTITY.getName(), "entity",
                                StringUtils.join(t.getEntities(), "/")),
                        k -> new AtomicDouble()).addAndGet(1.0);
            }

        });

    }

}
