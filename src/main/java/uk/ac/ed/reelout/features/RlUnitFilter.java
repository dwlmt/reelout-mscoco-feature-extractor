package uk.ac.ed.reelout.features;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;

import java.util.SortedSet;

/**
 * Allow the RlUnits of a RlDatabase to be filtered.
 */
public class RlUnitFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenericFeatureProcessor.class);

    /**
     * Get filtered Rl Units so that only RlUnits within the key range are filtered.
     * If no there are no filters then it will return the whole list.
     *
     * @param fromId key to subselect from.
     * @param toId   key to select to.
     * @return SortedSet<RlUnit>
     */
    public static SortedSet<RlUnit> getFilteredRlUnits(RlDatabase db, String fromId, String toId) {
        SortedSet<RlUnit> rlUnits;
        if (fromId == null && toId == null) {
            RlUnitFilter.LOGGER.debug("Filter RlUnits process all");
            rlUnits = db.getRlUnits();
        } else if (fromId != null && toId != null) {
            RlUnitFilter.LOGGER.debug("Filter RlUnits: from = {}, to = {}", fromId, toId);
            rlUnits = db.getRlUnits().subSet(new RlUnit(fromId), new RlUnit(toId));
        } else if (fromId != null) {
            RlUnitFilter.LOGGER.debug("Filter RlUnits tail: from = {}", fromId);
            rlUnits = db.getRlUnits().tailSet(new RlUnit(fromId));
        } else {
            RlUnitFilter.LOGGER.debug("Filter RlUnits head: to = {}", toId);
            rlUnits = db.getRlUnits().headSet(new RlUnit(toId));
        }
        RlUnitFilter.LOGGER.info("Filtered RL Units to process: total = {}", new Integer(rlUnits.size()));
        return rlUnits;
    }
}
