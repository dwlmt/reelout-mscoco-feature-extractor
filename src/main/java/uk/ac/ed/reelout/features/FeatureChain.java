package uk.ac.ed.reelout.features;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.util.concurrent.AtomicDouble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlDatabase;

import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * Feature chain for iterating over configured features.
 */
public class FeatureChain {

    private final Logger logger = LoggerFactory.getLogger(FeatureChain.class);

    private String fromIdRange;
    private String toIdRange;
    private List<GenericFeatureProcessor> features;

    /**
     * Construct a feature chain.
     *
     * @param features to execute.
     */
    public FeatureChain(List<GenericFeatureProcessor> features) {
        this();
        this.features = features;
    }

    private FeatureChain() {
    }

    public String getToIdRange() {
        return toIdRange;
    }

    public void setToIdRange(String toIdRange) {
        this.toIdRange = toIdRange;
    }

    /**
     * Set the end range for Id filtering.
     * Implement as a builder style method.
     *
     * @param toIdRange
     * @return FeatureChain
     */
    public FeatureChain withToIdRange(String toIdRange) {
        setToIdRange(toIdRange);
        return this;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("fromIdRange", getFromIdRange())
                .add("toIdRange", getToIdRange())
                .add("features", features)
                .toString();
    }

    public String getFromIdRange() {
        return fromIdRange;
    }

    public void setFromIdRange(String fromIdRange) {
        this.fromIdRange = fromIdRange;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || this.getClass() != o.getClass()) {
            return false;
        }
        FeatureChain that = (FeatureChain) o;
        return Objects.equal(getFromIdRange(), that.getFromIdRange()) &&
                Objects.equal(getToIdRange(), that.getToIdRange()) &&
                Objects.equal(features, that.features);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getFromIdRange(), getToIdRange(), features);
    }

    /**
     * Set the start range for Id filtering.
     * Implement as a builder style method.
     *
     * @param fromIdRange
     * @return FeatureChain
     */
    public FeatureChain withFromIdRange(String fromIdRange) {
        setFromIdRange(fromIdRange);
        return this;
    }

    /**
     * Iterates over each feature processor and RlUnit applying the features processing.
     *
     * @param rlDb    RlDatabase containing the base image information.
     * @param options
     * @return the updated RlDatabase.
     */
    public RlDatabase createFeatures(RlDatabase rlDb, Map<String, Object> options) {
        logger.debug("FeatureChain: RlDatabase before = {}", rlDb);

        // TODO: Look at parallel stream processing to improve performance.
        for (GenericFeatureProcessor feature : features) {
            rlDb = feature.createFeature(rlDb, getFromIdRange(), getToIdRange(), options);
        }
        logger.debug("FeatureChain: RlDatabase after = {}", rlDb);
        return rlDb;

    }

    /**
     * Iterates over each feature processor and RlUnits resetting (removing) the features.
     *
     * @param rlDb RlDatabase containing the base image information.
     * @return the updated RlDatabase.
     */
    public RlDatabase resetFeatures(RlDatabase rlDb) {
        logger.debug("FeatureChain: RlDatabase before reset = {}", rlDb);

        for (GenericFeatureProcessor feature : features) {
            rlDb = feature.resetFeature(rlDb, getFromIdRange(), getToIdRange());
        }
        logger.debug("FeatureChain: RlDatabase after reset = {}", rlDb);
        return rlDb;

    }

    /**
     * Accumulates counts and statistics for features in the file.
     *
     * @param rlDb    the ReelOut database.
     * @param options
     * @return NavigableMap<ReeloutStat,AtomicDouble>
     */
    public NavigableMap<ReeloutStat, AtomicDouble> countFeatures(RlDatabase rlDb, Map<String, Object> options) {

        NavigableMap<ReeloutStat, AtomicDouble> counts = new ConcurrentSkipListMap<ReeloutStat, AtomicDouble>();
        for (FeatureStatistics feature : features) {

            feature.countFeatures(rlDb, counts, getFromIdRange(), getToIdRange(), options);
        }
        return counts;

    }

    /**
     * Chain of features apply apply to each image in the RlDatabase.
     */
    public List<GenericFeatureProcessor> getFeatures() {
        return features;
    }

    public void setFeatures(List<GenericFeatureProcessor> features) {
        this.features = features;
    }

    /**
     * Implement as a builder style method.
     *
     * @param features
     * @return FeatureChain
     */
    public FeatureChain withFeatures(List<GenericFeatureProcessor> features) {
        this.features = features;
        return this;
    }
}
