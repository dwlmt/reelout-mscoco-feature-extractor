package uk.ac.ed.reelout.features;

import com.google.common.util.concurrent.AtomicDouble;
import edu.stanford.nlp.simple.Document;
import edu.stanford.nlp.util.StringUtils;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlUnit;

import java.util.*;

/**
 * Tokenizes and Part of Speech tags the message text using the Stanford CoreNLP parser.
 */
@Component
public class PosFeatureProcessor extends GenericFeatureProcessor {

    public PosFeatureProcessor() {
    }

    @Override
    public RlUnit createFeature(RlUnit rlUnit, Map<String, Object> options) {

        Document doc = new Document(rlUnit.getMessageText());

        doc.sentences().forEach(s -> {
            List<String> pos = s.posTags();
            List<String> words = s.originalTexts();
            rlUnit.appendTokens(words);
            rlUnit.appendTags(pos);
        });
        this.logger.debug("PosFeatureProcessor: Message = {}, Words = {}, POS tags = {}",
                rlUnit.getMessageText(), rlUnit.getTokens(), rlUnit.getTags());
        return rlUnit;
    }

    @Override
    public RlUnit resetFeature(RlUnit rlUnit) {
        rlUnit.setTokens(new LinkedList<String>());
        rlUnit.setTags(new LinkedList<String>());
        return rlUnit;
    }

    @Override
    public int getOrder() {
        return Ordered.HIGHEST_PRECEDENCE;
    }

    /**
     * Accumulates the lengths of the sentences in parallel.
     * <p>
     * TODO: Add cooccurence counts into the statistics.
     *
     * @param rlUnits the image features.
     * @param counts  to be added to.
     * @param options
     */
    @Override
    public void countFeatures(SortedSet<RlUnit> rlUnits, NavigableMap<ReeloutStat, AtomicDouble> counts, Map<String, Object> options) {
        // Go Over all of the RlUnits in parallel.
        rlUnits.parallelStream().forEach(s -> {

            // Add the text length as stat.
            counts.computeIfAbsent(
                    new ReeloutStat(FeatureType.POS.getName(), "textlength",
                            String.valueOf(s.getTags().size())),
                    k -> new AtomicDouble()).addAndGet(1.0);

            // Add each POS tag to the count.
            s.getTags().forEach(t -> {
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.POS.getName(), "tag",
                                t),
                        k -> new AtomicDouble()).addAndGet(1.0);
            });

            // Get uni to trigrams.

            StringUtils.getNgrams(s.getTokens(), 1, 1).forEach(t -> {
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.POS.getName(), "unigram",
                                t),
                        k -> new AtomicDouble()).addAndGet(1.0);
            });
            StringUtils.getNgrams(s.getTokens(), 2, 2).forEach(t -> {
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.POS.getName(), "bigram",
                                t),
                        k -> new AtomicDouble()).addAndGet(1.0);
            });

            StringUtils.getNgrams(s.getTokens(), 3, 3).forEach(t -> {
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.POS.getName(), "trigram",
                                t),
                        k -> new AtomicDouble()).addAndGet(1.0);
            });

            // Do a concurrence count.
            int position = 0;
            for (String a : s.getTokens()) {
                position++;
                // Subselect the list so cooccurences aren't counted twice.
                for (String b : s.getTokens().subList(position, s.getTokens().size())) {
                    int compare = a.compareTo(b);
                    // Make sure they are in the same order.
                    if (compare < 0) {
                        counts.computeIfAbsent(
                                new ReeloutStat(FeatureType.POS.getName(), "coocurrence", a + " " + b
                                ),
                                k -> new AtomicDouble()).addAndGet(1.0);
                    } else if (compare > 0) {
                        counts.computeIfAbsent(
                                new ReeloutStat(FeatureType.POS.getName(), "coocurrence", b + " " + a
                                ),
                                k -> new AtomicDouble()).addAndGet(1.0);
                    }
                }
            }

        });

    }
}
