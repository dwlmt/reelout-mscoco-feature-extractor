package uk.ac.ed.reelout.features;

import com.google.common.util.concurrent.AtomicDouble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;

import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedSet;

/**
 * Feature processor implementing a chain of responsibility pattern.
 * Allows RlDb to be incrementally enriched adding new features.
 */
public abstract class GenericFeatureProcessor implements Ordered, FeatureStatistics {

    final Logger logger = LoggerFactory.getLogger(GenericFeatureProcessor.class);


    /**
     * Iterate and apply the feature in the implemented subclass to each RlUnit.
     *
     * @param db      RlDatabase
     * @param options
     * @return RlDatabase
     */
    public RlDatabase createFeature(RlDatabase db, Map<String, Object> options) {
        return createFeature(db, null, null, options);
    }

    /**
     * Iterate and apply the feature in the implemented subclass to each RlUnit.
     *
     * @param db      RlDatabase
     * @param fromId  filter from.
     * @param toId    filter to.
     * @param options
     * @return RlDatabase
     */
    public RlDatabase createFeature(RlDatabase db, String fromId, String toId, Map<String, Object> options) {
        RlUnitFilter.getFilteredRlUnits(db, fromId, toId).parallelStream().forEach(rl -> {
            logger.debug("RLUnit before feature: {}", rl);
            rl = resetFeature(rl);
            rl = createFeature(rl, options);
            logger.debug("RLUnit after feature: {}", rl);
        });
        return db;
    }

    /**
     * Reset, i.e. remove the feature from the domain representation.
     *
     * @param db     RlDatabase
     * @param fromId filter from.
     * @param toId   filter to.
     * @return RlDatabase
     */
    public RlDatabase resetFeature(RlDatabase db, String fromId, String toId) {
        RlUnitFilter.getFilteredRlUnits(db, fromId, toId).parallelStream().forEach(rl -> {
            logger.debug("RLUnit before reset: {}", rl);
            rl = resetFeature(rl);
            logger.debug("RLUnit after reset: {}", rl);
        });
        return db;
    }

    /**
     * Reset, i.e. remove the feature from the domain representation.
     *
     * @param db RlDatabase
     * @return RlDatabase
     */
    public RlDatabase resetFeature(RlDatabase db) {
        return resetFeature(db, null, null);
    }

    /**
     * Create the feature for the RlUnit.
     *
     * @param rlUnit  to createFeature.
     * @param options
     * @return with the feature as implemented by the processor.
     */
    public abstract RlUnit createFeature(RlUnit rlUnit, Map<String, Object> options);

    /**
     * Reset of nullify the feature so the application is idempotent.
     *
     * @param rlUnit to createFeature.
     * @return with the feature as implemented by the processor.
     */
    protected abstract RlUnit resetFeature(RlUnit rlUnit);


    /**
     * Accumulate counts for distinct values of the attribute if applicable.
     *
     * @param db      RlDatabase
     * @param counts  accumulated counts.
     * @param fromId  filter from.
     * @param toId    filter to.
     * @param options
     * @return counts
     */
    @Override
    public void countFeatures(RlDatabase db, NavigableMap<ReeloutStat, AtomicDouble> counts, String fromId, String toId, Map<String, Object> options) {
        SortedSet<RlUnit> units = RlUnitFilter.getFilteredRlUnits(db, fromId, toId);
        logger.debug("Counts before: {}", counts);

        this.countFeatures(units, counts, options);

        logger.debug("Counter after: {}", counts);
    }

}