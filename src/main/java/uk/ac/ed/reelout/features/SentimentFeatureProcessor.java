package uk.ac.ed.reelout.features;

import com.google.common.util.concurrent.AtomicDouble;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.domain.Sentiment;
import uk.ac.ed.reelout.domain.SentimentHolder;

import java.util.*;

import static uk.ac.ed.reelout.features.FeatureType.SENTIMENT;

/**
 * Performs Sentiment analysis on the message text for each RlUnit.
 * Uses Stanford CoreNLP. Will set and enum that can be positive, neutral, or negative.
 */
@Component
public class SentimentFeatureProcessor extends GenericFeatureProcessor {

    private final StanfordCoreNLP pipeline;

    public SentimentFeatureProcessor() {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        pipeline = new StanfordCoreNLP(props);
    }

    @Override
    public RlUnit createFeature(RlUnit rlUnit, Map<String, Object> options) {
        Annotation annotation = new Annotation(rlUnit.getMessageText());
        pipeline.annotate(annotation, annotated -> {

            List<CoreMap> sentences = annotation
                    .get(SentencesAnnotation.class);

            int sentimentTotal = 0;
            for (CoreMap sentence : sentences) {
                Tree tree = sentence.get(SentimentAnnotatedTree.class);
                int score = RNNCoreAnnotations.getPredictedClass(tree);
                sentimentTotal += score;
            }

            /*
             * Average the sentiment if there are multiple sentences.
             * The parser produces 5 classes 0-4 with 2 being neutral.
             */
            int averageSentiment = Math.round((float) sentimentTotal / (float) sentences.size());
            if (averageSentiment > 2) {
                rlUnit.setSentiment(new SentimentHolder(Sentiment.positive));
            } else if (averageSentiment == 2) {
                rlUnit.setSentiment(new SentimentHolder(Sentiment.neutral));
            } else {
                rlUnit.setSentiment(new SentimentHolder(Sentiment.negative));
            }

            logger.debug("SentimentFeatureProcessor: text='{}', score={}, sentiment={}", rlUnit.getMessageText(),
                    averageSentiment, rlUnit.getSentiment());

        });
        return rlUnit;
    }

    @Override
    public RlUnit resetFeature(RlUnit rlUnit) {
        rlUnit.setSentiment(null);
        return rlUnit;
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    /**
     * Accumulates the counts of different sentiments.
     *
     * @param rlUnits the image features.
     * @param counts  to be added to.
     * @param options
     */
    @Override
    public void countFeatures(SortedSet<RlUnit> rlUnits, NavigableMap<ReeloutStat, AtomicDouble> counts, Map<String, Object> options) {
        // Go Over all of the RlUnits in parallel.
        rlUnits.parallelStream().forEach(s -> {
            if (s.getSentiment() != null) {
                // Add the sentiment as a stat.
                counts.computeIfAbsent(
                        new ReeloutStat(SENTIMENT.getName(), "",
                                s.getSentiment().getSentiment().name()),
                        k -> new AtomicDouble()).addAndGet(1.0);
            }

        });

    }
}
