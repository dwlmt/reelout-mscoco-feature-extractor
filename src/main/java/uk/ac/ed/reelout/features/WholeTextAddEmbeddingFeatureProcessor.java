package uk.ac.ed.reelout.features;

import com.google.common.util.concurrent.AtomicDouble;
import org.deeplearning4j.berkeley.Counter;
import org.deeplearning4j.models.embeddings.WeightLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.ops.transforms.Transforms;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.Embedding;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.utils.StopWords;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Adds embedding representing all the words in the text.
 * <p>
 * TODO: Extend with image features.
 */
@Component
public class WholeTextAddEmbeddingFeatureProcessor extends GenericFeatureProcessor {

    // Word embeddings classes.
    WordVectors wordVectors;
    WeightLookupTable lookup;
    VocabCache vocab;
    Set<String> stopWords;

    /**
     * Override so can set the Word embeddings before processing.
     *
     * @param db      RlDatabase
     * @param fromId  filter from.
     * @param toId    filter to.
     * @param options for the processor.
     * @return RlDatabase
     */
    @Override
    public RlDatabase createFeature(RlDatabase db, String fromId, String toId, Map<String, Object> options) {

        this.setup(options);

        RlUnitFilter.getFilteredRlUnits(db, fromId, toId).parallelStream().forEach(rl -> {
            this.logger.debug("RLUnit before feature: {}", rl);
            rl = resetFeature(rl);
            rl = createFeature(rl, options);
            this.logger.debug("RLUnit after feature: {}", rl);
        });
        return db;
    }

    /**
     * Setup, load the word embeddings.embed
     *
     * @param options
     */
    protected void setup(Map<String, Object> options) {
        // Try loading as text and a binary representation using the google model if it fails
        try {
            this.wordVectors = WordVectorSerializer.loadTxtVectors(new File((String) options.get("embeddingsFile")));
        } catch (Exception e) {
            try {
                this.wordVectors = WordVectorSerializer.loadGoogleModel(new File((String) options.get("embeddingsFile")), true);
            } catch (IOException e1) {
                this.logger.error("WholeTextEmbeddingFeatureProcessor: Embeddings file not found = {}", options.get("embeddingsFile"));
                throw new IllegalArgumentException("Embeddings file cannot be found or loaded.", e);
            }
        }

        this.lookup = this.wordVectors.lookupTable();
        this.vocab = this.lookup.getVocabCache();
        // Check the options for whether to remove stopwords. The default is to remove.
        if (!options.containsKey("removeStopWords") || options.get("removeStopWords").equals(true)) {
            this.stopWords = new TreeSet<String>(StopWords.getStopWords());
        } else {
            this.stopWords = new TreeSet<String>();
        }
    }

    @Override
    public RlUnit createFeature(RlUnit rlUnit, Map<String, Object> options) {

        if (rlUnit.getTokens() == null || rlUnit.getTokens().size() == 0)
            throw new IllegalStateException("POS tagging must have been applied before running add_word_embedding.");

        SortedSet<String> inWordVectors = new TreeSet<String>();
        SortedSet<String> notInWordVectors = new TreeSet<String>();

        // Iterate over
        List<String> tokens = rlUnit.getTokens();
        for (String t : tokens) {
            String tLower = t.toLowerCase();
            // Think about whether to remove stopwords.
            if (this.vocab.containsWord(tLower) && !stopWords.contains(tLower)) {
                inWordVectors.add(tLower);
            } else {
                notInWordVectors.add(tLower);
            }
        }

        this.logger.debug("WholeTextEmbeddingFeatureProcessor: Words in Embedding = '{}', Words not in Embedding = '{}'", inWordVectors, notInWordVectors);

        INDArray wordsVector = Nd4j.zeros(wordVectors.getWordVectorMatrix(".").length());
        String wordsLabel = "";
        for (String inWord : inWordVectors) {
            wordsLabel += inWord + " ";
            wordsVector = wordsVector.add(this.wordVectors.getWordVectorMatrix(inWord));
        }

        // Convert to Unit vec.
        wordsVector = Transforms.unitVec(wordsVector);
        wordsVector = Nd4j.toFlattened(wordsVector);
        wordsLabel = wordsLabel.trim();

        // Type is fixed for now. TODO: Support Word2Vec, eventually mixed text and image.
        this.logger.debug("WholeTextAddEmbeddingFeatureProcessor: Created RlUnit - embedding label = {}, type = {}, vector = {}, dimensionality = {}",
                wordsLabel, options.get("embeddingsType"), wordsVector, wordsVector.length());

        // This shouldn't be needed but the INDArray Serializer isn't working correctly with Jackson and so has to be stored
        // and serialized to XML as a simple list of doubles.
        double[] data = wordsVector.data().asDouble();
        List<Double> dataList = new ArrayList<Double>();
        for (int i = 0; i < data.length; i++) {
            dataList.add(Double.valueOf(data[i]));
        }

        rlUnit.getEmbeddings().add(Embedding.createEmbedding(wordsLabel, (String) options.get("embeddingsType"), "addition", "text",
                dataList, wordsVector.length()));

        return rlUnit;
    }

    @Override
    public RlUnit resetFeature(RlUnit rlUnit) {
        // TODO: When multiple embeddings are supported then will need to only reset those specific to this feature.
        rlUnit.setEmbeddings(new LinkedList<Embedding>());
        return rlUnit;
    }

    @Override
    public void countFeatures(SortedSet<RlUnit> rlUnits, NavigableMap<ReeloutStat, AtomicDouble> counts, Map<String, Object> options) {

        // Go Over all of the RlUnits in parallel.
        rlUnits.parallelStream().forEach(s -> {

            Counter<String> distances = new Counter<>();

            // Add the number of words that make up the embedding.
            for (Embedding t : s.getEmbeddings()) {

                // Add the length of each embedding.
                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "length",
                                String.valueOf(t.getLabel().split(" ").length)),
                        k -> new AtomicDouble()).addAndGet(1.0);

                // Keep a count of the words included in each embedding.
                for (String word : Arrays.asList(t.getLabel().split(" "))) {
                    counts.computeIfAbsent(
                            new ReeloutStat(FeatureType.ADD_WORD_EMBEDDING.getName(), "word",
                                    word),
                            k -> new AtomicDouble()).addAndGet(1.0);
                }
            }

        });
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
