package uk.ac.ed.reelout.features;

import com.google.common.util.concurrent.AtomicDouble;
import com.ibm.watson.developer_cloud.alchemy.v1.AlchemyLanguage;
import com.ibm.watson.developer_cloud.alchemy.v1.model.Taxonomies;
import com.ibm.watson.developer_cloud.alchemy.v1.model.Taxonomy;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.domain.Theme;

import java.util.*;

import static uk.ac.ed.reelout.features.FeatureType.THEME;

/**
 * Creates and adds theme information (topics).
 * <p>
 * These themes are from IBMs Bluemix Alchemy API (http://www.alchemyapi.com/)
 */
@Component
public class ThemeFeatureProcessor extends AlchemyFeatureProcessor {

    public ThemeFeatureProcessor() {
    }

    @Override
    public RlUnit createFeature(RlUnit rlUnit, Map<String, Object> options) {
        Map<String, Object> params = new HashMap<String, Object>(1);
        params.put(AlchemyLanguage.TEXT, rlUnit.getMessageText());
        Taxonomies taxonomies = alchLang.getTaxonomy(params).execute();

        List<Theme> themes = new LinkedList<Theme>();
        List<Taxonomy> taxonomyList = taxonomies.getTaxonomy();
        for (Taxonomy t : taxonomyList) {
            // The category hierarchies are forward slash delimited.
            List<String> categories = Arrays.asList(t.getLabel().split("/"));
            categories = categories.subList(1, categories.size()); // remove the start / from the list.
            Theme theme = new Theme(t.getScore(), categories);
            themes.add(theme);
        }

        rlUnit.setThemes(themes);
        logger.debug("ThemeFeatureProcessor: messageText = {}, themes = {}", rlUnit.getMessageText(), themes);
        return rlUnit;
    }

    @Override
    public RlUnit resetFeature(RlUnit rlUnit) {
        rlUnit.setThemes(new LinkedList<Theme>());
        return rlUnit;
    }

    @Override
    public int getOrder() {
        return Ordered.LOWEST_PRECEDENCE;
    }

    /**
     * Accumulates the counts of different Themes.
     *
     * @param rlUnits the image features.
     * @param counts  to be added to.
     * @param options
     */
    @Override
    public void countFeatures(SortedSet<RlUnit> rlUnits, NavigableMap<ReeloutStat, AtomicDouble> counts, Map<String, Object> options) {
        // Go Over all of the RlUnits in parallel.
        rlUnits.parallelStream().forEach(s -> {
            for (Theme t : s.getThemes()) {
                // Join the levels of the theme using a / separator.
                counts.computeIfAbsent(
                        new ReeloutStat(THEME.getName(), "",
                                StringUtils.join(t.getCategories(), "/")),
                        k -> new AtomicDouble()).addAndGet(1.0);
            }

        });

    }

}
