package uk.ac.ed.reelout.features;

import com.google.common.primitives.Doubles;
import com.google.common.util.concurrent.AtomicDouble;
import org.deeplearning4j.berkeley.Counter;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.*;
import uk.ac.ed.reelout.repository.ImageFeatureRepository;

import java.util.*;

/**
 * Statistics only processor for calculating the closest embeddings using Cosine similarity
 */
@Component
public class CosineSimilarityImageEmbeddingFeatureProcessor extends GenericFeatureProcessor {

    ImageFeatureRepository imageFeature;

    /**
     * Autowire in the image service.
     *
     * @param imageFeature
     */
    @Autowired
    public CosineSimilarityImageEmbeddingFeatureProcessor(ImageFeatureRepository imageFeature) {
        this();
        this.imageFeature = imageFeature;
    }

    public CosineSimilarityImageEmbeddingFeatureProcessor() {
    }


    /**
     * Override so has access to all RlUnits.
     *
     * @param db      RlDatabase
     * @param fromId  filter from.
     * @param toId    filter to.
     * @param options for the processor.
     * @return RlDatabase
     */
    @Override
    public RlDatabase createFeature(RlDatabase db, String fromId, String toId, Map<String, Object> options) {

        if (!options.containsKey("imageFeatureDirectory") || options.get("imageFeatureDirectory") == null) {
            throw new IllegalArgumentException("imageFeatureDirectory option must be provided when running Cosine Similarity for image features");
        }

        // Load the image features.
        imageFeature.loadFeatures((String) options.get("imageFeatureDirectory"));

        RlUnitFilter.getFilteredRlUnits(db, fromId, toId).parallelStream().forEach(rlUnit -> {
            this.logger.debug("RLUnit before feature: {}", rlUnit);
            rlUnit = resetFeature(rlUnit);

            if (!options.containsKey("embeddingClosestN") || options.get("embeddingClosestN") == null) {
                throw new IllegalArgumentException("embeddingClosestN must be provided for computing cosine similarity");
            }

            // Needs access to the other RlUnits so needs to be here.
            Counter<String> cosineSimilarityCounter = new Counter<>();
            List<Double> allSimilarities = new ArrayList<Double>();

            for (RlUnit rlUnit2 : db.getRlUnits()) {
                if (rlUnit.equals(rlUnit2)) {
                    continue; // Don't compare with self.
                }
                double similarity = imageFeature.cosineSimilarity(rlUnit, rlUnit2);
                cosineSimilarityCounter.incrementCount(rlUnit2.getId(), similarity);
                allSimilarities.add(similarity);

            }


            List<Similarity> similarities = new ArrayList<Similarity>();

            INDArray allSimArray = Nd4j.create(Doubles.toArray(allSimilarities));
            Double average = Nd4j.mean(allSimArray).getDouble(0);
            Double variance = Nd4j.var(allSimArray).getDouble(0);

            // Find the closest n, subtract 1 as the count starts at 0.
            cosineSimilarityCounter.keepTopNKeys((int) options.get("embeddingClosestN") - 1);
            List<String> keys = cosineSimilarityCounter.getSortedKeys();

            for (String key : keys) {
                similarities.add(Similarity.createSimilarity(key, cosineSimilarityCounter.getCount(key)));
            }

            rlUnit.getSimilarityMeasures().add(SimilarityMeasure.createSimilarityMeasure(
                    FeatureType.COSINE_SIMILARITY_IMAGE_EMBEDDING.getName(),
                    similarities, average, variance));


            this.logger.debug("RLUnit after feature: {}", rlUnit);
        });
        return db;
    }

    @Override
    public RlUnit createFeature(RlUnit rlUnit, Map<String, Object> options) {
        throw new IllegalStateException("CosineSimilarityEmbeddingFeatureProcessor only supports statistics not adding features.");
    }

    @Override
    public RlUnit resetFeature(RlUnit rlUnit) {
        rlUnit.getSimilarityMeasures().removeIf(item ->
                FeatureType.COSINE_SIMILARITY_IMAGE_EMBEDDING.getBeanName().equals(item.getType()));
        return rlUnit;
    }

    /**
     * Calculates the closest N RlUnits according to whole text cosine similarity on statistics and saves as statistics.
     * <p>
     *
     * @param rlUnits the image RlUnits.
     * @param counts  to accumulate into.
     * @param options for specific statistics being calculated.
     */
    @Override
    public void countFeatures(SortedSet<RlUnit> rlUnits, NavigableMap<ReeloutStat, AtomicDouble> counts, Map<String, Object> options) {

        // Go Over all of the RlUnits in parallel.
        rlUnits.parallelStream().forEach(s -> {

            Counter<String> distances = new Counter<>();

            // Add the number of words that make up the embedding.
            for (SimilarityMeasure t : s.getSimilarityMeasures()) {

                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.COSINE_SIMILARITY_IMAGE_EMBEDDING.getName(),
                                t.getType(),
                                "mean"),
                        k -> new AtomicDouble()).set(t.getMean()); // Value is the Cosine similarity.

                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.COSINE_SIMILARITY_IMAGE_EMBEDDING.getName(),
                                t.getType(),
                                "variance"),
                        k -> new AtomicDouble()).set(t.getVariance()); // Value is the Cosine similarity.

            }


        });
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
