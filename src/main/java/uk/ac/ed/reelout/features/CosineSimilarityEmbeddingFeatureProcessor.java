package uk.ac.ed.reelout.features;

import com.google.common.primitives.Doubles;
import com.google.common.util.concurrent.AtomicDouble;
import org.deeplearning4j.berkeley.Counter;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.ops.transforms.Transforms;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.*;

import java.util.*;

/**
 * Statistics only processor for calculating the closest embeddings using Cosine similarity
 */
@Component
public class CosineSimilarityEmbeddingFeatureProcessor extends GenericFeatureProcessor {


    /**
     * Override so can set the Word embeddings before processing.
     *
     * @param db      RlDatabase
     * @param fromId  filter from.
     * @param toId    filter to.
     * @param options for the processor.
     * @return RlDatabase
     */
    @Override
    public RlDatabase createFeature(RlDatabase db, String fromId, String toId, Map<String, Object> options) {

        RlUnitFilter.getFilteredRlUnits(db, fromId, toId).parallelStream().forEach(s -> {
            logger.debug("RLUnit before feature: {}", s);
            s = this.resetFeature(s);


            // Needs access to the other RlUnits so needs to be here.
            Counter<String> cosineSimilarityCounter = new Counter<>();
            List<Double> allSimilarities = new ArrayList<Double>();
            for (Embedding t : s.getEmbeddings()) {

                // If options are enabled then find the closest N embeddings using Cosine Similarity.

                if (!options.containsKey("embeddingClosestN") || options.get("embeddingClosestN") == null) {
                    throw new IllegalArgumentException("embedding_closest_is mandatory when add Similarity based features.");
                }


                for (RlUnit rl : db.getRlUnits()) {
                    if (s.equals(rl)) {
                        continue; // Don't compare with self.
                    }

                    // Compare each embedding. Currently will select the nearest if there are multiple.
                    // Potentially it could be extended to measure average or furthest, etc.

                    for (Embedding firstEmbedding : s.getEmbeddings()) {
                        for (Embedding secondEmbedding : rl.getEmbeddings()) {
                            double similarity = Transforms.cosineSim(
                                    firstEmbedding.getVectorAsIndArray(),
                                    secondEmbedding.getVectorAsIndArray());
                            this.logger.debug("Cosine similarity between whole text embeddings: first id = {} first label = {}, " +
                                            "second id = {}, second label = {}, cosine similarity = {}" +
                                            ", distance = {}", s.getId(), firstEmbedding.getLabel(), rl.getId(),
                                    secondEmbedding.getLabel(), similarity);

                            cosineSimilarityCounter.incrementCount(rl.getId(), similarity);
                            allSimilarities.add(similarity);
                        }
                    }

                }

            }

            List<Similarity> similarities = new ArrayList<Similarity>();

            INDArray allSimArray = Nd4j.create(Doubles.toArray(allSimilarities));
            Double average = Nd4j.mean(allSimArray).getDouble(0);
            Double variance = Nd4j.var(allSimArray).getDouble(0);

            // Find the closest n, subtract 1 as the count starts at 0.
            cosineSimilarityCounter.keepTopNKeys((int) options.get("embeddingClosestN") - 1);
            List<String> keys = cosineSimilarityCounter.getSortedKeys();

            for (String key : keys) {
                similarities.add(Similarity.createSimilarity(key, cosineSimilarityCounter.getCount(key)));
            }

            s.getSimilarityMeasures().add(SimilarityMeasure.createSimilarityMeasure(
                    FeatureType.COSINE_SIMILARITY_EMBEDDING.getName(),
                    similarities, average, variance));


            logger.debug("RLUnit after feature: {}", s);
        });
        return db;
    }

    @Override
    public RlUnit createFeature(RlUnit rlUnit, Map<String, Object> options) {
        throw new IllegalStateException("CosineSimilarityEmbeddingFeatureProcessor runs through the List of RlUnits method");
    }

    @Override
    public RlUnit resetFeature(RlUnit rlUnit) {
        rlUnit.getSimilarityMeasures().removeIf(item ->
                FeatureType.COSINE_SIMILARITY_EMBEDDING.getBeanName().equals(item.getType()));
        return rlUnit;
    }

    /**
     * Calculates the closest N RlUnits according to whole text cosine similarity on statistics and saves as statistics.
     * <p>
     * TODO: Needs to be reworked following the creation of the main feature that calculates a similar measure.
     *
     * @param rlUnits the image RlUnits.
     * @param counts  to accumulate into.
     * @param options for specific statistics being calculated.
     */
    @Override
    public void countFeatures(SortedSet<RlUnit> rlUnits, NavigableMap<ReeloutStat, AtomicDouble> counts, Map<String, Object> options) {

        // Go Over all of the RlUnits in parallel.
        rlUnits.parallelStream().forEach(s -> {

            Counter<String> distances = new Counter<>();

            // Add the number of words that make up the embedding.
            for (SimilarityMeasure t : s.getSimilarityMeasures()) {


                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.COSINE_SIMILARITY_EMBEDDING.getName(),
                                t.getType(),
                                "mean"),
                        k -> new AtomicDouble()).set(t.getMean()); // Value is the Cosine similarity.

                counts.computeIfAbsent(
                        new ReeloutStat(FeatureType.COSINE_SIMILARITY_EMBEDDING.getName(),
                                t.getType(),
                                "variance"),
                        k -> new AtomicDouble()).set(t.getVariance()); // Value is the Cosine similarity.

            }


        });
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
