package uk.ac.ed.reelout.features;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Dynamically create a feature chain based on the enums passed into the factory.
 */
@Component
public class FeatureChainFactory {

    @Autowired // Will automatically inject a map of all the different feature implementations with default names.
            Map<String, GenericFeatureProcessor> featureMap;

    public FeatureChainFactory() {
    }

    public Map<String, GenericFeatureProcessor> getFeatureMap() {
        return featureMap;
    }

    public void setFeatureMap(Map<String, GenericFeatureProcessor> featureMap) {
        this.featureMap = featureMap;
    }

    /**
     * Constructs a FeatureChain
     *
     * @param featureTypes
     * @return FeatureChain
     */
    public FeatureChain createFeatureChain(List<FeatureType> featureTypes) {
        List<GenericFeatureProcessor> featureBeans = new LinkedList<GenericFeatureProcessor>();
        for (FeatureType ft : featureTypes) {
            GenericFeatureProcessor processor = featureMap.get(ft.getBeanName());
            featureBeans.add(processor);
        }
        return new FeatureChain(featureBeans);
    }

}
