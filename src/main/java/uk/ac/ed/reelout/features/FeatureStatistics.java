package uk.ac.ed.reelout.features;

import com.google.common.util.concurrent.AtomicDouble;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;

import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedSet;

/**
 * Interfaces for being able to calculate statistics for features.
 */
public interface FeatureStatistics {

    /**
     * Create counts for features.
     *
     * @param db      reelout database.
     * @param counts  to accumulate into.
     * @param fromId  Id range from.
     * @param toId    Id range to.
     * @param options for specific statistics being calculated.
     */
    void countFeatures(RlDatabase db, NavigableMap<ReeloutStat, AtomicDouble> counts, String fromId, String toId,
                       Map<String, Object> options);

    /**
     * Create count for features.
     *
     * @param unit    to count for.
     * @param counts  to accumulate into.
     * @param options for specific statistics being calculated.
     */
    void countFeatures(SortedSet<RlUnit> unit, NavigableMap<ReeloutStat, AtomicDouble> counts, Map<String, Object> options);
}
