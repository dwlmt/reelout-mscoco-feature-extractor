package uk.ac.ed.reelout.repository;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.ops.transforms.Transforms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import uk.ac.ed.reelout.domain.RlUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Provides a service for calculating Word embedding based similarities on Demand.
 */
@Repository
public class ParagraphEmbeddingFeatureRepository implements OnDemandSimilarity {

    public static final int LAYERS_SIZE = 300;
    private final Logger logger = LoggerFactory.getLogger(ParagraphEmbeddingFeatureRepository.class);

    String path = "";
    private ParagraphVectors paragraphVectors;

    TokenizerFactory t = new DefaultTokenizerFactory();

    @Override
    public INDArray fillSimilarityArray(Set<RlUnit> first, Set<RlUnit> second,
                                        Map<String, Integer> idToNumMap, INDArray existing) {
        first.parallelStream().forEach(fRl -> {
            for (RlUnit sRl : second) {
                if (fRl.getId().equals(sRl.getId())) {
                    continue; // Don't create similarities between the same Ids.
                }
                double similarity = cosineSimilarity(fRl, sRl);
                // Subtract 1 to get to the zero index.
                int[] indices = {idToNumMap.get(fRl.getId()).intValue() - 1, idToNumMap.get(sRl.getId()).intValue() - 1};
                existing.putScalar(indices, similarity);
            }
        });
        return existing;
    }

    @Override
    public int loadFeatures(String path) {

        /**
         * Keep the existing word vectors if the path has not changed.
         */
        if (this.path.equals(path)) {
            return 0;
        }
        path = path;

        paragraphVectors = WordVectorSerializer.readParagraphVectorsFromText(path);

        // Tokenizer is required for comparing query sentence with a label.
        t.setTokenPreProcessor(new CommonPreprocessor());
        paragraphVectors.setTokenizerFactory(t);

        return paragraphVectors.getVocab().totalNumberOfDocs();
    }

    @Override
    public double cosineSimilarity(RlUnit rl1, RlUnit rl2) {
        double similarity = paragraphVectors.similarity(rl1.getId(), rl2.getId());

        logger.debug("Cosine similarity between id1 = {}, and id2 = {} is = {}", rl1.getId(), rl2.getId(), similarity);
        return similarity;
    }

    /**
     * Returns a similarity array 1 X rlUnits.size() for the Cosine similarity between the query rlUnits. The indices
     * are mapped from idToNumMap.
     *
     * @param query
     * @param rlUnits
     * @param idToNumMap
     * @return similarities
     */
    public INDArray similaritiesToQuery(String query, Set<RlUnit> rlUnits, Map<String, Integer> idToNumMap) {
        INDArray similarities = Nd4j.zeros(1, rlUnits.size());

        rlUnits.parallelStream().forEach(rl -> {
            double similarity = this.similarityToLabel(query, rl.getId());
            logger.debug("Cosine similarity between query and RlUnit {} = {}", rl.getId(), similarity);
            int[] indices = {0, idToNumMap.get(rl.getId()) - 1}; // Minus one to make it zero indexed.
            similarities.putScalar(indices, similarity);
        });

        return similarities;
    }

    /**
     *
     * * HACK because DeepLearning4J doesn't reload the correct layer size when reloading Paragraph Vectors.
     *
     * This method returns similarity of the document to specific label, based on mean value
     *
     * @param rawText
     * @param label
     * @return
     */
    public double similarityToLabel(String rawText, String label) {
        if (t == null) throw new IllegalStateException("TokenizerFactory should be defined, prior to predict() call");

        List<String> tokens = t.create(rawText).getTokens();
        List<VocabWord> document = new ArrayList<>();
        for (String token: tokens) {
            if (this.paragraphVectors.getVocab().containsWord(token)) {
                document.add(this.paragraphVectors.getVocab().wordFor(token));
            }
        }
        return this.similarityToLabel(document, label);
    }

    /**
     * HACK because DeepLearning4J doesn't reload the correct layer size when reloading Paragraph Vectors.
     *
     * This method returns similarity of the document to specific label, based on mean value
     *
     * @param document
     * @param label
     * @return
     */
    public double similarityToLabel(List<VocabWord> document, String label) {
        if (document.isEmpty()) throw new IllegalStateException("Document has no words inside");

        INDArray arr = Nd4j.create(document.size(),LAYERS_SIZE);
        for(int i = 0; i < document.size(); i++) {
            arr.putRow(i,paragraphVectors.getWordVectorMatrix(document.get(i).getWord()));
        }

        INDArray docMean = arr.mean(0);

        INDArray otherVec = paragraphVectors.getWordVectorMatrix(label);
        double sim = Transforms.cosineSim(docMean, otherVec);
        return sim;
    }

}
