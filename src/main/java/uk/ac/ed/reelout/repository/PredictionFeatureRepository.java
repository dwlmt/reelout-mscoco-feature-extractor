package uk.ac.ed.reelout.repository;

import org.springframework.stereotype.Repository;

/**
 * Empty class. Code is the same as the image repository, makes autowiring simpler.
 */
@Repository
public class PredictionFeatureRepository extends AbstractFeatureRepository {

    public PredictionFeatureRepository() {
    }
}
