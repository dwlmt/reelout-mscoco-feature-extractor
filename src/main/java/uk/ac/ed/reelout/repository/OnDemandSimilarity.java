package uk.ac.ed.reelout.repository;

import org.nd4j.linalg.api.ndarray.INDArray;
import uk.ac.ed.reelout.domain.RlUnit;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * Interface for calculating similarities between images on demand rather than from the feature file.
 */
public interface OnDemandSimilarity {
    /**
     * Fill an empty similarity array with the Similarities from the first to second RlUnits.
     *
     * @param first      Set of RlUnits.
     * @param second     Set of RlUnits
     * @param idToNumMap mapping between RlUnit Ids and array index position.
     * @param existing   the raw INDArray.
     * @return INDArray
     */
    INDArray fillSimilarityArray(Set<RlUnit> first, Set<RlUnit> second, Map<String, Integer> idToNumMap, INDArray existing);

    /**
     * Load external image or word embedding features.
     *
     * @param path to search.
     * @return int count of how many  features are loaded.
     * @throws IOException if the file directory path is inavalid.
     */
    int loadFeatures(String path);

    /**
     * Return the Cosine Similarity
     *
     * @param id  of first RlUnit.
     * @param id2 of second RlUnit.
     * @return double.
     */
    double cosineSimilarity(RlUnit id, RlUnit id2);
}
