package uk.ac.ed.reelout.repository;

import com.google.common.io.Files;
import org.apache.commons.io.FileUtils;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.ops.transforms.Transforms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.ed.reelout.domain.RlUnit;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Parent feature repository  for image and prediction features.
 */
public class AbstractFeatureRepository implements OnDemandSimilarity {

    private final Logger logger = LoggerFactory.getLogger(AbstractFeatureRepository.class);


    Map<String, INDArray> features = new ConcurrentHashMap<String, INDArray>();

    String path = "";

    /**
     * Finds CSV files in a directory containing matrices and load them into features.
     *
     * @param path to search.
     * @return int count of how many image features are loaded.
     * @throws IOException if the file directory path is invalid.
     */
    @Override
    public int loadFeatures(String path) {

        if (this.path.equals(path)) {
            return 0;
        }

        this.logger.info("Load features from = {}", path);

        this.path = path;

        int loaded = 0;


        String[] paths = path.split(",");
        for (int i = 0; i < paths.length; i++) {

            Collection<File> files =
                    FileUtils.listFiles(FileUtils.getFile(paths[i]), new String[]{"csv"}, true);

            loaded = files.size();
            files.parallelStream().forEach(file -> {

                try {

                    // For each file convert into a vector and put in the map.
                    List<String> lines = Files.readLines(file, Charset.defaultCharset());
                    String[] fileNameArray = file.getName().split("[.]");
                    String id = fileNameArray[0];
                    String[] numbers = lines.get(0).split(",");
                    double[] data = new double[numbers.length];
                    for (int j = 0; j < numbers.length; j++) {
                        data[j] = Double.valueOf(numbers[j].trim());
                    }
                    INDArray featureVector = Nd4j.create(data);
                    features.put(id, featureVector);
                    // Catch and wrap so the feature chain isn't full of specific exceptions for each feature.
                } catch (IOException ioe) {
                    throw new IllegalArgumentException("Features cannot be loaded from the specified directory.", ioe);
                }

            });

        }

        this.logger.info("Features loaded = {}", loaded);
        return loaded;
    }

    /**
     * Returns the cosine similarity between two image features.
     * <p>
     * If either or both are not present then the similarity is 0.0.
     *
     * @param rl1 of the RlUnit.
     * @param rl2 of the RlUnit.
     * @return double 0.0 - 1.0 of similarity between images.
     */
    @Override
    public double cosineSimilarity(RlUnit rl1, RlUnit rl2) {
        INDArray id1Vector = features.get(rl1.getId());
        INDArray id2Vector = features.get(rl2.getId());

        if (id1Vector == null || id2Vector == null) {
            this.logger.debug("Image features not present for id1 = {}, and id2 = {}", rl1.getId(), rl2.getId());
            return 0.0;
        }

        double similarity = 0.0;
        similarity = Transforms.cosineSim(id1Vector, id2Vector);

        this.logger.debug("Cosine similarity between id1 = {}, and id2 = {} is = {}", rl1.getId(), rl2.getId(), similarity);
        return similarity;
    }

    /**
     * Fills in the matrix with Cosine similarity between the image features from the first to the second.
     * The index positions comes from idToNumMap.
     * <p>
     * If there are no image features for either image then the similarity is 0.0.
     *
     * @param first      set of RlUnits.
     * @param second     set of RlUnits.
     * @param idToNumMap mapping of Ids to matrix positions.
     * @param existing   the matrix to fill.
     * @return the modified matrix.
     */
    @Override
    public INDArray fillSimilarityArray(Set<RlUnit> first, Set<RlUnit> second, Map<String, Integer> idToNumMap, INDArray existing) {
        first.parallelStream().forEach(fRl -> {
            for (RlUnit sRl : second) {
                if (fRl.getId().equals(sRl.getId())) {
                    continue; // Don't create similarities between the same Ids.
                }
                double similarity = this.cosineSimilarity(fRl, sRl);
                // Subtract 1 to get to the zero index.
                int[] indices = {idToNumMap.get(fRl.getId()).intValue() - 1, idToNumMap.get(sRl.getId()).intValue() - 1};
                existing.putScalar(indices, similarity);
            }
        });
        return existing;
    }
}
