package uk.ac.ed.reelout.repository;

import org.springframework.stereotype.Repository;

/**
 * Image feature service provides access to Fast RCNN features.
 */
@Repository
public class ImageFeatureRepository extends AbstractFeatureRepository {

    public ImageFeatureRepository() {
    }

}
