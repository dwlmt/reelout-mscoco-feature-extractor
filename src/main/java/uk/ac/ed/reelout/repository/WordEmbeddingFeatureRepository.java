package uk.ac.ed.reelout.repository;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import org.deeplearning4j.models.embeddings.WeightLookupTable;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.ops.transforms.Transforms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.exceptions.TriptychSelectionException;
import uk.ac.ed.reelout.utils.StopWords;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Provides a service for calculating Word embedding based similarities on Demand.
 */
@Repository
public class WordEmbeddingFeatureRepository implements OnDemandSimilarity {

    private final Logger logger = LoggerFactory.getLogger(WordEmbeddingFeatureRepository.class);

    // Word embeddings classes.
    WordVectors wordVectors;
    WeightLookupTable lookup;
    VocabCache vocab;
    Set<String> stopWords;
    String path = "";

    /**
     * Map the Id to the combined word embeddings.
     */
    Map<String, INDArray> IdToEmbeddings = new ConcurrentHashMap<String, INDArray>();
    private boolean excludeStopWords;

    /**
     * Set whether to exclude stopwords.
     *
     * @param excludeStopWords
     */
    public void setExcludeStopWords(boolean excludeStopWords) {
        this.excludeStopWords = excludeStopWords;
    }

    @Override
    public INDArray fillSimilarityArray(Set<RlUnit> first, Set<RlUnit> second,
                                        Map<String, Integer> idToNumMap, INDArray existing) {
        first.parallelStream().forEach(fRl -> {
            for (RlUnit sRl : second) {
                if (fRl.getId().equals(sRl.getId())) {
                    continue; // Don't create similarities between the same Ids.
                }
                double similarity = cosineSimilarity(fRl, sRl);
                // Subtract 1 to get to the zero index.
                int[] indices = {idToNumMap.get(fRl.getId()).intValue() - 1, idToNumMap.get(sRl.getId()).intValue() - 1};
                existing.putScalar(indices, similarity);
            }
        });
        return existing;
    }

    @Override
    public int loadFeatures(String path) {

        /**
         * Keep the existing word vectors if the path has not changed.
         */
        if (this.path.equals(path)) {
            return 0;
        }

        this.logger.info("Load word embedding features from = {}", path);

        this.path = path;

        // Try loading as text and a binary representation using the google model if it fails
        try {
            this.wordVectors = WordVectorSerializer.loadTxtVectors(new File(path));
        } catch (Exception e) {
            try {
                this.wordVectors = WordVectorSerializer.loadGoogleModel(new File(path), true);
            } catch (IOException e1) {
                this.logger.error("WordEmbeddingFeatureService: Embeddings file not found = {}", path);
                throw new IllegalArgumentException("Embeddings file cannot be found or loaded.", e);
            }
        }

        this.lookup = this.wordVectors.lookupTable();
        this.vocab = this.lookup.getVocabCache();

        this.stopWords = new TreeSet<String>(StopWords.getStopWords());

        this.logger.info("Word embeddings loaded from '{}' = {}", path, this.vocab.words().size());

        return this.vocab.words().size();
    }

    @Override
    public double cosineSimilarity(RlUnit rl1, RlUnit rl2) {

        // If there is no embedding for the RlUnit then populate the Id.
        if (!IdToEmbeddings.containsKey(rl1)) {
            this.populateEmbedding(rl1);
        }
        if (!IdToEmbeddings.containsKey(rl2)) {
            this.populateEmbedding(rl2);
        }

        double similarity = Transforms.cosineSim(IdToEmbeddings.get(rl1.getId()), IdToEmbeddings.get(rl2.getId()));
        logger.debug("Cosine similarity between id1 = {}, and id2 = {} is = {}", rl1.getId(), rl2.getId(), similarity);
        return similarity;
    }

    /**
     * Returns a similarity array 1 X rlUnits.size() for the Cosine similarity between the query rlUnits. The indices
     * are mapped from idToNumMap.
     *
     * @param query
     * @param rlUnits
     * @param idToNumMap
     * @return similarities
     */
    public INDArray similaritiesToQuery(INDArray query, Set<RlUnit> rlUnits, Map<String, Integer> idToNumMap) {
        INDArray similarities = Nd4j.zeros(1, rlUnits.size());

        rlUnits.parallelStream().forEach(rl -> {
            double similarity = Transforms.cosineSim(query, this.createWordEmbeddingForTokens(rl.getTokens()));
            logger.debug("Cosine similarity between query and RlUnit {} = {}", rl.getId(), similarity);
            int[] indices = {0, idToNumMap.get(rl.getId()) - 1}; // Minus one to make it zero indexed.
            similarities.putScalar(indices, similarity);
        });

        return similarities;
    }

    /**
     * Create the embeddings through simple addition and put the result into the cache map.
     *
     * @param rl RlUnit
     */
    private void populateEmbedding(RlUnit rl) {

        // Iterate over
        List<String> tokens = rl.getTokens();

        INDArray wordsVector = this.createWordEmbeddingForTokens(tokens);

        IdToEmbeddings.putIfAbsent(rl.getId(), wordsVector);
    }

    /**
     * Creates an embedding by parsing the given text and adding all of the word embeddings that exist for words in the text.
     *
     * @param querySentence
     * @return INDArray
     */
    public INDArray createWordEmbeddingForQuerySentence(String querySentence) {
        PTBTokenizer ptbt = new PTBTokenizer(new StringReader(querySentence.trim().toLowerCase()),
                new CoreLabelTokenFactory(), "");

        List<CoreLabel> tokens = ptbt.tokenize();
        List<String> words = new LinkedList<String>();
        // Get the strings for each of the received words.
        tokens.forEach(w -> {
            words.add(w.word());
        });

        if (words.isEmpty()) {
            throw new TriptychSelectionException("Query sentence must not be empty");
        }

        return createWordEmbeddingForTokens(words);
    }

    /**
     * Create a word embedding for a set of tokens.
     *
     * @param tokens
     * @return INDArray
     */
    private INDArray createWordEmbeddingForTokens(List<String> tokens) {
        SortedSet<String> inWordVectors = new TreeSet<String>();
        SortedSet<String> notInWordVectors = new TreeSet<String>();

        for (String t : tokens) {
            String tLower = t.toLowerCase();
            // Think about whether to remove stopwords.
            if (this.vocab.containsWord(tLower) && (!stopWords.contains(tLower) || !excludeStopWords)) {
                inWordVectors.add(tLower);
            } else {
                notInWordVectors.add(tLower);
            }
        }
        this.logger.debug("Words in Embedding = '{}', Words not in Embedding = '{}'", inWordVectors, notInWordVectors);

        INDArray wordsVector = Nd4j.zeros(wordVectors.getWordVectorMatrix(".").length());
        for (String inWord : inWordVectors) {
            wordsVector = wordsVector.add(this.wordVectors.getWordVectorMatrix(inWord));
        }

        // Convert to Unit vec.
        wordsVector = Transforms.unitVec(wordsVector);
        return wordsVector;
    }
}
