package uk.ac.ed.reelout.commands;

import com.google.common.base.Strings;

/**
 * Utility functions shared across command classes.
 */
class CommandHelper {
    /**
     * Mandatory checks for the source and destination.
     *
     * @param source      file.
     * @param destination destination.
     */
    public static void checkSourceAndDestination(String source, String destination) {
        if (Strings.isNullOrEmpty(source)) {
            throw new IllegalArgumentException("Source file must be specified.");
        }

        if (Strings.isNullOrEmpty(destination)) {
            throw new IllegalArgumentException("Destination file must be specified.");
        }
    }
}
