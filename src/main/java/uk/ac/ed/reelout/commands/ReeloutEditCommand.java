package uk.ac.ed.reelout.commands;

import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.features.FeatureChainFactory;
import uk.ac.ed.reelout.selection.SelectionFilters;
import uk.ac.ed.reelout.services.ReeloutConversionService;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListSet;

@Component
public class ReeloutEditCommand implements CommandMarker {

    private static final double DATABASE_FORMAT_VERSION = 0.2;
    private final Logger logger = LoggerFactory.getLogger(ReeloutEditCommand.class);
    private final ReeloutConversionService reelout;
    private final FeatureChainFactory featureChainFactory;
    private final SelectionFilters filters;

    /**
     * Constructor to autowire the command with the services it needs.
     *
     * @param reelout
     * @param featureChainFactory
     */
    @Autowired
    public ReeloutEditCommand(ReeloutConversionService reelout,
                              FeatureChainFactory featureChainFactory, SelectionFilters filters) {
        this.reelout = reelout;
        this.featureChainFactory = featureChainFactory;
        this.filters = filters;
    }

    @CliAvailabilityIndicator("reelout join")
    public boolean isReeloutJoinAvailable() {
        return true;
    }

    @CliAvailabilityIndicator("reelout cut")
    public boolean isReeloutCutAvailable() {
        return true;
    }

    @CliAvailabilityIndicator("reelout random")
    public boolean isRandomSelectionAvailable() {
        return true;
    }

    @CliAvailabilityIndicator("reelout swap text")
    public boolean isSwapTextAvailable() {
        return true;
    }

    @CliAvailabilityIndicator("reelout filter")
    public boolean isFilterAvailable() {
        return true;
    }

    /**
     * Combine multiple databases into a single one.
     *
     * @param sources     Source paths and file names for databases to be combined.
     * @param destination Destination for XML. Can be the same as source to overwrite.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "reelout join", help = "Join (or merge) multiple RL Database XML files.")
    public String reeloutJoin(
            @CliOption(key = "sources", mandatory = true, help = "List of XML files to be joined.") String[] sources,
            @CliOption(key = "destination", mandatory = true, help = "Destination file path for the combined XML file.") String destination
    ) throws IOException {
        this.logger.info("Join databases: sources = {}, destination = {}"
                , sources, destination);

        if (sources == null || sources.length == 0) {
            throw new IllegalArgumentException("Sources must be specified.");
        }
        List<String> sourcesList = Arrays.asList(sources);
        RlDatabase joinedDatabase = new RlDatabase();
        joinedDatabase.setVersion((float) ReeloutEditCommand.DATABASE_FORMAT_VERSION);

        // Join each database into the results.
        for (String source : sourcesList) {
            RlDatabase sourceDatabase = this.reelout.convertToDomainFromFile(source);
            joinedDatabase = this.reelout.join(joinedDatabase, sourceDatabase);
        }

        // Write to XML.
        this.logger.info("Write to XML file.");
        this.reelout.convertAndWriteXMLToFile(destination, joinedDatabase);
        return "Databases have been joined successfully.";

    }

    /**
     * Cut out a set of RlUnits from a file.
     *
     * @param source      Source path and file name.
     * @param destination Destination for XML. Can be the same as source to overwrite.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "reelout cut", help = "Cut out a range of RlUnits according to Id ranges and save in a new file.")
    public String reeloutCut(
            @CliOption(key = "source", mandatory = true, help = "Source file path and name for the Reelout XML.") String source,
            @CliOption(key = "destination", mandatory = true, help = "Destination file path and name for the XML.") String destination,
            @CliOption(key = "from_id", mandatory = false, help = "Apply features to a subset of the images starting from this Id. Ids sorted as a String.") String fromIdFilter,
            @CliOption(key = "to_id", mandatory = false, help = "Apply features to a subset of the images ending at this Id. Ids sorted as a String.") String toIdFilter
    ) throws IOException {
        this.logger.info("Cut databases: sources = {}, destination = {}, from = {}, to = {}"
                , source, destination, fromIdFilter, toIdFilter
        );

        CommandHelper.checkSourceAndDestination(source, destination);

        if (Strings.isNullOrEmpty(fromIdFilter) && Strings.isNullOrEmpty(fromIdFilter)) {
            throw new IllegalArgumentException("from_id or to_id must be specified.");
        }

        this.logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = this.reelout.convertToDomainFromFile(source);

        rlDb = this.reelout.cut(rlDb, fromIdFilter, toIdFilter);

        // Write to XML.
        this.logger.info("Write to XML file.");
        this.reelout.convertAndWriteXMLToFile(destination, rlDb);
        return "Database range has been cut and saved to a new XML file.";

    }

    /**
     * Cut out a set of RlUnits from a file.
     *
     * @param source      Source path and file name.
     * @param destination Destination for XML. Can be the same as source to overwrite.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "reelout random", help = "Randomly select RLUnits according to --select_n and save in a new file.")
    public String reeloutCut(
            @CliOption(key = "source", mandatory = true, help = "Source file path and name for the Reelout XML.") String source,
            @CliOption(key = "destination", mandatory = true, help = "Destination file path and name for the XML.") String destination,
            @CliOption(key = "select_n", mandatory = true, help = "How many RlUnits should be randomly selected.") int selectN
    ) throws IOException {
        this.logger.info("Randomly select RlUnits: sources = {}, destination = {}, select_n = {}"
                , source, destination, selectN
        );

        CommandHelper.checkSourceAndDestination(source, destination);

        if (selectN < 1) {
            throw new IllegalArgumentException("select_n must be a positive number.");
        }


        this.logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = this.reelout.convertToDomainFromFile(source);

        ArrayList<RlUnit> rlUnitList = new ArrayList<RlUnit>(rlDb.getRlUnits());

        if (selectN > rlUnitList.size()) {
            throw new IllegalArgumentException("select_n must be smaller than the number of RlUnits in the source file.");
        }

        Collections.shuffle(rlUnitList);

        // Subselect and change the RlUnits
        List<RlUnit> subList = rlUnitList.subList(0, selectN);
        rlDb.setRlUnits(new ConcurrentSkipListSet<RlUnit>(subList));

        // Write to XML.
        this.logger.info("Write to XML file.");
        this.reelout.convertAndWriteXMLToFile(destination, rlDb);
        return "Randomly selected RlUnits have been saved to a new file.";

    }

    /**
     * Swap the message text into another feature file.
     * <p>
     * This is really a shortcut to get around the low Alchemy API call limit so that
     *
     * @param sourceText  Source for the message text file.
     * @param destination Destination for XML. Can be the same as sourceText to overwrite.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "reelout swap text", help = "Swap the message text from one sourceText into another file. " +
            "XMLs must have the same Ids or there will be an error.")
    public String reeloutSwapText(
            @CliOption(key = "source_text", mandatory = true, help = "Source Reelout XML containing the XML.") String sourceText,
            @CliOption(key = "source_features", mandatory = true, help = "Source Reelout XML file containing the features.") String sourceFeatures,
            @CliOption(key = "destination", mandatory = true, help = "Destination file path and name for the XML.") String destination) throws IOException {
        this.logger.info("Reelout swap text: source = {}, destination = {}", sourceText, destination);

        CommandHelper.checkSourceAndDestination(sourceText, destination);


        this.logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = this.reelout.convertToDomainFromFile(sourceText);

        RlDatabase rlDbFeatures = this.reelout.convertToDomainFromFile(sourceFeatures);

        for (RlUnit rlUnit : rlDbFeatures.getRlUnits()) {
            RlUnit messageRlUnit = rlDb.getRlUnitFromId(rlUnit.getId());
            rlUnit.setMessageText(messageRlUnit.getMessageText());
        }

        // Write to XML.
        this.logger.info("Write to XML file.");
        this.reelout.convertAndWriteXMLToFile(destination, rlDbFeatures);
        return "Reelout message text has been swapped";

    }

    /**
     * Allow filtering of a database according to criteria.
     *
     * @param source      Source path and file name.
     * @param destination Destination for XML. Can be the same as source to overwrite.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "reelout filter", help = "Filters the RlUnits in an XML file according to criteria and outputs them to a new destination." +
            " Lists within each attribute are joined with an OR condition. Across different attributes with an AND condition.")
    public String reeloutFilter(
            @CliOption(key = "source", mandatory = true, help = "Source file path and name for the Reelout XML.") String source,
            @CliOption(key = "destination", mandatory = true, help = "Destination file path and name for the XML.") String destination,
            @CliOption(key = "ids", mandatory = false, help = "Comma separated list of Ids.") String[] ids,
            @CliOption(key = "message_text", mandatory = false, help = "Captions for the images.") String[] messageText,
            @CliOption(key = "themes", mandatory = false, help = "The Alchemy API theme of the image.") String[] themes,
            @CliOption(key = "theme_threshold", mandatory = false, specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0",
                    help = "Greater than this confidence for the theme. Value 0.0-1.0.") Double themeThreshold,
            @CliOption(key = "locations", mandatory = false, help = "Location from named entity recognition.") String[] locations,
            @CliOption(key = "entities", mandatory = false, help = "Entity from named entity recognition.") String[] entities,
            @CliOption(key = "subject", mandatory = false, help = "Subject of a caption sentence.") String[] subject,
            @CliOption(key = "verb", mandatory = false, help = "Verb/Relation of a caption sentence.") String[] relation,
            @CliOption(key = "object", mandatory = false, help = "Object of a caption sentence.") String[] object,
            @CliOption(key = "whole_triple", mandatory = false, specifiedDefaultValue = "false", unspecifiedDefaultValue = "false",
                    help = "Consider Subject/Verb,Object separately or together.") Boolean wholeTriple,
            @CliOption(key = "sentiments", mandatory = false, help = "Postive/Neutral/Negative") String[] sentiments,
            @CliOption(key = "before", mandatory = false, help = "Find those before this Datetime.") Date before,
            @CliOption(key = "after", mandatory = false, help = "Find those after this Datetime.") Date after

    ) throws IOException {
        this.logger.info("Filter: sources = {}, destination = {}, ids = {}, message_text = {}, themes = {}," +
                        "theme_threshold = {}, locations = {}, entities = {}, sentiments = {}, subject = {}, verb = {}, object = {}, all_triple = {}," +
                        "before = {}, after = {}"
                , source, destination, ids, messageText, themes, themeThreshold, locations, entities, sentiments,
                subject, relation, object,
                wholeTriple, before, after);

        CommandHelper.checkSourceAndDestination(source, destination);

        this.logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = this.reelout.convertToDomainFromFile(source);
        NavigableSet<RlUnit> filteredUnits = rlDb.getRlUnits();

        logger.info("Filtering the RlUnits according to the specified criteria.");

        filteredUnits = this.filters.filterRlUnits(ids, messageText, themes, themeThreshold, locations, entities, subject, relation,
                object, wholeTriple, sentiments, before, after, filteredUnits);


        logger.info("Filtered units = {}", filteredUnits.size());

        rlDb.setRlUnits(filteredUnits);

        // Write to XML.
        this.logger.info("Write to XML file.");
        this.reelout.convertAndWriteXMLToFile(destination, rlDb);
        return "RlUnits filtered and exported successfully.";

    }


}
