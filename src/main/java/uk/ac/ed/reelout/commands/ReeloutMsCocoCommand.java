package uk.ac.ed.reelout.commands;

import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.features.FeatureChainFactory;
import uk.ac.ed.reelout.features.FeatureType;
import uk.ac.ed.reelout.services.MsCocoConversionService;
import uk.ac.ed.reelout.services.ReeloutConversionService;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

@Component
public class ReeloutMsCocoCommand implements CommandMarker {

    private final Logger logger = LoggerFactory.getLogger(ReeloutMsCocoCommand.class);
    private final MsCocoConversionService coco;
    private final ReeloutConversionService reelout;
    private final FeatureChainFactory featureChainFactory;

    /**
     * Constructor to autowire the command with the services it needs.
     *
     * @param coco
     * @param reelout
     * @param featureChainFactory
     */
    @Autowired
    public ReeloutMsCocoCommand(MsCocoConversionService coco, ReeloutConversionService reelout,
                                FeatureChainFactory featureChainFactory) {
        this.coco = coco;
        this.reelout = reelout;
        this.featureChainFactory = featureChainFactory;
    }

    @CliAvailabilityIndicator("mscoco convert")
    public boolean isMsCocoConvertAvailable() {
        return true;
    }

    @CliCommand(value = "mscoco convert", help = "Convert MS-COCO annotations JSON to ReelOut XML")
    public String mscocoConvert(
            @CliOption(key = "source", mandatory = true, help = "Source file path and name for the MS-COCO JSON.")
                    String source,
            @CliOption(key = "destination", mandatory = true, help = "Destination file path and name for the XML.")
                    String destination,
            @CliOption(key = "join", unspecifiedDefaultValue = "true", specifiedDefaultValue = "true", mandatory = true,
                    help = "true/false - Join different captions for the same image into one RL Unit. Defaults to true.")
                    Boolean joinCaptions,
            @CliOption(key = "max_sentences", unspecifiedDefaultValue = "100", specifiedDefaultValue = "100",
                    mandatory = false, help = "Max number of sentences to select from MS-COCO. How many captions to " +
                    "include per image.")
                    Integer maxSentences) throws IOException {


        CommandHelper.checkSourceAndDestination(source, destination);

        logger.info("MSCOCO JSON to Reelout JSON convert: source = {}, destination = {}, join = {}, max_sentences = {}"
                , source, destination, joinCaptions, maxSentences);
        // Load the JSON object and convert to ReelOut domain objects.

        logger.info("Convert to ReelOut domain representation");
        String msCocoJson = Files.toString(new File(source), StandardCharsets.UTF_8);
        RlDatabase rlDb = coco.convertToReeloutDomain(msCocoJson, joinCaptions, maxSentences);


        List<FeatureType> featureTypes = new LinkedList<FeatureType>();
        featureTypes.add(FeatureType.POS);

        rlDb = featureChainFactory.createFeatureChain(featureTypes).createFeatures(rlDb, null);


        // Write to XML.
        logger.info("Write to XML file.");
        String xml = reelout.convertToXML(rlDb);
        Files.write(xml, new File(destination), StandardCharsets.UTF_8);
        return String.format("MS-COCO transformed and written to XML format: Source = %s, Destination = %s", source, destination);

    }

}
