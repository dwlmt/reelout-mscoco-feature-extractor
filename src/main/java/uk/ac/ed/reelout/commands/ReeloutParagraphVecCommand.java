package uk.ac.ed.reelout.commands;

import com.google.common.base.Strings;
import org.apache.uima.resource.ResourceInitializationException;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.paragraphvectors.ParagraphVectors;
import org.deeplearning4j.text.documentiterator.LabelsSource;
import org.deeplearning4j.text.sentenceiterator.CollectionSentenceIterator;
import org.deeplearning4j.text.sentenceiterator.SentenceIterator;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.services.ReeloutConversionService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Commands for gathering counts and statistics for features present in a ReelOut XML file.
 */
@Component
public class ReeloutParagraphVecCommand implements CommandMarker

{

    private final Logger logger = LoggerFactory.getLogger(ReeloutParagraphVecCommand.class);

    private final ReeloutConversionService reelout;

    /**
     * Constructor to autowire the command with the services it needs.
     *
     * @param reelout
     */
    @Autowired
    public ReeloutParagraphVecCommand(ReeloutConversionService reelout) {
        this.reelout = reelout;
    }

    @CliAvailabilityIndicator("doc2vec")
    public boolean isReeloutStatsAvailable() {
        return true;
    }


    @CliCommand(value = "doc2vec", help = "Train a doc2vec (Paragraph Vec) model from MS-COCO text.")
    public String reeloutStats(
            @CliOption(key = "source", mandatory = true, help = "Source file path and name for the Reelout XML.") String source,
            @CliOption(key = "destination", mandatory = true, help = "Save the stats to this file.") String destination,
            @CliOption(key = "embedding_dim", mandatory = false, specifiedDefaultValue = "300", unspecifiedDefaultValue = "300",
                    help = "Dimensions of the Doc2Vec embedding")
                    Integer embeddingDim,
            @CliOption(key = "iterations", mandatory = false, specifiedDefaultValue = "10", unspecifiedDefaultValue = "10",
                    help = "Training iterations")
                    Integer iterations,
            @CliOption(key = "epochs", mandatory = false, specifiedDefaultValue = "1", unspecifiedDefaultValue = "1",
                    help = "Training iterations")
                    Integer epochs,
            @CliOption(key = "window_size", mandatory = false, specifiedDefaultValue = "8", unspecifiedDefaultValue = "8",
                    help = "Word Window Size")
                    Integer windowSize,
            @CliOption(key = "negative_sample", mandatory = false, specifiedDefaultValue = "10", unspecifiedDefaultValue = "10",
                    help = "Negative sampling.")
                    Double negativeSample,
            @CliOption(key = "sampling", mandatory = false, specifiedDefaultValue = "0", unspecifiedDefaultValue = "0",
                    help = "Downsample high frequency words.")
                    Double sampling,
            @CliOption(key = "train_word_vectors", mandatory = false, specifiedDefaultValue = "false", unspecifiedDefaultValue = "false",
                    help = "Train the word vectors as well or only the paragraph vectors.")
                    Boolean trainWordVectors
    ) throws IOException, ResourceInitializationException {

        if (Strings.isNullOrEmpty(source)) {
            throw new IllegalArgumentException("Source file must be specified.");
        }

        if (Strings.isNullOrEmpty(destination)) {
            throw new IllegalArgumentException("Destination must must be specified.");
        }

        this.logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = this.reelout.convertToDomainFromFile(source);

        List<String> sentences = new ArrayList<String>();
        List<String> labels = new ArrayList<String>();
        for (RlUnit rl : rlDb.getRlUnits()) {
            sentences.add(rl.getMessageText());
            labels.add(rl.getId()); // Use the Id as the target label
        }

        logger.info("Number of documents to train = {}", sentences.size());

        TokenizerFactory t = new DefaultTokenizerFactory();
        t.setTokenPreProcessor(new CommonPreprocessor());

        SentenceIterator iter = new CollectionSentenceIterator(sentences);
        LabelsSource sourceLabels = new LabelsSource(labels);

        ParagraphVectors vec = new ParagraphVectors.Builder()
                .minWordFrequency(1)
                .iterations(iterations).epochs(epochs)
                .layerSize(embeddingDim)
                .useAdaGrad(true)
                .stopWords(new ArrayList<String>()) // No S
                .windowSize(windowSize)
                .iterate(iter)
                .labelsSource(sourceLabels)
                .trainWordVectors(trainWordVectors)
                .negativeSample(negativeSample)
                .sampling(sampling)
                .tokenizerFactory(t)
                .build();


        logger.info("Fit the Doc2Vec model");
        vec.fit();

        logger.info("Save paragraph vectors");
        WordVectorSerializer.writeWordVectors(vec, destination);

        return "Doc2Vec model trained";

    }

}
