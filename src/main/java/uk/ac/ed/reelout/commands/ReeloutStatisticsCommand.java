package uk.ac.ed.reelout.commands;

import com.google.common.base.Strings;
import com.google.common.io.Files;
import com.google.common.util.concurrent.AtomicDouble;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.ReeloutStat;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.features.FeatureChainFactory;
import uk.ac.ed.reelout.features.FeatureType;
import uk.ac.ed.reelout.services.ReeloutConversionService;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static java.util.Arrays.asList;

/**
 * Commands for gathering counts and statistics for features present in a ReelOut XML file.
 */
@Component
public class ReeloutStatisticsCommand implements CommandMarker

{

    private final Logger logger = LoggerFactory.getLogger(ReeloutStatisticsCommand.class);

    private final FeatureChainFactory featureChainFactory;
    private final ReeloutConversionService reelout;

    /**
     * Constructor to autowire the command with the services it needs.
     *
     * @param reelout
     */
    @Autowired
    public ReeloutStatisticsCommand(ReeloutConversionService reelout, FeatureChainFactory featureChainFactory) {
        this.reelout = reelout;
        this.featureChainFactory = featureChainFactory;
    }

    @CliAvailabilityIndicator("reelout stats")
    public boolean isReeloutStatsAvailable() {
        return true;
    }


    @CliCommand(value = "reelout stats", help = "Obtains the counts for the different features present in Reelout XML file.")
    public String reeloutStats(
            @CliOption(key = "source", mandatory = true, help = "Source file path and name for the Reelout XML.") String source,
            @CliOption(key = "destination", mandatory = true, help = "Save the stats to this file.") String destination,
            @CliOption(key = "features", mandatory = true, help = "Obtain stats for the features. Types: POS, SENTIMENT, " +
                    "ENTITY, THEME, ADD_WORD_EMBEDDING, COSINE_SIMILARITY_EMBEDDING")
                    String[] features,
            @CliOption(key = "embedding_closest_n", mandatory = false,
                    help = "For Cosine Similarity how many closest N should be saved to statistics.",
                    specifiedDefaultValue = "10", unspecifiedDefaultValue = "10")
                    int embeddingClosestN) throws IOException {
        logger.info("Reelout stats features: source = {}, destination = {}, features = {}, embedding_closest_n = {}"
                , source, destination, features, embeddingClosestN);

        if (Strings.isNullOrEmpty(source)) {
            throw new IllegalArgumentException("Source file must be specified.");
        }

        if (Strings.isNullOrEmpty(destination)) {
            throw new IllegalArgumentException("Destination for stats must must be specified.");
        }

        // Store options to be parsed through for
        Map<String, Object> options = new HashMap<String, Object>();
        options.put("embeddingClosestN", embeddingClosestN);

        logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = reelout.convertToDomainFromFile(source);

        // If there are features then apply them.
        if (features != null && features.length > 0) {
            List<FeatureType> featureTypes = new LinkedList<FeatureType>();
            for (String feature : asList(features)) {
                logger.info("Apply specified features = {}", feature);
                featureTypes.add(FeatureType.valueOf(feature.toUpperCase().trim()));
            }
            NavigableMap<ReeloutStat, AtomicDouble> counts =
                    featureChainFactory.createFeatureChain(featureTypes).countFeatures(rlDb, options);

            StringBuilder stats = new StringBuilder("Feature | Discriminator | Value | Count \n");
            counts.forEach((key, value) -> {
                stats.append(String.format("%s | %s \n", key.toPipeDelimited(), value.get()));
            });
            Files.write(stats.toString(), new File(destination), StandardCharsets.UTF_8);

            return String.format("Feature stats written to %s", destination);
        } else {
            throw new IllegalArgumentException("Features must be specified.");
        }

    }

}
