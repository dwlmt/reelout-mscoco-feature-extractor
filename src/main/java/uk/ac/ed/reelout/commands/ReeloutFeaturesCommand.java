package uk.ac.ed.reelout.commands;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.features.FeatureChainFactory;
import uk.ac.ed.reelout.features.FeatureType;
import uk.ac.ed.reelout.services.ReeloutConversionService;

import java.io.IOException;
import java.util.*;

@Component
public class ReeloutFeaturesCommand implements CommandMarker {

    public static final double DATABASE_FORMAT_VERSION = 0.2;
    private final Logger logger = LoggerFactory.getLogger(ReeloutFeaturesCommand.class);
    private final ReeloutConversionService reelout;
    private final FeatureChainFactory featureChainFactory;

    /**
     * Constructor to autowire the command with the services it needs.
     *
     * @param reelout
     * @param featureChainFactory
     */
    @Autowired
    public ReeloutFeaturesCommand(ReeloutConversionService reelout,
                                  FeatureChainFactory featureChainFactory) {
        this.reelout = reelout;
        this.featureChainFactory = featureChainFactory;
    }

    @CliAvailabilityIndicator("reelout add")
    public boolean isReeloutAddAvailable() {
        return true;
    }

    /**
     * Add Reelout features to an existing XML file.
     *
     * @param source       path and file name for XML.
     * @param destination  Destination file name for XML. Can be the same as source to overwrite.
     * @param features     to add.
     * @param fromIdFilter optional filtering of the features to apply from this Id.
     * @param toIdFilter   optional filtering of the features to apply to this Id.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "reelout add", help = "Add features to ReelOut XML.")
    public String reeloutAdd(
            @CliOption(key = "source", mandatory = true, help = "Source file path and name for the Reelout XML.") String source,
            @CliOption(key = "destination", mandatory = true, help = "Destination file path and name for the XML.") String destination,
            @CliOption(key = "features", mandatory = true, help = "Features to added. If already present then will be unchanged. Available are POS, SENTIMENT, " +
                    "THEME, ENTITY and RELATION_TRIPLE, ADD_WORD_EMBEDDING, COSINE_SIMILARITY_EMBEDDING, COSINE_SIMILARITY_IMAGE_EMBEDDING")
                    String[] features,
            @CliOption(key = "from_id", mandatory = false, help = "Apply features to a subset of the images starting from this Id. Ids sorted as a String.") String fromIdFilter,
            @CliOption(key = "to_id", mandatory = false, help = "Apply features to a subset of the images ending at this Id. Ids sorted as a String.") String toIdFilter,
            @CliOption(key = "word_embeddings_file", mandatory = false, help = "Mandatory if applying a word embedding feature. Must be in Glove or Word2Vec format.")
                    String embeddingsFile,
            @CliOption(key = "embeddings_type", mandatory = false, help = "Label for the type of embedding being loaded: e.g. glove or word2vec.", specifiedDefaultValue = "glove", unspecifiedDefaultValue = "glove")
                    String embeddingsType,
            @CliOption(key = "remove_stop_words", mandatory = false, help = "true/false remove stop words when creating word embeddings.", specifiedDefaultValue = "true", unspecifiedDefaultValue = "true")
                    boolean removeStopWords,
            @CliOption(key = "embedding_closest_n", mandatory = false,
                    help = "when measuring cosine similarity how many closest N should be saved in the feature list.",
                    specifiedDefaultValue = "100", unspecifiedDefaultValue = "100")
                    int embeddingClosestN,
            @CliOption(key = "image_feature_directory", mandatory = false,
                    help = "Directory path to where the image features are in CSV format.") String imageFeatureDirectory
    ) throws IOException {

        logger.info("Add Reelout features: source = {}, destination = {}, features = {}, word_embeddings_file = {}, " +
                        "embeddings_type = {}, remove_stop_words = {}, embedding_closest_n = {}, image_feature_embedding = {} "
                , source, destination, features, embeddingsFile, embeddingsType, removeStopWords, embeddingClosestN, imageFeatureDirectory);
        // Load the JSON object and convert to ReelOut domain objects.

        CommandHelper.checkSourceAndDestination(source, destination);

        Map<String, Object> options = new HashMap<String, Object>();
        options.put("embeddingsFile", embeddingsFile);
        options.put("embeddingsType", embeddingsType);
        options.put("removeStopWords", removeStopWords);
        options.put("embeddingClosestN", embeddingClosestN);
        options.put("imageFeatureDirectory", imageFeatureDirectory);

        logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = reelout.convertToDomainFromFile(source);

        // If there are features then apply them.
        if (features != null && features.length > 0) {
            List<FeatureType> featureTypes = new LinkedList<FeatureType>();
            for (String feature : Arrays.asList(features)) {
                logger.info("Apply specified features = {}", feature);
                featureTypes.add(FeatureType.valueOf(feature.toUpperCase().trim()));
            }

            rlDb = featureChainFactory.createFeatureChain(featureTypes)
                    .withFromIdRange(fromIdFilter)
                    .withToIdRange(toIdFilter)
                    .createFeatures(rlDb, options);
        } else {
            throw new IllegalArgumentException("Features must be specified.");
        }

        // Write to XML.
        logger.info("Write to XML file.");
        reelout.convertAndWriteXMLToFile(destination, rlDb);
        return String.format("Added Reelout features: Source = %s, Destination = %s, Features = %s",
                source, destination, Arrays.toString(features));
    }

    @CliAvailabilityIndicator("reelout remove")
    public boolean isReeloutRemoveAvailable() {
        return true;
    }

    /**
     * Remove Reelout features from an existing XML file.
     *
     * @param source       path and file name for XML.
     * @param destination  Destination file name for XML. Can be the same as source to overwrite.
     * @param features     to add.
     * @param fromIdFilter optional filtering of the features to apply from this Id.
     * @param toIdFilter   optional filtering of the features to apply to this Id.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "reelout remove", help = "Remove Reelout XML features.")
    public String reeloutRemove(
            @CliOption(key = "source", mandatory = true, help = "Source file path and name for the Reelout XML.") String source,
            @CliOption(key = "destination", mandatory = true, help = "Destination file path and name for the XML.") String destination,
            @CliOption(key = "features", mandatory = true, help = "Features to remove. Available are POS, SENTIMENT, THEME, ENTITY, and RELATION_TRIPLE.")
                    String[] features,
            @CliOption(key = "from_id", mandatory = false, help = "Apply features to a subset of the images starting from this Id. Keys sorted as a String.") String fromIdFilter,
            @CliOption(key = "to_id", mandatory = false, help = "Apply features to a subset of the images ending at this Id. Keys sorted as a String.") String toIdFilter) throws IOException {
        logger.info("Remove Reelout features: source = {}, destination = {}, features = {}"
                , source, destination, features);
        // Load the JSON object and convert to ReelOut domain objects.

        CommandHelper.checkSourceAndDestination(source, destination);

        logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = reelout.convertToDomainFromFile(source);

        // If there are features then apply them.
        if (features != null && features.length > 0) {
            logger.info("Apply specified features = {}", (Object[]) features);
            List<FeatureType> featureTypes = new LinkedList<FeatureType>();
            for (String feature : Arrays.asList(features)) {
                featureTypes.add(FeatureType.valueOf(feature.toUpperCase().trim()));
            }
            rlDb = featureChainFactory.createFeatureChain(featureTypes).
                    withFromIdRange(fromIdFilter)
                    .withToIdRange(toIdFilter)
                    .resetFeatures(rlDb);
        } else {
            throw new IllegalArgumentException("Features must be specified.");
        }

        // Write to XML.
        logger.info("Write to XML file.");
        reelout.convertAndWriteXMLToFile(destination, rlDb);
        return String.format("Removed Reelout features: Source = %s, Destination = %s, Features = %s",
                source, destination, Arrays.toString(features));
    }


}
