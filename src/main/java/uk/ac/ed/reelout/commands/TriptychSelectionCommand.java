package uk.ac.ed.reelout.commands;

import com.google.common.io.Files;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.core.CommandMarker;
import org.springframework.shell.core.annotation.CliAvailabilityIndicator;
import org.springframework.shell.core.annotation.CliCommand;
import org.springframework.shell.core.annotation.CliOption;
import org.springframework.stereotype.Component;
import uk.ac.ed.reelout.domain.RlDatabase;
import uk.ac.ed.reelout.domain.RlUnit;
import uk.ac.ed.reelout.domain.SelectionCriteria;
import uk.ac.ed.reelout.exceptions.TriptychSelectionException;
import uk.ac.ed.reelout.selection.SelectionFilters;
import uk.ac.ed.reelout.selection.StoppingCondition;
import uk.ac.ed.reelout.services.ILPImageSelectionService;
import uk.ac.ed.reelout.services.NearestNeighbourImageSelectionService;
import uk.ac.ed.reelout.services.RandomImageSelectionService;
import uk.ac.ed.reelout.services.ReeloutConversionService;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NavigableSet;

@Component
public class TriptychSelectionCommand implements CommandMarker {


    public static final String SOURCE_HELP = "Source file path and name for the Reelout XML.";
    public static final String DESTINATION_HELP = "Destination directory for where to save";
    public static final String WEIGHT_HELP_TEXT = "Weight applied to the maximising goal in the MILP. Default is 1.0." +
            " Larger weights increase the influence of the feature, lower decreases it. Negative weights will change" +
            "the goal to find dissimilar neighbours. 0.0 will switch the feature off.";
    private final Logger logger = LoggerFactory.getLogger(TriptychSelectionCommand.class);
    private final ReeloutConversionService reelout;
    private final RandomImageSelectionService randomSelection;
    private final NearestNeighbourImageSelectionService nearestSelection;
    private final ILPImageSelectionService ilpSelection;
    private final SelectionFilters filters;

    /**
     * Constructor to autowire the command with the services it needs.
     *
     * @param reelout         needed to load the feature file.
     * @param randomSelection service for selecting random Triptychs.
     */
    @Autowired
    public TriptychSelectionCommand(ReeloutConversionService reelout,
                                    RandomImageSelectionService randomSelection,
                                    NearestNeighbourImageSelectionService nearestSelection,
                                    ILPImageSelectionService ilpSelection, SelectionFilters filters) {
        this.reelout = reelout;
        this.randomSelection = randomSelection;
        this.nearestSelection = nearestSelection;
        this.ilpSelection = ilpSelection;
        this.filters = filters;
    }

    @CliAvailabilityIndicator("triptych random")
    public boolean isTriptychRandomAvailable() {
        return true;
    }


    /**
     * Randomly select a Triptych.
     *
     * @param source      Source path and file name.
     * @param destination Destination for XML. Can be the same as source to overwrite.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "triptych random", help = "Randomly create a number of Triptychs from the database.")
    public String triptychRandom(
            @CliOption(key = "source", mandatory = true, help = SOURCE_HELP) String source,
            @CliOption(key = "destination", mandatory = true, help = DESTINATION_HELP) String destination,
            @CliOption(key = "number_to_generate", mandatory = false, help = "Number of Triptychs to generate.",
                    specifiedDefaultValue = "50", unspecifiedDefaultValue = "50") Integer numberToGenerate
    ) throws IOException {
        logger.info("Generate random Triptychs: sources = {}, destination = {}, number_to_generate = {}"
                , source, destination, numberToGenerate);

        CommandHelper.checkSourceAndDestination(source, destination);

        logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = this.reelout.convertToDomainFromFile(source);

        // Generate the required number of Triptychs.

        logger.info("Generate Triptychs");
        List<List<RlUnit>> triptychs = new ArrayList<List<RlUnit>>();
        for (int i = 0; i < numberToGenerate; i++) {
            List<RlUnit> triptych = this.randomSelection.selectTriptychImages(rlDb, new SelectionCriteria());
            triptychs.add(triptych);
        }

        // Write to XML.
        logger.info("Write out Triptychs to destination");
        this.writeToHtml(triptychs, destination);
        return "Triptychs generated successfully";

    }

    @CliAvailabilityIndicator("triptych nearest")
    public boolean isTriptychNearestAvailable() {
        return true;
    }


    /**
     * Create a Triptych based on the nearest neighbour using Cosine similarity.
     *
     * @param source      Source path and file name.
     * @param destination Destination for XML. Can be the same as source to overwrite.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "triptych nearest", help = "Selects triptychs based on the " +
            "cosine similarities of the embeddings in the feature file")
    public String triptychNearest(
            @CliOption(key = "source", mandatory = true, help = SOURCE_HELP) String source,
            @CliOption(key = "destination", mandatory = true, help = "Destination directory for where to save") String destination,
            @CliOption(key = "number_to_generate", mandatory = false, help = "Number of Triptychs to generate.",
                    specifiedDefaultValue = "50", unspecifiedDefaultValue = "50") Integer numberToGenerate
    ) throws IOException {
        logger.info("Generate Triptychs: sources = {}, destination = {}, number_to_generate = {}"
                , source, destination, numberToGenerate);

        CommandHelper.checkSourceAndDestination(source, destination);

        logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = this.reelout.convertToDomainFromFile(source);

        // Generate the required number of Triptychs.

        logger.info("Generate Triptychs");
        List<List<RlUnit>> triptychs = new ArrayList<List<RlUnit>>();
        for (int i = 0; i < numberToGenerate; i++) {
            List<RlUnit> triptych = this.nearestSelection.selectTriptychImages(rlDb, new SelectionCriteria());
            triptychs.add(triptych);
        }

        // Write to XML.
        logger.info("Write out Triptychs to destination");
        this.writeToHtml(triptychs, destination);
        return "Triptychs generated successfully";

    }

    @CliAvailabilityIndicator("triptych filter")
    public boolean isTriptych() {
        return true;
    }

    /**
     * The main Triptych generating function.
     *
     * @param source      Source path and file name.
     * @param destination Destination for XML. Can be the same as source to overwrite.
     * @return String message with result of command.
     * @throws IOException
     */
    @CliCommand(value = "triptych filter", help = "Selects triptychs based on an MIP Solver. Parameters prefixed with a number are applied to the specific " +
            "position in the Triptych")
    public String triptych(
            @CliOption(key = "source", mandatory = true, help = SOURCE_HELP) String source,
            @CliOption(key = "destination", mandatory = true, help = "Destination directory for where to save") String destination,
            @CliOption(key = "type", mandatory = false, help = "Type of selector ILP or random.",
                    specifiedDefaultValue = "ilp", unspecifiedDefaultValue = "ilp") String type,
            @CliOption(key = "word_embeddings_file", mandatory = false, help = "Word embeddings file. Must be in Glove or Word2Vec format.")
                    String embeddingsFile,
            @CliOption(key = "paragraph_embeddings_file", mandatory = false, help = "Doc2Vec generated paragraph embeddings.")
                    String paragraphsFile,
            @CliOption(key = "image_feature_directory", mandatory = false,
                    help = "Directory path to where the image features are in CSV format.") String imageFeatureDirectory,
            @CliOption(key = "prediction_feature_directory", mandatory = false,
                    help = "Directory path to where the image prediction features are in CSV format.") String predictionFeatureDirectory,
            @CliOption(key = "calculate_similarities", mandatory = false,
                    help = "Calculate similarities, if specified then word_embeddings_file and image_feature_directory must be provided.",
                    specifiedDefaultValue = "true", unspecifiedDefaultValue = "true")
                    Boolean calculateSimilarities,
            @CliOption(key = "number_to_generate", mandatory = false, help = "Number of Triptychs to generate.",
                    specifiedDefaultValue = "10", unspecifiedDefaultValue = "10") Integer numberToGenerate,
            @CliOption(key = "iterations_without_improvement", mandatory = false, help = "Number of selection iteration to try without improvement before stopping.",
                    specifiedDefaultValue = "5", unspecifiedDefaultValue = "5") Integer iterationsWithoutImprovement,
            @CliOption(key = "improvement_threshold", mandatory = false, help = "How much of an improvement is considered an improvement. " +
                    "Used with --iteration_without_improvement to allow early stopping.",
                    specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0") Double improvementThreshold,
            @CliOption(key = "max_iterations", mandatory = false, help = "Maximum iterations in search before stopping..",
                    specifiedDefaultValue = "100", unspecifiedDefaultValue = "100") Integer maxIterations,
            @CliOption(key = "max_time_in_minutes", mandatory = false, help = "Maximum number of minutes to search before stopping.",
                    specifiedDefaultValue = "60", unspecifiedDefaultValue = "60") Integer maxTimeInMinutes,
            @CliOption(key = "weight_text", mandatory = false, help = WEIGHT_HELP_TEXT,
                    specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0") Double weightText,
            @CliOption(key = "weight_image", mandatory = false, help = WEIGHT_HELP_TEXT,
                    specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0") Double weightImage,
            @CliOption(key = "weight_prediction", mandatory = false, help = WEIGHT_HELP_TEXT,
                    specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0") Double weightPredictions,
            @CliOption(key = "weight_paragraph", mandatory = false, help = WEIGHT_HELP_TEXT,
                    specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0") Double weightParagraph,
            @CliOption(key = "max_similarity_text", mandatory = false, help = "Cap on the Cosine text similarity " +
                    "across the Triptych. Cap applied without the weighting. 0 the default is off.",
                    specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0") Double maxSimilarityText,
            @CliOption(key = "max_similarity_image", mandatory = false, help = "Cap on the Cosine image similarity across " +
                    "the Triptych. Cap applied without the weighting. 0 the default is off.",
                    specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0") Double maxSimilarityImage,
            @CliOption(key = "max_similarity_prediction", mandatory = false, help = "Cap on the prediction similarity across " +
                    "the Triptych. Cap applied without the weighting. 0 the default is off.",
                    specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0") Double maxSimilarityPrediction,
            @CliOption(key = "max_similarity_paragraph", mandatory = false, help = "Cap on the prediction similarity across " +
                    "the Triptych. Cap applied without the weighting. 0 the default is off.",
                    specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0") Double maxSimilarityParagraph,
            @CliOption(key = "sentence", mandatory = false, help = "Natural language query.") String sentence,
            @CliOption(key = "sentence_use_word", mandatory = false, specifiedDefaultValue = "true",
                    unspecifiedDefaultValue = "true", help = "Use word embedding or paragraph embeddings. Defaults to word.") Boolean sentenceUseWordEmbeddings,
            @CliOption(key = "weight_sentence", mandatory = false, help = WEIGHT_HELP_TEXT,
                    specifiedDefaultValue = "1.0", unspecifiedDefaultValue = "1.0") Double weightSentence,
            @CliOption(key = "exclude_stop_words", mandatory = false, help = "Exclude stop words for word embeddings and sentence queries. Defaults to true.",
                    specifiedDefaultValue = "true", unspecifiedDefaultValue = "true") Boolean excludeStopWords,
            @CliOption(key = "ids", mandatory = false, help = "Comma separated list of Ids.") String[] ids,
            @CliOption(key = "message_text", mandatory = false, help = "Captions for the images.") String[] messageText,
            @CliOption(key = "themes", mandatory = false, help = "The Alchemy API theme of the image.") String[] themes,
            @CliOption(key = "theme_threshold", mandatory = false, specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0",
                    help = "Greater than this confidence for the theme. Value 0.0-1.0.") Double themeThreshold,
            @CliOption(key = "locations", mandatory = false, help = "Location from named entity recognition.") String[] locations,
            @CliOption(key = "entities", mandatory = false, help = "Entity from named entity recognition.") String[] entities,
            @CliOption(key = "subject", mandatory = false, help = "Subject of a caption sentence.") String[] subject,
            @CliOption(key = "relation", mandatory = false, help = "Verb/Relation of a caption sentence.") String[] relation,
            @CliOption(key = "object", mandatory = false, help = "Object of a caption sentence.") String[] object,
            @CliOption(key = "whole_triple", mandatory = false, specifiedDefaultValue = "false", unspecifiedDefaultValue = "false",
                    help = "Consider Subject/Verb,Object separately or together.") Boolean wholeTriple,
            @CliOption(key = "sentiments", mandatory = false, help = "Postive/Neutral/Negative") String[] sentiments,
            @CliOption(key = "before", mandatory = false, help = "Find those before this Datetime.") Date before,
            @CliOption(key = "after", mandatory = false, help = "Find those after this Datetime.") Date after,
            @CliOption(key = "1_ids", mandatory = false, help = "Comma separated list of Ids.") String[] ids1,
            @CliOption(key = "1_message_text", mandatory = false, help = "Captions for the images.") String[] messageText1,
            @CliOption(key = "1_themes", mandatory = false, help = "The Alchemy API theme of the image.") String[] themes1,
            @CliOption(key = "1_theme_threshold", mandatory = false, specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0",
                    help = "Greater than this confidence for the theme. Value 0.0-1.0.") Double themeThreshold1,
            @CliOption(key = "1_locations", mandatory = false, help = "Location from named entity recognition.") String[] locations1,
            @CliOption(key = "1_entities", mandatory = false, help = "Entity from named entity recognition.") String[] entities1,
            @CliOption(key = "1_subject", mandatory = false, help = "Subject of a caption sentence.") String[] subject1,
            @CliOption(key = "1_relation", mandatory = false, help = "Verb/Relation of a caption sentence.") String[] relation1,
            @CliOption(key = "1_object", mandatory = false, help = "Object of a caption sentence.") String[] object1,
            @CliOption(key = "1_whole_triple", mandatory = false, specifiedDefaultValue = "false", unspecifiedDefaultValue = "false",
                    help = "Consider Subject/Verb,Object separately or together.") Boolean wholeTriple1,
            @CliOption(key = "1_sentiments", mandatory = false, help = "Postive/Neutral/Negative") String[] sentiments1,
            @CliOption(key = "1_before", mandatory = false, help = "Find those before this Datetime.") Date before1,
            @CliOption(key = "1_after", mandatory = false, help = "Find those after this Datetime.") Date after1,

            @CliOption(key = "2_ids", mandatory = false, help = "Comma separated list of Ids.") String[] ids2,
            @CliOption(key = "2_message_text", mandatory = false, help = "Captions for the images.") String[] messageText2,
            @CliOption(key = "2_themes", mandatory = false, help = "The Alchemy API theme of the image.") String[] themes2,
            @CliOption(key = "2_theme_threshold", mandatory = false, specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0",
                    help = "Greater than this confidence for the theme. Value 0.0-1.0.") Double themeThreshold2,
            @CliOption(key = "2_locations", mandatory = false, help = "Location from named entity recognition.") String[] locations2,
            @CliOption(key = "2_entities", mandatory = false, help = "Entity from named entity recognition.") String[] entities2,
            @CliOption(key = "2_subject", mandatory = false, help = "Subject of a caption sentence.") String[] subject2,
            @CliOption(key = "2_relation", mandatory = false, help = "Verb/Relation of a caption sentence.") String[] relation2,
            @CliOption(key = "2_object", mandatory = false, help = "Object of a caption sentence.") String[] object2,
            @CliOption(key = "2_whole_triple", mandatory = false, specifiedDefaultValue = "false", unspecifiedDefaultValue = "false",
                    help = "Consider Subject/Verb,Object separately or together.") Boolean wholeTriple2,
            @CliOption(key = "2_sentiments", mandatory = false, help = "Postive/Neutral/Negative") String[] sentiments2,
            @CliOption(key = "2_before", mandatory = false, help = "Find those before this Datetime.") Date before2,
            @CliOption(key = "2_after", mandatory = false, help = "Find those after this Datetime.") Date after2,

            @CliOption(key = "3_ids", mandatory = false, help = "Comma separated list of Ids.") String[] ids3,
            @CliOption(key = "3_message_text", mandatory = false, help = "Captions for the images.") String[] messageText3,
            @CliOption(key = "3_themes", mandatory = false, help = "The Alchemy API theme of the image.") String[] themes3,
            @CliOption(key = "3_theme_threshold", mandatory = false, specifiedDefaultValue = "0.0", unspecifiedDefaultValue = "0.0",
                    help = "Greater than this confidence for the theme. Value 0.0-1.0.") Double themeThreshold3,
            @CliOption(key = "3_locations", mandatory = false, help = "Location from named entity recognition.") String[] locations3,
            @CliOption(key = "3_entities", mandatory = false, help = "Entity from named entity recognition.") String[] entities3,
            @CliOption(key = "3_subject", mandatory = false, help = "Subject of a caption sentence.") String[] subject3,
            @CliOption(key = "3_relation", mandatory = false, help = "Verb/Relation of a caption sentence.") String[] relation3,
            @CliOption(key = "3_object", mandatory = false, help = "Object of a caption sentence.") String[] object3,
            @CliOption(key = "3_whole_triple", mandatory = false, specifiedDefaultValue = "false", unspecifiedDefaultValue = "false",
                    help = "Consider Subject/Verb,Object separately or together.") Boolean wholeTriple3,
            @CliOption(key = "3_sentiments", mandatory = false, help = "Positive/Neutral/Negative") String[] sentiments3,
            @CliOption(key = "3_before", mandatory = false, help = "Find those before this Datetime.") Date before3,
            @CliOption(key = "3_after", mandatory = false, help = "Find those after this Datetime.") Date after3
    ) throws IOException {

        CommandHelper.checkSourceAndDestination(source, destination);

        if (maxSimilarityText > 0.0 && maxSimilarityText > 2.0) {
            throw new IllegalArgumentException("max similarities must be in range between 0 and 2.");
        }

        if (calculateSimilarities && imageFeatureDirectory == null && embeddingsFile == null
                && predictionFeatureDirectory == null && paragraphsFile == null) {
            throw new IllegalArgumentException("When calculating Similarity features then word, paragraph, image" +
                    "or prediction features must be provided.");
        }

        if (sentence != null && embeddingsFile == null && paragraphsFile == null) {
            throw new IllegalArgumentException("Embeddings must be provided when use the '--sentence' parameter.");
        }

        SelectionCriteria criteria = new SelectionCriteria();
        criteria.setMaxTextSimilarity(maxSimilarityText);
        criteria.setMaxImageSimilarity(maxSimilarityImage);
        criteria.setTextSimilarityWeight(weightText);
        criteria.setImageSimilarityWeight(weightImage);
        criteria.setCalculateSimilarities(calculateSimilarities);
        criteria.setWordEmbeddings(embeddingsFile);
        criteria.setImageFeatures(imageFeatureDirectory);
        criteria.setStoppingCondition(
                new StoppingCondition(iterationsWithoutImprovement, maxIterations, maxTimeInMinutes, improvementThreshold));
        criteria.setSentence(sentence);
        criteria.setExcludeStopWords(excludeStopWords);
        criteria.setPredictionFeatures(predictionFeatureDirectory);
        criteria.setPredictionSimilarityWeight(weightPredictions);
        criteria.setMaxPredictionSimilarity(maxSimilarityPrediction);
        criteria.setSentenceSimilarityWeight(weightSentence);
        criteria.setParagraphEmbeddings(paragraphsFile);
        criteria.setParagraphSimilarityWeight(weightParagraph);
        criteria.setMaxParagraphSimilarity(maxSimilarityParagraph);
        criteria.setSentenceUseWordEmbeddings(sentenceUseWordEmbeddings);

        logger.info("Generate Triptychs: sources = {}, destination = {}, Number to generate = {}, ILP criteria = {}"
                , source, destination, numberToGenerate, criteria);

        logger.info("Convert to ReelOut domain representation");
        RlDatabase rlDb = this.reelout.convertToDomainFromFile(source);
        NavigableSet<RlUnit> filteredUnits = rlDb.getRlUnits();


        logger.info("Filter the Triptychs using provided criteria");

        // Apply all the general criteria.
        filteredUnits = this.filters.filterRlUnits(ids, messageText, themes, themeThreshold, locations, entities, subject, relation,
                object, wholeTriple, sentiments, before, after, filteredUnits);

        NavigableSet<RlUnit> start = this.filters.filterRlUnits(ids1, messageText1, themes1, themeThreshold1, locations1, entities1, subject1, relation1,
                object1, wholeTriple1, sentiments1, before1, after1, filteredUnits);
        NavigableSet<RlUnit> middle = this.filters.filterRlUnits(ids2, messageText2, themes2, themeThreshold2, locations2, entities2, subject2, relation2,
                object2, wholeTriple2, sentiments2, before2, after2, filteredUnits);
        NavigableSet<RlUnit> end = this.filters.filterRlUnits(ids3, messageText3, themes3, themeThreshold3, locations3, entities3, subject3, relation3,
                object3, wholeTriple3, sentiments3, before3, after3, filteredUnits);


        this.logger.info("Start images count = {}", start.size());
        this.logger.info("Middle images count = {}", middle.size());
        this.logger.info("End images count = {}", end.size());

        List<List<RlUnit>> triptychs;
        if (type.trim().toLowerCase().equals("ilp")) {
            triptychs = ilpSelection.selectTriptychWithPositions(start, middle, end, criteria, numberToGenerate);
        } else if (type.trim().toLowerCase().equals("random")) {
            triptychs = this.randomSelection.selectTriptychWithPositions(start, middle, end, criteria, numberToGenerate);
        } else {
            throw new TriptychSelectionException("Invalid type of selection has been selected: ILP or random.");
        }

        // Write to XML.
        logger.info("Write out Triptychs to destination");
        writeToHtml(triptychs, destination);
        writeToListFile(triptychs, destination);
        return "Triptychs generated successfully";

    }

    /**
     * Write to HTML. Just a test output at the moment.
     *
     * @param triptychs   to write.
     * @param destination file name.
     * @throws IOException if the html file cannot be written.
     */
    private void writeToHtml(List<List<RlUnit>> triptychs, String destination) throws IOException {

        this.logger.info("Write output HTML");

        String html = "";

        String htmlHeader = "<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>MS-COCO Reelout Triptych</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<!-- Based on the example in http://alijafarian.com/responsive-image-grids-using-css/ -->\n" +
                "\n" +
                "<style>\n" +
                "\n" +
                "    ul.rig {\n" +
                "        list-style: none;\n" +
                "        font-size: 0px;\n" +
                "        margin-left: -2.5%;\n" +
                "    }\n" +
                "    ul.rig li {\n" +
                "        display: inline-block;\n" +
                "        padding: 10px;\n" +
                "        margin: 0 0 2.5% 2.5%;\n" +
                "        background: #fff;\n" +
                "        border: 1px solid #ddd;\n" +
                "        font-size: 16px;\n" +
                "        font-size: 1rem;\n" +
                "        vertical-align: top;\n" +
                "        box-shadow: 0 0 5px #ddd;\n" +
                "        box-sizing: border-box;\n" +
                "        -moz-box-sizing: border-box;\n" +
                "        -webkit-box-sizing: border-box;\n" +
                "    }\n" +
                "    ul.rig li img {\n" +
                "        max-width: 100%;\n" +
                "        height: auto;\n" +
                "        margin: 0 0 10px;\n" +
                "    }\n" +
                "    ul.rig li h3 {\n" +
                "        margin: 0 0 5px;\n" +
                "    }\n" +
                "    ul.rig li p {\n" +
                "        color: #999;\n" +
                "    }\n" +
                "\n" +
                "    /* class for 3 columns */\n" +
                "    ul.rig.columns-3 li {\n" +
                "        width: 30.83%;\n" +
                "    }\n" +
                "\n" +
                "    @media (max-width: 480px) {\n" +
                "        ul.grid-nav li {\n" +
                "            display: block;\n" +
                "            margin: 0 0 5px;\n" +
                "        }\n" +
                "        ul.grid-nav li a {\n" +
                "            display: block;\n" +
                "        }\n" +
                "        ul.rig {\n" +
                "            margin-left: 0;\n" +
                "        }\n" +
                "        ul.rig li {\n" +
                "            width: 100% !important;\n" +
                "            margin: 0 0 20px;\n" +
                "        }\n" +
                "    }\n" +
                "</style>\n" +
                "\n" +
                "<header></header>\n" +
                "\n" +
                "<section>\n" +
                "    <h1>Triptychs</h1>";

        html += htmlHeader;

        int triptychCounter = 1;
        for (List<RlUnit> triptych : triptychs) {


            html += String.format("<article>\n <h3>Triptych %s</h3> \n <ul class=\"rig columns-3\">", triptychCounter);

            for (RlUnit rl : triptych) {
                html += String.format(" <li>\n" +
                        "                <figure>\n" +
                        "                    <img src=\"%s\" alt=\"%s\">\n" +
                        "                    <h4>%s</h4>\n" +
                        "                    <p>%s</p>\n" +
                        "                </figure>\n" +
                        "            </li>", rl.getImage(), rl.getId(), rl.getId(), rl.getMessageText());
            }

            html += "</ul></article>";

            triptychCounter++;

        }

        String htmlFooter = "</section>\n" +
                "\n" +
                "<footer>Generated from MS-COCO Reelout Feature Extractor </footer>\n" +
                "\n" +
                "</body>\n" +
                "</html>";

        html += htmlFooter;
        Files.write(html, new File(destination), StandardCharsets.UTF_8);
    }

    /**
     * Write to a list file. The file is space separated with each Triptych on a new line.
     * <p>
     * The format allows the file to be fed into the Triptych generator
     *
     * @param triptychs   to write.
     * @param destination file name.
     * @throws IOException if the html file cannot be written.
     */
    private void writeToListFile(List<List<RlUnit>> triptychs, String destination) throws IOException {

        this.logger.info("Write ids to file");

        String idList = "";

        for (List<RlUnit> triptych : triptychs) {

            String ids = "";
            for (RlUnit rl : triptych) {
                ids += String.format(" %s ", rl.getId());
            }

            ids += " \n";

            idList += ids;

        }
        // Write to a space separated file.
        Files.write(idList, new File(String.format("%s.%s", destination, "ssv")), StandardCharsets.UTF_8);
    }

}
