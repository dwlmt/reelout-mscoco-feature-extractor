/* number of images */
param n, integer;

/* images */
set V := 1..n;

/* Sets for images that occur in positions */
set START;
set MIDDLE;
set FINISH;

/* arcs between text similarities */
set E dimen 2;

/* Text cosine similarity between i and j */
param c{(i,j) in E};

/* Image cosine similarity between i and j */
param d{(i,j) in E};

/* Prediction similarity */
param b{(i,j) in E};

/* Paragraph similarity */
param a{(i,j) in E};

/* Path over the Triptych */
var x{i in START, j in MIDDLE, k in FINISH: i != j && j != k && i != k}, binary;






