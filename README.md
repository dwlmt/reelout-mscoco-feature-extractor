# reelout-mscoco

## Build

- Requires the setup of Java JDK-8 
    (http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 

- JAVA_HOME must be set as appropriate for your environment.

- After cloning from Git.

- RUN './gradew build' to build the project using Gradle. 
- RUN './gradew check' to run unit tests and code quality checks.

- To deploy untar or unzip the built deployment file from under
the created directory ./build/distributions/

- To run the command line shell run the reelout-mscoco-feature-extractor script
from inside the bin directory of the distribution. JVM options can be changed via the environment variable JAVA_OPTS.

(When running from windows use the *.cmd version of the same scripts).

## Shell Commands

The command line shell supports the following commands. They can also be found by running help on the application shell.

They can also be run directly from a LINUX shell or windows command prompts by prefixing them by the shell startup script 
- reelout-mscoco-feature-extractor or reelout-mscoco-feature-extractor.bat

### mscoco convert

Converts the annotations JSON in the Reelout.xml feature file format with POS tagging performed.


    mscoco convert --source '' --destination '' --join '' --max_sentences ''
    
- source: the source directory and file path for the annotations JSON.
- destination: the output XML directory and file path.
- join: true/false join the captions for the same images or keep them separate.
- max_sentences: int when joining how many captions to join. For example if 3 then the first 3 sentences will be concatenated as the image text.

### reelout add

Add features to the Reelout XML feature file.

    reelout add --source '' --destination '' --features '' --from_id '' --to_id '' --word_embeddings_file '' --embeddings_type '' --remove_stop_words '' --embedding_closest_n ''
    
- source: the source directory and file path for the annotations JSON.
- destination: the output XML directory and file path. 
- features: Features to added. If already present then will be unchanged. 
    - POS part-of-speech tagging performed using Stanford Core NLP with the Penn Treebank tags.
    - SENTIMENT positive/neutral/negative created with Stanford Core NLP.
    - THEME Assigns themes based on the Alchemy Taxonomy API http://www.alchemyapi.com/api/taxonomy
    - ENTITY Location and Entity extraction using the Alchemy Entity Service http://www.alchemyapi.com/api/entity/types
    - RELATION_TRIPLE  Extract Subject, Relation, Object triples for the text from Stanford Open IE.
    - ADD_WORD_EMBEDDING Adds word embedding representations of image text either based on Word2Vec or Glove embeddings.
    - COSINE_SIMILARITY_EMBEDDING Uses the embeddings to identify the closest N images.
- from_id: Apply features to a subset of the images starting from this Id. Ids sorted as a String.
- to_id: Apply features to a subset of the images ending at this Id. Ids sorted as a String.
- word_embeddings_file: Mandatory if applying a word embedding feature. Must be in Glove or Word2Vec format. 
- embeddings_type: Label for the type of embedding being loaded: e.g. glove or word2vec. 
- remove_stop_words: true/false remove stop words when creating word embeddings. 
- embedding_closest_n: when measuring cosine similarity how many closest N should be saved in the feature list.
    